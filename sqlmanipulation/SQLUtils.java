package me.nina.sqlmanipulation;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import me.nina.maker.Main; 

public class SQLUtils {
	 
	private static Statement statement;

	//##################################################
	public static void updateSqlViewer()  { try {
		Main.sqlPanel.updateTableList( getTables()); 
	} catch (Exception e1) { e1.printStackTrace(); }}
 
	//##################################################
	public static void log(String s) {
		System.out.println( s);
	} 
	//##################################################
	   public static Map<String, List<String>> getTables() {
	        Map<String, List<String>> temp = new HashMap<>();
	        try (ResultSet rs = Main.sql.getMetaData().getTables(null, null, null, new String[]{"TABLE"})) {
	            while (rs.next()) {
	                String tableName = rs.getString("TABLE_NAME");
	                temp.put(tableName, getColumns(tableName));
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return temp;
	    }

	    public static List<String> getColumns(String tableName) {
	        List<String> columns = new ArrayList<>();
	        String sql = "SELECT * FROM " + tableName + " LIMIT 0";
	        try (Statement statement = Main.sql.createStatement();
	             ResultSet rs = statement.executeQuery(sql)) {
	            ResultSetMetaData mrs = rs.getMetaData();
	            for (int i = 1; i <= mrs.getColumnCount(); i++) {
	                columns.add(mrs.getColumnLabel(i));
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return columns;
	    }

	    public static Map<String, String> getMap(String table, int id, List<String> columns) {
	        Map<String, String> temp = new HashMap<>();
	        String sql = "SELECT * FROM " + table + " WHERE id = ?";
	        try (PreparedStatement ps = Main.sql.prepareStatement(sql)) {
	            ps.setInt(1, id);
	            try (ResultSet rs = ps.executeQuery()) {
	                if (rs.next()) {
	                    for (String col : columns) {
	                        temp.put(col, rs.getString(col));
	                    }
	                }
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	        return temp;
	    }
	
	//##################################################
	public static void execute(String s) {try { 
		if(statement==null)statement = Main.sql.createStatement(); 
		log(s);//new Throwable().printStackTrace();
		statement.execute(s.replaceAll(";;",";") );
		 
		SQLUtils.updateSqlViewer(); 
	} catch (SQLException e) { e.printStackTrace();log("error in query "+s); }}
	//##################################################


	public static void executeQueue(List<String> todo) {try {
		if(statement==null)statement = Main.sql.createStatement(); 
		for(String s : todo) {
			log(s); 
			statement.execute(s.replaceAll(";;",";") );
			statement.close();
		}
		 SQLTable.reload();
		SQLUtils.updateSqlViewer();
	} catch (SQLException e) {e.printStackTrace();log("error in queue job "); }}
	
	
	
	

 
}
