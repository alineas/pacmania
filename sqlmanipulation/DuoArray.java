package me.nina.sqlmanipulation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import me.nina.gameobjects.TransitionPoint;
import me.nina.maker.Utils;

public class DuoArray<K, V> implements KeyValue<K, V>, SQLWrapper<DuoArray<K, V>> {
    private final K key;
    private V value;

    public DuoArray(K key, V value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public K getKey() {
        return key;
    }

    @Override
    public V getValue() {
        return value;
    }

    @Override
    public String toSqlValues() { 
        // Logique de conversion vers une représentation SQL
        return key + "=" + value;
    }

    @Override
    public DuoArray<K, V> instanciate(String value,int id) {
    	String[] parts = value.split("=");
        if (parts.length == 2) { 
            K key = (K) parts[0];
            V val = (V) parts[1];
            return new DuoArray<>(key, val);
        } else {
            Utils.log("error in duoarray : size !=2");
            return null;
        }
    }

  

	@Override
	public DuoArray<K, V> getObject() { 
		return this;
	}

	public static final DuoArray DEFAULT_INSTANCE = new DuoArray(null, null);

	public void setValue(String val) {
		value=(V) val;
		
	}
 


	 
}




interface KeyValue<K, V> {
    K getKey();
    V getValue();
	String toSqlValues(); 
}