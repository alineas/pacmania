package me.nina.sqlmanipulation;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException; 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import me.nina.maker.Main;
import me.nina.maker.Utils;

public class SQLTable {
	private static Map <String,SQLTable> tables = new HashMap<>();
	private Map <Integer,SQLLine> lines = new LinkedHashMap<>();
	String name; 
	List<String> columns; 
	String cnames = "";  
	//##################################################
	public SQLTable(String name) {try {
		this.name=name.replaceAll("'", "`");
		 
		this.columns = SQLUtils.getColumns(name);
		for(String a : columns) 
			cnames=cnames+a+",";
			cnames=cnames.replaceAll(",$", "");
		
		PreparedStatement ps = Main.sql.prepareStatement("Select * from "+name);
		ResultSet rs = ps.executeQuery();
		while(rs.next())
			try {int id=Integer.parseInt(rs.getString("id"));
				lines.put(id,new SQLLine(this,id));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		rs.close();
		ps.close();
		
	}catch (SQLException e){ e.printStackTrace(); } }
	//##################################################
	public SQLLine getById(int id) {  
		return lines.get(id);
	}
	//##################################################
	public static SQLTable getTable(String tableName) { tableName=tableName.replaceAll("'", "`");
		if(!tables.containsKey(tableName))
			tables.put(tableName, new SQLTable(tableName));
		return tables.get(tableName);
	}
	//##################################################
	public void drop() { tables.remove(name); 
		SQLUtils.execute("drop table "+name);  }
 
	//##################################################
	public static void createNewTable(String name) {name=name.replaceAll("'", "`");
		SQLUtils.execute("CREATE TABLE IF NOT EXISTS "+name+" ( id integer not null );");  
		tables.put(name, new SQLTable(name));}
	//##################################################
	public SQLLine createNewLine() { int id = SQLLine.getUniqueID();
		SQLLine line = new SQLLine(this,id);
		lines.put(id,line);
		insert(new HashMap<String,String>()); 
		
		return line; }
	//##################################################
		public SQLLine getEmptyLine() { 
			SQLLine line = new SQLLine(this,SQLLine.getUniqueID());
			return line;
		}

	//##################################################
	public void addcolumn(String column) { column=column.replaceAll("'", "`");
		SQLUtils.execute("ALTER TABLE "+name+" ADD COLUMN "+column);  
		reload();
	}
	static void reload() { 
		for(SQLTable a : tables.values()) 
			a.lines.clear();
		tables.clear(); 
		
	}
	//##################################################
	public void remColumn(String column) { 
		SQLUtils.execute( "ALTER TABLE "+name+" DROP COLUMN "+column); 
		reload();
	}
	//##################################################
	public void truncate() { 
		SQLUtils.execute("DELETE FROM "+name); 
		reload();
		
	}
	//##################################################
	public int insert(Map<String, String> mappe) {
		int id = SQLLine.getUniqueID();
		mappe.put("id", id+"");
		String vals="";
		for(String a : columns) 
			if(!mappe.containsKey(a))
				vals=vals+"NULL,";
			else  
				vals=vals+"'"+mappe.get(a).replaceAll("'","`")+"',";  
		vals=vals.replaceAll(",$", "");
		SQLUtils.execute ("INSERT INTO "+name+"("+cnames+") VALUES("+vals+")"); 
		lines.put(id,new SQLLine(this,id));return id;}
	
	static List<String> todo = new ArrayList<>();
	public void queue(Map<String, String> mappe) {
		int id = SQLLine.getUniqueID();
		mappe.put("id", id+"");
		String vals="";
		for(String a : columns) 
			if(!mappe.containsKey(a))
				vals=vals+"NULL,";
			else  
				vals=vals+"'"+mappe.get(a).replaceAll("'","`")+"',";  
		vals=vals.replaceAll(",$", "");
		todo.add("INSERT INTO "+name+"("+cnames+") VALUES("+vals+")");
		 
	}
	public static void doTheQueue() {
		SQLUtils.executeQueue (todo);
		todo.clear();
		reload();
	}
	public Map<Integer, SQLLine> getLines() { 
		return lines;
	}
}