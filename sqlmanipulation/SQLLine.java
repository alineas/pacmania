package me.nina.sqlmanipulation;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;
import java.awt.geom.Line2D.Float;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JTextField;

import me.nina.gameobjects.Scene;
import me.nina.gameobjects.TransitionPoint;
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Main;
import me.nina.maker.Pane;
import me.nina.maker.Utils; 

public class SQLLine {
	
	private SQLTable table;
	public int id;
	private Map <String,String> datas = new HashMap<>();  
	public DuoArrayManager<String, String> duoManager;
	//##########################################################################################
	public SQLLine(SQLTable table, int id) {
		duoManager = new DuoArrayManager<>(this, DuoArray.DEFAULT_INSTANCE);
		 
		this.table = table;
		this.id = id ;
		reload();
	}
	private void reload() { 
		datas = SQLUtils.getMap(table.name,id,table.columns); 
	}
	//##########################################################################################
	public String getString(String column) {
		String d = datas.get(column);
		if(d==null)return"";
		return d.replaceAll("`","'"); }
	//##########################################################################################
	public int getInt(String column) {  
		try {
			return Integer.parseInt(datas.get(column));
		} catch (NumberFormatException e) {
			return -998; //dont touch !!! i used this value cause there is no null check on an integer
		}  
		
	
	}
	//##########################################################################################
	public void update(String column, String value) {  
		value=value.replaceAll("'", "`");
		datas.put(column, value);
		SQLUtils.execute("UPDATE "+table.name+" set "+column+"='"+value+"' where id ="+id); 
		reload();
	}

	//##########################################################################################
	static int last=-1;
	public static int getUniqueID() {
		int res = Utils.getTimestampId();
		while (last == res)
			try { Thread.sleep(10); res = Utils.getTimestampId();} catch (InterruptedException e) {e.printStackTrace(); }
		last = res;
		return res;
	}
  
	//##########################################################################################
	public Map<String, String> getDatas() {
		return datas; }
	//##########################################################################################
	public void toFrame(boolean update) {
		JFrame frame = new JFrame();
		frame.setTitle("insert into "+table.name);
		frame.setSize(new Dimension(600,600));
		Pane p = new Pane(); 
		p.setSize(600,600); 
		p.setLayout(new GridLayout(20, 2));

		for(String s : table.columns) { 
			p.createLabel(s , p,100,20);
			JTextField txt = p.createText(datas.get(s) , p,500,20,null);
			txt.setName(s);
		}

		 p.createBoutton("update",p,100,20,new ActionListener() { @Override public void actionPerformed(ActionEvent e) { 
			String values="";
			for(int i = 0 ; i < p.getComponentCount() ; i ++) { 
				Component a = p.getComponent(i);
				if(a instanceof JTextField) {
					if(update)
						update(a.getName(),((JTextField) a).getText());
					values=values+"'"+((JTextField)a).getText()+"',"; 
				}
			}
			if(!update) { 
				values=values.replaceAll(",$", ""); 
				SQLUtils.execute("INSERT INTO "+table.name+"("+table.cnames+") VALUES("+values+")");
				table.getLines().put(id,new SQLLine(table, id));
			}
			Main.sqlPanel.showTable(table.name);
			frame.dispose();

		} }); 
		frame.getContentPane().add(p,BorderLayout.WEST); 
		frame.setVisible(true);
		frame.pack(); 
	}

	//##########################################################################################
	public void delete() { table.getLines().remove(id);
		SQLUtils.execute("DELETE FROM "+table.name+" WHERE id = "+id);  }
	//##########################################################################################
	public String toStr() {String s = "";
		for(Entry<String, String> a : datas.entrySet())
			s=s+" --- "+a.getKey()+":"+a.getValue();
		return s;
	}
	//##########################################################################################
	public String[] getArray(String column) {  
		String res = getString(column);
		 res=res.replaceAll(",", ";");
		if(!res.contains(";"))
			return new String[] {res};
		return res.replaceAll("^;", "").split(";");
	}
	//##########################################################################################
 
	public void addInArray(String column, String s) {
		s=s.replaceAll("'", "`");
		String newe = getString(column)+";"+s;
		newe = newe.replaceAll("^;", "");
		update(column, newe);
	}
	public void remFromArray(String column, String s) {
		remFromArray(column, s,-1);
	}
	public void remFromArray(String column, String s,int index) {
		int i = 0 ;
		s=s.replaceAll("'", "`");
		String res=""; 
		for(String a:getArray(column)) { if(index!=-2)
			if(!a.equals(s) || index!=-1&&i != index ) { 
				res=res+";"+a;
			}
			i++;
		}
		update(column, res.replaceAll("^;", ""));
	}
	 
	//##################################################

	public LinkedList<Integer> getList(String column) {
		LinkedList<Integer> temp = new LinkedList<>();
		for(String a : getArray(column))
			if(a!=null&&!a.equals(""))
				temp.add(Integer.parseInt(a));
		return temp;
	}
 
 
	
	
	
	
	public Map<String,String> duoArray(String column) {
        String[] array = getArray(column);
        Map<String,String> temp = new HashMap<>();  
		for(String l : array) { if(l.length()<2)continue;// empty "^;" 
			 temp.put( l.split("=")[0].replaceAll(" ", ""),l.split("=")[1].replaceAll(" ", "") );
		}
		return temp; 
    }
 public int getInt(String column, String key) {  
	 try {
		return Integer.parseInt(duoArray(column).get(key));
	} catch (NumberFormatException e) {
		 Utils.log( key+" was not found in column "+column);
	}
	return -998;
            
    } 

 public String getString(String column, String key) { 
	 return  duoArray(column).get(key) ;
    } 
	 
 
	
	
	 
}
