package me.nina.sqlmanipulation;

import java.util.List;
 

public interface SQLWrapper<T> { 
    String toSqlValues(); 
    T instanciate(String sql,int uniqueId);
	T getObject(); 
}