package me.nina.sqlmanipulation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.nina.gameobjects.Zone;
import me.nina.maker.Utils;

public class ListManager<T extends SQLWrapper<?>> {

    protected final SQLLine line; 
    private final SQLWrapper<T> factory;

    public ListManager(SQLLine line, SQLWrapper<T> factory) {
        this.factory = factory;
        this.line = line;
    }

    public List<T> readArray(String column) {
        String[] array = line.getArray(column);
        List<T> temp = new ArrayList<>();  
		for(String l : array) { if(l.length()<2)continue;// empty "^;" 
			 temp.add(factory.instanciate(l,-1));
		}
		return temp; 
    }

    public void updateArray(String column, List<T> array) { 
    	String vals = ""; 
		for(T l : array) {
			vals=vals+";"+l.toSqlValues();
		}
		vals=vals.replaceAll("^;", "");
		line.update(column,vals);
    }

    public void addInArray(String column,SQLWrapper<T> f) {
        List<T> array = readArray(column);
        array.add(f.getObject());    
        updateArray(column, array);
         
    }

 

    public boolean deleteInArray(String column, T objectToRemove) {
        List<T> actualValue = readArray(column);
        if (actualValue.remove(objectToRemove)) {
            updateArray(column, actualValue);
            return true;
        } else {
            // L'objet � supprimer n'a pas �t� trouv� dans la liste
            System.out.println("object to delete "+objectToRemove+" not found in list !");
            for(T a : actualValue)
            	 System.out.println(a+"");
            return false;
        }
        
    }
 
    
            
            
    public void updateInArray(String column, T objectToRemove, SQLWrapper<T> objectToUpdate) {
    	if(deleteInArray(column,objectToRemove))
    		addInArray(column, objectToUpdate);
    }
            
          
        }
    


    
    
   