package me.nina.sqlmanipulation;

import java.util.List;

import me.nina.maker.Utils;

public class DuoArrayManager<K, V> extends ListManager<DuoArray<K, V>> {
    public DuoArrayManager(SQLLine line, DuoArray<K, V> defaultInstance) {
        super(line, defaultInstance);
    }

    public List<DuoArray<K, V>> readArray(String column) {
        return super.readArray(column);
    }
    
    public int getInt(String column, K key) { 
        List<DuoArray<K, V>> array = readArray(column);
        for (DuoArray<K, V> duoArray : array) {   
            if (duoArray.getKey().equals(key)) {
                return Integer.parseInt(duoArray.getValue().toString());
            }
        }
        return -1; 
    } 
    
    public String getString(String column, K key) { 
        List<DuoArray<K, V>> array = readArray(column);
        for (DuoArray<K, V> duoArray : array) { 
            if (duoArray.getKey().equals(key)) { 
                return duoArray.getValue().toString();
            }
        }
        return null; 
    } 
    

	public void setValue(String column, String key, String val) {
		List<DuoArray<K, V>> array = readArray(column);
		
		if(getString(column,(K) key) == null) {
			super.addInArray(column, new DuoArray(key,val));
			return;
		}
		for (DuoArray<K, V> duoArray : array)  
            if (duoArray.getKey().equals(key))  
                duoArray.setValue(val);
             
        super.updateArray(column,array);
	}
    
	public void removeValue(String column, String key ) {
		List<DuoArray<K, V>> array = readArray(column);
		 
		DuoArray<K, V> toRem = null;
		for (DuoArray<K, V> duoArray : array)  
            if (duoArray.getKey().equals(key))  
               toRem =  duoArray ;
             
        if(toRem!=null)
        	super.deleteInArray(column, toRem);
        else Utils.log("unable to remove in duoarray");
	}
    
  
}