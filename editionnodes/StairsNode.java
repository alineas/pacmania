package me.nina.editionnodes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import me.nina.agenerator.Generator; 
import me.nina.agenerator.StairGenerator;
import me.nina.editors.Editor;
import me.nina.editors.SceneEditor.EditType; 
import me.nina.gameobjects.Scene;
import me.nina.gameobjects.Stair;
import me.nina.gameobjects.StairList;
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Cbox;
import me.nina.maker.Main;
import me.nina.maker.Pane;
import me.nina.maker.RosellaZone;
import me.nina.maker.Utils;
import me.nina.maker.Utils;
import me.nina.objects2d.Img;
import me.nina.objects2d.Pixel;
import me.nina.objects2d.Point;

public class StairsNode extends EditNode<Integer> { 
	
	public List<Zone> zones = new ArrayList<>();
	public List<Integer> priorities = new ArrayList<>(); 
	private List<Zone> sList;
	public Cbox<Zone> stairList;    
	private boolean actuallyModifyingGeneratorZone;
	public boolean generatorIsRunning; 
	private StairGenerator generator = new StairGenerator(this);
	public int percentFactor = 100;
	private Pane lab, but;
	private Zone zoneEdited;
	private Img pic;
	boolean showPriorities; 
	
	
	//#################################################################################### 
	public StairsNode(Editor e ) {
		super(EditType.STAIRS,e );
		lab=Main.leftController.panneauLabels;
		but=Main.leftController.panneauBoutons; 
	}
	
	//#################################################################################### 
	@Override public void populate() { 
		//rosella = new RosellaZone(editor.scene,true );  
		//rosella.scale=1.0;
		zones.clear();
		priorities.clear();
		lab.createLabel("stairs list",lab,150,20); 
		sList = Stair.getByOwner(editor.scene.id);
		stairList = but.createCbox(sList ,but,150,20,null); 
		
		lab.createLabel("add a stair",lab,150,20);
		but.createBoutton("new stair",but,100,20,(ActionListener) e->  {
			generatorIsRunning=true;
			generator.generateStair();  } );
		

		lab.createLabel("remove selected stair",lab,150,20);
		but.createBoutton("del",but,100,20,( ActionListener)e-> {  
			stairList.getSelectedItem().delete();
			repopulate(); } );
		 
		lab.createLabel("show priorities",lab,150,20);
		but.createCheckBox("",but,100,20,(ActionListener) e-> {
			showPriorities = !showPriorities;
			repopulate(); } ); 
		
		 
	}
 
	//#################################################################################### 
	@Override void mouseReleased(Zone modifiedZone, int actualX, int actualY ) {
		 
		if(actuallyModifyingGeneratorZone)  {  
			zones.add(modifiedZone); 
			actuallyModifyingGeneratorZone=false;
		}
		else if(generatorIsRunning){ 
				zones.add(modifiedZone);
			 
		}
		else if(zoneEdited != null){//modify recorded
			Stair item = (Stair) stairList.getSelectedItem();
			for(Zone a : item.primaryZones)  
				if(a.equals(zoneEdited)) {
					a.update(modifiedZone.toSqlValues());
				}
					
			for(Zone a : item.secondaryZones)  
				if(a.equals(zoneEdited)) {
					a.update(modifiedZone.toSqlValues());
				}
			 //item.update(item.toSqlValues());
			zoneEdited = null;
		}
		 
	}
  
	//#################################################################################### 
	@Override void paintComponents(Graphics g ) {  
		if(stairList == null)return;
		Stair item = (Stair) stairList.getSelectedItem(); 
		 if(item!=null) { 
			 if (!generatorIsRunning && showPriorities) {
				    for (int rgb : item.priorities) {
				        for (Pixel pixel : Utils.getMatrix(editor.scene,false)) {
				            
				            if (pixel.rgb == rgb) { // V�rifier si le pixel a la m�me couleur que rgb
				                g.setColor(Color.BLACK);
				                // Dessiner chaque pixel avec les coordonn�es x et y correspondantes
				                for (Point p : pixel.points) { 
				                    g.fillRect(p.x, p.y, 1, 1);
				                }
				            }
				        }
				    }
				} else if (generatorIsRunning) {
				    for (int rgb : generator.temp.priorities) {
				        for (Pixel pixel : Utils.getMatrix(editor.scene,false)) { 
				            if (pixel.rgb == rgb) { // V�rifier si le pixel a la m�me couleur que rgb
				                g.setColor(Color.BLACK);
				                // Dessiner chaque pixel avec les coordonn�es x et y correspondantes
				                for (Point p : pixel.points) { 
				                    g.fillRect(p.x, p.y, 1, 1);
				                }
				            }
				        }
				    }
				}
		//draw the actual selected stair lines
		for(Zone a : item.primaryZones) a.draw(false, g,Color.GRAY);
		for(Zone a : item.secondaryZones) a.draw(false, g,Color.GRAY);
		 }
		if(!generatorIsRunning)return;
		//draw the line we are generating if its the case
		 
		for(Zone a : zones)   {
			a.draw(false, g,Color.GRAY); 
		}
		for(Zone a : generator.temp.primaryZones)   a.draw(false, g,Color.GRAY); 
		for(Zone a : generator.temp.secondaryZones)   a.draw(false, g,Color.GRAY);
		
		//rosella.paint(g,editor.getActualX(),editor.getActualY(),-2);
		 
	}
	
	//#################################################################################### 
	@Override public Zone mouseClic(int x, int y) {
		Point p = new Point(x,y);
		 if(Main.leftController.pencilList.getSelectedItem() != ZoneType.LINE)
			 Main.leftController.pencilList.setSelectedItem(ZoneType.ARC); //only line or arc in a stair
		 if(generatorIsRunning) { //if generate a new stair modify line of the temporary we are working with
			  
				Zone found = Utils.getZoneFromPoint(p, zones, 10); 
				 
				if(found != null) {
					zones.remove(found);
					actuallyModifyingGeneratorZone = true;  
					return found;
					  
				} 
				for(Zone a : zones)
					if(Utils.getDistanceBetweenPoints(a.getP3(), new Point(x,y) )<6) {
						zones.remove(a);
						actuallyModifyingGeneratorZone = true;
						return a;
					}
				
				return null;
		 }
		 
		  
		 else {//we are not generating ? act as usual
			 for(Zone a : ((Stair)stairList.getSelectedItem()).primaryZones)  
				 if(a.isNear(p,  10) || Utils.getDistanceBetweenPoints(p, a.getP3()) <6) {
					 zoneEdited = a.copie();
					 return a;
				 }
			 for(Zone a : ((Stair)stairList.getSelectedItem()).secondaryZones)  
				 if(a.isNear(p,  10) || Utils.getDistanceBetweenPoints(p, a.getP3()) <6) {
					 zoneEdited = a.copie();
					 return a;
				 }
		}
		 
		return null;  
	}
 
	//#################################################################################### 
	@Override protected void clickRight(int x, int y) {
		if(generatorIsRunning) {
			Generator .click(x, y); 
		}else {
			//generator is not running, update the stair (add/remove the rgb
			int selectedRGB = Main.actualEditor.imagePri.getRGB(x,y); 
			Stair item = (Stair) stairList.getSelectedItem();
			if(item.priorities.contains(selectedRGB))
				item.priorities.remove(new Integer(selectedRGB));
			else item.priorities.add(selectedRGB);
			item.update(item.toSqlValues());
		}
        
	}
 
	//#################################################################################### 
	@Override protected void pressed(int keyCode) {
		if(keyCode == KeyEvent.VK_ENTER) 
			Generator.pressEnter();
        
		if (keyCode >= KeyEvent.VK_0 && keyCode <= KeyEvent.VK_9)  
			Generator.pressNumber(keyCode);
          
	}
	//#################################################################################### 
	@Override void mouseMoved(int clickedX, int clickedY, int actualX, int actualY ) {  
	}
	
	//#################################################################################### 
	@Override public boolean handleImgDraggd() {
		return true;
	}
	
	//#################################################################################### 
	@Override protected void clickCenter(int x, int y) { 
	} 
	
	//#################################################################################### 
	@Override protected void handleWheel(int wheelRotation, int i, int j) { 
	      /*  Main.videoScroll.getVerticalScrollBar().setValue(0);
			percentFactor += 5*((wheelRotation > 0) ? -1 : 1);
			if(percentFactor>100)percentFactor = 100;
			if(percentFactor<10)percentFactor = 10;
			rosella.scale=percentFactor/100;
			rosella.paint(editor.getGraphics(),editor.getActualX(),editor.getActualY(),(double)percentFactor/100);*/
	}
	
	//############################################
	boolean isRunningThis(Editor editor) { return Main.actualNode.type == type;}
	boolean isEnabledFor(Object obj) {return obj instanceof Scene;}
  
}
