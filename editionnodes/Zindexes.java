package me.nina.editionnodes;

import java.awt.Color;
import java.awt.Graphics;
import java.util.List;
import java.util.Set;

import me.nina.editors.Editor;
import me.nina.editors.SceneEditor.EditType;
import me.nina.gameobjects.Scene;
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Cbox;
import me.nina.maker.Main;
import me.nina.maker.RosellaZone;
import me.nina.maker.Utils;
import me.nina.maker.Utils;
import me.nina.objects2d.Pixel;
import me.nina.objects2d.Point;

public class Zindexes extends EditNode<Integer> { 
	
	public Cbox<Integer> liste;  
	Zone zindexesBar;   
	boolean movingBar;
	public Zindexes( Editor e ) { super(EditType.ZINDEXES,e );  } 
	@Override public void populate() {
		 
		lab.createLabel("z-indexes",lab,150,20); 
		List<Integer> zList = Utils.getPriLevels(Utils.getMatrix(editor.scene,false)); 
		liste = but.createCbox(zList ,but,150,20,null); 
		lab.createLabel("click right on a color",lab,150,20);
		but.createLabel("move priority limit bar",but,150,20);
	}
 
	//#################################################################################### 
	@Override void mouseReleased(Zone modifiedZone, int actualX, int actualY ) {
		if(!movingBar)
			return;
		if(zindexesBar!=null) { 
			editor.scene.updateZindex( liste.getSelectedItem(),zindexesBar.getP1().y+""); 
			Utils.getMatrix(editor.scene,true);//update
			editor.lastZone=null;
			editor.repaint();
			movingBar=false;
		}
	}
	//#################################################################################### 
	@Override void mouseMoved(int clickedX, int clickedY, int actualX, int actualY ) { 
		if(!movingBar)
			return;
		int y = zindexesBar.getP1().y;  
		int deltaY = clickedY-actualY;
		int yindex=editor.scene.getZindex(liste.getSelectedItem()); 
		if(yindex<0)
			yindex=200;
		if(y>editor.image.getHeight() ) { 
			zindexesBar.setP1(0,editor.image.getHeight()-2); 
			zindexesBar.setP2(editor.getWidth(),editor.image.getHeight()+2); 
		}
		else if (y<0) { 
			zindexesBar.setP1(0,0); 
			zindexesBar.setP2(editor.getWidth(),4); 
		}
		else {
			zindexesBar.setP1(0,yindex-deltaY); 
			zindexesBar.setP2(editor.getWidth(),yindex-deltaY+4); 
		}
		editor.repaint();
	 
	}
	//#################################################################################### 
	@Override void paintComponents(Graphics g ) {  
		//g.drawImage(editor.imagePri, 0, 0, null);
		for (Pixel pixel : Utils.getMatrix(editor.scene,false)) {
		  
		    int rgb = pixel.rgb;
		    
		    // Parcourir les listes de coordonn�es x et y
		    for (Point p : pixel.points) { 
		        
		        // Convertir l'entier RGB en objet Color
		        Color color = new Color(rgb);
		        
		        // Assigner la couleur au pixel
		        g.setColor(color);
		        
		        // Dessiner le pixel
		        g.fillRect(p.x, p.y, 1, 1);
		    }
		}
		if( zindexesBar != null) 
			zindexesBar.draw(true, g,Color.GREEN);  
		
		  
			editor.rosella.paint(g,editor.getActualX(),editor.getActualY(),null);
	}
	
	//#################################################################################### 
	@Override public Zone mouseClic(int x, int y) { 
		Point p = new Point(x,y);
		if(zindexesBar != null && zindexesBar.isNear(p, 10) ) 
			movingBar=true;
		return null;
	}

	//#################################################################################### 
	@Override protected void clickRight(int x, int y) {  
		int rgb = editor.imagePri.getRGB(x, y);
		liste.setSelectedItem(rgb);
		int stored = editor.scene.getZindex(rgb); 
		if(stored == -1 || stored >editor.image.getHeight()-2 || stored <0) {
			stored = 200;
			editor.scene.updateZindex( liste.getSelectedItem(),stored+""); 
			}
		 
		zindexesBar=new Zone(0,stored-2, editor.image.getWidth(),stored+2,ZoneType.RECTANGLE); 

		zindexesBar.setUnmodifiable();
		zindexesBar.setUnmodifiableX();
		editor.repaint();
		movingBar=false;
	}

	//#################################################################################### 
	@Override protected void clickCenter(int x, int y) { } 
	@Override protected void pressed(int keyCode) { } 
	@Override protected void handleWheel(int wheelRotation, int i, int j) { }
	@Override public boolean handleImgDraggd() { return true; }
	boolean isRunningThis(Editor editor) { return Main.actualNode.type == type;}
	boolean isEnabledFor(Object obj) {return obj instanceof Scene;}
  
}
