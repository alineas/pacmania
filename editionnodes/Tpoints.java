package me.nina.editionnodes;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.swing.JCheckBox;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;

import me.nina.agenerator.BasicObjectGenerator;
import me.nina.editors.Editor;
import me.nina.editors.SceneEditor.EditType;
import me.nina.gameobjects.Action;
import me.nina.gameobjects.GameObject; 
import me.nina.gameobjects.Scene;
import me.nina.gameobjects.TransitionPoint;
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Character.PosType;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Utils.FloatingMenu;
import me.nina.maker.Cbox;
import me.nina.maker.Main;
import me.nina.maker.Pane;
import me.nina.maker.RosellaZone;
import me.nina.maker.Utils;
import me.nina.maker.Utils;
import me.nina.objects2d.Img;
import me.nina.objects2d.Point;
import me.nina.objects2d.Position;
import me.nina.sqlmanipulation.SQLLine;

public class Tpoints extends EditNode<TransitionPoint> {
	
	public Cbox<Zone> liste;
	private boolean actuallyModifyingZone; 
	 
	private boolean imMovingRosella;
 
	public Tpoints(Editor e) { 
		super(EditType.TRANSITIONPOINTS,e );  
		
	}
	
	@Override public void populate() {
		lab.createLabel("points",lab,150,20); 
		liste = but.createCbox(TransitionPoint.getByOwner(editor.scene.id),but,150,20,new ActionListener() { public void actionPerformed(ActionEvent e) { 
			
			editor.repaint();
		}});
		lab.createLabel("",lab,150,20); 

		 
		 
         
		editor.repaint();
	}
	//############################################
	
	boolean isEnabledFor(Object obj) {return obj instanceof Scene;}
	
 
 
	//############################################
 
 
 


	@Override
	void paintComponents(Graphics g ) {
		 
		
		for(int i = 0 ; i < liste.getItemCount() ; i++) {
			TransitionPoint item = (TransitionPoint) liste.getItemAt(i);  
			new Zone(item.mid().x-10,item.mid().y-10,item.mid().x+10,item.mid().y+10,ZoneType.RECTANGLE).draw(g);
			  if(liste.getSelectedIndex() == i)
				  item.fill(g,new Color(200, 200, 200, 128) );
				   
			  else  
				  item.fill(g,new Color(255,0,0,125) );
				   
			  
			  
			   
		}
		if(imMovingRosella  ) 
			editor.rosella.paint(g, editor.getActualX() ,editor.getActualY() ,null);
			
		else if(liste.getSelectedItem()!=null) {
			TransitionPoint tp = (TransitionPoint) liste.getSelectedItem();
			Position pos = tp.getDesiredPos();
			editor.rosella.paint(g, pos.x , pos.y ,pos.pos); 
		}
			
		
		
	}

 
 
	@Override void mouseReleased(Zone modifiedZone, int actualX, int actualY ) {
		TransitionPoint tp = (TransitionPoint) liste.getSelectedItem();
		
		if(imMovingRosella) {   
			imMovingRosella = false; 
			tp.setDesiredPos(new Position(editor.getActualX() ,editor.getActualY() ,editor.rosella.getPosType()));
			 
			tp.update(tp.toSqlValues());
			return;
		} 
		
		
		if(!actuallyModifyingZone) 
			makeNewZone(modifiedZone);
		else {  
			tp.setP1(modifiedZone.getP1()); 
			tp.setP2(modifiedZone.getP2());  
			tp.update(tp.toSqlValues()); 
			actuallyModifyingZone = false; 
			repopulate();
			for(int i = 0 ; i < liste.getItemCount() ; i++) {
				TransitionPoint item = (TransitionPoint) liste.getItemAt(i); 
				if(item.line.id == tp.line.id)
					liste.setSelectedIndex(i);
		 }
		} 
		 
	}
 

	private void makeNewZone(Zone z) {
		if(z.getType() == ZoneType.UNFILLED_OVALE || z.getType() == ZoneType.OVALE)  
			z.setType(ZoneType.OVALE) ; 
		else 
			z.setType(ZoneType.RECTANGLE) ;  
	
	 
		 saveTpoint(editor.scene,z.x(),z.y(),z.i(),z.j(),z.getType());
 
		 repopulate();
	}


	@Override
	void mouseMoved(int clickedX, int clickedY, int actualX, int actualY ) {
		// TODO Auto-generated method stub
		
	}
 
	@Override public Zone mouseClic(int x, int y) { 
		if(imMovingRosella||editor.rosella.isNear(new Point(x,y))) { 
			imMovingRosella=true;
			return Zone.DEFAULT_INSTANCE;
		}
		for(Zone item : TransitionPoint.getByOwner(editor.scene.id)) {  
			 	if(item.isNear(new Point(x,y),  10)){ 
			 		actuallyModifyingZone = true;  
			 		liste.setSelectedItem(item);
			 		return item;
			 	}
		}
		
		return null;
	}


	@Override
	protected void handleWheel(int wheelRotation, int i, int j) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public boolean handleImgDraggd() {
		return false;
	}


	@Override
	protected void clickRight(int x, int y) {
		JPopupMenu popupMenu = new JPopupMenu();
		 

		for(int i = 0 ; i < liste.getItemCount() ; i++) {
			TransitionPoint item = (TransitionPoint) liste.getItemAt(i);   
				   if(item.isNear(new Point(x,y),  10)){
					   liste.setSelectedItem(item);
					   lab.createJMenu(popupMenu,"tpoint "+item.title(),e -> {   ;    }); 
					   
					   List<Action> actionList = Action.getByDetector(item);
					   for(Action a : actionList) {
						   FloatingMenu popup = new Utils.FloatingMenu(a.getHtml());  
						  JMenuItem iteme = lab.createJMenu(popupMenu,"edit action "+a.title(),e -> { popup.hideFloatingContent();  new Action(a,false).populateRight();    }); 
						  iteme.addMouseListener(popup);
					   }
					   lab.createJMenu(popupMenu,"move rosella",e -> {  imMovingRosella=true;    }); 
					   lab.createJMenu(popupMenu,"change name",e -> {  
item.updateTitle(Main.getUserInput());    }); 
					   lab.createJMenu(popupMenu,"delete tpoint",e -> {  
						   item.delete();
						   liste.removeItem(item);  
							editor.lastZone = null;
							repopulate();  }); 
					   lab.createJMenu(popupMenu,"create action",e -> {  
						   Action a = new Action(item.id );
						   a=a.save();new Action(a,false).populateRight();}); 
					   if(Editor.hasInitialAsh(item.id,AshType.WILL_BE_DISABLED) )
						   lab.createJMenu(popupMenu,"enable at start",e -> {  
								Editor.remInitialAsh(item.id,AshType.WILL_BE_DISABLED);
	    }); 
					   else lab.createJMenu(popupMenu,"disable at start",e -> {  
						   Editor.setInitialAshes(item.id,AshType.WILL_BE_DISABLED);
    }); 
					   
					   
					   
					   
					   
					   popupMenu.show(Main.actualEditor, x, y);
					   
					   
				   }
					   
		}
		  
		
	}


	@Override
	protected void clickCenter(int x, int y) {
		// TODO Auto-generated method stub
		
	}


	@Override
	protected void pressed(int keyCode) {
		// TODO Auto-generated method stub
		
	}




	public void saveTpoint(GameObject scene, int x, int y, int i, int j, ZoneType selectedPencil ) { 
		 int uniqueId = SQLLine.getUniqueID();
		 
		if(selectedPencil != ZoneType.RECTANGLE)selectedPencil = ZoneType.OVALE;
		int posx = (x + i) / 2;
	 int posy = (y + j) / 2;
	 int w =  ((Scene)scene).bgImage().getWidth();
	 int h = ((Scene)scene).bgImage().getHeight();
	 PosType direction = PosType.BOTTOM;
		TransitionPoint tp = new TransitionPoint(x,y,i,j, selectedPencil, posx, posy, scene.id , -1, 
				uniqueId, -1, direction.getValeur());
		 tp.save(editor.scene.id, editor.scene.title()+"_tpoint_"+SQLLine.getUniqueID()); 
		  
	}
 
	 
 



 
	
}
