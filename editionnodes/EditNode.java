package me.nina.editionnodes;

import java.awt.Graphics;

import me.nina.editors.Editor;
import me.nina.editors.SceneEditor.EditType;
import me.nina.gameobjects.Zone;
import me.nina.maker.Main;
import me.nina.maker.Pane;

public abstract class EditNode<T> {
	final EditType type; 
	final Editor editor;
	final protected Pane but,lab; 
	 
	public EditNode(EditType type, Editor e ) {
		this.type = type; 
		this.editor = e; 
		but=Main.leftController.panneauBoutons;
		lab=Main.leftController.panneauLabels;
	}
	 
	abstract boolean isEnabledFor(Object obj);
	abstract void mouseReleased(Zone modified,int x, int y );
	abstract void mouseMoved(int clickedX,int clickedY,int actualX,int actualY);
	abstract void paintComponents(Graphics g2 );
	public abstract Zone mouseClic(int x, int y) ;
	protected abstract void clickRight(int x, int y);
	protected abstract void clickCenter(int x, int y);
	protected abstract void handleWheel(int wheelRotation, int i, int j);
	public abstract boolean handleImgDraggd();
	protected abstract void pressed(int keyCode);
	public abstract void populate();
	
	public void moved(int clickedX,int clickedY,int actualX, int actualY ) {
		if(	Main.actualNode.type == type )
			mouseMoved(clickedX,clickedY,actualX,actualY );
	}
	public void release(Zone z,int x,int y) {
		if(	Main.actualNode.type == type )
			mouseReleased(z,x,y);
	}
	public void paint(Graphics g2 ) { 
		if(	Main.actualNode.type == type)
			paintComponents(g2 ); 
	}
 
	public Zone clicked(int x, int y) { 
		if(	Main.actualNode.type == type)
			return mouseClic(x, y); 
		return null;
	}

	public void wheel(int wheelRotation, int x, int y) { 
		if(	Main.actualNode.type == type)
			handleWheel(wheelRotation,x, y); 
	}
 
	public boolean handleImgDragged() { 
		if(	Main.actualNode.type == type)
			return handleImgDraggd(); 
		return false;
	}
 
	public void clickR(int x, int y) {
		if(	Main.actualNode.type == type)
			clickRight(x,y);
	}
 
	public void clickC(int x, int y) {
		if(	Main.actualNode.type == type)
			clickCenter(x,y);
	}
 
	public void keyPressed(int keyCode) {
		if(	Main.actualNode.type == type)
			pressed(keyCode);
	}
 
	public void repopulate() {
		if(	Main.actualNode.type == type) {
			Main.leftController.removeAll();
			editor.updateLeftEditor();
			populate();
			Main.leftController.revalidate();
		}
	}
 
}
