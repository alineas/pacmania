package me.nina.editionnodes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.MouseInfo;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import me.nina.agenerator.ActionGenerator;
import me.nina.agenerator.BasicObjectGenerator;
import me.nina.agenerator.FullScreenObjectGenerator;
import me.nina.agenerator.Generator; 
import me.nina.agenerator.ObjectStateGenerator;
import me.nina.editors.AnimationEditor;
import me.nina.editors.Editor;
import me.nina.editors.SceneEditor.EditType;
import me.nina.gameobjects.Action;
import me.nina.gameobjects.Animation;
import me.nina.gameobjects.BasicObject;
import me.nina.gameobjects.Character;
import me.nina.gameobjects.Character.PosType;
import me.nina.gameobjects.GameObject;
import me.nina.gameobjects.MegaAction;
import me.nina.gameobjects.ObjectState; 
import me.nina.gameobjects.Scene;
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Cbox;
import me.nina.maker.JCbox;
import me.nina.maker.JCbox.CustomComboBoxListener;
import me.nina.maker.Main;
import me.nina.maker.Pane;
import me.nina.maker.RosellaZone;
import me.nina.maker.Utils;
import me.nina.maker.Utils.FloatingMenu;
import me.nina.maker.Utils;
import me.nina.maker.Ashe.AshType;
import me.nina.objects2d.Img;
import me.nina.objects2d.Point;
import me.nina.objects2d.Position;

public class ObjectsNode extends EditNode<Integer>  implements CustomComboBoxListener { 
	 
	boolean isSetupActionStates;  
	public Cbox<BasicObject> objectBoxx; 
	private boolean imMovingRosella;   
	private java.awt.Point currentLocation;  
	private Zone zoneToModify; 
	private ObjectState stateToMove; 
	private java.awt.Point clickedCenter;
	JCbox visibleObjectsJCboxx; 
	Pane lab = Main.leftController.panneauLabels, but=Main.leftController.panneauBoutons;
	private List<BasicObject> allTheObjects;
	private ObjectsNodeUtilities utilities;
	private GameObject[] checkBoxes;  
double zoom = 1.0;
private ArrayList<BasicObject> visibleObjects;
public final static ArrayList<Zone> zonesToDraw = new ArrayList<>();
	//#################################################################################### 
	
	public ObjectsNode(Editor e ) {
		super(EditType.OBJECTS,e );
		reset();
        			
        	
	}

	private void reset() { 
		utilities = new ObjectsNodeUtilities(editor,this);
		allTheObjects = BasicObject.getObjects(editor.scene);  
		
        Utils.setSize(editor, 3000, 3000); 
        
allTheObjects = BasicObject.getObjects(editor.scene);  
		
		checkBoxes = new GameObject[allTheObjects.size()]; 
		int i = 0;
        for (BasicObject obj : allTheObjects)  {
        	obj.setSelected(true); 
        	checkBoxes[i] = obj;
        	i++;
        }
        for(BasicObject a: allTheObjects)
        	for(ObjectState aa:ObjectState.getByObject(a.id))
        		if(aa.isFullscreen())
        			a.setSelected(false);
	}
	//############################################
	public void populate() {      
		BasicObject.hiddenObjects.clear();
		editor.removeAll();  Main.leftController.removeAll();  
editor.setZoom(zoom);
		//update all objects box
		allTheObjects = BasicObject.getObjects(editor.scene);  
		
		
		
		
		objectBoxx = lab.createCbox(allTheObjects ,lab,150,20,(ActionListener) e-> {   
			if(objectBoxx.getItemCount()!=0)  try { objectBoxx.getSelectedItem().icone.selected = true;Main.actualEditor.repaint(); } catch (Exception e1) { } });
		
		 //update object to show box
		 List<GameObject> chkBoxes = new ArrayList<>(Arrays.asList(checkBoxes)); 
		 for (GameObject gm : allTheObjects) 
		     if (!chkBoxes.contains(gm)) { 
		    	 gm.setSelected(true); 
		    	 chkBoxes.add(gm); 
		    	 BasicObject bo = (BasicObject)gm;
		    	 if(bo.selectedState == null)
	                	bo.setSelectedState(bo.initialState());
		    	}
		 checkBoxes = chkBoxes.toArray(new GameObject[0]); 
			visibleObjectsJCboxx = new JCbox(count -> "nbr of obj showed: " + count, chkBoxes);  
			visibleObjectsJCboxx.addCustomComboBoxListener(this);
		//hide the childs of fullscreen objects
		 for(GameObject a : chkBoxes) {BasicObject obj = (BasicObject)a;
			 if(obj.childs().size()!=0) for(BasicObject b : obj.childs())
					 if(!a.isSelected())BasicObject.hiddenObjects.add(b);
					 else BasicObject.hiddenObjects.remove(b);
		 }
		  
		 
		 
        lab.createLabel("selected object =", lab, 100, 20); 
        but.add(objectBoxx);
        lab.createLabel("show what ?", lab, 100, 20); 
        but.add(visibleObjectsJCboxx);  
          
        
        //add objects in zindex order
         visibleObjects = new ArrayList<>();
        for (int i = 0; i < visibleObjectsJCboxx.getItemCount(); i++) {
            BasicObject bo = (BasicObject) visibleObjectsJCboxx.getItemAt(i);
            if (bo.isSelected() && !BasicObject.hiddenObjects.contains(bo)) 
                visibleObjects.add(bo);  
        }
        Collections.sort(visibleObjects, new Comparator<BasicObject>() {
            @Override  public int compare(BasicObject obj1, BasicObject obj2) { 
                return obj1.zindex() - obj2.zindex(); } });
        Collections.reverse(visibleObjects);
       
         for (BasicObject bo : visibleObjects) if(bo.zindex()>=50){ 
            bo.populateFrames(zoom); 
            editor.add(bo.icone); 
        } 
         editor.remove(editor.rosella);editor.add(editor.rosella);
        for (BasicObject bo : visibleObjects) if(bo.zindex()<50){ 
            bo.populateFrames(zoom); 
            editor.add(bo.icone); 
        } 
        
       
        
     
        //populate the actions cases
		if(isSetupActionStates) 
			utilities.populate(visibleObjects,zoom);
		 
		
		editor.revalidate(); editor.repaint();
	}
	
	//#################################################################################### 
	@Override void paintComponents(Graphics g ) { 
		BasicObject item =  objectBoxx.getSelectedItem();
		if(item==null)return;
		Zone rectangle = new Zone(item.getSelectedState().pos().x-5,item.getSelectedState().pos().y-5,item.getSelectedState().pos().x+5,item.getSelectedState().pos().y+5,ZoneType.RECTANGLE);
		rectangle.unzoome(zoom);
		rectangle.draw(g);
		int deltaX = (int) (editor.getActualX()*zoom-editor.getClickedX()*zoom);
		int deltaY = (int) (editor.getActualY()*zoom-editor.getClickedY()*zoom);
		zonesToDraw.clear();
		//paint actions cases
		if (utilities.allObjectStateCases != null)  
			zonesToDraw.addAll(utilities.paint(deltaX,deltaY,g));
		 for(Zone z:Zone.getByOwner(item.selectedState.line.id)) { 
			 z.unzoome(zoom);
			 zonesToDraw.add(z );
			 }
		 //if we are moving an object, paint it
		if(currentLocation != null)  item.icone.setLocation( currentLocation.x+deltaX, currentLocation.y+ deltaY ); 
		  
		//if we are clicking and moving rosella, paint her
		if(imMovingRosella && editor.isClicking()) 
			editor.rosella.paint(g, (int) (editor.getActualX() ), (int) (editor.getActualY() ),null);
		else
		if(objectBoxx.getItemCount() != 0 && item.getSelectedState() != null) {  
			Position pos = item.getSelectedState().requiredPlayerPosition();
		//
			//else, paint rosella of the required player position
			  if(zoneToModify == null)
			 editor.rosella.paint(g,(int)(pos.x*zoom),  (int)(pos.y*zoom),pos .pos);
		 
			//draw the zones (of actual and initial state) and give it to the icones for paint beside
			 if(stateToMove==null) {
			for(Zone z : Zone.getByOwner(item.getSelectedState().id)) {
					z.fill(g,zoom,new Color(255,0,0,125)); Zone z2 = z.copie();z2.unzoome(zoom);//zonesToDraw.add(z2); 
					}
			for(Zone z:Zone.getByOwner(item.initialState().id)) {
					z.fill(g,zoom,new Color(255,0,0,125));  Zone z2 = z.copie();z2.unzoome(zoom); //zonesToDraw.add(z2); 
					} 
			if(editor.corner1!=null){editor.corner1.fill(g, Color.red);
				zonesToDraw.add(editor.corner1);
			}
			if(editor.corner2!=null){editor.corner2.fill(g, Color.red);
				zonesToDraw.add(editor.corner2);
			}
			if(editor.offsetZone!=null){editor.offsetZone.fill(g, Color.red);
				zonesToDraw.add(editor.offsetZone);
			}
			if(editor.modifiedZone != null) {
				 
				zonesToDraw.add(editor.modifiedZone);
			}
			if(editor.lastZone != null) { 
				 zonesToDraw.add(editor.lastZone);
			}
			 }
		}
 
	}
	


	//#################################################################################### 
	@Override void mouseReleased(Zone modifiedZone, int actualX, int actualY ) {try {Utils.log("called released");
		BasicObject item = objectBoxx .getSelectedItem();
		clickedCenter = null;
		int deltaX = (int) (editor.getActualX()*zoom-editor.getClickedX()*zoom);
		int deltaY = (int) (editor.getActualY()*zoom-editor.getClickedY()*zoom);
		modifiedZone.zoome(zoom);
		Generator.zoneDrawn(modifiedZone);
       
		utilities.mouseReleased(modifiedZone,actualX,actualY);
 
		if(imMovingRosella) {  Utils.log(item.title()+" "+item.getSelectedState().title());
			imMovingRosella = false; 
			item.getSelectedState() .updateRequiredPlayerPosition((int) (editor.getActualX()/zoom), (int) (editor.getActualY()/zoom), editor.rosella.getPosType());
		} 

		if(stateToMove != null) {
			item.getSelectedState() .updatePos((int) (currentLocation.x/zoom+deltaX/zoom),  (int) (currentLocation.y/zoom+deltaY/zoom),0);
			for(Zone zz:Zone.getByOwner(item.getSelectedState().id)) {
					Zone z2 = zz.xp((int) (deltaX/zoom)).yp((int) (deltaY/zoom)); 
					
					zz.update(z2.toSqlValues());
			}
		} 
		stateToMove = null;
		currentLocation=null;
 
		//defaultinstance = create a zone
		if(  zoneToModify != null && zoneToModify == Zone.DEFAULT_INSTANCE) { 
			String title = Main.getUserInput();  
			
			modifiedZone.save(item.getSelectedState().id,title); 
			zoneToModify = null;
		}

		//no default instance = update zone
		if( zoneToModify != null) {  
			   
			 zoneToModify.update(modifiedZone.toSqlValues());  
			    
		}
	} catch (Exception e) { e.printStackTrace(); } } 
	
	//#################################################################################### 
	@Override void mouseMoved(int clickedX, int clickedY, int actualX, int actualY ) { 
		if(stateToMove != null) ((BasicObject)stateToMove.possessorObj()).icone.rewindAll();
		utilities.mouseMoved(actualX,actualY); 
		if(  clickedCenter != null) {
   	   	 java.awt.Point newMouseLocation = MouseInfo.getPointerInfo().getLocation();
   		    SwingUtilities.convertPointFromScreen(newMouseLocation, editor);//centerclic
   	   	    int dax = (int) ((newMouseLocation.x-clickedCenter.x) );
   			int day = (int) ((newMouseLocation.y-clickedCenter.y) ); 
   		    Main.videoScroll.getViewport().setViewPosition(
   		    		new java.awt.Point( (int) (Main.videoScroll.getViewport().getViewPosition().x-dax),(int) (Main.videoScroll.getViewport().getViewPosition().y-day)));
   	   }
	}

	//#################################################################################### 
	@Override public Zone mouseClic(int x, int y) {
		if(this.isSetupActionStates)
			return Zone.DEFAULT_INSTANCE;
		Point p = new Point((int) (x/zoom),(int) (y/zoom));
		 Generator .click((int) (x/zoom),(int) (y/zoom));
		//let the action generator know wat to do
		List <BasicObject>bl = new ArrayList<>();
		ArrayList <Zone >zl = new ArrayList<>();
		 
    	for (BasicObject object : objectBoxx.values()) {   
    			if(object.icone==null)continue;//invisible childs 
    		if (object.icone.getBounds().contains(new java.awt.Point(x,y)))
    			bl.add(object);
    		for (Zone zone : Zone.getByOwner(object.getSelectedState().id)) 
    			if (zone.isNear(p, 10)) zl.add(zone);
    		for (Zone zone : Zone.getByOwner(object.initialState().id)) 
    			if (zone.isNear(p, 10)) zl.add(zone); 
    	} 
    	Generator.zoneSelected(zl); 
    	Generator.objectSelected(bl);
     	       
    	 
		if(imMovingRosella &&editor.rosella.isNear(new Point(x,y))) 
			 return Zone.DEFAULT_INSTANCE; //fake for remember we do nothing, and let videopanel think we work wit it
		else imMovingRosella = false;
		
		if(stateToMove != null) {	  
			currentLocation =  new java.awt.Point((int) (objectBoxx.getSelectedItem().getSelectedState().pos().x*zoom),(int) (objectBoxx.getSelectedItem().getSelectedState().pos().y*zoom)) ;   
			return Zone.DEFAULT_INSTANCE; //fake for say to videopanel we working on it
		}
		
		if(zoneToModify != null && zoneToModify != Zone.DEFAULT_INSTANCE) {
			Zone z = zoneToModify.copie(); 
			 
			if( z.isNear(p, 10))  { //so unselectable
				z.unzoome(zoom);
				return z; 
			}
			else { 
				zoneToModify = null;
				editor.lastZone = null;
			}
		}
		Zone stateclicked = utilities.mouseClic(new Point(x,y));
		if(stateclicked != null)
			return stateclicked;
		  
		return null;
	} 
 
	//####################################################################################
	@Override protected void clickRight(int x, int y) {
		JPopupMenu popupMenu = new JPopupMenu();
		zoneToModify = null;
		stateToMove = null;
		imMovingRosella=false;
		Point clickPoint = new Point((int) (x/zoom), (int) (y/zoom));
		Map<Zone,BasicObject> zones = new HashMap<>();
		List<BasicObject> objects = new ArrayList<>();

		for(int i =0 ; i < visibleObjectsJCboxx.getItemCount() ; i++)		
			if(visibleObjectsJCboxx.getItemAt(i).isSelected()) {
				BasicObject object = (BasicObject) visibleObjectsJCboxx.getItemAt(i);
				  
				if (object.icone!=null&&object.icone.getBounds().contains(new java.awt.Point((int) (x),(int) (y)))) { 

					if(!object.equals(objectBoxx.getSelectedItem()))
						isSetupActionStates=false;
					objects.add(object);   
				}
				for (Zone zone : Zone.getByOwner(object.getSelectedState().id)) 
					if (zone.isNear(clickPoint,  10)) 
						zones.put(zone,object);  

				for (Zone zone : Zone.getByOwner(object.initialState().id)) 
					if (zone.isNear(clickPoint,  10)) 
						zones.put(zone,object);    
			}

		//unselect all icones
		for(BasicObject a:objectBoxx.values()) {
			if(a.icone==null)continue;//invisible childs
			a.icone.selected=false;
		}
		JMenu sub = new JMenu("character to work with " ); 
		for(Character c : me.nina.gameobjects.Character.getAll())
			lab.createJMenu(sub,c.title()  ,e -> {Editor.getGameInfo().update("actualcharacter",c.id+"");
			RosellaZone.actualCharacter = c; });  

		popupMenu.add(sub);
		for(BasicObject object : objects) {
			JMenu subMenu = new JMenu("object "+object.title()); 
			lab.createJMenu(subMenu,"select it" , e -> { objectBoxx.setSelectedItem(object);  });
			lab.createJMenu(subMenu,"Create new child object " ,e -> { BasicObjectGenerator.generateBasicObject(object.id); }); 
			lab.createJMenu(subMenu,"add a new state " ,e -> { ObjectStateGenerator.generateNewState(object); });  
			lab.createJMenu(subMenu,"change deep for object use its own deep on scene "  ,e -> {object.updateZindex(-10);populate();  }); 
			lab.createJMenu(subMenu,"change deep for character be under "  ,e -> {object.updateZindex(51);populate();  }); 
			lab.createJMenu(subMenu,"change deep for character be beside "  ,e -> {object.updateZindex(49);populate();  }); 
			lab.createJMenu(subMenu,"change deep for be near "  ,e -> {object.updateZindex(object.zindex()+1);populate();  }); 
			lab.createJMenu(subMenu,"change deep for be far "  ,e -> {object.updateZindex(object.zindex()-1);populate();  }); 
			lab.createJMenu(subMenu,"change name "  ,e -> {object.updateTitle(Main.getUserInput());  }); 
			lab.createJMenu(subMenu,"delete "  ,e -> {
				object.delete(); reset();populate();visibleObjects.clear(); utilities.populate(visibleObjects, zoom); });  
			popupMenu.add(subMenu);
 
			List<ObjectState> objectStates = ObjectState.getByObject(object.id);
			for (ObjectState a : objectStates) {
				JMenu stmenu = new JMenu(a.title()); 
 				 lab.createJMenu(stmenu,"select this state",e -> { 
					
					object.setSelectedState(a); populate(); Generator.stateSelected(a); 
					 object.icone.rewindAll();objectBoxx.setSelectedItem(object);
				}); 
 				lab.createJMenu(stmenu,"Add rectangle Zone "  ,e -> { objectBoxx.setSelectedItem(object);object.setSelectedState(a); Main.leftController.pencilList.setSelectedItem(ZoneType.RECTANGLE); zoneToModify = Zone.DEFAULT_INSTANCE; });  
 				lab.createJMenu(stmenu,"Add ovale Zone " ,e -> {objectBoxx.setSelectedItem(object);object.setSelectedState(a);Main.leftController.pencilList.setSelectedItem(ZoneType.OVALE); zoneToModify = Zone.DEFAULT_INSTANCE; }); 
 				lab.createJMenu(stmenu,"Move state " ,e -> { objectBoxx.setSelectedItem(object);object.setSelectedState(a);stateToMove = object.getSelectedState(); });  
 				lab.createJMenu(stmenu,"update character place   "  ,e -> { objectBoxx.setSelectedItem(object); object.setSelectedState(a); imMovingRosella=true;  });
 				lab.createJMenu(stmenu,"align positions to initial state  "  ,e -> { objectBoxx.setSelectedItem(object); object.setSelectedState(a); a.resetPlayerPosition();  });
 				
 				 
 				JMenu sube = new JMenu("update character position" ); 
 				for(PosType p : PosType.values())
 					lab.createJMenu(sube,p+""  ,e -> {objectBoxx.setSelectedItem(object);object.setSelectedState(a); 
 					a.updateRequiredPlayerPosition(a.requiredPlayerPosition().x,a.requiredPlayerPosition().y,  p.getValeur());
 					});  
				lab.createJMenu(stmenu,"edit animation",event -> { Main.setActualEditor(new AnimationEditor(Animation.get(a.animID())));    });
				lab.createJMenu(stmenu,"click position for ground anim priority",event -> {  Animation.get(a.animID()).updatePriorityGroundPoint(y);    });


 				stmenu.add(sube);
 				
 				
 				lab.createJMenu(stmenu,"change name " ,e -> {a.updateTitle(Main.getUserInput());  }); 
 				 lab.createJMenu(stmenu,"delete " ,e -> {a.delete();populate();    }); 
 				 
 				popupMenu.add(stmenu);
			}
			
			lab.createJMenu(popupMenu, " "  ,e -> {   });

			
			
		} 
		lab.createJMenu(popupMenu,"------------" ,e -> {  });  

		for(Zone zone : zones.keySet()) {
			JMenu subMenu = new JMenu("zone "+zone.title());
			lab.createJMenu(subMenu," " ,e -> {     });

			lab.createJMenu(subMenu,"Modify zone " , e -> {
				BasicObject o = (BasicObject) zone.possessorObj().possessorObj();
				objectBoxx.setSelectedItem(o); o.setSelectedState((ObjectState) zone.possessorObj());zoneToModify = zone;  });
			lab.createJMenu(subMenu,"change zone name" , e -> { zone.updateTitle(Main.getUserInput());  });
			lab.createJMenu(subMenu,"del Zone " ,e -> { zone.delete(); populate(); utilities.populate(visibleObjects, zoom);  });
			lab.createJMenu(subMenu,"open Zone in sql " ,e -> { zone.line.toFrame(true);  });
			lab.createJMenu(subMenu," " ,e -> {     });

			if(Editor.hasInitialAsh(zone.id,AshType.WILL_BE_DISABLED)  )
				   lab.createJMenu(subMenu,"enable at start",e -> {  
						Editor.remInitialAsh(zone.id,AshType.WILL_BE_DISABLED);});   
			   else lab.createJMenu(subMenu,"disable at start",e -> {  
				   Editor.setInitialAshes(zone.id,AshType.WILL_BE_DISABLED);}); 
			
			
			
			lab.createJMenu(subMenu,"actions :  " ,e -> {     });
			for (Action a : Action.getByDetector(zone)) {
				FloatingMenu popup = new Utils.FloatingMenu(a.getHtml());
				JMenuItem item = lab.createJMenu(subMenu,a.title() ,e -> { popup.hideFloatingContent(); new Action(a,false).populateRight();   });
				item.addMouseListener(popup);
				lab.createJMenu(subMenu,"copy action" ,e -> {   Action.copy(a).populateRight(); });
			}
			JMenu sub2 = new JMenu("regroup these actions to");
			lab.createJMenu(subMenu," " ,e -> {     });
			for (MegaAction a : MegaAction.getAll()) {
				lab.createJMenu(sub2,a.title() ,e -> {  
					for (Action acc : Action.getByDetector(zone)) {
						acc.updateMegaOwner(a.id);
					}
						
				});
				
			}
			lab.createJMenu(sub2,"   " ,e -> {     });
			lab.createJMenu(sub2,"new massAction" ,e -> { 
				String title = Main.getUserInput();
				 MegaAction megaAction = new MegaAction(title);
		            megaAction = megaAction.save();
				for (Action acc : Action.getByDetector(zone)) {
					acc.updateMegaOwner(megaAction.id);
				}
					
			});
			subMenu.add(sub2);
			lab.createJMenu(subMenu," " ,e -> {     });
			lab.createJMenu(subMenu,"create action" ,e -> {   Action a = new Action(zone.id );
			   a=a.save();new Action(a,false).populateRight();}    );

			  
		lab.createJMenu(subMenu," " ,e -> {     });

			lab.createJMenu(subMenu,"-----------" ,e -> {     });
			lab.createJMenu(subMenu," " ,e -> {     });
			 
			popupMenu.add(subMenu);
		} 
		
		
		
		
		
		
		
		
		
		
		lab.createJMenu(popupMenu, " "  ,e -> {   });

		JMenu subMenu = new JMenu("create object " );
		lab.createJMenu(subMenu,"Create simple ground Object who give item",e -> { BasicObjectGenerator.generateGroundToInvObject(-1);   });  
		lab.createJMenu(subMenu,"Create multiZones ground Object",e -> { BasicObjectGenerator.generateBasicObject(-1);   });  
		lab.createJMenu(subMenu,"Create action",e -> { ActionGenerator.generateAction( ); });  
		lab.createJMenu(subMenu,"create fullscreen obj" ,e -> { FullScreenObjectGenerator.generateFullscreenObject( ); });  
		popupMenu.add(subMenu);
		lab.createJMenu(popupMenu,"edit state actions",e -> { isSetupActionStates=true;populate(); }); 

		
		
		
		
		
		
		
		

		if(utilities.allObjectStateCases!=null) 
			for(ObjectState a : utilities.allObjectStateCases)
				if (a.getBounds().contains(new java.awt.Point(x,y))) {
					JMenuItem del = new JMenuItem("delete state");
					del.addActionListener(event -> { a.delete(); populate();});
					popupMenu.add(del);

					lab.createJMenu(popupMenu,"edit animation",event -> { Main.setActualEditor( new AnimationEditor(Animation.get(a.animID())));    });

				}

		popupMenu.show(Main.actualEditor, x, y);
	}



	//####################################################################################
	@Override protected void pressed(int keyCode) {
		int x = 0 ;
		int y = 0 ;
		BasicObject item = objectBoxx.getSelectedItem(); 
		switch (keyCode) {
		case KeyEvent.VK_UP: y--;  break;
		case KeyEvent.VK_DOWN: y++;  break;
		case KeyEvent.VK_LEFT: x--;  break;
		case KeyEvent.VK_RIGHT:  x++; break;
		case KeyEvent.VK_ENTER:{

			Generator.pressEnter();

			break;
		}
		default: }
		if(imMovingRosella) { 
			item.getSelectedState() .updateRequiredPlayerPosition(item.getSelectedState().requiredPlayerPosition().x+x, item.getSelectedState().requiredPlayerPosition().y+y, editor.rosella.getPosType());
			editor.repaint();
		}else if(stateToMove != null){
			item.getSelectedState().updatePos(item.getSelectedState().pos().x+x,  item.getSelectedState().pos().y+y,0);
			item.icone.setLocation(new java.awt.Point((int) (item.getSelectedState().pos().x*zoom),(int) (item.getSelectedState().pos().y*zoom))); 
			((BasicObject)stateToMove.possessorObj()).icone.rewindAll();
		}

	}

	
	
	
	//######################################################
	@Override public boolean handleImgDraggd() {  return true; } 
	@Override protected void clickCenter(int x, int y) { 
       	 	clickedCenter = MouseInfo.getPointerInfo().getLocation();
       	 SwingUtilities.convertPointFromScreen(clickedCenter, editor); 
	}
	@Override protected void handleWheel(int wheelRotation, int i, int j) {   
		double old = zoom;
    	zoom += ((wheelRotation > 0) ? -0.15 : 0.15);
		if(zoom>2)zoom = 2;
		if(zoom<0.2)zoom = 0.2;
		if(old != zoom) {
			 editor.setZoom(zoom); 
			 Main.videoScroll.getViewport().setViewPosition(new java.awt.Point((int) (i*zoom-200), (int)(j*zoom-200)));
			populate ();
		}
		
		
	}
	boolean isRunningThis(Editor editor) { return Main.actualNode.type == type;}
	boolean isEnabledFor(Object obj) {return obj instanceof Scene;}
	@Override public void customEventOccurred() {  populate();if(this.isSetupActionStates) utilities.populate(visibleObjects, zoom);}
} 
 