package me.nina.editionnodes;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.sound.sampled.Line;
import javax.swing.JButton;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import me.nina.gameobjects.Character;
import me.nina.editors.Editor;
import me.nina.gameobjects.Action;
import me.nina.gameobjects.Animation;
import me.nina.gameobjects.BasicObject;
import me.nina.gameobjects.GameObject;
import me.nina.gameobjects.InventoryObject;
import me.nina.gameobjects.ObjectState;
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Ashe;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Cbox;
import me.nina.maker.Main;
import me.nina.maker.Utils;
import me.nina.maker.Utils;
import me.nina.objects2d.Img;
import me.nina.objects2d.Point;
import me.nina.sqlmanipulation.DuoArray;

public class ObjectsNodeUtilities {
	Action currentAction;ObjectState actualStateClicked;
	private static Zone zoneClicked;
    public List<ObjectState> allObjectStateCases;
    public List<Action> allActionCases;  
	private Editor editor;
	private ObjectsNode objectNode;
	private ObjectState caseToMove,newStateClicked,oldStateClicked; 
	double zoom = 1.0;
	private Action actionToMove;
	private Action actionRefuse;
	private Action actionNeed;
	private Action actionToRem;
	private Action actionToSet;
	
	public ObjectsNodeUtilities(Editor e, ObjectsNode o) {
		this.objectNode=o;
		this.editor = e;
	}
	//#################################################################################### 
	void setActionCase(Rectangle startBounds, Rectangle endBounds, Action ac  ) {
		 
		int startX = (int) ((int) startBounds.getCenterX()*zoom);
		int startY = (int) ((int) startBounds.getCenterY()*zoom);
		int endX = (int) ((int) endBounds.getCenterX()*zoom);
		int endY = (int) ((int) endBounds.getCenterY()*zoom);

		int middleX = (startX + endX) / 2;
		int middleY = (startY + endY) / 2;

		Action action = new Action(ac,middleX,middleY,zoom,editor); 
		
		action.buttonNodes.addActionListener(new ActionListener() {   @Override public void actionPerformed(ActionEvent e) {
			currentAction = action;
					action.populateRight( ); 
		} });
 
		 if(!allActionCases.contains(action) ) {
			 allActionCases.add(action);
		editor.add(action,JLayeredPane.DEFAULT_LAYER);  
		boolean recorded = false;
		for(DuoArray<String, String> entry : Editor.getOrderedActionStateGrid())
			if(Integer.parseInt(entry.getKey()) == action.id )
				recorded = true;
		
		if(!recorded)
			Editor.recordActionStatePlacement(
					action.id, (int) (
							action.getLocation().x
							/zoom), 
					(int) (action.getLocation().y/zoom));

	} else{editor.remove(action.panelToSet);editor.remove(action.buttonNodes);editor.remove(action.panelToRem);editor.remove(action.panelNeededs);editor.remove(action.panelrefuseds);}}

	//#################################################################################### 
	void populate(List<BasicObject> visibleObjects, double zoom) { this.zoom=zoom;
		allObjectStateCases = new ArrayList<>(); 
		allActionCases = new ArrayList<>();
 
		objectNode.lab.createLabel("color blue =", objectNode.lab, 100, 20); 
		objectNode.but.createLabel("detector", objectNode.but, 200, 20); 
		
		objectNode.lab.createLabel("color red =", objectNode.lab, 100, 20); 
		objectNode.but.createLabel("will forget this", objectNode.but, 200, 20);
		
		objectNode.lab.createLabel("color green =", objectNode.lab, 100, 20); 
		objectNode.but.createLabel("will remember this", objectNode.but, 200, 20);
		
		for(BasicObject obj : visibleObjects) 
			for (int i = 0; i < objectNode.visibleObjectsJCboxx.getItemCount(); i++) {
	            BasicObject bo = (BasicObject) objectNode.visibleObjectsJCboxx.getItemAt(i);
	            if (bo.equals(obj)&&bo.isSelected() && !BasicObject.hiddenObjects.contains(bo)) 
	                 for(ObjectState st:ObjectState.getByObject(obj.id)) 
				 allObjectStateCases.add(st);  
			}
		
		int nbr =  0; int xx = 0 ; int yy = (int) ( Main.actualEditor.scene.bgImage().getHeight()*zoom) ;
		for (ObjectState c : allObjectStateCases) {  //create the first location for components 
			c.populate(zoom);nbr++;  xx+=c.getWidth()*zoom; if(nbr > 5) {yy+=c.getHeight()*zoom; xx=0;nbr = 0 ;}
			c.setVisible(true);  c.setLocation(new java.awt.Point(xx,yy));  
			for (DuoArray<String, String> a :Editor.getOrderedObjectStateGrid()) 
				if(a.getKey().equals(c.id+""))
					c.setLocation(new java.awt.Point((int) (Integer.parseInt(a.getValue().split("\\.")[0])*zoom),(int) (Integer.parseInt(a.getValue().split("\\.")[1])*zoom))); 
			editor.add(c,JLayeredPane.DEFAULT_LAYER); 
			
		}
		 
		
		try {
			//place the actions rectangles at the middle of the line
			for (BasicObject obj : visibleObjects) 
				for(ObjectState state : ObjectState.getByObject(obj.id)) 
					for(Zone zone : Zone.getByOwner(state.id)) 
					for (Action a : Action .getByDetector(GameObject.getById(zone.line.id))) {
							test(a,zone,state,obj);
						
						for(Ashe toSet : a.ashToSet())
							if(toSet.type() == AshType.ACTION_TO_DO_WITH_BEFORE_CONDITION_CHECK)
								test((Action) toSet.possessor(),zone,state,obj);
					}
						
			//place at a already recorded position
			for (Action c : allActionCases) 
				for (DuoArray<String, String> a :Editor.getOrderedActionStateGrid()) 
					if(a.getKey().equals(c.id+""))
						c.setLocation(new java.awt.Point((int) (Integer.parseInt(a.getValue().split("\\.")[0])*zoom),(int) (Integer.parseInt(a.getValue().split("\\.")[1])*zoom)));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		
		
		
		
	} 
 
	private void test(Action a, Zone zone, ObjectState state, BasicObject obj) {
		for(Ashe toSet : a.ashToSet()) {
			
			if(toSet.type() == AshType.WILL_BE_IN_STATE && toSet.possessor () instanceof ObjectState)
				try {
					
					setActionCase(zone.toRect(), ((ObjectState)toSet.possessor ()).image1.getBounds(), a);
				} catch (Exception e) {
					Utils.log("unable to handle action state for "+toSet.title()+" "+toSet.possessor().id);
				}
				  
			 
				 
		}
	}
	//#################################################################################### 
	//#################################################################################### 
	public Collection<? extends Zone> paint (int deltaX, int deltaY, Graphics g) {
		List<Zone> liste = new ArrayList<>();try {
		
		
		if (zoneClicked != null) {
			Zone z = new Zone((int) (zoneClicked.getP3().x*zoom), (int) (zoneClicked.getP3().y*zoom), (int) (zoneClicked.getP3().x*zoom+deltaX/zoom), (int) (zoneClicked.getP3().y*zoom+deltaY/zoom),ZoneType.LINE);
			z.draw(true, g,Color.GREEN);
			//z.zoome(zoom);
			liste.add(z);
		
		}
		if (oldStateClicked != null) {
			Rectangle r = oldStateClicked.image1.getBounds();
			Zone z = new Zone((int) ((r.x + r.width / 2)), (int) ((r.y + r.height / 2)),(int) ((r.x + r.width / 2+deltaX/zoom)), (int) ((r.y + r.height / 2+deltaY/zoom)),ZoneType.LINE);
			z.draw(true, g,Color.GREEN); 
			//z.zoome(zoom);
			liste.add(z);
			
		}
		if (newStateClicked != null) {
			Animation img = newStateClicked.image3;
			if(img == null)img = newStateClicked.image1;//invobj
			Rectangle r = img.getBounds();
			Zone z = new Zone((int) ((r.x + r.width / 2)), (int) ((r.y + r.height / 2)),(int) ((r.x + r.width / 2+deltaX/zoom)), (int) ((r.y + r.height / 2+deltaY/zoom)),ZoneType.LINE);
			z.draw(true, g,Color.GREEN);
			//z.zoome(zoom);
			liste.add(z); 
		}
		if (actionNeed != null) {
			Rectangle r = actionNeed.panelNeededs.getBounds();
			Zone z = new Zone((int) ((r.x + r.width / 2)), (int) ((r.y + r.height / 2)),(int) ((r.x + r.width / 2+deltaX/zoom)), (int) ((r.y + r.height / 2+deltaY/zoom)),ZoneType.LINE);
			z.draw(true, g,Color.GREEN);
			//z.zoome(zoom);
			liste.add(z); 
		}
		if (actionRefuse != null) {
			Rectangle r = actionRefuse.panelrefuseds.getBounds();
			Zone z = new Zone((int) ((r.x + r.width / 2)), (int) ((r.y + r.height / 2)),(int) ((r.x + r.width / 2+deltaX/zoom)), (int) ((r.y + r.height / 2+deltaY/zoom)),ZoneType.LINE);
			z.draw(true, g,Color.GREEN);
			//z.zoome(zoom);
			liste.add(z); 
		}
		if (actionToSet != null) {
			Rectangle r = actionToSet.panelToSet.getBounds();
			Zone z = new Zone((int) ((r.x + r.width / 2)), (int) ((r.y + r.height / 2)),(int) ((r.x + r.width / 2+deltaX/zoom)), (int) ((r.y + r.height / 2+deltaY/zoom)),ZoneType.LINE);
			z.draw(true, g,Color.GREEN);
			//z.zoome(zoom);
			liste.add(z); 
		}
		if (actionToRem != null) {
			Rectangle r = actionToRem.panelToRem.getBounds();
			Zone z = new Zone((int) ((r.x + r.width / 2)), (int) ((r.y + r.height / 2)),(int) ((r.x + r.width / 2+deltaX/zoom)), (int) ((r.y + r.height / 2+deltaY/zoom)),ZoneType.LINE);
			z.draw(true, g,Color.GREEN);
			//z.zoome(zoom);
			liste.add(z); 
		}
				
		for (Action a : allActionCases) {  
			try {
				for (Ashe theAsh : a.ashToSet()) { 
					if(theAsh.getNbr()<0) {
						if(theAsh.type() == AshType.WILL_BE_IN_STATE) 
							liste.addAll(stateToSet(theAsh,a,g,Color.red ));
						
						else if(theAsh.type() == AshType.HAS_IN_INVENTORY)  
							liste.addAll(invToSet(theAsh,a,g,Color.red)); 
					}else {
					if(theAsh.type() == AshType.WILL_BE_IN_STATE)
						liste.addAll(stateToSet(theAsh,a,g,Color.green ));
					
					else if(theAsh.type() == AshType.HAS_IN_INVENTORY)  
						liste.addAll(invToSet(theAsh,a,g,Color.green));
					}
				}
				 
				
				for (Ashe theAsh : a.ashNeeded()) {   
					if(theAsh.type() == AshType.IS_IN_STATE) {
						ObjectState stateItWillBe =  (ObjectState) theAsh.possessor (); 
						liste.add(drawLine (Color.black,g, stateItWillBe.image1.getBounds(),a.panelNeededs.getBounds(),  a,false,false));
					}
					
					else if(theAsh.type() == AshType.HAS_IN_HAND) {
						BasicObject invObjNeeded =  (BasicObject) theAsh.possessor ();   
						liste.add(drawLine (Color.black,g, invObjNeeded.getSelectedState().image1.getBounds(),a.panelNeededs.getBounds(),  a,true,false));  
					}
				}
				
				for (Ashe theAsh : a.ashRefused()) {   
					if(theAsh.type() == AshType.IS_IN_STATE) {
						ObjectState stateItWillBe =  (ObjectState) theAsh.possessor (); //state to set can only be an onbjectState
						liste.add(drawLine (Color.DARK_GRAY,g, stateItWillBe.image3.getBounds(),a.panelrefuseds.getBounds(),  a,true,false));
					}
					 
						 
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
			
			 
		}} catch (Exception e) { e.printStackTrace(); }
		return liste;
	}
 
	//####################################################################################  
	private List<Zone> invToSet(Ashe theAsh, Action a, Graphics g,Color color) {
		List<Zone> liste = new ArrayList<>();
		try {
			BasicObject invObjToGive =  (BasicObject) theAsh.possessor ();//we always give the object, not the state
			liste.add(drawLine (color,g, invObjToGive.getSelectedState().image1.getBounds(),color==Color.red?a.panelToRem.getBounds():a.panelToSet.getBounds(),  a,true,false)); //stateItWillBe.image3->action=green line
				Zone zoneActingTheChange = Zone.get(a.possessor()); 
				if(zoneActingTheChange!=null) {
					//ObjectState zonePossessor = ObjectState.get(ZoneList.getPossessorByZoneId(zoneActingTheChange.uniqueId()));
					liste.add(drawLine (Color.blue,g, invObjToGive.getSelectedState().image1.getBounds(), a.buttonNodes.getBounds(),a,true,false));
					return liste;
				}
				 
				BasicObject objActingTheChange = BasicObject.get(a.possessor());
				if(objActingTheChange != null) {
					liste.add(drawLine (Color.blue,g, invObjToGive.getSelectedState().image3.getBounds(), a.buttonNodes.getBounds(),a,true,false));
					return liste;
				}
				
				Character charActingTheChange = Character.get(a.possessor());
				if(charActingTheChange != null) {//not handled yet
					//liste.add(drawLine (Color.blue,g, invObjToGive.getSelectedState().image3.getBounds(), ?????.getBounds(),a,true));
					return liste;
				}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
				 
		 
		return liste;	
	}
	//####################################################################################  
	private List<Zone> stateToSet(Ashe theAsh, Action a, Graphics g, Color color ) {
		List<Zone> liste = new ArrayList<>();
		 
		ObjectState stateItWillBe = (ObjectState) theAsh.possessor ();  
		if (stateItWillBe != null) { 
			if(stateItWillBe.image3 == null)
				try {
					liste.add(drawLine (color,g, stateItWillBe.image1.getBounds(),color==Color.red?a.panelToRem.getBounds():a.panelToSet.getBounds(),  a,false,false)); //stateItWillBe.image3->action=green line
				} catch (Exception e) {
					Utils.log(stateItWillBe.title()+" is not a ground object, maybe HIDDEN or inventory");
					 
				}
			else
				liste.add(drawLine (color,g, stateItWillBe.image3.getBounds(),color==Color.red?a.panelToRem.getBounds():a.panelToSet.getBounds(),  a,false,false)); //stateItWillBe.image3->action=green line
		for(int id :a.detectors()) 
				if(GameObject.getById(id) instanceof ObjectState) { 
					ObjectState stateActingTheChange = (ObjectState) ObjectState.get (id); 
					liste.add(drawLine (Color.blue,g, stateActingTheChange.image1.getBounds(), a.buttonNodes.getBounds(),a,false,true));
				}
				else if(GameObject.getById(id) instanceof Zone) { 
					liste.add(drawLine (Color.blue,g, ((Zone)GameObject.getById(id)).toRect(), a.buttonNodes.getBounds(),a,false,true));
				}
			 
		  
		}else Utils.log("found a null state to set in object node utilities ! : "+theAsh.title()+" "+a.toString());
		return liste;
	}
	//####################################################################################  
	private Zone drawLine (Color c, Graphics g, Rectangle startBounds, Rectangle endBounds,Action a,boolean dashedStroke,boolean zoome) {
		int startX = (int) ((int) startBounds.getCenterX() );if(zoome)startX*=zoom;
		int startY = (int) ((int) startBounds.getCenterY() );if(zoome)startY*=zoom;
		int endX = (int) ((int) endBounds.getCenterX());
		int endY = (int) ((int) endBounds.getCenterY());
		Zone z = new Zone(startX, startY, endX, endY,ZoneType.LINE);
		    
		if(currentAction==null || currentAction.equals(a)
				//||actualStateClicked!=null&&ObjectState.getById(a.stateToSet()).equals(actualStateClicked)
				||actualStateClicked!=null&&a.ashNeeded().contains(new Ashe(actualStateClicked.id,AshType.WILL_BE_IN_STATE,1))
				||actualStateClicked!=null&&a.ashRefused().contains(new Ashe(actualStateClicked.id,AshType.WILL_BE_IN_STATE,1))
				||actualStateClicked!=null&&a.ashToSet().contains(new Ashe(actualStateClicked.id,AshType.WILL_BE_IN_STATE,1))
				) {
	   
	   if(!dashedStroke)
		   z.draw(false, g,c);
		
	   else { 
			Graphics2D g2d = (Graphics2D) g;
			g2d.setColor(c);
			 Stroke oldStroke = g2d.getStroke();
			 float[] dashPattern = {5, 5}; // 5 pixels de trac�, 5 pixels d'espacement
			 BasicStroke bs = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dashPattern, 0.0f);
	        g2d.setStroke(bs); 
	        g2d.drawLine(startX, startY, endX, endY);
	        g2d.setStroke(oldStroke);
	   } 
	   return new Zone((int)(startX),(int) (startY),(int)(endX),(int) (endY),ZoneType.LINE);}
		return Zone.DEFAULT_INSTANCE;
	}
	
 
	//#################################################################################### 
	//#################################################################################### 
	public void mouseReleased(Zone modifiedZone, int actualX, int actualY) {
		
		if(caseToMove!=null)
    		Editor.recordObjectStatePlacement(caseToMove.id, (int) (caseToMove.getLocation().x/zoom), (int) (caseToMove.getLocation().y/zoom));
		caseToMove = null;
		
		if(actionToMove!=null)
    		Editor.recordActionStatePlacement(actionToMove.id, (int) (actionToMove.getLocation().x/zoom), (int) (actionToMove.getLocation().y/zoom));
		actionToMove = null;
		
		
		if(allObjectStateCases != null) {
			for(ObjectState a : allObjectStateCases)
				if (a.getBounds().contains(new java.awt.Point(actualX,actualY))) {
					if (a.image2==null) { 
						if (a.image1.getBounds().contains(new java.awt.Point(actualX,actualY))) 
							actualStateClicked=newStateClicked = a; 
						 
					}
					else if (a.image1.getBounds().contains(new java.awt.Point(actualX,actualY)))  
						oldStateClicked = a;
					else if (a.image3.getBounds().contains(new java.awt.Point(actualX,actualY)))  
						newStateClicked = a; 

				}

			for (BasicObject object : objectNode.objectBoxx.values()) 
				for (Zone zone : Zone.getByOwner(object.getSelectedState().id)) 
					if (zone.isNear(new Point((int) (actualX/zoom),(int) (actualY/zoom)), 10)) 
						zoneClicked=zone; 
						 
		}
		 
		
		if(newStateClicked != null && oldStateClicked != null && zoneClicked == null) {
			String[] z = null;
			try {
			/*	for(ActionBasicObject a : ActionBasicObject.getByObject(newStateClicked.possessor()))
					if(a.stateToSet() == newStateClicked.id)
						z = a.line.getArray("zone"); 
				for(ActionBasicObject a : ActionBasicObject.getByObject(oldStateClicked.possessor()))
					if(a.stateToSet() == oldStateClicked.id)
						z = a.line.getArray("zone"); */
			} catch (Exception e) {Utils.log("no zone found in objectnode generating action");}
			//if(z!=null) {
			//newStateClicked.createStateAction(BasicObject.get(newStateClicked.possessor()),null,z,oldStateClicked,null);
			objectNode.populate();
			//}
		}
		if(oldStateClicked != null && zoneClicked != null) {
			//oldStateClicked.createStateAction(BasicObject.get(oldStateClicked.possessor()), zoneClicked,false);
			objectNode.populate();
		} 	
		
		if(newStateClicked != null && zoneClicked != null) {
			//newStateClicked.createStateAction(BasicObject.get(newStateClicked.possessor()), zoneClicked,true);
			objectNode.populate();
		}
		
		
		if(this.actionToRem != null && newStateClicked != null) { 
			actionToRem.addAshToSet(askForAsh(newStateClicked,true));objectNode.populate();}
		if(this.actionToSet != null && newStateClicked != null) {
			actionToSet.addAshToSet(askForAsh(newStateClicked,false));objectNode.populate();}
		
		if(this.actionRefuse != null && newStateClicked != null) {
			actionRefuse.addAshRefused(askForAsh(newStateClicked,false));objectNode.populate();}
		if(this.actionNeed != null && newStateClicked != null) {
			actionNeed.addAshNeeded(askForAsh(newStateClicked,false));objectNode.populate();}
		
		
		
		
		
		
		
		
		actionRefuse=null;
		actionNeed=null;
		actionToSet=null;
		actionToRem=null;
		
		
		newStateClicked = null;
		oldStateClicked=null;
		zoneClicked = null; 

	}
	private Ashe askForAsh(ObjectState stateClicked, boolean toRemove) {
        BasicObject owner = BasicObject.get(stateClicked.possessor());
        boolean isGround = !(owner instanceof InventoryObject);

        Object[] options;

        if (isGround) {
            options = new Object[]{
                    AshType.HAS_BEEN_COLLIDED,
                    AshType.WILL_BE_DISABLED ,
                    AshType.WILL_BE_IN_STATE,
                    AshType.SCENE_HAS_BEEN_VISITED,
                    AshType.IS_CLICKED_ON,
                    AshType.ACTUAL_POSITION 
            };
        } else {
            options = new Object[]{  
                    AshType.WILL_BE_IN_STATE, 
                    AshType.IS_CLICKED_ON,
                    AshType.HAS_IN_HAND,
                    AshType.HAS_IN_INVENTORY
            };
        }
        	
        AshType selectedAshType = (AshType) JOptionPane.showInputDialog(null, "Choose AshType:", "Select", JOptionPane.PLAIN_MESSAGE, null, options, null);
        

        if (selectedAshType != null) {
        	if(selectedAshType ==  AshType.IS_CLICKED_ON
                    ||selectedAshType == AshType.HAS_IN_HAND
                    ||selectedAshType == AshType.HAS_IN_INVENTORY)
        		return new Ashe(owner.id,selectedAshType,toRemove?-1:1); 
        	return new Ashe(stateClicked.id,selectedAshType,toRemove?-1:1);
            
        }

        return null;
    }
	//#################################################################################### 
	//#################################################################################### 
	public void mouseMoved(int actualX, int actualY) {
		if ( caseToMove != null) { 
            int newX = (int) ((actualX - caseToMove.getWidth() / 2));
            int newY = (int) ((actualY - caseToMove.getHeight() / 2));
            caseToMove.setLocation(newX, newY);
              
        } 
		if ( actionToMove != null) { 
            int newX = (int) ((actualX - actionToMove.getWidth() / 2));
            int newY = (int) ((actualY - actionToMove.getHeight() / 2));
            actionToMove.setLocation(newX, newY);
              
        } 
	}
	//#################################################################################### 
	//#################################################################################### 
	public Zone mouseClic(Point p) { 
		if(allObjectStateCases == null)return null;
		for(ObjectState a : allObjectStateCases)
    		if (a.getBounds().contains(new java.awt.Point(p.x,p.y))) { 
    			if (a.image2==null) { 
    				if (a.image1.getBounds().contains(new java.awt.Point(p.x,p.y))) 
    					actualStateClicked=newStateClicked = a; 
    				else
    					actualStateClicked=caseToMove = a; 
    			}
    			else if (a.image2.getBounds().contains(new java.awt.Point(p.x,p.y)))  
    				actualStateClicked=caseToMove = a; 
    			else if (a.image1.getBounds().contains(new java.awt.Point(p.x,p.y)))  
    				actualStateClicked=oldStateClicked = a;
    			else if (a.image3.getBounds().contains(new java.awt.Point(p.x,p.y)))  
    				actualStateClicked=newStateClicked = a; 
    			 
    			return Zone.DEFAULT_INSTANCE;
    		}
		
		for(Action a : allActionCases)
    		if (a.getBounds().contains(new java.awt.Point(p.x,p.y))) {  
    			if(a.panelToSet.getBounds().contains(new java.awt.Point(p.x,p.y)))
    				actionToSet = a;
    			else if(a.panelToRem.getBounds().contains(new java.awt.Point(p.x,p.y)))
    				actionToRem = a;
    			else if(a.panelNeededs.getBounds().contains(new java.awt.Point(p.x,p.y)))
    				actionNeed = a;
    			else if(a.panelrefuseds.getBounds().contains(new java.awt.Point(p.x,p.x)))
    				actionRefuse = a;
    			else
    				actionToMove = a; 
    				
    					 
    			return Zone.DEFAULT_INSTANCE;
    		}
		
		
		for (BasicObject object : objectNode.objectBoxx.values()) {  
        	for (Zone zone : Zone.getByOwner(object.getSelectedState().id)) {
        		if (zone.isNear(p, 10)) {
        			zoneClicked=zone; 
        			return Zone.DEFAULT_INSTANCE; } }}
		
		return null;
	}
}
