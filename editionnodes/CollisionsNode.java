package me.nina.editionnodes;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.swing.JCheckBox;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import me.nina.editors.Editor;
import me.nina.editors.SceneEditor.EditType;
import me.nina.gameobjects.Action;
import me.nina.gameobjects.Scene;
import me.nina.gameobjects.TransitionPoint;
import me.nina.gameobjects.Zone; 
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Utils.FloatingMenu;
import me.nina.maker.Cbox;
import me.nina.maker.Main;
import me.nina.maker.RosellaZone;
import me.nina.maker.Utils;
import me.nina.maker.Utils;
import me.nina.objects2d.Point;
import me.nina.sqlmanipulation.SQLLine;

public class CollisionsNode extends EditNode<Zone> {
  
	public Cbox<Zone> liste;   
	private boolean actuallyModifyingZone; 
	Scene scene;  
 
	public CollisionsNode( Editor e ) { super(EditType.COLLISIONS,e );  } 
	
	//############################################ 
	@Override public void populate() { 
		scene = editor.scene; 
		liste = but.createCbox(Zone.getByOwner(scene.id),but,150,20,(ActionListener) e->{ 
			editor.repaint(); }); 
		
		 
		    
		
		 
	}
	  
	//############################################ 
	@Override void paintComponents(Graphics g ) { 
		for(int i = 0 ; i < liste.getItemCount() ; i++) {
			Zone item = liste.getItemAt(i);  
			new Zone(item.mid().x-10,item.mid().y-10,item.mid().x+10,item.mid().y+10,ZoneType.RECTANGLE).draw(g);
			 if(item.getType() == ZoneType.ARC)
				 new Zone(item.getP3().x-5,item.getP3().y-5,item.getP3().x+5,item.getP3().y+5,ZoneType.RECTANGLE).draw(true, g,Color.GREEN);
			if( !actuallyModifyingZone||(actuallyModifyingZone &&  liste.getSelectedIndex()!=i )) 
				item .draw(false, g,Color.GRAY);
		}
		
		  
	}
	

	//############################################ 
	@Override public Zone mouseClic(int x, int y) {
		Point p = new Point(x,y);
		Zone found = Utils.getZoneFromPoint(p, Zone.getByOwner(scene.id), 10); 
		 
		if(found != null) {
			liste.setSelectedItem(found) ;
			actuallyModifyingZone = true;  
			return found;
			  
		} 
		for(Zone a : Zone.getByOwner(scene.id))
			if(Utils.getDistanceBetweenPoints(a.getP3(), new Point(x,y) )<10) {
				liste.setSelectedItem(a) ;
				actuallyModifyingZone = true;
				return a;
			}
		
		return null;
	}
 
	//############################################
	@Override void mouseReleased(Zone modifiedZone, int actualX, int actualY ) {
		 
		if(!actuallyModifyingZone) 
			makeNewZone(modifiedZone);
		else  { 
			liste.getSelectedItem().update(modifiedZone.toSqlValues());
			
				actuallyModifyingZone = false;
				repopulate();
				for(int i = 0 ; i < liste.getItemCount() ; i++) {
					Zone item = liste.getItemAt(i); 
					if(item.equals(modifiedZone))
						liste.setSelectedIndex(i);
			 }
		}
		 
		 
	}
 
	//############################################
	private void makeNewZone(Zone z) { 
		if(z.getType() == ZoneType.RECTANGLE || z.getType() == ZoneType.UNFILLED_RECT)  
			z.setType(ZoneType.UNFILLED_RECT) ; 
		else if(z.getType() == ZoneType.OVALE || z.getType() == ZoneType.UNFILLED_OVALE) 
			z.setType(ZoneType.UNFILLED_OVALE) ;
		  
		z.save(scene.id,""+SQLLine.getUniqueID());  
		repopulate();
	}
	
	@Override void mouseMoved(int clickedX, int clickedY, int actualX, int actualY ) { }
	@Override protected void handleWheel(int wheelRotation, int i, int j) { }
	@Override public boolean handleImgDraggd() { return true; }
	@Override protected void clickRight(int x, int y) { 
		
		
		JPopupMenu popupMenu = new JPopupMenu();
		 

		for(int i = 0 ; i < liste.getItemCount() ; i++) {
			Zone item = liste.getItemAt(i);   
				   if(item.isNear(new Point(x,y), 10)){
					   liste.setSelectedItem(item);
					   lab.createJMenu(popupMenu,item.title(),e -> {   ;    }); 
					   
					   
					   
					   List<Action> actionList = Action.getByDetector(item);
					   for(Action a : actionList) {
						   FloatingMenu popup = new Utils.FloatingMenu(a.getHtml());  
						  JMenuItem iteme = lab.createJMenu(popupMenu,"edit action "+a.title(),e -> { popup.hideFloatingContent();  new Action(a,false).populateRight();    }); 
						  iteme.addMouseListener(popup);
					   }
					    
					   lab.createJMenu(popupMenu,"change name",e -> {  
						   item.updateTitle(Main.getUserInput());    }); 
					   lab.createJMenu(popupMenu,"delete zone",e -> {  
						   item.delete();
						   liste.removeItem(item);  
							editor.lastZone = null;
							repopulate();  }); 
					   
					   lab.createJMenu(popupMenu,"create action",e -> {  
						   Action a = new Action(item.id );
						   a=a.save();new Action(a,false).populateRight();}); 
					   if(Editor.hasInitialAsh(item.id,AshType.WILL_BE_DISABLED)  )
						   lab.createJMenu(popupMenu,"enable at start",e -> {  
								Editor.remInitialAsh(item.id,AshType.WILL_BE_DISABLED);
	    }); 
					   else lab.createJMenu(popupMenu,"disable at start",e -> {  
						   Editor.setInitialAshes(item.id,AshType.WILL_BE_DISABLED);
    }); 
					   
					   
					   
					   
					   
					   popupMenu.show(Main.actualEditor, x, y);
					   
					   
				   }
					   
		}
		
	}
	@Override protected void clickCenter(int x, int y) { }
	@Override protected void pressed(int keyCode) { }
	boolean isEnabledFor(Object obj) {return obj instanceof Scene;}  
	
	
}
