package me.nina.editionnodes;

import java.awt.Color;
import java.awt.Graphics;

import me.nina.editors.Editor;
import me.nina.editors.SceneEditor.EditType;
import me.nina.gameobjects.Scene;
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Cbox;
import me.nina.maker.Main;
import me.nina.maker.Pane;
import me.nina.maker.RosellaZone;
import me.nina.maker.Utils;
import me.nina.maker.Utils;
import me.nina.objects2d.Img;
import me.nina.objects2d.Point;

public class ScaleFactor extends EditNode{
	 
	public int percentFactor;
	Zone topLineZone,botLineZone; 
	private Zone actuallyModifyingZone; 
	
	public ScaleFactor(Editor e ) {
		super(EditType.SCALEFACTOR,e ); 
	}

	@Override public void populate() { 
		lab.createLabel("black bar =",lab,150,20);
		but.createLabel("no reduce after",but,150,20);
		lab.createLabel("blue bar =",lab,150,20);
		but.createLabel("start to reduce",but,150,20); 
		lab.createLabel("move rosella",lab,150,20);
		but.createLabel("for test algo",but,150,20);
		lab.createLabel("wheel on rosella",lab,150,20);
		but.createLabel("for change scale",but,150,20);
		
		percentFactor = editor.scene.scalePercents();
		int botline = editor.scene.scalefactorBotLimit();
		int topline = editor.scene.scalefactorTopLimit();
		if(topline == -1)topline = 100;
		if(botline == -1)botline = 300;
		if(percentFactor == -1)percentFactor = 30;
		topLineZone = new Zone(0,topline-5,editor.image.getWidth(),topline+5, ZoneType.RECTANGLE);
		botLineZone = new Zone(0,botline-5,editor.image.getWidth(),botline+5, ZoneType.RECTANGLE);
		topLineZone.setUnmodifiable();
		botLineZone.setUnmodifiable();
		topLineZone.setUnmodifiableX();
		botLineZone.setUnmodifiableX();
	}
	//############################################
	boolean isRunningThis(Editor editor) { return Main.actualNode.type == type;}
	boolean isEnabledFor(Object obj) {return obj instanceof Scene;}
 
 
	//############################################
	@Override void paintComponents(Graphics g ) { 
		topLineZone.draw(true, g,Color.BLACK); 
		g.setColor(Color.BLUE);
		botLineZone.draw(true, g,Color.BLUE); 
		editor.rosella.paint(g,editor.getActualX(),editor.getActualY(),null);
	}
	//############################################
	@Override public Zone mouseClic(int x, int y) {
		if(topLineZone.isNear(new Point(x,y), 10)){ 
			actuallyModifyingZone = topLineZone;  
	 		 
	 		return Zone.DEFAULT_INSTANCE;
		}
		if(botLineZone.isNear(new Point(x,y), 10)){ 
			actuallyModifyingZone = botLineZone;  
	 		 
	 		return Zone.DEFAULT_INSTANCE;
		}
		 
		return null;
	}

	//############################################
	@Override void mouseReleased(Zone modifiedZone, int actualX, int actualY ) {
		if(actuallyModifyingZone==null)return;
		if(actuallyModifyingZone.equals(botLineZone))
		editor.scene.updateScaleFactors(topLineZone.getP1().y, actualY, percentFactor);
		else
			editor.scene.updateScaleFactors(actualY, botLineZone.getP1().y, percentFactor);

		actuallyModifyingZone=null;
		 
		editor.lastZone=null;
		populate();
		editor.repaint();
	}

	//############################################
	@Override void mouseMoved(int clickedX, int clickedY, int actualX, int actualY ) {
 
		 if(actuallyModifyingZone==null)return;
		 if(botLineZone.y()<0)
			 botLineZone = new Zone(0,300,editor.image.getWidth(),305, ZoneType.RECTANGLE);
		if(actuallyModifyingZone.equals(botLineZone) && topLineZone.isNear(new Point(actualX,actualY),10)) {
			botLineZone=topLineZone.yp(20);
			if(botLineZone.getP1().y> editor.scene.bgImage().getHeight())
				botLineZone.setP1(0,editor.scene.bgImage().getHeight()-10);
			
		}
		if(actuallyModifyingZone.equals(topLineZone) && botLineZone.isNear(new Point(actualX,actualY),10)) {
			topLineZone=botLineZone.yp(-20);
			if(topLineZone.getP1().y<0)
				topLineZone.setP1(0,10);
			
		}
		
	}

	//############################################
	@Override protected void handleWheel(int wheelRotation,int x, int y) { 
		 
		 int old = percentFactor;
        Main.videoScroll.getVerticalScrollBar().setValue(0);
		percentFactor += 5*((wheelRotation > 0) ? -1 : 1);
		if(percentFactor>100)percentFactor = 100;
		if(percentFactor<10)percentFactor = 10;
		if(old != percentFactor) {
		editor.scene.updateScaleFactors(topLineZone.getP1().y, botLineZone.getP1().y, percentFactor);
		 
		}

	  
	}


	@Override
	public boolean handleImgDraggd() {
		return false;
	}


	@Override
	protected void clickRight(int x, int y) {
		// TODO Auto-generated method stub
		
	}


	@Override
	protected void clickCenter(int x, int y) {
		// TODO Auto-generated method stub
		
	}


	@Override
	protected void pressed(int keyCode) {
		// TODO Auto-generated method stub
		
	}



 
 

}
