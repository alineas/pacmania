package agame;
 
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import me.nina.gameobjects.Stair;
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Utils;
import me.nina.objects2d.Point;

public class GameCharacterStairManager {  
	boolean isColimacon;
	private Stair actualMountain,prioritizedMountain,secondaryMountain;
	Zone visionLine;
	private int actualStairIam =-5; 
	boolean forcingToGoDown;
	private MainGame mainGame;
	List<Zone> stairList;
	//private Zone zoneWhoIntersected; //for debug
	private Point intersectPointOfSecondary, intersectPointOfPrimary;
	int totalDistanceY = 1;
	int totalDistanceYwalked = 1;
	private GameCharacter character;
	Pathfinding pathfinding;
	//##################################################################
 
	
	public GameCharacterStairManager( MainGame mainGame,GameCharacter c) {  this.mainGame = mainGame; character = c;this.pathfinding = new Pathfinding( ); }

	public boolean isBusy() {  return actualMountain != null; }
	int posX() {return character.feetX();}
	int posY() {return character.feetY();} 
	int clicX() {return character.getClicPose().x;}
	int clicY() {return character.getClicPose().y;} 
	private boolean walkingBot() { return character.getClicPose().y > posY(); }  
 
	//################################################################## 
    private Stair findMountain(boolean secondary ) {Stair retur = null; 
	int lvl = walkingBot() ? -1 : Integer.MAX_VALUE;  
	for (Zone s : stairList) {
	    Stair stair = (Stair) s; 
	    if(walkingBot() && stair.level ==actualStairIam)continue; 
	    if(!walkingBot() && stair.level ==actualStairIam-1)continue; 
	    for (Zone z : stair.primaryZones) { 
	    	Point pt = visionLine.getIntersectPoint(z);
	        if (pt != null) {
	            if ((walkingBot() && stair.level > lvl &&(actualStairIam==-5|| stair.level<actualStairIam)) || (!walkingBot() && stair.level < lvl)) {
	               if(!secondary) { lvl = stair.level; 
	               // zoneWhoIntersected = z; //for debug
	                retur= stair;
	                intersectPointOfPrimary = pt;}else {
	                	if(stair.level != prioritizedMountain.level) {
	    	                lvl = stair.level;  
	    	                retur= stair;
	    	                intersectPointOfSecondary = pt;
	                }
	            } } } }}
	return retur; }

  //##################################################################
	public void click() {
		actualMountain = null; //reset actual job finished
		if(!hasStairs())return; 
		if(((Stair)stairList.iterator().next()).primaryZones.get(0).getType() == ZoneType.LINE)
			isColimacon = true;
		if(isColimacon){
			clickColimacon();
			return;
		}
		
		
		try {
			visionLine = Utils.getProlongedLine(clicX(),clicY(),posX(),posY());
		} catch (Exception e) {
			Utils.log("error in "+character);
		}
		Point playerPosition = new Point (posX(),posY());
		prioritizedMountain = findMountain(false);
		secondaryMountain = findMountain( true); 
		if(secondaryMountain!=null){
		double distanceOfSecondary = Utils.getDistanceBetweenPoints(playerPosition, intersectPointOfSecondary);
		double distanceOfPrimary = Utils.getDistanceBetweenPoints(playerPosition, intersectPointOfPrimary);
		 
		if(distanceOfSecondary > distanceOfPrimary)secondaryMountain=null; //used only if hidden
		}
	}
	


	public boolean hasStairs() {
		if(stairList==null)stairList = Stair.getByOwner(character.actualScene.id);
		
		if(stairList.size()==0)
			return false;
		return true;
	}

	//##################################################################
	protected void run(  ) { 
		if(!hasStairs())return; 
		if(isColimacon){
			runColimacon();
			return;
		}
		
		
		
	    Zone actualPlayerFeetPosition = new Zone(posX()-1, posY(), posX() + 1, posY(), ZoneType.LINE);
	    
	 
	
	    
	    
	    if (prioritizedMountain !=null   ) {  
	    	
	        for (Zone prioZone : prioritizedMountain.primaryZones) { 
	        	if (actualPlayerFeetPosition.getIntersectPoint(prioZone) == null) continue;
	        	 
	          	if(!isBusy() )  
	          		if( actualStairIam == -5  || //if actualstair not defined
	          			(!walkingBot() && actualStairIam < prioritizedMountain.level +1 ) ||  
	          			(walkingBot() && actualStairIam > prioritizedMountain.level )) { 
	          			 	totalDistanceY = calculateDistanceToWalk();
	          			 	totalDistanceYwalked = 1;
	          			 	if(!walkingBot())totalDistanceYwalked=totalDistanceY;
	          				actualMountain = prioritizedMountain;
	          				forcingToGoDown = true;  
	          				return;  
	          		}
	          	
	            if(isBusy() && !forcingToGoDown ) { //finished to go up
	            	 
	            	if (!walkingBot()) actualStairIam = prioritizedMountain.level + 1; 
	        		else actualStairIam = prioritizedMountain.level;  
	        		 
	            	if(secondaryMountain !=null) { // finished run do hidden stair if needed
	            		
	            		actualMountain=secondaryMountain; 
	            		if(intersectPointOfSecondary.y> posY())
            				forcingToGoDown=true; 
	            		if(walkingBot())actualStairIam--;else actualStairIam++;
	            		prioritizedMountain = secondaryMountain; 
	            		secondaryMountain=null; 
	            		return;		 
	            	}
	            	click();//reset vars
	            	return;
	            }
	        }
	        
	        for (Zone secondaryZone : prioritizedMountain.secondaryZones) //if we reached secondary line, return to primary for finish the job
	            if (isBusy() && secondaryZone.getIntersectPoint(actualPlayerFeetPosition) != null && forcingToGoDown) {
	            	forcingToGoDown = false;   
	 	            return;
	            }
	    }
	    if (isBusy()) {
			if (forcingToGoDown) {
				totalDistanceYwalked++;  character.setPos(new Point(character.feetPosition.x,character.feetPosition.y+1));   } 
				else { 
			totalDistanceYwalked--; character.setPos(new Point(character.feetPosition.x,character.feetPosition.y-1));    }
		if( posX()<10||posY()<10
         	    		||posX()>mainGame.backgroundImage.getWidth()-10
         	    		||posY()>mainGame.backgroundImage.getHeight()-10) {
         	    	prioritizedMountain = null;actualMountain = null;forcingToGoDown=false;
         	    }
		}
 
	}

	private int calculateDistanceToWalk() {
		visionLine = Utils.getProlongedLine(clicX(),clicY(),posX(),posY());
		Point pt = null;
		Point pt2 = null;
		for(Zone z : prioritizedMountain.primaryZones) {
			pt = visionLine.getIntersectPoint(z);
		}
		for(Zone z : prioritizedMountain.secondaryZones) {
			pt2 = visionLine.getIntersectPoint(z);
		}if(pt==null||pt2==null)return 1;
		return Math.abs(pt.y-pt2.y)*2;
	}

	//##################################################################
	public List<Integer> getHiddenRgb() { //use the right priorities for generateMovingImage in character class
		List<Integer> temp = new ArrayList<>(); 
		if(isColimacon)
			for (Zone a : stairList) {
				Stair stair = (Stair)a;
				if(stair.level<= actualStairIam)
					temp.addAll(stair.priorities);
			}
		else{if(hasStairs())
			for (Zone a : stairList) {
				Stair stair = (Stair)a;
				if(walkingBot()&&stair.level< actualStairIam
					||!walkingBot()&&stair.level<= actualStairIam&&isBusy()
					||!walkingBot()&&stair.level<  actualStairIam )//hidden mountain finished 
						temp.addAll(stair.priorities);
			}
			if(actualMountain!=null )temp.addAll(actualMountain.priorities);//first mountain
		}
		
		return temp;
	}
	
	//##################################################################
	//##################################################################
	//##################################################################
	private void clickColimacon() { }
	
	Stair primaryColimacon,secondaryColimacon;
	private Zone feets;  
	List<Integer>last = new ArrayList<>();
	
	//##################################################################
	private void runColimacon() {  
		if(!hasStairs())return; 
		Stair detect1=null,detect2=null; feets = new Zone(posX()-5, posY()-5, posX() + 5, posY()+5, ZoneType.RECTANGLE);
		primaryColimacon=secondaryColimacon=null;
		//clear all if i have a detection and record it
		for (Zone s : stairList) { Stair stair = (Stair) s; 
			for (Zone z : stair.primaryZones) if (feets.isCollided(z)  )   	
				 detect1=stair ;  
			for (Zone z : stair.secondaryZones) if (feets.isCollided(z)  )   	 
				 detect2=stair;  
		}
		if( detect1==null&&detect2==null)last.clear();
		if( detect1!=null && !last.contains(detect1.id )|| detect2!=null&&!last.contains(detect2.id )){
			last.clear();
			if( detect1!=null){last.add(detect1.id);Utils.log("det1 "); }
			if( detect2!=null){last.add(detect2.id);Utils.log("det2 ");}
			primaryColimacon=detect1;
			secondaryColimacon = detect2;
		}
		 
		
		//one zone detected : take the level
		if( primaryColimacon!=null && secondaryColimacon==null 
				|| primaryColimacon==null && secondaryColimacon!=null ) {
			if(primaryColimacon!=null ){
				if(actualStairIam == primaryColimacon.level)
					actualStairIam = -5;
				else
					actualStairIam = primaryColimacon.level ;
			}
			if(secondaryColimacon!=null) actualStairIam = secondaryColimacon.level ;
			Utils.log("im on stair "+actualStairIam); 
		}
		
		//two detections
		if(primaryColimacon!=null && secondaryColimacon!=null) { 
			if(primaryColimacon.level  == actualStairIam )
				actualStairIam = secondaryColimacon.level ;
			else
				actualStairIam = primaryColimacon.level ;
			Utils.log("______im on stair "+actualStairIam);
			 

		}
  
	}

	 

	
}
	