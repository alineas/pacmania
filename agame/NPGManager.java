package agame;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
 
import me.nina.gameobjects.Action;
import me.nina.gameobjects.Animation;
import me.nina.gameobjects.GameObject;
import me.nina.maker.Ashe;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Utils;
import me.nina.objects2d.Point; 
public class NPGManager  {
	 
	private MainGame mainGame;
	public final List<GameCharacter> allThePnj = new CopyOnWriteArrayList<GameCharacter>();
	final List<Animation> objWithOwnZindex = new ArrayList<Animation>();
  
	//#################################################### 
	public NPGManager(MainGame m) { this.mainGame = m ; }

	//#################################################### 
	public void paintImage(Graphics g) { //sort characters/objects by zindex and paint em
	    List<Object> drawables = new ArrayList<Object>( );
 for(GameCharacter pnj : allThePnj)
	 if(pnj.feetPosition!=null && pnj.hasRun) 
		 drawables.add(pnj);
		   
 if(mainGame.player!=null&&mainGame.player.feetPosition!=null)drawables.add(mainGame.player);
 
	    drawables.addAll(objWithOwnZindex);
 
	    for (int i = 0; i < drawables.size() - 1; i++) {
	        for (int j = 0; j < drawables.size() - i - 1; j++) {
	            
					Object drawable1 = drawables.get(j);
					Object drawable2 = drawables.get(j + 1);
					int y1 = 0, y2 = 0;
					if (drawable1 instanceof GameCharacter) {
					    y1 = ((GameCharacter) drawable1).feetY();
					} else if (drawable1 instanceof Animation) {
						try {y1 = ((Animation) drawable1).getY() + ((Animation) drawable1).getHeight();} catch (Exception e) { Utils.log(((Animation) drawable1).title()); }
					}
					if (drawable2 instanceof GameCharacter) {
					    y2 = ((GameCharacter) drawable2).feetY();
					} else if (drawable2 instanceof Animation) {
						try { y2 = ((Animation) drawable2).getY() + ((Animation) drawable2).getHeight();} catch (Exception e) {Utils.log(((Animation) drawable2).title());   }
					}
					if (y1 > y2) { 
					    drawables.set(j, drawable2);
					    drawables.set(j + 1, drawable1);
					}
				
	        }
	    }
 
	    for (Object drawable : drawables) {
	        if (drawable instanceof GameCharacter) {
	            GameCharacter character = (GameCharacter) drawable; 
	            if (character.actualScene.equals(mainGame.actualSceneOnScreen )) {
	                character.paintImage(g); 
	            }
	        } else if (drawable instanceof Animation) {
	            ((Animation) drawable).paintIt(g);
	        }
	    }
	    objWithOwnZindex.clear();
	}
  
	//#################################################### 
	public void run() { for(GameCharacter pnj : allThePnj)  pnj.run(); }
 
	//#################################################### 
	public GameCharacter get(int id) {
		if(id == mainGame.player.line.id)return mainGame.player;
		for (GameCharacter a : allThePnj)
			if(a.line.id == id)
				return a;  
		return null;
	}
 
	public GameCharacter getCharacter(GameObject p) { return get( p.line.id); }

	//#################################################### 
	public GameCharacter getCharacter(Point p ) {
		for(GameCharacter pnj : allThePnj)
			if(pnj.getZone().isNear(p,  10) && pnj.actualScene.equals(MainGame.instance.actualSceneOnScreen))
					return pnj;
		if(mainGame.player.getZone().isNear(p,  10))
			return mainGame.player;
		return null;
	}
	//#################################################### 
	public boolean havePnj(int id) {
		for(GameCharacter a:allThePnj)
			if(a.line.id == id)
				return true;
		return false;
	}
		//#################################################### 
	public void spawnPnj(Ashe ash ) { 
		GameCharacter pnj = new GameCharacter(mainGame, ash.possessor().line );
		 if(mainGame.player==null) mainGame.player=pnj;
		 else allThePnj.add(pnj);
		Utils.log("spawn pnj "+pnj.title());  
		MainGame .instance.addAsh(new Ashe(pnj.getInitialState().line.id,AshType.IS_IN_STATE,1)); 
	}
	//#################################################### 
	public void despawnPnj(Ashe dest) {
		allThePnj.remove(dest.possessor());
		if(dest.possessor().equals(mainGame.player))
			mainGame.player=null;
	}
	//#################################################### 
	public boolean clickHasActedOnCharacter(Point p,GameCharacter clickOwner) {
		GameCharacter character = getCharacter(p);
		if(character != null ) {
			if (executeClicOnCharacter(clickOwner,character))
			return true;
		} 
		//clickOwner.characterToRejoin=null;
		return false;
	}
	//#################################################### 
		public boolean executeClicOnCharacter(GameCharacter clickOwner,GameCharacter clickedChar) {
			 
			mainGame.addAsh(new Ashe(clickedChar.id,AshType.HAS_BEEN_CLICKED,1));
			boolean acted = mainGame.actionMgr.handleAction(Action.getByDetector(clickedChar),clickOwner);
			mainGame.removeAsh(new Ashe(clickedChar.id,AshType.HAS_BEEN_CLICKED,1));
	return acted;
			/*
			if(clickedChar.isNear(clickOwner.feetPosition, 100)) { 
				mainGame.addAsh(new Ashe(clickOwner,AshType.IS_ARRIVED,1));
				for(Action action : Action.getByDetector(clickedChar))
					mainGame.actionMgr.handleAction(action,clickOwner); 
				mainGame.removeAsh(new Ashe(clickOwner,AshType.IS_ARRIVED,1));

				clickOwner.characterToRejoin=null;
				return;
			}
			Utils.log("setclicpose");*/
			//clickOwner.setClicPose(clickedChar.feetPosition);
			//clickOwner.characterToRejoin = clickedChar;
			
		}
	//#################################################### 
	public void handleCharacterArrived(GameCharacter c) {
		/*List<GameCharacter>liste = new ArrayList<>();
		liste.add(mainGame.player);
		for(GameCharacter a:allThePnj)
			liste.add(a);
		
		for(GameCharacter a:liste)
			if(a.characterToRejoin!=null)
				executeClicOnCharacter(a,a.characterToRejoin);*/
		
	}



	
	
 
}
