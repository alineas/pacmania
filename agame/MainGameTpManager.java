package agame;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JLabel;

import me.nina.editors.Editor;
import me.nina.gameobjects.Action;
import me.nina.gameobjects.TransitionPoint;
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Character.PosType;
import me.nina.gameobjects.InventoryObject;
import me.nina.maker.Ashe;
import me.nina.maker.Main;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Utils;
import me.nina.objects2d.Img;
import me.nina.objects2d.ImgUtils;
import me.nina.objects2d.Point;
import me.nina.objects2d.Position;
import me.nina.gameobjects.Scene;

public class MainGameTpManager { 
	boolean cursorSet=false;
	private MainGame mainGame; 

	//##########################################################
	public MainGameTpManager(MainGame mainGame) {
		this.mainGame=mainGame;
	}

	//##########################################################
	public boolean tpHasBeenClicked(Point p,GameCharacter c) {
		for( Zone pt : TransitionPoint.getByOwner(mainGame.actualSceneOnScreen .line.id))  {
			TransitionPoint tPoint = (TransitionPoint)pt;
			 
			if(!mainGame.objMgr.isFullScreen() &&tPoint.isNear(p,  10)) {  
				c.setClicPose(tPoint.getDesiredPos()) ;
				c.tpClicked=tPoint; 
				Utils.log(c.title()+" tpClicked = "+tPoint.title()+" "+tPoint.getDesiredPos());
				
					return true;
				
			}
		}
		c.tpClicked=null;
		return false;
	}
	
	public void changeCursor(Point p){
	 	if(cursorSet)return;
		 new Timer() .schedule( new TimerTask() {
	            @Override
	            public void run() {
	            	cursorSet=false;mainGame.frame.setCursor( null );
	            }
	        }, 1000);
		boolean set = false;
		for( Zone pt : TransitionPoint.getByOwner(mainGame.actualSceneOnScreen .line.id))  {
			TransitionPoint tPoint = (TransitionPoint)pt;
			if( !mainGame.haveAsh(new Ashe( tPoint.line.id,AshType.HAS_BEEN_DISABLED,1 )))
			if(!mainGame.objMgr.isFullScreen() &&tPoint.isNear(p,  10)) { 
				set = true;
				PosType pp = PosType.calculatePosType(mainGame.player.feetX(),mainGame.player.feetY(), tPoint.getDesiredPos().x,tPoint.getDesiredPos().y);
				String fileName = Editor.getGameInfo() .getString("cursors",  "TELEPORT"+pp);
	              Image customimage = ImgUtils.getPicture("games/"+mainGame .gameName+"/"+fileName);
				Cursor customCursor = Toolkit.getDefaultToolkit().createCustomCursor(customimage, new java.awt.Point(0, 0), "customCursor");
				mainGame.frame.setCursor( customCursor );
			}
		}
		if(!set)
			mainGame.frame.setCursor( null ); 
}
	//##########################################################
	public void handleCharacterArrived(GameCharacter c) {
		if(c.tpClicked==null)  return; 
		
		for( Zone b: TransitionPoint.getByOwner(c.actualScene.line.id ))  
			if(b.equals(c.tpClicked)) {
				c.tpClicked=null;
				TransitionPoint tpoint = (TransitionPoint) b;
				
				if( !mainGame.haveAsh(new Ashe( tpoint.line.id,AshType.HAS_BEEN_DISABLED,1 )))
				handleTpClicked(tpoint,c);
				else {
					Utils.log("not handle TP "+tpoint.title()+" normal action cause ash disabled present");
					mainGame.actionMgr.handleAction( Action.getByDetector( tpoint ),c); 
				}
				
				
			}
	}
	
	//##########################################################
	private void handleTpClicked(TransitionPoint pointFrom,GameCharacter c) { 
		 
		Utils.log("_____________andl tp clic for "+c.title());
		Scene destScene = Scene.get(pointFrom.getDestinationScene());
		if(destScene == null){
			Utils.log("no destination for "+pointFrom.title());
			mainGame.actionMgr.handleAction( Action.getByDetector( pointFrom ),c); 
			return;
		}
		List<Zone> sceneTpLst = TransitionPoint.getByOwner(destScene.line.id); 
		TransitionPoint destTp=null; 
		for(Zone dest : sceneTpLst)
			if(((TransitionPoint)dest).getSourceId()==pointFrom.getDestinationId())
				destTp=(TransitionPoint) dest;
		
		mainGame.addAsh(new Ashe( pointFrom.getSourceScene() ,AshType.SCENE_WAS_EXITED,1));
		mainGame.actionMgr.handleAction( Action.getByDetector( pointFrom ),c); 
		mainGame.removeAsh(new Ashe( pointFrom.getSourceScene() ,AshType.SCENE_WAS_EXITED,1));
		
		
		handleTeleport(destScene,destTp.getDesiredPos(),c);
		
		mainGame.addAsh(new Ashe( pointFrom.getDestinationScene() ,AshType.SCENE_HAS_BEEN_ENTERED,1));
		mainGame.actionMgr.handleAction( Action.getByDetector( destTp ),c);
		mainGame.removeAsh(new Ashe( pointFrom.getDestinationScene() ,AshType.SCENE_HAS_BEEN_ENTERED,1));
	}
	//##########################################################
	public void handleTeleport(Scene destScene, Position position, GameCharacter dest) {
		if(dest.equals(mainGame.player) && !mainGame.actualSceneOnScreen .equals(destScene))
			mainGame.changeScene(destScene); 
		dest.actualScene=destScene;
		 
		Utils.log("tp handled for "+dest.title()+" scene="+destScene.title());
		dest.setPos(position  ); 
		dest.setPosType(position.pos) ;
		dest.setClicPose( position ); 
		dest.calculateScale(position.y);
		dest.generateMovingImage();
		//if(dest.equals(mainGame.player))
			//mainGame.centerScrollPaneOne(position.x,position.y); 
		
		
		int h =  (int) ((int) ((mainGame.backgroundImage.getHeight()/MainGame.zoom- MainGame.instance.menuBar.menuZone().h())));
		int w = (int) ((int) (mainGame.backgroundImage.getWidth()/MainGame.zoom/*-mainGame.menuBar.menuZone().w()*/) );//TODO handle different menu positions
		   
		dest.stairManager = new GameCharacterStairManager(mainGame,dest); 
		
		if( dest.feetY() > h) { Utils.log("tp case 1");
			dest.setClicPose(new Point (dest.feetX(),h ));
 		}
		else  if( dest.feetX() > w) { Utils.log("tp case 2");
		 	dest.setClicPose(new Point ( w-dest.getScalledW() ,dest.feetY() ));
 		}
		else  if( dest.blitY()<0) { Utils.log("tp case 3");
		  	dest.setClicPose(new Point (dest.feetX(), (int) (dest.getScalledH() /MainGame.zoom) ));
 		}
		else  if( dest.blitX()<0) { Utils.log("tp case 4"); 
		  	dest.setClicPose(new Point ( (int) (dest.getScalledW() /MainGame.zoom),dest.feetY() ));
 		}
		  
	}

	public boolean isBusy(){ return mainGame.player.tpClicked!=null; }
}
