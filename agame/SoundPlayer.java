package agame;

import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sound.midi.MetaEventListener;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Receiver;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.sound.midi.Synthesizer;
import javax.sound.midi.Transmitter;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
 
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.advanced.AdvancedPlayer;
import javazoom.jl.player.advanced.PlaybackEvent;
import javazoom.jl.player.advanced.PlaybackListener;
import me.nina.maker.Utils;
 

public class SoundPlayer  implements ChangeListener{  

	 private Sequencer sequencer;
	 int midiVolume=60;
	 
	 JPanel p;
	private JSlider midiSlider;
	private JSlider fxSlider;
	private float fxvolume;
	private Thread playbackThread;
	private Synthesizer synthesizer;
	private Receiver synthReceiver;
	private Transmitter seqTransmitter;
	private Sequence oldSequence;
	public String actualMidiPath="";
	private boolean loopMidi;

	
	
	 public JPanel getPanel(){
		 if(p==null){
			 p = new JPanel(new GridLayout(0,2));
			 Utils.setSize(p, 200, 60);
			 midiSlider = new JSlider(0,128,30);
			 midiSlider.addChangeListener(this); 
			 p.add(new JLabel("music"));p.add(midiSlider);
			 
			 fxSlider = new JSlider(-88,0,-10);
			 fxSlider.addChangeListener(this); 
			 p.add(new JLabel("effects"));p.add(fxSlider);
			 JButton b = new JButton();
			 b.addActionListener((ActionListener)e-> {MainGame.instance.restoreGame();});
			 
			 p.add(new JLabel("restore last"));p.add(b);  
		 }
		 return p;
	 }
	 public void stateChanged(ChangeEvent e)  { 
		 if(e.getSource().equals(midiSlider))midiVolume= midiSlider.getValue() ; 
		 else fxvolume = fxSlider.getValue() ; 
	 } 
	 
	 
	//####################################################################
	public void playMidi(String path,boolean replaceCurrent,boolean loop) {loadSequencerIfNeeded();
	loopMidi = loop;
   if( sequencer.isRunning() && !replaceCurrent){ 
		
		 return;
	 } sequencer.stop();
   if(!replaceCurrent) {
   	 try { InputStream str = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
		oldSequence = MidiSystem.getSequence(str);  } catch ( Exception e) {e.printStackTrace(); }
   }
	 	 
	 		actualMidiPath=path; 
   	 
   	Sequence seq = null;
   	try { 
			InputStream str = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
			seq = MidiSystem.getSequence(str);
			str.close();
   	 } catch (Exception e) { Utils.log("____________ no midi found here : "+path); }
		  final Sequence seq2 = seq;
		  if(loopMidi) oldSequence=seq;
       	 playbackThread = new Thread(() -> {try { 
       		 sequencer.setSequence(seq2);
       	     sequencer.start();
           while (sequencer.isRunning()) {
           	MidiChannel[] channels = synthesizer.getChannels();
               for (int i = 0; i < channels.length; i++)
                    channels[i].controlChange(7, midiVolume);
	                Thread.sleep(1000);
           }
           
          
            
         } catch (Exception e) { e.printStackTrace(); }
	        });playbackThread.start();
   }

   
   
   private void loadSequencerIfNeeded() {try {  
   	if(sequencer==null){
  		 MidiDevice.Info[] info = MidiSystem.getMidiDeviceInfo();
  		 for (int i = 0; i < info.length; i++)
  		       if(MidiSystem.getMidiDevice(info[i])  instanceof Sequencer)
  		    	   sequencer = (Sequencer) MidiSystem.getMidiDevice(info[i]);
  	     sequencer.open();
  	     synthesizer = MidiSystem.getSynthesizer();
  	     synthesizer.open();
  	     synthReceiver = synthesizer.getReceiver();
  	     seqTransmitter = sequencer.getTransmitter();
  	     seqTransmitter.setReceiver(synthReceiver);
  	     
  	  sequencer.addMetaEventListener(new MetaEventListener() {
         @Override
         public void meta(MetaMessage meta) {
             if (meta.getType() == 47) {  
           	  playbackThread = new Thread(() -> {try { sequencer.setSequence(oldSequence);
           	  sequencer.setMicrosecondPosition(0);sequencer.start();
           	     while (sequencer.isRunning()) {
                    	MidiChannel[] channels = synthesizer.getChannels();
                        for (int i = 0; i < channels.length; i++)
                             channels[i].controlChange(7, midiVolume);
        	                Thread.sleep(1000);
                    }
           	  } catch (Exception e) { e.printStackTrace(); }
      	        });playbackThread.start();}
         }
     });}
   } catch (Exception e) { e.printStackTrace(); }}
















	//####################################################################
   private List<AdvancedPlayer> players = new ArrayList<>();
	private Looper actualLoop;
   static Map<AdvancedPlayer, InputStream> fisses = new HashMap<>();
   //#################################################################### 
   public AdvancedPlayer playAudio(String path,boolean fx ) {
   	return playAudio(path,fx,-998);
   }
	public AdvancedPlayer playAudio(String path,boolean fx,int loopTime) { try { 
        InputStream fis=Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
        if(loopTime != -998) { actualLoop= new Looper(path,loopTime);return null;}
       AdvancedPlayer player = new AdvancedPlayer(fis);
       PlayerListener listener = new PlayerListener(player);
       player.setPlayBackListener(listener);
      if(fx)player.setGain(fxvolume);
       players.add(player); 
       fisses.put(player, fis);
       //fis.close();
       Thread playbackThread = new Thread(() -> {
           try { player.play();  players.remove(player); } catch (Exception e) { e.printStackTrace(); }
       });
       playbackThread.start();
       return player;
   } catch (Exception e) { e.printStackTrace(); return null; } }

   public void closeAll() { this.stopMidi();this.stopLoop();for (AdvancedPlayer player : players) player.close();  }

   //####################################################################
   private static class PlayerListener extends PlaybackListener {
       private AdvancedPlayer player; 
       public PlayerListener(AdvancedPlayer player) {  this.player = player; } 
       @Override  public void playbackFinished(PlaybackEvent event) { player.close();try {
			fisses.get(player).close();fisses.remove(player);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  }
   }
   //####################################################################
   public boolean isPlaying(AdvancedPlayer player) { return players.contains(player);  }

   public boolean isPlayingMidi() {  return sequencer!=null&&sequencer.isRunning(); }
	public void stopMidi() { if(sequencer!=null) {sequencer.stop(); playbackThread.interrupt();}}
	public void stopLoop() { if(actualLoop!=null)actualLoop.arreter(); }
   

	    
}



class Looper {
   private String fichierAudio;
   private Thread lectureThread;
	private boolean continuer = true;
	private long dureeTotale;
	private int loop;

   public Looper(String fichierAudio, int loopTime) {
   	loop=loopTime;
       this.fichierAudio = fichierAudio;
       new Thread(() -> {
       dureeTotale = getDureeTotale(); 
       loop();}).start(); 
   }

   public void loop() {
   	if (continuer) {  try { 
   		new Thread(() -> { try { 
   		InputStream fis = Thread.currentThread().getContextClassLoader().getResourceAsStream(fichierAudio);
   		AdvancedPlayer lecteur = new AdvancedPlayer(fis);
   		lecteur.play();fis.close();} catch (Exception e) { }}).start(); 
   		Thread.sleep(dureeTotale - loop);loop();} catch (Exception e) { } 
   	} }

   private long getDureeTotale( ) {try { 
   	InputStream fis = Thread.currentThread().getContextClassLoader().getResourceAsStream(fichierAudio);
   	AdvancedPlayer lecteur = new AdvancedPlayer(fis); 
       long start = System.currentTimeMillis();
       lecteur.play(); 
       long end = System.currentTimeMillis();
       lecteur.close();
       fis.close();
       return end - start;
   } catch ( Exception e) { e.printStackTrace(); }return 0;}

   public void arreter() { continuer  = false;
       if (lectureThread != null && lectureThread.isAlive()) {
           lectureThread.interrupt();
       }
   }

    
}