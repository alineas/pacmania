package agame;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
 
import me.nina.gameobjects.Action;
import me.nina.gameobjects.Animation;
import me.nina.gameobjects.Animation.AnimationState;
import me.nina.gameobjects.Zone;
import me.nina.maker.Ashe;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Utils;
import me.nina.objects2d.Point;
import me.nina.gameobjects.BasicObject;
import me.nina.gameobjects.InventoryObject;
import me.nina.gameobjects.ObjectState;

public class MainGameObjectsManager { 
	private List<BasicObject> allTheObjects; 
	private MainGame mainGame;    
	private ObjectState actualFullScreen;  
	List<BasicObject> hiddenObject = new ArrayList<BasicObject>(); 
	//###############################################
	public MainGameObjectsManager(MainGame m) { mainGame = m; }
	
	//###############################################
	public void handleSceneChanged( ) {   
		hiddenObject.clear(); 
		allTheObjects = BasicObject.getObjects(mainGame.actualSceneOnScreen  ); 
		for(BasicObject bo : allTheObjects) {
			if(bo.selectedState==null) {
				mainGame.addAsh(new Ashe(bo.initialState().line.id,AshType.IS_IN_STATE,1));
				bo.selectedState=bo.initialState();
			}
			
			if(bo.childs().size()!=0)
				for(BasicObject b : bo.childs()) {
					if(b.selectedState==null) {
						mainGame.addAsh(new Ashe(b.initialState().line.id ,AshType.IS_IN_STATE,1));
						b.selectedState=b.initialState();
					}
					hiddenObject.add(b); //1: hide the fullscreen childs objects
				}
		}

		for(BasicObject bo : allTheObjects){   //show the others 
				bo.populate();  
				Animation.get(bo.selectedState.animID()).forwardAll();
		} 
		
	}
	
	//#################################################### clic sent from mainGame :
	public boolean objectHasBeenClicked(GameCharacter c, Point clickPoint) {
		 
	    boolean clicked = false;long l = System.currentTimeMillis();
	    List<BasicObject> listeToCheck = new ArrayList<BasicObject>();
	    if (isFullScreen()) {clicked = true;
	    
	        BasicObject fullScreenObject = (BasicObject) actualFullScreen.possessorObj();
	        listeToCheck = getAllChildren(fullScreenObject); 
	        checkObjectZones(fullScreenObject, actualFullScreen, clickPoint,c );
	    } else {  
	        for (BasicObject obj : allTheObjects) {
	            if (!hiddenObject.contains(obj)) {
	                listeToCheck.add(obj);
	            }
	        }
	    } 
	    for (BasicObject a : listeToCheck) {
	        if (a.canBeFullscreen()) { 
	            clicked |= checkObjectZones(a, a.initialState(), clickPoint,c );
	        } else {
	        	if (Animation.get(a.getSelectedState().animID()).state() != AnimationState.INVISIBLE) { 
 	                clicked |= checkObjectZones(a, a.initialState(), clickPoint,c );
	        	}
	             if(!a.getSelectedState().equals(a.initialState())) { 
	            clicked |= checkObjectZones(a, a.getSelectedState(), clickPoint,c );
	             }
	        }
	    } long l2 = System.currentTimeMillis()-l;Utils.log("obj dured : "+l2);
	    return clicked;
	}

	private boolean checkObjectZones(BasicObject obj, ObjectState state, Point clickPoint, GameCharacter c ) {
	    boolean clicked = false; 
	    for (Zone z : Zone.getByOwner(state.line.id)) {  
	        if (!mainGame.haveAsh(new Ashe(z.line.id, AshType.HAS_BEEN_DISABLED, 1))) {
	        	 
	            if (z.isNear(clickPoint,   10)) { 
	                if (isFullScreen()) {
	                    clicked = true;
	                    mainGame.actionMgr.handleAction(Action.getByDetector(z), c);
	                } else if (haveActionAllowed(z, c)) {
	                    if (!c.isNear(state.requiredPlayerPosition(), 20)) {
	                        c.zoneClicked=z;
	                        Utils.log(state.title());
	                        c.setClicPose( state.requiredPlayerPosition() );
	                        c.thePosTypeOfZoneClicked = state.requiredPlayerPosition().pos;
	                    } else {
	                        c.setPosType(state.requiredPlayerPosition().pos); 
	                        c.feetPosition =  state.requiredPlayerPosition() ;
	                        clicked = true;
	                        mainGame.actionMgr.handleAction(Action.getByDetector(z), c);
	                    }
	                    clicked = true;
	                }
	            }
	        }
	    }
	     
	    return clicked;
	}
	//###############################################
	public boolean haveActionAllowed(Zone zone,GameCharacter dest) {  
		for(Action a : Action.getByDetector(zone ))
			if(mainGame.actionMgr.canDoAction(a, dest))
				return true;
 		return false;
	}
	//#################################################### 
	public void handleCharacterArrived(GameCharacter c) { 
		if(c.zoneClicked ==null)
			return; 
		c.setPosType(c.thePosTypeOfZoneClicked); 
		 mainGame.actionMgr.handleAction( Action.getByDetector(c.zoneClicked ),c);
		 
		 //c.setZoneClicked(null);
	} 
 
	//####################################################
		public void drawObjectsBackground(Graphics g) {  
			List<BasicObject> visibleObjects = new ArrayList<BasicObject>();
			
			if(isFullScreen()){BasicObject obj = (BasicObject) actualFullScreen.possessorObj();visibleObjects.add(obj);
			for( BasicObject bo : obj.childs()) {
				 visibleObjects.add( bo); 
				 for( BasicObject boo : bo.childs())
					 visibleObjects.add( boo); 
	        }
			}
			else{
	        List<Animation> objWithOwnZindex = new ArrayList<Animation>();
	        
	        for(BasicObject bo : allTheObjects) {mainGame.actionMgr.handleObjectStatePainted(bo);
				if(! hiddenObject.contains(bo))//dont take the childs obj s
					if(bo.zindex()<0)
						objWithOwnZindex.add(bo.icone);
					else
						visibleObjects.add(bo);
	        }
	        mainGame.npgMgr.objWithOwnZindex.addAll(objWithOwnZindex);}
	        Collections.sort(visibleObjects, new Comparator<BasicObject>() { //zindex order
	            @Override public int compare(BasicObject obj1, BasicObject obj2) { 
	                return obj1.zindex() - obj2.zindex(); }   });
	         
	        for (BasicObject bo : visibleObjects)  if(isFullScreen()||bo.zindex()<50){ 
	        	try {
					bo.icone.paintIt(g );
				} catch (Exception e) {
					Utils.log(bo.title());
					
				}
	        	
	        	 
	        }
	        
	         
		}
		//####################################################
		public void drawObjectsFront(Graphics g) {  if(isFullScreen())return;
	        List<BasicObject> visibleObjects = new ArrayList<BasicObject>();
	        
	        for(BasicObject bo : allTheObjects) 
				if(! hiddenObject.contains(bo))//dont take the childs obj s
					if(bo.zindex()>0)
	                visibleObjects.add(bo);
			   
	        Collections.sort(visibleObjects, new Comparator<BasicObject>() { //zindex order
	            @Override public int compare(BasicObject obj1, BasicObject obj2) { 
	                return obj1.zindex() - obj2.zindex(); }   });
	         
	        for (BasicObject bo : visibleObjects)  {if(bo.zindex()>=50)
	        	bo.icone.paintIt(g);
	        	 
	        }
	          
		}
	 

	public void handleStateChange(Ashe ash, GameCharacter dest) {
	 
			
			ObjectState state = (ObjectState) ash.possessor(); 
			BasicObject obj =BasicObject.get(state.possessor());
			if (obj == null ) obj = InventoryObject.get(state.possessor());
			
			if(isFullScreen() && obj.getSelectedState().equals(this.actualFullScreen)  && !state.isFullscreen()){
				this.actualFullScreen=null;
				doTheChildrenJob(obj,true);
			}
		
			obj.setSelectedState(state); Utils.log(obj.title()+" go in state "+obj.getSelectedState().title());
		
			if(!isFullScreen()  && obj.getSelectedState().isFullscreen()){
				actualFullScreen=obj.getSelectedState();
				doTheChildrenJob(obj,false);
				
			}
			obj.populate();
		
			
		 
	} 
	
	public List<BasicObject> getAllChildren(BasicObject obj) {
	    List<BasicObject> childrenList = new ArrayList<BasicObject>();
	    addChildrenToList(obj, childrenList);
	    return childrenList;
	}

	private void addChildrenToList(BasicObject obj, List<BasicObject> childrenList) {
	    List<BasicObject> children = obj.childs();
	    for (BasicObject child : children) {
	        childrenList.add(child);
	        addChildrenToList(child, childrenList);  
	    }
	}
	private void doTheChildrenJob(BasicObject obj,boolean hide) {
	    for (BasicObject child : obj.childs()) {
	        if(hide)hiddenObject.add(child);else hiddenObject.remove(child);
	        doTheChildrenJob(child,hide); 
	    }
	}
  
	public boolean isFullScreen() { 
		return actualFullScreen!=null;
	}

	public BasicObject getObject(int id) {
		for(BasicObject a:allTheObjects)
			if(a.line.id == id)
				return a;
		return null;
	}

}
