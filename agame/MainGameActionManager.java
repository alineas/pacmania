package agame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
 

import javazoom.jl.player.advanced.AdvancedPlayer;
import javazoom.jl.player.advanced.PlaybackListener; 

import java.util.Random;
import java.util.Set;

import me.nina.gameobjects.Action;
import me.nina.gameobjects.Animation;
import me.nina.gameobjects.Animation.AnimationState;
import me.nina.gameobjects.BasicObject;
import me.nina.gameobjects.Chapter;
import me.nina.gameobjects.Character;
import me.nina.gameobjects.CharacterState;
import me.nina.gameobjects.GameObject;
import me.nina.gameobjects.InventoryObject;
import me.nina.gameobjects.Message;
import me.nina.gameobjects.ObjectState;
import me.nina.gameobjects.Scene;
import me.nina.gameobjects.SpawnPoint;
import me.nina.gameobjects.TransitionPoint;
import me.nina.gameobjects.Zone;
import me.nina.maker.Ashe;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Utils;
import me.nina.objects2d.Point;
import me.nina.objects2d.Position;
import me.nina.gameobjects.Character.PosType;
 
public class MainGameActionManager  { 
	private MainGame mainGame;  
	public boolean freezedWalk;
	private GameCharacter characterWhoFreezWholeAnim; 
	ObjectState theObjectStateToWait;
	private GameCharacter theTalkertoWait;
	private GameCharacter characterWhoFreezSingle; 
	private GameCharacter secondCharacterWhoFreezSingle;
	Map<Action,GameCharacter> actionsOwners = new HashMap<Action, GameCharacter>();
	public Map<Action,Long> currentTimers = new ConcurrentHashMap<Action, Long>();  
	private ObjectState theObjectStateToDo; 
	private Character nextActor; 
	public Map<Ashe,GameCharacter>pendingAshesToSet = new LinkedHashMap<Ashe, GameCharacter>();
	public AdvancedPlayer actualAudio; 
	
	public MainGameActionManager(MainGame m) {  mainGame = m; }
	 
	//####################################################
	public void handleAction(Action a,GameCharacter c) {
		List<Action> lst = new ArrayList<Action>();
		lst.add(a);
		
		boolean autorised = false;
		boolean haveActor=false;
		GameObject possessor = null;
		for(int i = 0 ; i < a.ashNeeded().size() ; i++) {
			Ashe actual = a.ashNeeded().get(i);  
			if(actual.type() == AshType.IS_THE_ACTOR ) {
				haveActor=true;
				if(actual.possessor().equals(c)) { 
					handleAction(lst,c);
					autorised=true;
				}else possessor=actual.possessor();
			}
			}
		if(!haveActor)  
			handleAction(lst,c);
		 
	if(!autorised&&haveActor) {
		if(possessor.equals(mainGame.player))
			handleAction(lst,mainGame.player);
		for(GameCharacter p : mainGame.npgMgr.allThePnj) 
			if(p.equals(possessor))
				handleAction(lst,p);
	}
	
	} 
	//#################################################### 
	public boolean handleAction( List<Action> actions,GameCharacter c) {  
		List<Action> actionsAccepteds = new ArrayList<Action>();
		for(Action action:actions)     
			if(canDoAction(action,c))   {
				if(action.timer()==-998){
					if(action.random()==-998)
						actionsAccepteds.add(action);
					else{
						int rand = new Random().nextInt(10);
						int chances = action.random();
						if(chances>rand)
							actionsAccepteds.add(action);
						else Utils.log(action.title()+" not executed cause random="+rand+" and chances="+chances);
					}
				}
				else {Utils.log(action.title()+" add a timer !!!");
					if(!currentTimers.containsKey(action)){
						currentTimers.put( action,System.currentTimeMillis());
						actionsOwners.put( action,c);
						mainGame.addAsh( new Ashe(action.line.id , AshType.WILL_DO_AFTER_TIMER,1));
					}else{ actionsAccepteds.add(action);
					mainGame.removeAsh( new Ashe(action.line.id , AshType.WILL_DO_AFTER_TIMER,1));}//run action after timer
				}
			}
			   
		for(Action a:actionsAccepteds) 
				execute(a,c); 
		return actionsAccepteds.size()!=0;
	}
	
	
	
	//#################################################### 
	public boolean canDoAction(Action a, GameCharacter c) {  
    for (List<Ashe> group : getAllOrGroups(a.ashNeeded())) { 
        boolean isGroupFulfilled = false;
        for (Ashe ash : group) 
            if (isConditionFulfilled(ash, c, true)) { 
                isGroupFulfilled = true; break; } 
        if (!isGroupFulfilled)  
            return false;   }

    for (List<Ashe> group : getAllOrGroups(a.ashRefused())) { 
        boolean isGroupFulfilled = true; 
        for (Ashe ash : group) 
            if (!isConditionFulfilled(ash, c, false)) { 
                isGroupFulfilled = false; break; } 
        if (!isGroupFulfilled)  
            return false;   }
    return true;
}
	//#################################################### 
	List<List<Ashe>> getAllOrGroups(List<Ashe> ashes) { 
	    List<List<Ashe>> allOrGroups = new ArrayList<List<Ashe>>(); 
	    int startIndex = 0;
	    while (startIndex < ashes.size()) { 
	        int endIndex = findEndIndex(ashes, startIndex);
	        if (endIndex != -1) {
	        	  List<Ashe> group = new ArrayList<Ashe>();
	              for (int i = startIndex; i <= endIndex; i++)  
	                  if (ashes.get(i).type() != AshType.OR) 
	                      group.add(ashes.get(i)); 
	              allOrGroups.add(group); 
	            startIndex = endIndex + 1;  
	        } else  
	            break; 
	    }
 
	    return allOrGroups;
	}
	//#################################################### 
	int findEndIndex(List<Ashe> ashes, int startIndex) {
	    int index = startIndex;
	    while (index < ashes.size()) {
	        if (index == ashes.size() - 1)
	            return index;
	        else if (ashes.get(index).type() != AshType.OR && ashes.get(index + 1).type() != AshType.OR)  
	            return index; 
	        index++;
	    }
	    return -1; // Si rien n'est trouv�
	}
	//#################################################### 
	boolean isConditionFulfilled(Ashe ash, GameCharacter c, boolean isNeeded) {
		 
	    if (ash.type() == AshType.HAS_BEEN_SPAWNED) {
 	        boolean toReturn = isNeeded ? mainGame.npgMgr.havePnj(ash.possessor().line.id) : !mainGame.npgMgr.havePnj(ash.possessor().line.id);
	        if (!toReturn ) {
	        	 
	            return toReturn;
	        }
	    } else if (ash.type() == AshType.IS_THE_ACTOR) {
	        boolean toReturn = isNeeded ? ash.possessor().equals(c) : !ash.possessor().equals(c);
	        if (!toReturn ) {
	        	 
	            return toReturn;
	        }
	    }
	    else if (ash.type() == AshType.HAS_NOTHING_IN_HAND) { 
	        boolean toReturn = isNeeded ? MainGame.instance.menuBar.inHand==null&&MainGame.instance.menuBar.inHandObj==null : MainGame.instance.menuBar.inHand!=null||MainGame.instance.menuBar.inHandObj!=null;
	        if (!toReturn ) { 
	        	 
	            return toReturn;
	        }
	    } else if (ash.type() == AshType.IS_IN_ZONE) {
	    	Zone z = (Zone) ash.possessor(); 
	    	boolean toReturn;
	 
	    	if (isNeeded) {
	    	    toReturn = z.isInZone(c.feetPosition) ;
	    	} else {
	    	    toReturn = !z.isInZone(c.feetPosition);
	    	}
	    	 
	    	if (!toReturn ) {
	        	 return toReturn;
	        }
	    } 
	    else if (ash.type() == AshType.IS_ACTUAL_SCENE) {
	        boolean toReturn = isNeeded ? ash.possessor().equals(c.actualScene) : !ash.possessor().equals(c.actualScene);
	        if (!toReturn ) {
	        	 
	            return toReturn;
	        }
	    } 
	    else if (ash.type() == AshType.ACTION_CAN_BE_EXECUTED) {
	        boolean toReturn = isNeeded ? canDoAction((Action)ash.possessor(),c) : !canDoAction((Action)ash.possessor(),c);
	        if (!toReturn ) {
	        	 
	            return toReturn;
	        }
	    } 
	    else {
	        if (isNeeded && ((!mainGame.haveAsh(ash)&&ash.getNbr()==0) ||( (ash.getNbr() != 0 && ash.getNbr() != mainGame.howManyAsh(ash))))) {
	             
	            return false;
	        } else if (!isNeeded && ((mainGame.haveAsh(ash)&&ash.getNbr()==0) || (ash.getNbr() != 0 && ash.getNbr() == mainGame.howManyAsh(ash)))) {
	            
	            return false;
	        }
	    } 
	    return true;
	}
	//#################################################### 
	public void execute(Action a,GameCharacter dest) {    
		if(dest!=null)Utils.log("_____________________________________executing action "+a.title()+" for dest="+dest.title());
		Ashe last = null;
 		for(Ashe ash:a.ashToSet()){ 

 			
			if (ash.getNbr()<0) {//ashesToRem

				if(isfreezed() && ash.type() != AshType.WILL_DO_AFTER_TIMER ) {  Utils.log("adding to the pending ashes to rem : "+ash.title());
 					pendingAshesToSet.put(ash,dest);
 				 
				}else {
 					executeRemoveAsh( ash);
				
				}

			}else { //ashesToSet
			 boolean force = false;
			  
			 if(this.pendingAshesToSet.size()==0&&last!=null)
				 if(last.type()==AshType.WILL_INSIDE_THIS_STATE || last.type()==AshType.WILL_OUTSIDE_THIS_STATE)
					 if(ash.type()==AshType.WILL_INSIDE_THIS_STATE || ash.type()==AshType.WILL_OUTSIDE_THIS_STATE) { 
						CharacterState lastState = (CharacterState) last.possessor(); 
						GameCharacter lastOwner = mainGame.npgMgr.get(lastState.possessor());
						CharacterState actualState = (CharacterState) ash.possessor(); 
						GameCharacter actualOwner = mainGame.npgMgr.get(actualState.possessor());
						if(!lastOwner.equals(actualOwner) && (this.characterWhoFreezSingle==null||this.secondCharacterWhoFreezSingle==null))
							force=true;
			 }
			 
				if(!force&&isfreezed() &&  ash.type() != AshType.WILL_DO_AFTER_TIMER) {  
					Utils.log("adding to the pending ashes to set : "+ash.title());
					
				if(ash.type() == AshType.ACTION_TO_DO_WITH_BEFORE_CONDITION_CHECK ) {
					if(canDoAction((Action) ash.possessor(),dest)) {
	 					pendingAshesToSet.put(ash,dest);
	 				}else Utils.log("will not execute the action "+ash.possessor().title()+" cause not fullfilled before adding it to the pending ashes to set !!!!!!!!!!");
				}else if(ash.type() == AshType.ACTION_TO_DO_WITH_AFTER_CONDITION_CHECK ) {
	 					pendingAshesToSet.put(ash,dest);
	 			}
				else {
	 				pendingAshesToSet.put(ash,dest);
	 			}
				}
				else   
					doTheJobAshToSet( ash,dest); 

				
		} 
			 last = ash;
		} 
	} 
	
	
	
	

	private void executeRemoveAsh( Ashe ash) {
		if(ash.type() == AshType.WILL_DO_AFTER_TIMER) {
			Utils.log("timer canceled"+currentTimers.containsKey(ash.possessor())+currentTimers.keySet());
			currentTimers.remove(ash.possessor()); 
			actionsOwners.remove(ash.possessor()); 
			  
			mainGame.removeAsh(ash);
		}
		if(ash.type() == AshType.HAS_IN_HAND && MainGame.instance.menuBar.inHandObj!=null && ash.possessor().equals(MainGame.instance.menuBar.inHandObj)) 
			MainGame.instance.menuBar.inHandObj = null;
		
		if(ash.type() == AshType.HAS_IN_INVENTORY) {
			MainGame.instance.menuBar.removeItem( (BasicObject) ash.possessor() );
			mainGame.removeAsh(new Ashe(ash.possessor ,AshType.HAS_IN_HAND,1));
		}
		mainGame.removeAsh(ash);
	}

	public boolean isfreezed() {  
		return  characterWhoFreezWholeAnim!=null || theObjectStateToDo != null || theObjectStateToWait != null || theTalkertoWait != null || characterWhoFreezSingle!=null|| secondCharacterWhoFreezSingle!=null;
	}
 
	//#################################################### 
	public void doTheJobAshToSet( Ashe ash, GameCharacter dest) { try{
		 
		
		if(nextActor != null) dest = mainGame.npgMgr.getCharacter(nextActor);
		 
		if(ash.type() == AshType.IS_THE_ACTOR ) {
			nextActor = (Character) ash.possessor();
			return;
		}
		 
		if(ash.type() == AshType.WILL_BE_SPAWNED )  
			mainGame.npgMgr.spawnPnj(ash );
		if(ash.type() == AshType.WILL_BE_DESPAWN )  
			mainGame.npgMgr.despawnPnj(ash );
		if(ash.type() == AshType.WILL_BE_LOOKED ) {
			 GameCharacter looked = mainGame.npgMgr.getCharacter(ash.possessor());
			PosType p = PosType.calculatePosType(dest.feetX(),dest.feetY(), looked.feetX(),looked.feetY());
			 dest.setPosType(p); 
			
		}
		if(ash.type() == AshType.WILL_LOOK_RIGHT  ) {
			mainGame.npgMgr.getCharacter(ash.possessor()).setPosType(PosType.RIGHT); }
		if(ash.type() == AshType.WILL_LOOK_LEFT  ) {
			mainGame.npgMgr.getCharacter(ash.possessor()).setPosType(PosType.LEFT); }
		if(ash.type() == AshType.WILL_LOOK_BOTTOM  ) {
			mainGame.npgMgr.getCharacter(ash.possessor()).setPosType(PosType.BOTTOM); }
		if(ash.type() == AshType.WILL_LOOK_TOP  ) {
			mainGame.npgMgr.getCharacter(ash.possessor()).setPosType(PosType.TOP); }
		
		if(ash.type() == AshType.WILL_LOOK_TOP_RIGHT  ) {
			mainGame.npgMgr.getCharacter(ash.possessor()).setPosType(PosType.TOP_RIGHT); }
		if(ash.type() == AshType.WILL_LOOK_TOP_LEFT  ) {
			mainGame.npgMgr.getCharacter(ash.possessor()).setPosType(PosType.TOP_LEFT); }
		if(ash.type() == AshType.WILL_LOOK_BOT_RIGHT  ) {
			mainGame.npgMgr.getCharacter(ash.possessor()).setPosType(PosType.BOT_RIGHT); }
		if(ash.type() == AshType.WILL_LOOK_BOT_LEFT  ) {
			mainGame.npgMgr.getCharacter(ash.possessor()).setPosType(PosType.BOT_LEFT); }
		
		
		if(ash.type() == AshType.SAVE_BEFORE_DEAD  ) mainGame.saveBeforeDead();
 
		if(ash.type() == AshType.RESTORE_BEFORE_DEAD  )  mainGame.restoreBeforeDead();
 
		
		if(ash.type() == AshType.ACTION_TO_DO_WITH_BEFORE_CONDITION_CHECK  || ash.type() == AshType.ACTION_TO_DO_WITH_AFTER_CONDITION_CHECK  ){
			 Action actionToDoAfter = (Action)ash.possessor();
			 if(actionToDoAfter.timer() == -998){//no timer, launch
				 handleAction(actionToDoAfter,dest);
			 }
			 else{
				 if(mainGame.haveAsh(new Ashe(actionToDoAfter.line.id , AshType.WILL_DO_AFTER_TIMER,1))  ){
						handleAction(actionToDoAfter,actionsOwners.get(actionToDoAfter));
						mainGame.removeAsh( new Ashe(actionToDoAfter.line.id , AshType.WILL_DO_AFTER_TIMER,1));
				 }else {
					currentTimers.put( actionToDoAfter,System.currentTimeMillis());
					actionsOwners.put( actionToDoAfter,dest);
					mainGame.addAsh( new Ashe(actionToDoAfter.line.id , AshType.WILL_DO_AFTER_TIMER,1));
				}
			
			 }
			
			
		}
		if(ash.type() == AshType.ENTER_CHAPTER ) {
			mainGame.currentAshes.clear();
			mainGame.menuBar.inventory.clear();
			mainGame.npgMgr.allThePnj.clear();
			mainGame.player=null;
			Chapter ch = (Chapter) ash.possessor(); 
			mainGame.changeScene(ch.startScene() ); 
			mainGame.centerScrollPaneOn(ch.positionStart().x,ch.positionStart().y);
	 
			mainGame.actionMgr.execute(ch.firstAction(), null);
		}
		if(ash.type() == AshType.WILL_BE_DISABLED )
			mainGame.addAsh(new Ashe(ash.possessor().line.id,AshType.HAS_BEEN_DISABLED,ash.getNbr()));
		if(ash.type() == AshType.WILL_SAY )  
			handleMessage(ash,dest,true);
		if(ash.type() == AshType.WILL_SAY_UNFREEZE )  
			handleMessage(ash,dest,false);
		  
		if(ash.type() == AshType.WILL_BE_IN_POSITION ) 
			handlePositionning(ash,dest,false);
		
		if(ash.type() == AshType.WILL_HAVE_IN_HAND ) 
			handleVolatileInHand(ash,dest);
		
		if(ash.type() == AshType.WILL_BE_CLICKED ) 
			handlePositionning(ash,dest,true);
		
		if(ash.type() == AshType.WILL_HAVE_IN_INVENTORY) { 
			mainGame.addAsh(new Ashe(ash.possessor().line.id,AshType.HAS_IN_INVENTORY,1));
			MainGame.instance.menuBar.addInvObject((InventoryObject) ash.possessor()); 
		}
		if(ash.type() == AshType.WILL_BE_IN_STATE) { 
			handleStateChange(ash,dest);
		}
		if(ash.type() == AshType.WILL_INSIDE_WAIT_AND_OUTSIDE_STATE )  
			handleStateFreezeWholeAnim(ash,dest); 
		if(ash.type() == AshType.WILL_INSIDE_OUTSIDE_THIS_STATE ||ash.type() == AshType.WILL_INSIDE_THIS_STATE || ash.type() == AshType.WILL_OUTSIDE_THIS_STATE)  
			handleStateFreezeSingle(ash,dest); 
		 
		
		if(ash.type() == AshType.RECORD_ACTUAL_POSITION){
			dest.oldScene = mainGame.actualSceneOnScreen ;
			dest.oldPlayerPosition = mainGame.player.feetPosition;
		}
		if(ash.type() == AshType.WILL_BE_IN_POSITION_RECORDED){
			if(dest.equals(mainGame.player ) && !mainGame.actualSceneOnScreen .equals(dest.oldScene))
				mainGame.changeScene(dest.oldScene);
			dest.setPos(dest.oldPlayerPosition);
			
		}nextActor=null; 
	} catch (Exception e) { e.printStackTrace(); } }
	
	
	//#################################################### 
	private void handleVolatileInHand(Ashe ash, GameCharacter dest) { 
		BasicObject obj = (BasicObject) ash.possessor(); 
		MainGame.instance.menuBar.inHandObj = obj;  
		mainGame.addAsh(new Ashe(obj.line.id,AshType.HAS_IN_HAND,1));
	}

	//#################################################### 
	private void handleMessage(Ashe ash, GameCharacter dest, boolean freeze) {  
		Message msg = (Message)ash.possessor();
		Ashe ashe = new Ashe(ash.possessor().line.id,AshType.HAS_BEEN_SAID,1); 
		mainGame.addAsh(ashe);
		if(freeze)
		for(Character a:Character.getAll())
			if(a.talker() == msg.talker())  {
				boolean idleStateNeeded = true;
				if(characterWhoFreezSingle!=null&&characterWhoFreezSingle.line.id == a.line.id 
						|| characterWhoFreezWholeAnim!=null&&characterWhoFreezWholeAnim.line.id == a.line.id )
					idleStateNeeded = false;
				theTalkertoWait = mainGame.npgMgr.get(a.line.id);
				
				theTalkertoWait .setTalking(true,idleStateNeeded);  
			}
		if(freeze)actualAudio = MainGame .instance.soundPlayer.playAudio(msg.getSound(),false); 
		else  MainGame .instance.soundPlayer.playAudio(msg.getSound(),false);  
		 
	}
	
	//#################################################### 
	private void handleStateFreezeWholeAnim(Ashe ash, GameCharacter dest) { 
		 if(ash.possessor() instanceof CharacterState) { 
			CharacterState state = (CharacterState) ash.possessor(); 
			GameCharacter owner = mainGame.npgMgr.get(state.possessor());

			characterWhoFreezWholeAnim=owner; 
				owner.setSelectedStat(state);   
		}
	}
	//#################################################### 
	private void handleStateFreezeSingle(Ashe ash, GameCharacter dest) {
		 if(ash.possessor() instanceof CharacterState) { 
			CharacterState state = (CharacterState) ash.possessor(); 
			GameCharacter owner = mainGame.npgMgr.get(state.possessor()); 
			if(owner==null||owner.actualScene!=null&&!owner.actualScene.equals(MainGame.instance.actualSceneOnScreen))return;//despawned pnj...			owner.actualAshType = ash.type();

			owner.actualAshType = ash.type();
			if(characterWhoFreezSingle == null)
				characterWhoFreezSingle=owner;  
			else 
				secondCharacterWhoFreezSingle=owner; 
			Utils.log(owner.title()+" go in freezed state "+state.title());
				 
				owner.setSelectedStat(state); 
				if(owner.actualAshType == AshType.WILL_OUTSIDE_THIS_STATE) {
					owner.getCurrentAnime().forwardAll();
					owner.getCurrentAnime().setReversed(true);
				}
		}
	}
	//#################################################### 
	private void handleStateChange(Ashe ash, GameCharacter dest)  { //change state detected
		if(ash.possessor() instanceof ObjectState) { 
			BasicObject object = (BasicObject) ash.possessor().possessorObj();
			mainGame.removeAsh(new Ashe(object.getSelectedState().line.id,AshType.IS_IN_STATE,0));
			mainGame.addAsh(new Ashe(ash.possessor().line.id,AshType.IS_IN_STATE,1));
			ObjectState state = (ObjectState) ash.possessor();
			Animation anim = Animation.get(state.animID());
			mainGame.objMgr.handleStateChange(ash,dest);
			if(anim.state()!=AnimationState.INVISIBLE && anim.frameOrder().size()>1)
			if(characterWhoFreezWholeAnim == null && !(object instanceof InventoryObject))
				theObjectStateToDo = (ObjectState) ash.possessor();
			else {
				if(!(object instanceof InventoryObject))
				theObjectStateToWait =(ObjectState) ash.possessor();
			}
		}
	
		if( ash.possessor()  instanceof CharacterState) { 
			CharacterState state =  (CharacterState) ash.possessor() ;
			if(mainGame.npgMgr.havePnj(state.possessor()) || state.possessor() == mainGame.player.line.id) {
				GameCharacter character = mainGame.player;
			if(mainGame.npgMgr.havePnj(state.possessor()))
				character = mainGame.npgMgr.get(state.possessor()); 
			  
				mainGame.removeAsh(new Ashe(character.getSelectedStat().line.id,AshType.IS_IN_STATE,1));
				 character .setSelectedStat( state );  
				 
				 
			}
		}
	}
	//#################################################### 
	private void handlePositionning(Ashe ash,GameCharacter dest, boolean clic) { 
		if(dest==null)dest = mainGame.player;
		dest.zoneClicked=null; 
		dest.characterToRejoin=null;
		dest.tpClicked=null;
		GameObject z = ash.possessor() ;
		 
		 Scene scene=null;Position pos=null;
			if(!clic) Utils.log("setpos called for "+dest.title()+" "+z);
			else {
				Utils.log( dest.title()+" has clicked on "+z.title());
				if(dest.equals(mainGame.player) && !mainGame.objMgr.isFullScreen())freezedWalk = true;
			}
			
		if(z instanceof TransitionPoint){
			TransitionPoint tpoint = ((TransitionPoint)z);
			pos =  tpoint.getDesiredPos() ;
			if(clic) {
				dest.tpClicked=tpoint;
				dest.setClicPose(pos);
				dest.thePosTypeOfZoneClicked = pos.pos;
 				 
				return;
			}
			scene = (Scene)tpoint.possessorObj();
			mainGame.tpMgr.handleTeleport(scene, pos,dest);
			 return;
		}
		
		else if(z instanceof Character){  
			GameCharacter zone = mainGame.npgMgr.get(z.line.id);
			 
			if(clic) {
				mainGame.npgMgr.executeClicOnCharacter(dest, zone); 
				dest.stairManager.click();
				return;
			}
			scene = (Scene)zone.possessorObj() ;  
			dest.setPos(zone.feetPosition);
			return;
		}
		 
		else if(z instanceof ObjectState){  
			ObjectState state = (ObjectState)ash.possessor();
			BasicObject obj = (BasicObject)state.possessorObj();
			pos = state.requiredPlayerPosition();
			if(clic) { 
				dest.setClicPose(pos);
				dest.thePosTypeOfZoneClicked = state.requiredPlayerPosition().pos;
 				return;
			}
			scene = (Scene)obj.possessorObj() ; 
			if(dest.equals(mainGame.player) && !scene.equals(mainGame.actualSceneOnScreen ))
				mainGame.changeScene(scene);
			dest.actualScene=scene;
			dest.setPos(pos);
			dest.setPosType(pos.pos);
			return;
		}
		else if(z instanceof Zone){  
			Zone zone = ((Zone)z);  
			if(clic) {
				if(mainGame.objMgr.isFullScreen()) {
					mainGame.objMgr.objectHasBeenClicked(dest, zone.mid()); 
					return;
				} 
				
				if(!(z instanceof SpawnPoint))dest.zoneClicked=zone ;
				dest.setClicPose(zone.mid());
 				return;
			}
			if(zone.possessorObj() instanceof ObjectState) 
				scene = (Scene)zone.possessorObj().possessorObj().possessorObj() ;
			else
			    scene = (Scene)zone.possessorObj() ;
			
			if(dest.equals(mainGame.player) && !scene.equals(mainGame.actualSceneOnScreen ))
				mainGame.changeScene(scene);
			dest.actualScene=scene;
			dest.setPos(zone.mid());
			 
		}
		
	}
 
        
boolean busy;
private String theUsingOfactionmgr;


	public void run() {if(busy) {Utils.log("action manager get concurrent problem when doing:");
		new Throwable().printStackTrace();Utils.log("");Utils.log("the actual using is : "+theUsingOfactionmgr);Utils.log("");Utils.log("");return;
	}busy=true;theUsingOfactionmgr=new Throwable().getMessage();
 			
	
	Action toRem=null;
	for(Entry< Action,Long> entry : currentTimers.entrySet()){
		int timerDuration = entry.getKey().timer();
		long passed = System.currentTimeMillis()-entry.getValue();
		passed/=1000;
		if(passed>=timerDuration){
			Utils.log("timer is passed ! launch action "+entry.getKey().title());
			handleAction( entry.getKey(),actionsOwners.get(entry.getKey()) ); 
			toRem= entry.getKey() ;break;
	}}  if(toRem!=null)currentTimers.remove(toRem);
	
	if(characterWhoFreezSingle!=null) { 
				if(finishedTheJobSingleFreez(characterWhoFreezSingle))
					characterWhoFreezSingle=null;
			}
			 
			if(secondCharacterWhoFreezSingle!=null)  { 
				if(finishedTheJobSingleFreez(secondCharacterWhoFreezSingle))
					secondCharacterWhoFreezSingle=null; 
			}
			else if(characterWhoFreezSingle!=null || secondCharacterWhoFreezSingle!=null) {
				busy=false;
				return;
			}
			else if(theObjectStateToDo != null ) {
				if( !Animation.get(theObjectStateToDo.animID()).loopFinished())
					Animation.get(theObjectStateToDo.animID()).run();
				else theObjectStateToDo = null;
				runPending();
			}
			else if (theObjectStateToWait!=null 
					&& (Animation.get(theObjectStateToWait.animID()).state()==AnimationState.INVISIBLE || mainGame.objMgr.hiddenObject.contains(theObjectStateToWait.possessorObj())))
				theObjectStateToWait=null;
			else if(MainGame.instance .soundPlayer.isPlaying(actualAudio) || theObjectStateToWait!=null) {
				busy=false;return;}
			
			else if(theTalkertoWait!=null) {
				theTalkertoWait.setTalking(false,characterWhoFreezWholeAnim == null); 
				theTalkertoWait=null;
				actualAudio=null;  
			}
			
			else if(characterWhoFreezWholeAnim != null) {
				Animation anim = characterWhoFreezWholeAnim.getCurrentAnime();
				if( !anim.loopFinished() && !anim.isReversed() )  {
					anim.run();	// Utils.log("run anim"+anim.currentIndex());
				}
				else runPending(); 
				if( pendingAshesToSet.size()==0 && theObjectStateToWait==null){ //Utils.log("reverse"+anim.currentIndex());
					if( anim.loopFinished() && !anim.isReversed()) {
						anim.setReversed(true);
					}
					else if( anim.loopFinished() && anim.isReversed())  { //Utils.log("finished the anim"); 
						anim.setReversed(false);   
						GameCharacter owner = mainGame.npgMgr.get(characterWhoFreezWholeAnim.line.id);
						 //Utils.log("char frezer of the whole = "+characterWhoFreezWholeAnim.title());
						if(owner!=null)//!despawned png
						owner.setSelectedStat(owner.getInitialState(),false);
						 
						 
						characterWhoFreezWholeAnim=null;
					
					}else {
						anim.run();
					}
				}
			
			}else runPending();
		
		busy=false;
	}
	
	
	
	




private boolean finishedTheJobSingleFreez(GameCharacter character) {
	 
		Animation anim = character.getCurrentAnime();// Utils.log(anim.title()+" "+anim.currentIndex()+" "+anim.idle+" "+anim.frameOrder().size());
		if( !anim.loopFinished() )  
			anim.run();	  
		if( character.actualAshType != AshType.WILL_INSIDE_THIS_STATE && anim.loopFinished() && !anim.isReversed()) {
			anim.setReversed(true); 
		}
		if(anim.loopFinished() && anim.isReversed()&&character.actualAshType == AshType.WILL_OUTSIDE_THIS_STATE ||
				anim.loopFinished() && !anim.isReversed()&&character.actualAshType == AshType.WILL_INSIDE_THIS_STATE ||
				anim.loopFinished() && anim.isReversed()&&character.actualAshType == AshType.WILL_INSIDE_OUTSIDE_THIS_STATE
				) {//Utils.log("finish_");
			
			

			if(character.actualAshType == AshType.WILL_INSIDE_OUTSIDE_THIS_STATE) {
				anim.setReversed(false);   
				
				GameCharacter owner = mainGame.npgMgr.get(character.line.id);
				owner.setSelectedStat(owner.getInitialState(),false);
				
				
				
				 
			}
			return true;
		}
		 
	return false;
	}


boolean busy2;String user; 

	void runPending() {if(!busy2) {busy2=true;user = new Throwable().getMessage();
		if( pendingAshesToSet.size()!=0 ) { Ashe next = null,next2=null;
		try { 
				 Iterator<Entry<Ashe, GameCharacter>> iterator = pendingAshesToSet.entrySet().iterator();
			        if (iterator.hasNext()) {
			        	Entry<Ashe, GameCharacter> secondEntry = null;
			            Entry<Ashe, GameCharacter> firstEntry = iterator.next();
			            next = firstEntry.getKey();
			            if(theObjectStateToDo != null && !(next.possessor() instanceof CharacterState)) {busy2=false;return;}
			          
			             

			            
			            
			            if(next.getNbr() <0) {
			            	executeRemoveAsh( next);
 							pendingAshesToSet.remove(next);
			            }else {
			            if (iterator.hasNext()) {
			            	secondEntry = iterator.next();
			            	next2 = secondEntry.getKey();
			            	
			            }
			            
			             
			            if(next.type() != AshType.ACTION_TO_DO_WITH_AFTER_CONDITION_CHECK||(next.type() == AshType.ACTION_TO_DO_WITH_AFTER_CONDITION_CHECK  && canDoAction((Action)next.possessor(), firstEntry.getValue())))
						doTheJobAshToSet( next,firstEntry.getValue());
						
						if(next2!=null) {
							
							
				             

						boolean forceToExecute = forceToExecute(next2);
						if(next2.type() != AshType.ACTION_TO_DO_WITH_AFTER_CONDITION_CHECK||(next2.type() == AshType.ACTION_TO_DO_WITH_AFTER_CONDITION_CHECK  && canDoAction((Action)next2.possessor(), secondEntry.getValue())))
						if(forceToExecute)
							doTheJobAshToSet( next2,secondEntry.getValue());
						else next2=null;
						//if(next.possessor()instanceof ObjectState )
							//theObjectStateToWait =(ObjectState) next.possessor();
			            }
			            }
			        }
				if(next!=null) {
 					pendingAshesToSet.remove(next); 
				}
				if(next2!=null) {
 					pendingAshesToSet.remove(next2); 
				} 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		}busy2=false;
		}else {
			Utils.log("error in run pending this want to access : ");
			new Throwable().printStackTrace();
			Utils.log("when it is in use by "+user);
		}
	}

	public void handleObjectStatePainted(BasicObject bo) {
		if(theObjectStateToWait==null )return;
	
		if(bo.selectedState.equals(theObjectStateToWait) && bo.icone.loopFinished()) 
			theObjectStateToWait = null;
	}

 
	private boolean forceToExecute(Ashe next) {   
		 
		if(secondCharacterWhoFreezSingle != null && characterWhoFreezSingle == null) {Utils.log("reversing the freezers___________________");
			characterWhoFreezSingle = secondCharacterWhoFreezSingle;
			secondCharacterWhoFreezSingle = null;
			
		} 
		return characterWhoFreezSingle != null&& secondCharacterWhoFreezSingle == null &&
						(next.type()==AshType.WILL_INSIDE_THIS_STATE  
						|| next.type()==AshType.WILL_OUTSIDE_THIS_STATE
						|| next.type()==AshType.WILL_INSIDE_OUTSIDE_THIS_STATE) 
						&& !characterWhoFreezSingle.equals(next.possessor().possessorObj()); 
		
		 
	}


 
 
} 


