package agame;
  
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.MouseInfo;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import me.nina.editors.Editor;
import me.nina.gameobjects.Action;
import me.nina.gameobjects.Animation; 
import me.nina.gameobjects.SpecialZoneInv;
import me.nina.gameobjects.Zone; 
import me.nina.gameobjects.BasicObject;
import me.nina.gameobjects.InventoryObject;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Ashe;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Main;
import me.nina.maker.Utils;
import me.nina.objects2d.Img;
import me.nina.objects2d.ImgUtils;
import me.nina.objects2d.Point;
import me.nina.sqlmanipulation.SQLLine; 

public class MenuBar extends JPanel { private static final long serialVersionUID = 1L;

	List<InventoryObject> inventory= new ArrayList<>();
	private MainGame mainGame;
	private JPanel invExplorer;
	private JPanel igMenu; 
	public boolean freezed;
	InventoryObject inHand;
	public Animation inHandAnim; 
	JLabel itemTitle = new JLabel("");
	
	//############################################################################
	public MenuBar(MainGame mainGame) {
		if(mainGame==null)return;
		this.mainGame = mainGame;  
		invExplorer = new JPanel(){  private static final long serialVersionUID = 1L; 
			@Override protected void paintComponent(Graphics g) { super.paintComponent(g);   if(invExplorer.isVisible()) {
	            	g.drawImage(ImgUtils.getPicture(invExplorerBg() ), 0,0,this); 
	            	g.drawImage(ImgUtils.getPicture(exitButtonInvExplorerBg() ), closeInvExplorerZone().x(),closeInvExplorerZone().y(),this); 
	            	g.drawImage(inHandAnim.getActualFrame(), 
	            			invExplorer.getWidth()/2-inHandAnim.getActualFrame().getWidth()/2,
	            			 invExplorer.getHeight()/2-inHandAnim.getActualFrame().getHeight()/2,this); 
	    } } };   
		igMenu = new JPanel(){ private static final long serialVersionUID = 1L;
			@Override protected void paintComponent(Graphics g) { super.paintComponent(g);  if(igMenu.isVisible()) {
				g.drawImage(ImgUtils.getPicture(igMenuBg() ), 0,0,this); 
				g.drawImage(ImgUtils.getPicture(exitButtonMenuBg() ), closeMenuZone().x(),closeMenuZone().y(),this); 
  
		} } }; 
		 
		mainGame.add(invExplorer);  
		mainGame.add(igMenu); 
		invExplorer.add(itemTitle); itemTitle.setOpaque(true);itemTitle.setBackground(Color.white);
		invExplorer.setLayout(null);
		invExplorer.setVisible(false);
		igMenu.setVisible(false); 
		igMenu.add(mainGame.soundPlayer.getPanel());
		setBounds(menuZone().x(),menuZone().y(),menuZone().w(),menuZone().h()); 
		Utils.setSize(this, menuZone().w(),menuZone().h());
		addMouseListener(new MouseAdapter() { @Override public void mousePressed(MouseEvent e) { 
            int x = e.getX(), y = e.getY();
			Point p = new Point(e.getX(),e.getY()); 
			if(menuZone().isInZone(p))
				Utils.log("menu clicked"); 
			if(dragObjZone().isInZone(p)) 
				handleClicDragObjectZone(x,y);  
			if(igMenuOpenerZone().isInZone(p)) 
				openInGameMenu(x,y); 
			if(invZone().isInZone(p) )  
				handleClicInventoryBar(x,y);
		}}); 
		igMenu.addMouseListener(new MouseAdapter() { @Override public void mousePressed(MouseEvent e) {
            closeInGameMenuIfNeeded(e.getX(),e.getY());   } });
		invExplorer.addMouseListener(new MouseAdapter() {   @Override public void mousePressed(MouseEvent e) { 
            closInvExplorerIfNeeded(e.getX(),e.getY()); 
            handleZoneClickInvExplorer(e.getX(),e.getY()); } });
		invExplorer.addMouseMotionListener(new MouseAdapter() { @Override public void mouseDragged(MouseEvent e) {  
			moveObjectInExplorer(e.getX(), e.getY()); }  });
		  
	}
 
	//############################################################################
	@Override protected void paintComponent(Graphics g) { super.paintComponent(g); 
        g.drawImage(ImgUtils.getPicture(bgFolder() ), 0,0,this); 
 
        populateInventory();
        for(InventoryObject obj : inventory) {
        	 Animation.get(obj.getSelectedState().animID()).setSize(30);
        	 BufferedImage img = ImgUtils.getPicture(obj.getFolder()+obj.getSelectedState().cursorImg() ) ;
        	
        	g.drawImage(ImgUtils.resiz(img,30, 30), obj.zone.x(),obj.zone.y(),this); 
        	 
        }
        if(inHand != null && !freezed) {
            int x = MouseInfo.getPointerInfo().getLocation().x;
            int y = MouseInfo.getPointerInfo().getLocation().y; 
            java.awt.Point localMousePosition = new java.awt.Point(x, y) ;
            SwingUtilities.convertPointFromScreen(localMousePosition, this); 
            g.drawImage(ImgUtils.resiz(ImgUtils.getPicture(inHand.getFolder() + inHand.getSelectedState().cursorImg()),30, 30),
                        localMousePosition.x, localMousePosition.y, null);
        }
        
    }
	 public BasicObject inHandObj; //ground objects we can take
		public BufferedImage inHandVolatileFrame; 
		
	public void paintInHandOutsideBar(Graphics g, JScrollPane scrollGame) {
		if(inHand != null ||  inHandObj != null) { 
		    int x = MouseInfo.getPointerInfo().getLocation().x;
		    int y = MouseInfo.getPointerInfo().getLocation().y; 
		    Component viewport = scrollGame.getViewport().getView(); 
		    if(viewport != null) { 
		        java.awt.Point localMousePosition = new java.awt.Point(x, y) ;
		        SwingUtilities.convertPointFromScreen(localMousePosition, viewport); 
		        x = localMousePosition.x ;
		        y = localMousePosition.y; 
		        if(inHandObj != null) 
		        	g.drawImage(inHandVolatileFrame, x, y, null); 
		        else
		        	g.drawImage(ImgUtils.resiz(ImgUtils.getPicture(inHand.getFolder() + inHand.getSelectedState().cursorImg()),30, 30), x, y, null);
	} } }
	//############################################################################
	public void addInvObject(InventoryObject obj) { 
		if(obj.selectedState==null)
			mainGame.addAsh(new Ashe(obj.initialState().id,AshType.IS_IN_STATE,1));
		Utils.log("adding "+obj.title()+" in inventory bar");inventory.add(obj);   }
	public void removeItem(BasicObject basicObject) { Utils.log("removing  "+basicObject.title()+" from inventory bar");inventory.remove(basicObject); mainGame.removeAsh(new Ashe(inHand.id,AshType.HAS_IN_HAND,1));inHand=null; }
	//############################################################################
	public void populateInventory() {if(inventory.size()==0)return; 
		int x = invZone().x() ; int y = invZone().y();
        for(InventoryObject obj : inventory) {
        	obj.setZone(new Zone(x,y,x+30,y+30,ZoneType.RECTANGLE));
        	x+=30;
        	if(x>invZone().x()+invZone().w()-20) {
        		x=invZone().x();
        		y+=30;
        	}
        }
	}
	//############################################################################
	public boolean invObjectHasActed(Point clickPoint) { //sent by maingame
		if(inHand==null)
			return false;
		List<Action> actions = Action.getByDetector(inHand.selectedState );
		for(Action action:actions) {  
			if(mainGame.actionMgr.canDoAction(action,mainGame.player)) {  
				mainGame.actionMgr.execute(action,mainGame.player);
				mainGame.removeAsh(new Ashe(inHand.id,AshType.HAS_IN_HAND,1));
				inHand=null; 
				return true;
			}
		
		}
		return false;
	}
	//############################################################################
	public boolean invObjectHasActedOnCharacter(Point p) { //sent by maingame
		/*if(inHand==null)
			return false;
		GameCharacter charact = mainGame.npgMgr.getCharacter(p);
		
		if(charact == null) 
			return false;
		
		List<Action> actions = Action.getByDetector(charact );
		actions.addAll(Action.getByDetector(charact.getSelectedStat() ) );
		Utils.log("testera"+actions.size());
		for(Action action:actions) {  
			if(mainGame.actionMgr.canDoAction(action,charact)) {  
				mainGame.actionMgr.execute(action,mainGame.player);
				inHand=null; 
				return true;
			}///////////////////useless
		
		}*/
		return false;
	}
 
	//############################################################################
	//############################################################################
	//###########################################################listeners
	
	
	//################################################### 
	//################################## menuBar 
	private void handleClicInventoryBar(int x, int y) { 
		 if(mainGame.actionMgr.isfreezed())return;
		for(InventoryObject obj : inventory)  
			if(obj.zone.isInZone(new Point(x,y)))  {  
				if(inHand == null&& inHandObj == null ) {
					Utils.log("clicked on "+obj.title());
					mainGame.addAsh(new Ashe(obj.id,AshType.HAS_IN_HAND,1));
					inHand=obj; 
					
					 
					return;
				}
				if(inHand != null) {  
					if(inHand.id == obj.id) {
						mainGame.removeAsh(new Ashe(inHand.id,AshType.HAS_IN_HAND,1));
						inHand=null;
						return;
					}
					Utils.log("clicked on "+obj.title()+" with "+inHand.title());
					InventoryObject oldInHand = inHand;
					mainGame.addAsh(new Ashe(obj.id,AshType.IS_CLICKED_ON,1));
					List<Action> actions = Action.getByDetector(obj.selectedState );
					for(Action action:actions)    
						if(mainGame.actionMgr.canDoAction(action,mainGame.player))   
							mainGame.actionMgr.execute(action,mainGame.player);
  
					mainGame.removeAsh(new Ashe(obj.id,AshType.IS_CLICKED_ON,1));
					mainGame.removeAsh(new Ashe(obj.id,AshType.HAS_IN_HAND,1));
					mainGame.removeAsh(new Ashe(oldInHand.id,AshType.HAS_IN_HAND,1));

				} 
		}
		
		
		
		if(inHand != null) {
			Utils.log("invzone clicked with "+inHand.title()); 
			mainGame.removeAsh(new Ashe(inHand.id,AshType.HAS_IN_HAND,1));
			inHand=null; 
			
			
		}
		
	}
	//######################
	private void handleClicDragObjectZone(int x, int y) {
		if(inHand == null)
			return;
		inHandAnim=Animation.get(inHand.getSelectedState().animID());
		
		freezed=true;  
		
		int actualX = mainGame.scrollGame.getViewport().getViewPosition().x;  
		int width = mainGame.frame.getWidth();
		BufferedImage img = ImgUtils.getPicture(invExplorerBg() );
		int w = img.getWidth();
		int h = img.getHeight(); 
		invExplorer.setBounds(width/2-w/2 ,0,w,h);
		itemTitle.setBounds( invZoneTitles().x(),invZoneTitles().y(),invZoneTitles().w(),invZoneTitles().h());
		itemTitle.setText(inHand.title());
		Utils.setSize(invExplorer, w,h);invExplorer.setVisible(true); 
	}
	
	//################################################### 
	//################################## inv explorer
	private void handleZoneClickInvExplorer(int x, int y) {
		 
			for(Zone z: SpecialZoneInv.getByOwner(inHand.getSelectedState() .id)) {//tcheck inv zones
				//inHandAnim=Animation.get(inHand.getSelectedState().animID());
				 Point p = new Point(
 						x-(invExplorer.getWidth()/2-inHandAnim.getActualFrame().getWidth()/2),
 						y-(invExplorer.getHeight()/2-inHandAnim.getActualFrame().getHeight()/2)); 
				 if(z.isInZone(p)  && (((SpecialZoneInv)z).frames.contains(-1) || ((SpecialZoneInv)z).frames.contains(inHandAnim.currentIndex()))) {
					mainGame.actionMgr.handleAction( Action.getByDetector(z ),mainGame.player);
					Utils.log("clic pos is contained in"+inHand.getSelectedState().title());
					inHandAnim=Animation.get(inHand.getSelectedState().animID());//if changed state
					inHandAnim.forwardAll();
					//inHandAnim.setIdleFrame(0);
				} 	 
			//}
			 } 
	}

	//######################################
	private void closInvExplorerIfNeeded(int x, int y) { 
		if(closeInvExplorerZone().isInZone(new Point(x,y))) {
        	freezed=false;
        	invExplorer.setVisible(false); 
        	inHandAnim=null;
        }  
	}
	//######################################
	private void moveObjectInExplorer(int newX, int newY) { 
 	   if( Math.abs(movableX - newX) <10 && Math.abs(movableY - newY) <10 )return; 
 	    if (newX > movableX && newY > movableY)  inHandAnim.rewindOneFrame();  
 	    else if (newX < movableX && newY > movableY) inHandAnim.forwardOneFrame();  
 	    else if (newX > movableX && newY < movableY)  inHandAnim.rewindOneFrame();  
 	    else if (newX < movableX && newY < movableY) inHandAnim.forwardOneFrame();  
 	    movableX = newX;
 	    movableY = newY;
	}int movableX=-1,movableY; 
	
	//###################################################
	//######################################### menu InGame
	
	private void openInGameMenu(int x, int y) {
		freezed=true;   
		int width= mainGame.frame.getWidth();
		BufferedImage img2 = ImgUtils.getPicture(igMenuBg() );
		int w2 = img2.getWidth();
		int h2 = img2.getHeight();
		igMenu.setBounds(width/2-w2/2 ,0,w2,h2); 
		Utils.setSize(igMenu, w2,h2);igMenu.setVisible(true); 
	} 
	private void closeInGameMenuIfNeeded(int x, int y) {
		if(closeMenuZone().isInZone(new Point(x,y))) {
        	freezed=false;
        	igMenu.setVisible(false); 
		}
	} 
	

	//###############################################################################################SQL
	public String bgFolder(){return "games/"+Main .gameName+"/"+Editor.getGameInfo().getString("menubar", "bgname") ;}
	public String invExplorerBg(){return "games/"+Main .gameName+"/"+Editor.getGameInfo().getString("menubar", "invbgname") ;}
	public String igMenuBg(){return "games/"+Main .gameName+"/"+Editor.getGameInfo().getString("menubar", "igmenubg") ;}
	public String exitButtonMenuBg(){return "games/"+Main .gameName+"/"+Editor.getGameInfo().getString("menubar", "exitmenubg") ;}
	public String exitButtonInvExplorerBg(){return "games/"+Main .gameName+"/"+Editor.getGameInfo().getString("menubar", "exitexplorerbg") ;}
	public Zone menuZone() { return Zone.get(Editor.getGameInfo().getInt("menubar","bgzone"));}
	public Zone invZone() { return Zone.get(Editor.getGameInfo().getInt("menubar","invzone"));}
	public Zone dragObjZone() { return Zone.get(Editor.getGameInfo().getInt("menubar","dragobjzone"));}
	public Zone closeMenuZone() { return Zone.get(Editor.getGameInfo().getInt("menubar","closemenuzone"));}
	public Zone closeInvExplorerZone() { return Zone.get(Editor.getGameInfo().getInt("menubar","closeinvexplorerzone"));}
	public Zone igMenuOpenerZone() { return Zone.get(Editor.getGameInfo().getInt("menubar","igmenuopenerzone"));}
 
	  
	public Zone invZoneTitles() { return Zone.get(Editor.getGameInfo().getInt("menubar","invzonetitles"));}
	 


		public void updateBg(String f) { Editor.getGameInfo().duoManager.setValue("menubar", "bgname", f); }
		public void updateInvExplorerBg(String f) { Editor.getGameInfo().duoManager.setValue("menubar", "invbgname", f); }
		public void updateMenuIgBg(String f) { Editor.getGameInfo().duoManager.setValue("menubar", "igmenubg", f); }
		public void updateExitButMenuBg(String f) { Editor.getGameInfo().duoManager.setValue("menubar", "exitmenubg", f); }
		public void updateExitButInvExplorer(String f) { Editor.getGameInfo().duoManager.setValue("menubar", "exitexplorerbg", f); }

		public void updateCloseMenuZone(int z) { Editor.getGameInfo().duoManager.setValue("menubar", "closemenuzone", z+""); }
		public void updateCloseInvExplorerZone(int i) { Editor.getGameInfo().duoManager.setValue("menubar", "closeinvexplorerzone", i+""); }

		public static  enum MenuType {
		     EDIT_BG(0),
		     EDIT_INV_EXPLORER(1),
		     EDIT_IG_MENU(2) ;  
		    private final int valeur; 
		    MenuType(int valeur) { this.valeur = valeur;  } 
		    public int getValeur() {  return valeur; } 
		    public static MenuType from(String s) { 
		    	return from(Integer.parseInt(s));
		    }
		    public static MenuType from(int valeur) {
		        for (MenuType type : values()) {
		            if (type.getValeur() == valeur) {
		                return type;
		            }
		        }
		        throw new IllegalArgumentException("Aucun MenuType correspondant à la valeur : " + valeur);
		    }
		    
		     
		    
		}

}
