package agame;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream; 
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList; 
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities; 
  
import javazoom.jl.player.advanced.AdvancedPlayer; 
import me.nina.gameobjects.Action;
import me.nina.gameobjects.BasicObject;
import me.nina.gameobjects.Chapter;
import me.nina.gameobjects.Character.PosType;
import me.nina.gameobjects.Scene;
import me.nina.gameobjects.SpawnPoint;
import me.nina.gameobjects.Zone;
import me.nina.maker.Ashe;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Main;
import me.nina.maker.Utils;
import me.nina.objects2d.Img;
import me.nina.objects2d.ImgUtils;
import me.nina.objects2d.Pixel;
import me.nina.objects2d.Point;
import me.nina.objects2d.Position;
import me.nina.gameobjects.CharacterState;
import me.nina.gameobjects.GameObject;
import me.nina.gameobjects.InventoryObject;
import me.nina.gameobjects.ObjectState;

public class MainGame extends JPanel implements Runnable { private static final long serialVersionUID = 1L;
	 static double zoom = 1.0;
	//managers
	public MainGameActionManager actionMgr; MainGameObjectsManager objMgr; MainGameTpManager tpMgr; NPGManager npgMgr;  
	  
	//video, panels, thread
	JFrame frame; JScrollPane scrollGame; JPanel videoPanel; Thread updateThread; public static MainGame instance; 
	
	//game engine stuff
	public Connection sql = Main.sql; boolean continuer = false; public String gameName ="game-name"; public GameCharacter player;
	
	//game stuff
	public MenuBar menuBar; List<Ashe>currentAshes = new CopyOnWriteArrayList<>(); 
	public static SoundPlayer soundPlayer = new SoundPlayer();
	
	//Scene stuff
	Scene actualSceneOnScreen; public List<Pixel> priorityArray; BufferedImage backgroundImage; 
	//final Set<Point> colli = new HashSet<>();   
	Ashe sceneExited; 
	 
	//save game stuff
	List<Ashe>savedAshes = new CopyOnWriteArrayList<>(); 
	Scene savedScenePosition; Point savedPlayerPosition;  CharacterState savedPlayerState;  
	 
	//##################################################################
	public MainGame (Chapter ch ) {  
		
	 
		GameObject.reset();
		currentAshes.clear();
		
		instance=this;
		setLayout(null);
		
		//init this for the intro video
		frame = new JFrame("king quest 7 beta") ; 
		
		//when we close the game do this
		frame.addWindowListener(new WindowAdapter() { @Override public void windowClosing(WindowEvent e) { 
			saveBeforeDead();soundPlayer.closeAll(); continuer = false; frame.dispose(); 
			  }}); 
		
		initializeMouseAndPainter(); 
		
		menuBar = new MenuBar(this); 
        Utils.setSize(menuBar, 640, 200); 
        scrollGame = new JScrollPane(videoPanel);
        scrollGame.setViewportView(videoPanel);
        scrollGame.setPreferredSize(new Dimension(640, 480)); 
        scrollGame.setBounds(0,0,640,480); 
        add(menuBar); 
        add(scrollGame); 
        frame.getContentPane().add(this);
        frame.pack(); 
        frame.setVisible(true); 
        frame. setSize(640, 480);
        setVisible(true);
         
        
		 
		actionMgr = new MainGameActionManager(this);
		objMgr= new MainGameObjectsManager(this);
		tpMgr = new MainGameTpManager(this);
		npgMgr=new NPGManager(this); 
		
		changeScene(ch.startScene() ); 
		centerScrollPaneOn(ch.positionStart().x,ch.positionStart().y);
 
		actionMgr.execute(ch.firstAction(), null);
		continuer=true;
		updateThread = new Thread(this);
		updateThread.start();
 
		 
	}
	 
	
	//##################################################################
	private void initializeMouseAndPainter() { 
	    videoPanel = new JPanel() {  private static final long serialVersionUID = 1L; 
			@Override protected void paintComponent(Graphics g) { if(!continuer)return;
	            super.paintComponent(g);  
	    		g.drawImage(backgroundImage, 0,0,this); //draw background
	    		if(!menuBar.freezed  ) {
	    			
	    				objMgr.drawObjectsBackground(g);//if fullscreen obj show this+childs
	    			if(!objMgr.isFullScreen()){//or draw the obje we forced to zindex<50  
	    				npgMgr.paintImage(g);//then, paint player, characters, and object with own zindex
	    				objMgr.drawObjectsFront(g);//then draw the obje we forced to zindex>50
	    			}
		    		menuBar.paintInHandOutsideBar(g,scrollGame); } } }; 
		
		    		videoPanel.addMouseMotionListener(new MouseAdapter() { 
		    	    	@Override public void mouseMoved(MouseEvent e) {
		    	    		if(player!=null)
		    	    		tpMgr.changeCursor(new Point(e.getX(),e.getY()));
		    	    	}});
	    videoPanel.addMouseListener(new MouseAdapter() { @Override public void mousePressed(MouseEvent e) { 
	    	frame.setCursor( null );
	    	  
			if( menuBar.getBounds().contains(e.getPoint()))return;
			SwingUtilities.invokeLater(() -> { 
				if(actionMgr.actualAudio != null)
					try {
						actionMgr.actualAudio.stop();
					} catch (Exception e1) {
						Utils.log("audio nulled");actionMgr.actualAudio=null;
					}
				if(menuBar.freezed || actionMgr.isfreezed()|| !objMgr.isFullScreen()&&actionMgr.freezedWalk || actionMgr.pendingAshesToSet.size()!=0) { 
					Utils.log("clic canceled by freezer"+actionMgr.freezedWalk); return; }
				actionMgr.freezedWalk=false;//if fullscreen dont go to desired position
				if(!player.stairManager.isBusy())
					if(e.getButton() == 1 ) {   
						player.zoneClicked = null;
						player.characterToRejoin=null;
						player.tpClicked=null;
					
						Point p = new Point(e.getPoint().x,e.getPoint().y); 
						if(!objMgr.isFullScreen())if(menuBar.invObjectHasActedOnCharacter(p)) return ; 
 						if(menuBar.invObjectHasActed(p)) return ;  
 						if(objMgr.objectHasBeenClicked(player,p))return;
 						if(tpMgr.tpHasBeenClicked(p,player)) return;  
 						if(!objMgr.isFullScreen())if(npgMgr.clickHasActedOnCharacter(p,player)) return ; 
 						if(!objMgr.isFullScreen()) {
 							 boolean press = true;
 							addAsh(new Ashe(actualSceneOnScreen.id,AshType.SCENE_HAS_BEEN_CLICKED,1));
 							if(actionMgr.handleAction(Action.getByDetector(actualSceneOnScreen), player)) {
 								Utils.log("clic cancel by scene press");
 							 press=false;
 							}
 							removeAsh(new Ashe(actualSceneOnScreen.id,AshType.SCENE_HAS_BEEN_CLICKED,1)); 
 							 
 							for(Zone sp:SpawnPoint.getByOwner(actualSceneOnScreen.id))
 								if(sp.isInZone(p)) {
 									addAsh(new Ashe(sp.id,AshType.ZONE_HAS_BEEN_CLICKED,1));
 									if(actionMgr.handleAction(Action.getByDetector(sp), player)) {
 										Utils.log("clic cancel by spawn press : "+sp.title());
 			 							 press=false;
 									}
 									removeAsh(new Ashe(sp.id,AshType.ZONE_HAS_BEEN_CLICKED,1));
 								}
 						if(press)player.mousePressed(e);
 						}
					} 
					if(e.getButton() ==3 )player.mousePressed(e); 
			 });
			//if(menuBar.inHand!=null&&actionMgr.pendingAshesToSet.size()==0){
				//removeAsh(new Ashe(menuBar.inHand,AshType.HAS_IN_HAND,1));menuBar.inHand=null;} 
			} }  );
	             
	}
 
	//###############################################
	public void centerScrollPaneOn(int characterX, int characterY) {  
	    JViewport viewport = scrollGame.getViewport();
	    int viewX = Math.max(0, characterX - viewport.getWidth() / 2);
	    int viewY = Math.max(0, characterY - viewport.getHeight() / 2); 
	    int sceneWidth = backgroundImage.getWidth();
	    int sceneHeight = backgroundImage.getHeight(); 
	    if (characterX + viewport.getWidth() / 2 > sceneWidth)  viewX = sceneWidth - viewport.getWidth();  
	    if (characterY + viewport.getHeight() / 2 > sceneHeight) viewY = sceneHeight - viewport.getHeight();  
	     
	    viewport.setViewPosition(new java.awt.Point(viewX, viewY));  
	     
	}
	//###############################################
	public void changeScene(Scene destScene ) {  
		 if(player!=null&&player.path!=null) {player.path.clear();player.path=null;}

		if(sceneExited!=null) this.removeAsh(sceneExited);
		if(actualSceneOnScreen!=null) {
			sceneExited=new Ashe(actualSceneOnScreen.line.id,AshType.SCENE_WAS_EXITED,1);
			if(sceneExited!=null)this.addAsh(sceneExited);
		}
			 
		changeScene(destScene,true );
		 ImgUtils.mappe.clear();
		if(player != null) {
			if(sceneExited!=null)//do exit actions if needed
				actionMgr.handleAction( Action.getByDetector(sceneExited.possessor() ),player); 
			
			//do enter actions if needed
			Ashe ashSceneEntered = new Ashe(actualSceneOnScreen.line.id,AshType.SCENE_HAS_BEEN_ENTERED,1);
			this.addAsh(ashSceneEntered);
			actionMgr.handleAction( Action.getByDetector(actualSceneOnScreen ),player);   
			this.removeAsh(ashSceneEntered);
			
			//record the visit
			Ashe ashSceneVisited = new Ashe(actualSceneOnScreen.line.id,AshType.SCENE_HAS_BEEN_VISITED,1);
			if(!this.haveAsh(ashSceneVisited))
				this.addAsh(ashSceneVisited);
			player.stairManager = new GameCharacterStairManager(this,player); 
		}
		 
	}
	//###############################################
	public void changeScene(Scene destScene ,boolean dontDoAshJob) {  
		    actualSceneOnScreen = destScene; 
			if(player!=null)player.actualScene = destScene;
			backgroundImage = actualSceneOnScreen.bgImage() ;
			backgroundImage = ImgUtils.scal(backgroundImage, zoom);
			Utils.setSize(videoPanel,backgroundImage.getWidth(), backgroundImage.getHeight() );
			new Thread(new Runnable() { @Override public void run() {try {  //go faster in the scene 
	              priorityArray = Utils.getMatrix(actualSceneOnScreen, false);
			} catch (Exception e) { 
				priorityArray = null;
				Utils.log("no priority bg found for " + destScene.title());
			}
			} }).start();
		
		objMgr.handleSceneChanged( ); //draw the new ground obj please 
		soundPlayer.stopLoop();
		if(destScene.music().contains(".mid"))
			soundPlayer.playMidi(destScene.getFolder()+destScene.music(),actualSceneOnScreen.replaceSong(),actualSceneOnScreen.loopMusic()!=-998);//and play nice song
		else {
			if(  destScene.replaceSong()) {
				if(soundPlayer.isPlayingMidi())soundPlayer.stopMidi(); 
				soundPlayer.playAudio(destScene.getFolder()+destScene.music(), true,destScene.loopMusic());
			}
		}			
		 
		if(player!=null)player.stairManager = new GameCharacterStairManager(this,player); 
	}
	//##################################################################
	void handleCharacterArrived(GameCharacter c) {
		SwingUtilities.invokeLater(() -> {  
			boolean wasFreeze = actionMgr.freezedWalk;
			if(c.equals(player))
			actionMgr.freezedWalk=false;
			tpMgr.handleCharacterArrived(c); 
			objMgr.handleCharacterArrived(c);
			npgMgr.handleCharacterArrived(c);
			if(!c.equals(player)||wasFreeze)for(Zone a:SpawnPoint.getByOwner(c.actualScene.id))
				if(Utils.getDistanceBetweenPoints(c.feetPosition, a.mid())<20) {
					addAsh(new Ashe(a.id,AshType.HAS_BEEN_COLLIDED,1));
					actionMgr.handleAction(Action.getByDetector(a),c);
					removeAsh(new Ashe(a.id,AshType.HAS_BEEN_COLLIDED,1));
				}
		});
	}
	long test = System.currentTimeMillis();
	//##################################################################
	@Override public void run() { 
		try { while (continuer) {long ll = System.currentTimeMillis()-test; if(ll>100) Utils.log(ll+" outside"); 
			SwingUtilities.invokeAndWait(() -> {  
				if(player!=null && player.feetPosition!=null)this.centerScrollPaneOn( player.feetPosition.x,  player.feetPosition.y);
				long l = System.currentTimeMillis();
				actionMgr.run();
				long l2 = System.currentTimeMillis()-l;
				if(l2>100)Utils.log("action"+l2);l = System.currentTimeMillis();
				npgMgr.run();   
				l2 = System.currentTimeMillis()-l;
				if(l2>100)Utils.log("npg"+l2);l = System.currentTimeMillis();
				if(player!=null)
					player.run();  
				l2 = System.currentTimeMillis()-l;
				if(l2>100)Utils.log("player"+l2);l = System.currentTimeMillis();
				repaint(); 
				l2 = System.currentTimeMillis()-l;
				if(l2>100)Utils.log("repaint"+l2);l = System.currentTimeMillis();

			});
			
				Thread.sleep(100);
				test = System.currentTimeMillis();
	}} catch (Exception e) { e.printStackTrace(); } }
  
	//###############################################
	public void removeAsh(Ashe ash) { 
		String log = "__ash rem in "+currentAshes.size()+ "total ashes";
		int contain = 0; int index=-1; int i = 0;
		for(Ashe a : currentAshes){ 
			if(a!=null&&a.equals(ash)){
				contain++;
				index = i; 
			}
			i++;
		}
		if(index!=-1 && contain !=1){
			currentAshes.remove(index); 
			log=log+". 1/"+contain+" occurence of "+ash.title()+" removed "   ;
			 
		}
		else{
			currentAshes.remove(ash); 
			log=log+". removed : "+ash.title()  ;
		}
		//Utils.log(log+" new size="+currentAshes.size());Utils.log(currentAshes+"");
	} 
	//###############################################
	public void addAsh(Ashe ash) {  
		String log = "__ash add in "+currentAshes.size()+" total list.";
		int occurences=1;
		for(Ashe a : currentAshes)
			if(a!=null&&a.equals(ash))
				occurences++; 	  
		currentAshes.add(ash);
		log=log+" We have now "+occurences+" occurences of "+ash.title();
		//Utils.log(log);Utils.log(currentAshes+"");
	} 
	public int howManyAsh(Ashe ash){
		  int i = 0;
		  for(Ashe a : currentAshes) 
				if(a!=null&&a.equals(ash)) 
					i++; 
		  return i;
	} 
	 
	public boolean haveAsh(Ashe ash) {  return currentAshes.contains(ash); }
	//############################################### 
	//need to be rewritten if future 
	//TODO :
	public void saveBeforeDead(){
		savedAshes = new CopyOnWriteArrayList<>( currentAshes);
		savedAshes.add(new Ashe(player.id,AshType.WILL_BE_SPAWNED,1));
		savedScenePosition = this.actualSceneOnScreen;
		savedPlayerPosition = player.feetPosition; 
		savedPlayerState=player.getSelectedStat(); 
		saveToFile();
	}
	public void restoreBeforeDead(){actionMgr.freezedWalk=false;menuBar.inHand=null;this.npgMgr.allThePnj.clear();player=null;
		menuBar.inventory.clear();GameObject.reset();
		currentAshes = new CopyOnWriteArrayList<>(savedAshes);
		
		for(Ashe a : currentAshes){if(a.type() == AshType.HAS_IN_HAND||a.type() == AshType.SCENE_HAS_BEEN_ENTERED||a.type() == AshType.IS_CLICKED_ON) {
			currentAshes.remove(a);
			continue;
		}
			if(a.type() == AshType.HAS_IN_INVENTORY)
				menuBar.inventory.add((InventoryObject) a.possessor());
			if(a.type()==AshType.IS_IN_STATE ) { 
				if(a.possessor().possessorObj() instanceof BasicObject)
				((BasicObject)a.possessor().possessorObj()).setSelectedState((ObjectState) a.possessor());
				else currentAshes.remove(a);
			}
			if(a.type() == AshType.WILL_BE_SPAWNED) {
				npgMgr.spawnPnj(a);
				currentAshes.remove(a);
	}}
		this.changeScene(savedScenePosition,true);
		player.setPos(savedPlayerPosition); 
		player.setSelectedStat(savedPlayerState);
		centerScrollPaneOn (player.feetX(), player.feetY());
	}
 
	public void saveToFile() {
	    try { Properties props = new Properties(); int i = 0;
	    props.setProperty(new Ashe(player.id,AshType.WILL_BE_SPAWNED,1).toSqlValue()+":"+i, new Ashe(player.id,AshType.WILL_BE_SPAWNED,1).toSqlValue());i++;
	    	for(Ashe ash : currentAshes){
	    		props.setProperty(ash.toSqlValue()+":"+i, ash.toSqlValue());i++;
	    	} 
	    	 
	    	 
	    	props.setProperty("scene", savedScenePosition.line.id+"");
	    	props.setProperty("pos", savedPlayerPosition.x +","+savedPlayerPosition.y);
	    	props.setProperty("state", savedPlayerState.line.id+"");
	    	
	    	for(Entry<Action, Long> a : actionMgr.currentTimers.entrySet())
	    		props.setProperty(new Ashe(a.getKey().line.id,AshType.ACTION_TO_DO_WITH_BEFORE_CONDITION_CHECK,0).toSqlValue(), a.getValue()+"");
	        File f = new File("save");
	        OutputStream out = new FileOutputStream( f ); 
	        props.store(out, "User properties");
	        out.close();
	    }
	    catch (Exception e ) {
	        e.printStackTrace();
	    }
	}
	public void restoreGame() {try {if(!new File("save").exists())return;
		actionMgr.pendingAshesToSet.clear();currentAshes.clear(); 
		FileInputStream fs = new FileInputStream( new File("save") );
		menuBar.inHand=null;
		menuBar.inventory.clear();
		savedAshes = new CopyOnWriteArrayList<>();
		  Properties props = new Properties(); props.load(fs); 
	    	for( Entry<Object, Object> e : props.entrySet()){
	    		String key = (String)e.getKey();
	    		if(key.equals("scene"))savedScenePosition = Scene.get(Integer.parseInt((String) e.getValue())); 
	    		else if(key.equals("state"))savedPlayerState = CharacterState.get(Integer.parseInt((String) e.getValue())); 
	    		else if(key.equals("pos"))savedPlayerPosition = new Position(Integer.parseInt(((String) e.getValue()).split(",")[0]),Integer.parseInt(((String) e.getValue()).split(",")[1]),PosType.BOTTOM.getValeur()); 
	    		else {
	    			Ashe ash = Ashe.fromSqlValue(key);
	    			if(ash.type()==AshType.ACTION_TO_DO_WITH_BEFORE_CONDITION_CHECK)
	    				actionMgr.currentTimers.put( (Action) ash.possessor() , Long.parseLong((String)e.getValue()));
	    			else savedAshes.add(ash);
	    			
	    		}
	    	} 
	    	 fs.close();
	    	 restoreBeforeDead() ;
	    }
	    catch (Exception e ) {
	        e.printStackTrace();
	    }
	}
	public static void log(String string) {
		System.out.println(string);
	}
	  
}

 
	 	
