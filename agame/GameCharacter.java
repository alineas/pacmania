package agame;

import java.awt.AlphaComposite;
import java.awt.Color; 
import java.awt.Graphics;
import java.awt.Graphics2D; 
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
 

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set; 
import me.nina.gameobjects.Character; 
import me.nina.gameobjects.Scene;
import me.nina.gameobjects.TransitionPoint;
import me.nina.gameobjects.Action;
import me.nina.gameobjects.Animation; 
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Animation.AnimationState;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Ashe;
import me.nina.maker.Main;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Utils;
import me.nina.objects2d.Img;
import me.nina.objects2d.ImgUtils;
import me.nina.objects2d.Point;
import me.nina.sqlmanipulation.SQLLine;

public class GameCharacter extends Character {  
	  
	public Scene actualScene;
	Point oldPlayerPosition; Scene oldScene; 
	protected MainGame mainGame;  
	private BufferedImage movingImage;  
	 public List<Point> path; 
	    private int stepIndex;
	public GameCharacterStairManager stairManager; 
	 
	boolean isTalking; 
	boolean isMovinge;
	
	double angle;   
	Point clicPos = new Point (100,100);
	 public GameCharacter characterToRejoin;
	public Zone zoneClicked;
	public PosType thePosTypeOfZoneClicked; 
	public TransitionPoint tpClicked;
	AshType actualAshType;

	public boolean hasRun;
	//#################################################################################### 
	public GameCharacter( MainGame mainGame,SQLLine line ) {
		super(line);
		try {
			this.mainGame=mainGame;  
			actualScene = mainGame.actualSceneOnScreen ;
			stairManager = new GameCharacterStairManager(mainGame,this); 
			setCurrentAnime(new Animation( getSelectedStat() .movingAnimation(getPosTyp()).line),false);  
			movingImage = getCurrentAnime().getActualFrame(); 
		} catch (Exception e) { e.printStackTrace();  }
		 
	 
	}
	//#################################################################################### 
	 public void run() { if (feetPosition==null)return;
		 if(!mainGame.actionMgr.isfreezed() ){  try {
			 if(!isMovinge&&getCurrentAnime().idle==false&&getCurrentAnime().state() == AnimationState.INFINITE_LOOP)
				 goToIdleState();
					if(!isNear(clicPos,10) && isMovinge || stairManager.isBusy()) {  
						getCurrentAnime().unsetIdle(); 
						setCurrentAnime(getSelectedStat() .movingAnimation(getPosTyp()),false);
						getCurrentAnime().run(); 
						moveToMousePosition(clicPos); 
					}  
					else if(isNear(clicPos,10) && isMovinge && !stairManager.isBusy() ){  
						if(path!=null) {
							if ( stepIndex >= path.size()) {
					            path.clear();path=null;
					             
					        }else { 
							clicPos=path.get(stepIndex);
					        stepIndex++; }
							}else {
						goToIdleState(); Utils.log(title()+" idle");
						mainGame.handleCharacterArrived(this);  }
					}else if(!isMovinge&&getCurrentAnime().idle==false&&getCurrentAnime().state() == AnimationState.INFINITE_LOOP)getCurrentAnime().run();  
		 } catch (Exception e) { e.printStackTrace(); }   }   
			 
		 generateMovingImage( );
		 calculateScale(feetY()); 
		 hasRun=true;
		 //if(characterToRejoin!=null)if(isNear(characterToRejoin.feetPosition,90)) {
			//	this.setClicPose(feetPosition);mainGame.handleCharacterArrived(this);}
			
	 }

	//####################################################################################
	 public void paintImage (Graphics g) {  
		 int offsetX = (int) (getCurrentAnime().isReversed() ? getCurrentAnime().posTo().x : getCurrentAnime().posFrom().x);
		 int offsetY = (int) (getCurrentAnime().isReversed() ? getCurrentAnime().posTo().y : getCurrentAnime().posFrom().y);
		
		 int x = (int) (MainGame.zoom*(int) (blitX() -offsetX*(scale+getSelectedStat().scaleCorrection())));
			int y = (int) (MainGame.zoom*(int) (blitY() -offsetY*(scale+getSelectedStat().scaleCorrection()) ));
			movingImage = ImgUtils.scal(movingImage, MainGame.zoom);
		 g.drawImage(movingImage,x,y, getScalledW(), getScalledH(), null);
		
		if(isTalking) {
			Animation talk = this.getSelectedStat().talkAnimation(getPosTyp()); 
			 if(talk==null)
				 return; 
			 
			 talk.run(); 
			 int toffsetX = (int) (talk.isReversed() ? talk.posTo().x : talk.posFrom().x);
		        int toffsetY = (int) (talk.isReversed() ? talk.posTo().y : talk.posFrom().y);
		        int x2 = (int) (x-MainGame.zoom*(toffsetX*(scale+getSelectedStat().scaleCorrection()) ));
				 int y2 = (int) (y -MainGame.zoom*(toffsetY*(scale+getSelectedStat().scaleCorrection()) ));
				 
			 BufferedImage img = ImgUtils.scal2(talk.getActualFrame(),MainGame.zoom*(scale+getSelectedStat().scaleCorrection()));
			 g.drawImage(img, x2,y2, img.getWidth(), img.getHeight() ,null);
		
		}
		//if(path!=null)
		//new PathfindingVisualizer(pathfinding).draw(g, Main .instance.backgroundImage.getWidth(),Main .instance.backgroundImage.getHeight(),path  );
	} 
	 
	//#################################################################################### 
	public void setTalking(boolean talking,boolean goToIdleState) {
		isTalking = talking;
		if(goToIdleState) goToIdleState();
		else getCurrentAnime().unsetIdle();  
	}
	
	public void goToIdleState() { 
		setClicPose(feetPosition); 
		isMovinge = false; 
		Animation idleAnim = getSelectedStat() .idleAnimation(getPosTyp());
		if(idleAnim!=null)
			setCurrentAnime(idleAnim,true);  
		else getCurrentAnime().setIdleFrame(getCurrentAnime().currentIndex());
		 
		 
	}
	//####################################################################################

	public void generateMovingImage( ) {
		
		movingImage = ImgUtils.getCopy(getCurrentAnime().getActualFrame()); 
		movingImage = ImgUtils.scal (movingImage,(scale+getSelectedStat().scaleCorrection())); 
 
			//init some stuff
			
			boolean hasStairs = stairManager.hasStairs();
			int offsetY = 0;
			int from = getCurrentAnime().prioFrom();
			
			if(from!=0){
				int to = getCurrentAnime().prioTo(); 
				double progression= (double)((double)getCurrentAnime().currentIndex()/getCurrentAnime().frameOrder().size());
				offsetY = (int)((from-to)*progression); 
				if(from<to)offsetY = (int)((from-to)*(1-progression)); 
			}
			
			//do some more stuff if we need to draw the transparent pixels of the priorities
			if(mainGame.priorityArray!=null ){
				final int incFeetY = feetY()+  getCurrentAnime().priorityToAddForBypass()+offsetY; 
				final int addx = (int) (blitX() - getCurrentAnime().posFrom().x*(scale+getSelectedStat().scaleCorrection()));
				final int addy = (int) (blitY() - getCurrentAnime().posFrom().y*(scale+getSelectedStat().scaleCorrection()));
				  
				 
				movingImage = Utils.unprioritize(movingImage, addx, addy, incFeetY,hasStairs,this);
		 
			} 
	  } 
	//####################################################################################
	public void calculateScale(int feetY) {   
	    double factor = ((double)actualScene.scalePercents())/100;
	    int HlimiteTop = actualScene.scalefactorTopLimit() ;
	    int HlimiteBot = actualScene.scalefactorBotLimit(); 
	    if (feetY < HlimiteTop) {  scale = factor;   return;  } 
	    else if (feetY > HlimiteBot) { scale = 1.0; return;  } 
	    double m = (1.0 - factor) / (HlimiteBot - HlimiteTop);
	    double b = factor - m * HlimiteTop; 
	    double distanceRatio = (double) stairManager.totalDistanceYwalked / stairManager.totalDistanceY;
	    scale = m * (feetY-distanceRatio) + b;
	    
	}

	//################################################################
	public int getScalledH() { return movingImage.getHeight(); }
	public int getScalledW() { return movingImage.getWidth(); } 
	int blitY() { return feetPosition.y - getScalledH(); } 
	int blitX() { return feetPosition.x - getScalledW()/2; }
	public Point getClicPose() { return clicPos; }
	public void setClicPose(Point clicPose) { 
		isMovinge=true; 
		if(!this.equals(MainGame.instance.player)) {
			this.clicPos = clicPose;
			stairManager.click(); 
			return;
		}
		path = stairManager.pathfinding.findPath(feetPosition, clicPose);
		stepIndex = 0; 
		if(path!=null)
			clicPos=path.get(stepIndex) ;
		else
			this.clicPos = clicPose;
		stairManager.click(); 
	}
	//################################################################
	public boolean isNear(Point p,int distance) { 
		return Utils.getDistanceBetweenPoints(p,  feetPosition) < distance; 
	}
	//################################################################
	public Zone getZone( ) { 
		try {
			return new Zone((int) (blitX()-getCurrentAnime().posFrom().x*scale),
					(int) (blitY()-getCurrentAnime().posFrom().y*scale),
					(int) (blitX()+getScalledW()-getCurrentAnime().posFrom().x*scale),
					(int) (feetPosition.y-getCurrentAnime().posFrom().y*scale),ZoneType.RECTANGLE );
		} catch (Exception e) {e.printStackTrace();
			Utils.log("erreur getzone for "+title());
		}
		return new Zone(0,0,0,0,ZoneType.RECTANGLE);
	} 
 
	 
	//##################################################################
	private void calculatePosType() {if(stairManager.isBusy() || mainGame.actionMgr.isfreezed())return;
		double angleDegrees = Math.toDegrees(angle); 
		if (angleDegrees < 0)   angleDegrees += 360; 
		if (angleDegrees >= 337.5 || angleDegrees < 22.5) { setPosType(PosType.RIGHT);
		} else if (angleDegrees >= 22.5 && angleDegrees < 67.5) { setPosType(PosType.BOT_RIGHT);
		} else if (angleDegrees >= 67.5 && angleDegrees < 112.5) { setPosType(PosType.BOTTOM);
		} else if (angleDegrees >= 112.5 && angleDegrees < 157.5) {  setPosType(PosType.BOT_LEFT);
		} else if (angleDegrees >= 157.5 && angleDegrees < 202.5) { setPosType(PosType.LEFT);
		} else if (angleDegrees >= 202.5 && angleDegrees < 247.5) { setPosType(PosType.TOP_LEFT);
		} else if (angleDegrees >= 247.5 && angleDegrees < 292.5) { setPosType(PosType.TOP);
		} else { setPosType(PosType.TOP_RIGHT); }
	}


 
	
	
	//##################################################################
	//##################################################################
	 
		int offset = 2;
		
		
		public void moveToMousePosition(Point mousePosition) {
		    int deltaX = mousePosition.x - feetPosition.x;
		    int deltaY = mousePosition.y - feetPosition.y;
		    angle = Math.atan2(deltaY, deltaX);
		    calculatePosType();

		    double xComponent = Math.cos(angle);
		    double yComponent = Math.sin(angle);

		    double speed = 10 * 0.20;
		    double test = 10 * scale;
		    if (test > speed)
		        speed = test;
		    stairManager.run();

		    int steps = 10;
		    boolean collisionDetected = false;

		    for (int i = 1; i <= steps; i++) {
		        int newX = feetPosition.x + (int) (speed * xComponent * i / steps);
		        int newY = feetPosition.y + (int) (speed * yComponent * i / steps);

		        Point collisionPoint = getCollision(new Point(newX, newY));
		        if (collisionPoint != null) {
		            collisionDetected = true;
		            break;
		        }

		        if (i == steps) { // Update position if no collision is detected in the final step
		            feetPosition = new Point(newX, stairManager.isBusy() ? feetPosition.y : newY);
		            if (!mainGame.tpMgr.isBusy() && this.equals(mainGame.player))
		                mainGame.centerScrollPaneOn(feetX(), feetY());
		        }
		    }

		    if (collisionDetected) {
		        mainGame.addAsh(new Ashe(collider.line.id, AshType.HAS_BEEN_COLLIDED, 1));
		        mainGame.actionMgr.handleAction(Action.getByDetector(collider), this);
		        mainGame.removeAsh(new Ashe(collider.line.id, AshType.HAS_BEEN_COLLIDED, 1));

		        double originalAngle = angle;
		        double angleIncrement = Math.PI / 16;
		        boolean newPathFound = false;

		        for (int i = 0; i < 25; i++) {
		            double currentAngle = originalAngle + (i % 2 == 0 ? i : -i) * angleIncrement;
		            xComponent = Math.cos(currentAngle);
		            yComponent = Math.sin(currentAngle);

		            for (int j = 1; j <= steps; j++) {
		                int newX = feetPosition.x + (int) (speed * xComponent * j / steps);
		                int newY = feetPosition.y + (int) (speed * yComponent * j / steps);

		                Point newCollisionPoint = getCollision(new Point(newX, newY));
		                if (newCollisionPoint != null) {
		                    break; // Collision detected, try the next angle
		                }

		                if (j == steps) { // Update position if no collision is detected in the final step
		                    feetPosition = new Point(newX, stairManager.isBusy() ? feetPosition.y : newY);
		                    if (!mainGame.tpMgr.isBusy() && this.equals(mainGame.player))
		                        mainGame.centerScrollPaneOn(feetX(), feetY());
		                    newPathFound = true;
		                    break;
		                }
		            }

		            if (newPathFound) {
		                break;
		            }
		        }

		        if (!newPathFound) {
		            this.clicPos = feetPosition;
		             
		            isMovinge = false;
		            Utils.log("Movement stopped due to collision.");
		        }
		    }
		}
		Zone collider;

		
		protected Point getCollision(Point point) {
		    if (mainGame.actionMgr.isfreezed())
		        return null; 
		    List<Zone> collisionnersList = Zone.getByOwner(actualScene.id);
		    List<Zone> activeZones = new ArrayList<>(); 
		    for (Zone collider : collisionnersList) 
		        if (!mainGame.haveAsh(new Ashe(collider.id, AshType.HAS_BEEN_DISABLED, 1)))  
		            activeZones.add(collider);
		    Set<Point> offsetPoints = new HashSet<>();
		    for (int dx = -offset; dx <= offset; dx++) {
		        for (int dy = -offset; dy <= offset; dy++) {
		            offsetPoints.add(new Point(point.x + dx, point.y + dy));
		        }
		    }
		    for (Point p : offsetPoints) {  
		            Point colliderGridPoint = Zone.getGridPointByNormalPoint(p);
		            for (Zone collider : activeZones) {
		                Set<Point> colliderPoints = collider.grid.get(colliderGridPoint);
		                if ( colliderPoints != null && colliderPoints.contains(p) ) {
		                    this.collider = collider;
		                    return p;
		                }
		            }
		         
		    }
		    return null;
		}
	 
	public void mousePressed(MouseEvent e) {Utils.log("pressed");
		if(e.getButton()==1) {
			setClicPose(new Point(e.getX(),e.getY())); 
			stairManager.click();
		}
		if(e.getButton()==3) {  
			feetPosition = new Point(e.getX() ,e.getY() );
			mainGame.centerScrollPaneOn(feetX(), feetY());
		} 
	}

}
class Pathfinding { 
    private static final int SAFETY_MARGIN = 5; // secu bound
    private Point gridLimit;
    private List<Zone> zones;
    private Set<Point> nonWalkablePoints;
	private Point goal;
	private Point start;

	static Map<Integer,Set<Point>> mappe = new HashMap();
    public Pathfinding() {
    	if(!mappe.containsKey(MainGame.instance.actualSceneOnScreen.id)) {
        zones = new ArrayList<>();
        List<Zone> zonList = Zone.getByOwner(MainGame.instance.actualSceneOnScreen.id);
        for (Zone z : zonList) {
            if (Action.getByDetector(z).size() != 0)
                continue;
            zones.add(z);
        }
        
        mappe.put(MainGame.instance.actualSceneOnScreen.id, calculateNonWalkablePoints());
    	}
    	
    	this.gridLimit = new Point(MainGame.instance.backgroundImage.getWidth(),
                MainGame.instance.backgroundImage.getHeight() - MainGame.instance.menuBar.menuZone().h());
    	nonWalkablePoints = mappe.get(MainGame.instance.actualSceneOnScreen.id);
    }


    private Set<Point> calculateNonWalkablePoints() {
        Set<Point> nonWalkable = new HashSet<>();
        for (Zone zone : zones) {
            for (Map.Entry<Point, Set<Point>> entry : zone.grid.entrySet()) {
                Set<Point> points = entry.getValue();
                for (Point point : points) {
                    for (int x = point.x - SAFETY_MARGIN; x <= point.x + SAFETY_MARGIN; x++) {
                        for (int y = point.y - SAFETY_MARGIN; y <= point.y + SAFETY_MARGIN; y++) {
                            nonWalkable.add(new Point(x, y));
                        }
                    }
                }
            }
        }
        return nonWalkable;
    }

    public List<Point> findPath(Point start, Point goal) {
        if (isDirectPathClear(start, goal)) {
            return null;
        }

        PriorityQueue<Node> openSet = new PriorityQueue<>();
        Set<Point> closedSet = new HashSet<>();
        Map<Point, Node> allNodes = new HashMap<>();

        Node startNode = new Node(start, null, 0, heuristic(start, goal));
        openSet.add(startNode);
        allNodes.put(start, startNode);
long now = System.currentTimeMillis();
        while (!openSet.isEmpty()) {if(System.currentTimeMillis()-now >5000)return null;
            Node current = openSet.poll();
            if (current.point.equals(goal)) {
                return  simplifyPath(reconstructPath(current));
            }

            closedSet.add(current.point);

            for (Point neighbor : getNeighbors(current.point)) {
                if (closedSet.contains(neighbor) || !isWalkable(neighbor)) {
                    continue;
                }

                double tentativeG = current.g + 1;
                Node neighborNode;

                if (allNodes.containsKey(neighbor)) {
                    neighborNode = allNodes.get(neighbor);
                    if (tentativeG < neighborNode.g) {
                        neighborNode.g = tentativeG;
                        neighborNode.parent = current;
                        openSet.add(neighborNode);
                    }
                } else {
                    neighborNode = new Node(neighbor, current, tentativeG, heuristic(neighbor, goal));
                    allNodes.put(neighbor, neighborNode);
                    openSet.add(neighborNode);
                }
            }
        }

        return null; // No path found
    }

    private List<Point> reconstructPath(Node goalNode) {
        List<Point> path = new ArrayList<>();
        for (Node node = goalNode; node != null; node = node.parent) {
            path.add(node.point);
        }
        Collections.reverse(path);
        return path;
    }

    private List<Point> getNeighbors(Point point) {
        List<Point> neighbors = new ArrayList<>();
        int[][] deltas = {
            {1, 0}, {-1, 0}, {0, 1}, {0, -1},
            {1, 1}, {-1, -1}, {1, -1}, {-1, 1}
        };
        for (int[] delta : deltas) {
            Point neighbor = new Point(point.x + delta[0], point.y + delta[1]);
            if (isValidPoint(neighbor)) {
                neighbors.add(neighbor);
            }
        }
        return neighbors;
    }

    private boolean isWalkable(Point point) {
        return isValidPoint(point) && !nonWalkablePoints.contains(point);
    }

    private boolean isValidPoint(Point point) {
        return point.x >= 0 && point.y >= 0 && point.x < gridLimit.x && point.y < gridLimit.y;
    }

    private boolean isDirectPathClear(Point start, Point goal) {
        List<Point> linePoints = Utils.getLinePoints(start.x, start.y, goal.x, goal.y);
        for (Point p : linePoints) {
            if (nonWalkablePoints.contains(p)) {
                return false;
            }
        }
        return true;
    }

    private double heuristic(Point a, Point b) {
        return Math.abs(a.x - b.x) + Math.abs(a.y - b.y); // Manhattan distance
    }

    private static class Node implements Comparable<Node> {
        Point point;
        Node parent;
        double g; // cost from start to this node
        double h; // heuristic cost to goal

        public Node(Point point, Node parent, double g, double h) {
            this.point = point;
            this.parent = parent;
            this.g = g;
            this.h = h;
        }

        public double getF() {
            return g + h;
        }

        @Override
        public int compareTo(Node other) {
            return Double.compare(this.getF(), other.getF());
        }
    }
    
    
    

private List<Point> simplifyPath(List<Point> fullPath) {
List<Point> simplifiedPath = new ArrayList<>();
if (fullPath.isEmpty()) return simplifiedPath;

int startIndex = 0;

while (startIndex < fullPath.size()) {
    simplifiedPath.add(fullPath.get(startIndex));
    int endIndex = fullPath.size() - 1;

    for (int i = fullPath.size() - 1; i > startIndex; i--) {
        if (!hasCollision(fullPath.get(startIndex), fullPath.get(i))) {
            endIndex = i;
            break;
        }
    }

    if (endIndex == startIndex) {
        break; // To avoid infinite loop
    }

    startIndex = endIndex;
}

simplifiedPath.add(fullPath.get(fullPath.size() - 1));
return simplifiedPath;
}
private boolean hasCollision(Point p1, Point p2) {
List<Point> linePoints = Utils.getLinePoints(p1.x, p1.y, p2.x, p2.y);
for (Point p : linePoints) {
    if (nonWalkablePoints.contains(p)) {
        return true;
    }
}
return false;
}
}