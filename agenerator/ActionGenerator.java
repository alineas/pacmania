package me.nina.agenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import me.nina.gameobjects.Action;
import me.nina.gameobjects.BasicObject;
import me.nina.gameobjects.CharacterState;
import me.nina.gameobjects.GameObject;
import me.nina.gameobjects.InventoryObject;
import me.nina.gameobjects.ObjectState;
import me.nina.gameobjects.SpecialZoneInv;
import me.nina.gameobjects.Zone; 
import me.nina.editors.InventoryObjectsEditor;
import me.nina.maker.Ashe;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Main;
import me.nina.maker.Utils;
import me.nina.gameobjects.Character;
public class ActionGenerator extends Generator {
 
	private Zone zoneSelected;  
	private boolean waitForObject; 
	private boolean waitForChar;
	private SpecialZoneInv specialZone;
	private boolean waitForZone;

	//###############################################
	public ActionGenerator( ) { super();  }
	
	//###############################################
	public static void generateAction( ) { 
		new Thread(() -> { try {
	            new ActionGenerator().generate(); } catch (Exception e) { e.printStackTrace();   } 
	}).start(); }
	
	//###############################################
	public static void generateInventoryAction( ) { 
		new Thread(() -> { try { 
	            new ActionGenerator().generateInventory(); } catch (Exception e) { e.printStackTrace();   } 
	}).start(); }
	 
	//###############################################
	private void generateInventory() { try {
			int choice1 = ask("were does we click ?:", 
					new String[]{"zone in object viewer", "click on a character", "click on another inventory object"});
			 Action action=null;
			  
			switch (choice1) {
			 
			case 0: //clic on zone
				((InventoryObjectsEditor)Main.actualEditor).editZones=true;
				((InventoryObjectsEditor)Main.actualEditor).populate();
				 
				say("clic the zone");
				pauseGenerator();  
				action = new Action( specialZone.line.id ).save();
				 
						
				int choice = ask("what will happen ?:", 
						new String[]{"change state", "give inventory object and change state"});
			    
			     if(choice==0 || choice==1) { /////////change state (?give item?)
			        	if(choice==1){
			    	    InventoryObject invObjectToGive = Utils.askForInvObj( 
			    			 "Select the inventory object to give" )  ;
			        	action.addAshToSet(new Ashe(invObjectToGive.id,AshType.WILL_HAVE_IN_INVENTORY,1) ); 
			        	}
			    		ObjectState state1 = Utils.askForState(specialZone.possessorObj().possessorObj(), 
			    				"Select the before state") ;
			    		action.addAshNeeded(new Ashe(state1.id,AshType.IS_IN_STATE,1 ));  
			    		ObjectState state2= Utils.askForState(specialZone.possessorObj().possessorObj(), "Select the state to have") ; 	
 			    		action.addAshToSet(new Ashe(state2.id,AshType.WILL_BE_IN_STATE,1 ));   //add inv
 			        	action.updateTitle(BasicObject.get(state1.possessor()).title()+" "+state1.title()+" to "+state2.title());  
			        	new Action(action,false).populateRight();
			         	remGenerator(this);
			        	
				} 
				 
				break;
				 
			case 1://clic on character
				Character character = Utils.askForCharacter(
						"Select the character ");    
				CharacterState characterState = Utils.askForCharacterState(character, 
						"select the state needed or cancel");
				if(characterState== null) 
					action = new Action(  character.id ).save();  
				else action = new Action(  characterState.id ).save(); 
				BasicObject object = Utils.askForInvObj(
						"select the object we need to clic with"); 
				ObjectState objectState = Utils.askForState(object, 
						"select the state needed or cancel");
				if(objectState!= null)  
					action.addAshNeeded( new Ashe(objectState.id,AshType.IS_IN_STATE,1) );   
				action.addAshNeeded(new Ashe(object.id,AshType.HAS_IN_HAND,1)); 
				 
				int choice3 = ask("what will happen ?:", 
						new String[]{"change state of "+character .title(), 
								"change state of "+object.title()});
			
				if(choice3==0) {//change state of character 
					CharacterState charStateSelected =  Utils.askForCharacterState( character , 
							"select the state character will have") ; 
					action.addAshToSet(new Ashe(charStateSelected.id,AshType.WILL_BE_IN_STATE,1)); 
					action.updateTitle(character.title()+" go in state "+charStateSelected.title()+
							" with "+object.title());   
					new Action(action,false).populateRight();
					remGenerator(this); 
				}
				if(choice3==1) {//change state of object  
					ObjectState stateSelected =  Utils.askForState(object, 
							"Select the state it will have") ; 
 					action.addAshToSet( new Ashe(stateSelected.id,AshType.WILL_BE_IN_STATE,1));  
					action.updateTitle(object.title()+" go in state "+stateSelected.title()+
							" after click on "+character.title());   
					new Action(action,false).populateRight();
			  		remGenerator(this);
				}
				break;
				
				
			case 2://clic on another object
				String actionTitle="";
			    	 InventoryObject invObjectInHand = Utils.askForInvObj(
			    			 "Select the inventory object needed in hand" )  ;
			    	 InventoryObject invObjectClickedOn=Utils.askForInvObj(
			    			 "Select the inventory object to click");
			    	 ObjectState stateNeededInClickedObject = Utils.askForState(invObjectClickedOn, 
			    			 "select the state needed for the clicked object or cancel");
			    	 	if(stateNeededInClickedObject==null)
			    	 		action = new Action(invObjectClickedOn.id).save(); 
			    	 	else{
			    		 	action = new Action(stateNeededInClickedObject.id).save(); 
							action.addAshNeeded(new Ashe(stateNeededInClickedObject.id,AshType.IS_IN_STATE,1) );  
						}
			    	 	
			    	 	ObjectState stateNeededInHand = Utils.askForState(invObjectInHand, 
				    			 "select the state needed for the  object in hand or cancel");
			    	 	if(stateNeededInHand!=null)
			    	 		action.addAshNeeded(new Ashe(stateNeededInHand.id,AshType.IS_IN_STATE,1) );
			        	action.addAshNeeded(new Ashe(invObjectClickedOn.id,AshType.IS_CLICKED_ON,1) );  
			    		action.addAshNeeded(new Ashe(invObjectInHand.id,AshType.HAS_IN_HAND,1) );  
			    		ObjectState stateClickedObjWillHave = Utils.askForState(invObjectClickedOn, 
			    				"select the state clicked object will have or cancel");
			    		if(stateClickedObjWillHave!=null){
			    			action.addAshToSet(new Ashe(stateClickedObjWillHave.id,AshType.WILL_BE_IN_STATE,1) ); 
			    			actionTitle = invObjectClickedOn.title()+" go in state "+stateClickedObjWillHave.title();
			    		}
			    		ObjectState stateinHandWillHave = Utils.askForState(invObjectInHand, 
			    				"select the state object in hand will have or cancel");
			    		if(stateinHandWillHave!=null){
			    			action.addAshToSet(new Ashe(stateinHandWillHave.id,AshType.WILL_BE_IN_STATE,1) ); 
			    			actionTitle=actionTitle+" "+invObjectInHand.title()+" go in state "+stateinHandWillHave.title();
			    		}
			    		
			        	
			        	
			        	InventoryObject invObjectSelected=Utils.askForInvObj(
			        			"Select an inventory object to remove if needed");
			        	if(invObjectSelected!=null){
			        		action.addAshToSet(new Ashe(invObjectSelected.id,AshType.HAS_IN_INVENTORY,-1)); 
			        		actionTitle=actionTitle+" "+invObjectSelected.title()+" is removed from inventory";
			        	}
			        	action.updateTitle( actionTitle );  
			        		
			        	if(Main.getUserConfirmation("is the reverse allowed (clic with the other object")){
				    	 	if(stateNeededInHand==null)
				    	 		action = new Action(invObjectInHand.id).save(); 
				    	 	else{
				    		 	action = new Action(stateNeededInHand.id).save(); 
								action.addAshNeeded(new Ashe(stateNeededInHand.id,AshType.IS_IN_STATE,1) );  
							} 
				    	 	
				        	action.addAshNeeded(new Ashe(invObjectInHand.id,AshType.IS_CLICKED_ON,1) );  
				    		action.addAshNeeded(new Ashe(invObjectClickedOn.id,AshType.HAS_IN_HAND,1) );   
				    		if(stateClickedObjWillHave!=null){
				    			action.addAshToSet(new Ashe(stateClickedObjWillHave.id,AshType.WILL_BE_IN_STATE,1) ); 
				    			actionTitle = invObjectClickedOn.title()+" go in state "+stateClickedObjWillHave.title();
				    		} 
				    		if(stateinHandWillHave!=null){
				    			action.addAshToSet(new Ashe(stateinHandWillHave.id,AshType.WILL_BE_IN_STATE,1) ); 
				    			actionTitle=actionTitle+" "+invObjectInHand.title()+" go in state "+stateinHandWillHave.title();
				    		} 
				        	if(invObjectSelected!=null){
				        		action.addAshToSet(new Ashe(invObjectSelected.id,AshType.HAS_IN_INVENTORY,-1)); 
				        		actionTitle=actionTitle+" "+invObjectSelected.title()+" is removed from inventory";
				        	}
				        	action.updateTitle( actionTitle );   	
			        	}
						        	
						 new Action(action,false).populateRight();
			         	remGenerator(this);
			        	
			 
				 
				break; 
			} 
		} catch ( Exception e) { remGenerator(this); e.printStackTrace(); }  }


	
	
	
	
	//###############################################
	@Override protected CompletableFuture<Integer> generate() { try {
			 
			say("Select the zone");
			pauseGenerator();  
			 
			Action action = new Action(zoneSelected.line.id).save();
			     
			int choice = ask("what will happen ?:", 
					new String[]{"change state", "give in inventory and change state"});
			
			 if(choice==0 || choice==1) { /////////change state (?give item?)
			    	
				 	if(choice==1 ){
				 		InventoryObject invObjectSelected = Utils.askForInvObj("Select the inventory object to give");
				 		action.addAshToSet(new Ashe(invObjectSelected.id,AshType.WILL_HAVE_IN_INVENTORY,1) );  
				 	}
			    		 ObjectState state1 = Utils.askForState(zoneSelected.possessorObj().possessorObj(), "Select the before state");
					action.addAshNeeded(new Ashe( state1.id,AshType.IS_IN_STATE,1) );  
					 	
					ObjectState state2=Utils.askForState(zoneSelected.possessorObj().possessorObj(), "Select the state to have");
					action.addAshToSet(new Ashe(state2.id,AshType.WILL_BE_IN_STATE,1) );   
			    	action.updateTitle(zoneSelected.possessorObj().title()+" "+state1.title()+" to "+state2.title());  
			    	  
			    	if(Main.getUserConfirmation("is the reverse allowed ?")) { 
			    		Action action2 = new Action(zoneSelected.line.id).save();
			     		action2.addAshNeeded(new Ashe(state2.id,AshType.IS_IN_STATE,1));  
			    		action2.addAshToSet(new Ashe(state1.id,AshType.WILL_BE_IN_STATE,1));   
			        	action2.updateTitle(BasicObject.get(state1.possessor()).title()+" "+state2.title()+" to "+state1.title());  
			    		 
			     		JFrame j = new JFrame();
			     		j.setSize(300,640);
			     		j.setVisible(true);
			     		j.getContentPane().add(new Action(action2,false));
			     		
			 		}
			    	 
			 		 JFrame j = new JFrame();
			  		j.setSize(300,640);
			  		j.setVisible(true);
			     	j.getContentPane().add(new Action(action,false));
			     	remGenerator(this);
			    	
			}return null;
		} catch ( Exception e) { remGenerator(this); e.printStackTrace(); return null;}  }
	  
	//###############################################
  
		@Override public void onZoneSelected(ArrayList<Zone> zl) {
			
			 JPopupMenu popupMenu = new JPopupMenu(); 
		   	 for (Zone z : zl) {
		            JMenuItem menuItem = new JMenuItem(z.title() );
		            menuItem.addActionListener(e -> {
		            	zoneSelected = z; 
		            	if(z!=null)
		            	resumeGenerator();
		            });
		            popupMenu.add(menuItem);
		        } 
			   	popupMenu.show(Main.actualEditor,Main.actualEditor.getActualX(),Main.actualEditor.getActualY());
		 
			 }


		//###############################################
		@Override public void onObjectSelected(List <BasicObject> bl) {
			if(waitForObject) {  
				 JPopupMenu popupMenu = new JPopupMenu(); 
			   	 for (BasicObject bo : bl) {
			            JMenuItem menuItem = new JMenuItem(bo.title() );
			            menuItem.addActionListener(e -> { 
			            	if(bo!=null)
			            	resumeGenerator();
			            });
			            popupMenu.add(menuItem);
			        } 
				   	popupMenu.show(Main.actualEditor,Main.actualEditor.getActualX(),Main.actualEditor.getActualY());
			 
			}
			
		}

		//###############################################
		@Override public void onStateSelected(ObjectState a) { }
		//###############################################
		@Override public void charSelected(CharacterState characterState) { }
		//###############################################
		@Override public void onInvZoneDrawn(SpecialZoneInv zone) {
			specialZone = zone;
			if(zone!=null )
			resumeGenerator();Utils.log("resumed by invzonedraw"+zone);
		}
	//############################################### 
	@Override public void onClick(int clickedX, int clickedY) { } 
	@Override public void onPressEnter() { resumeGenerator();} 
	@Override public void onFileSelected(String selectedFile) { } 
	@Override public void onPressNumber(int keyCode) { } 
	@Override public void onGenerationComplete(int stateId) { } 
	@Override public void onZoneDrawn(Zone z ) {  } 
}
