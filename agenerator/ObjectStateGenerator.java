package me.nina.agenerator;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import me.nina.gameobjects.Animation;
import me.nina.gameobjects.BasicObject;
import me.nina.gameobjects.Character;
import me.nina.gameobjects.CharacterState;
import me.nina.gameobjects.GameObject;
import me.nina.gameobjects.ObjectState;
import me.nina.gameobjects.SpecialZoneInv;
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Animation.AnimationState;
import me.nina.maker.Main;
import me.nina.maker.Utils;
import me.nina.objects2d.Img;
import me.nina.sqlmanipulation.SQLTable;

public class ObjectStateGenerator extends Generator {

	private BasicObject objectWhoNeedAnewState;
	private int frameSelected,clickedY,clickedX,firstFrame=-1;
	private boolean enterAccepted; 
	private String file; 
	private int lastFrame;
	 Zone zone=null;
	private boolean waitForClick=true;
	
	//###############################################
	public ObjectStateGenerator(BasicObject obj ) {
		super();  
		objectWhoNeedAnewState = obj;
		
	}
	
	 
	 
	
	//###############################################
	public static int generateNewState(BasicObject obj) {  new Thread(() -> {
		    new ObjectStateGenerator(obj).generateNewState();
	}).start(); return -1; } 
	
 
	
	 
	//###############################################################
	private CompletableFuture<Integer> generateNewState() {  try {
			 
			String title = Main.getUserInput(); 
			if(title==null)return null; 
			ObjectState theNewState = new ObjectState(title).save(); 
			theNewState.updatePossessor(objectWhoNeedAnewState);
			say("Select a file");
			pauseGenerator();
			file = Main.fileDisplayer.getUpdatedImage(objectWhoNeedAnewState.getFolder(), null);
			theNewState.updateTitle(title); 
			  
			int choice = ask("in this state, the object will be:", 
					new String[]{"Invisible", "Immobile","Immobile with transition", "Infinite Loop"} );
 
			ObjectState stateFrom;
			switch (choice) {
			    case 0: frameSelected = AnimationState.INVISIBLE.getValeur(); 
			    		stateFrom = Utils.askForState(objectWhoNeedAnewState,"select corresponding state");
			    		theNewState.updateStateFrom( stateFrom);
			    	break;  
			    case 1: frameSelected = Utils.askForFrame(objectWhoNeedAnewState.getFolder()+ file, "select idle frame",true);
			    		stateFrom = Utils.askForState(objectWhoNeedAnewState,"select corresponding state");
			    		theNewState.updateStateFrom( stateFrom);
			    	break;
			    case 2: stateFrom = Utils.askForState(objectWhoNeedAnewState,"select corresponding state");
			    		theNewState.updateStateFrom( stateFrom);
			    		firstFrame = Utils.askForFrame(objectWhoNeedAnewState.getFolder()+ file, "select first frame of the transition",true  );
			    		lastFrame = Utils.askForFrame(objectWhoNeedAnewState.getFolder()+ file, "select last frame of the transition",true  );
			    		frameSelected = AnimationState.LOOP.getValeur(); 
			    	break;
			    case 3: frameSelected = AnimationState.INFINITE_LOOP.getValeur(); 
			    		stateFrom = Utils.askForState(objectWhoNeedAnewState,"select corresponding state");
			    		theNewState.updateStateFrom( stateFrom);
			    	break;
			    default : return null;
			}
			int animTransitionId = Animation.generateAnimation( objectWhoNeedAnewState, title, frameSelected, file); 
			theNewState.updateAnimID(animTransitionId);
			
			//say("Click at the position of the state or press enter to use the initial state's position");
			clickedX = objectWhoNeedAnewState.initialState().pos().x; 
			clickedY = objectWhoNeedAnewState.initialState().pos().y;
			//enterAccepted = true; pauseGenerator(); enterAccepted = false;
			theNewState.updatePos(clickedX, clickedY,0);
 
			//say("Click at the required player position for interaction with the state or press enter to use the initial state's position");
			clickedX = objectWhoNeedAnewState.initialState().requiredPlayerPosition().x; 
			clickedY = objectWhoNeedAnewState.initialState().requiredPlayerPosition().y;
			//enterAccepted = true; pauseGenerator(); enterAccepted = false;
			theNewState.updateRequiredPlayerPosition(clickedX , clickedY,0 );
 
			//Ash.setAsh(title, AshType.ACTUAL_STATE,false,theNewState);
			//Ash.setAsh(theNewState.title()+"_actual_position", AshType.ACTUAL_POSITION, false, theNewState); 
			
			if(firstFrame!=-1) {//we are in a single loop, make animation from->to
				Animation theAnim = Animation.get(animTransitionId);
				theAnim.resetOrder();
				List<Integer> order = generateTransitionList(theAnim.frameOrder(),firstFrame,lastFrame);
				theAnim.updateOrder(order);
				objectWhoNeedAnewState.setSelectedState( stateFrom );
				Main.actualEditor.populate(); 

			}
			remGenerator(this);
			Main.actualEditor.populate();
			return CompletableFuture.completedFuture(-999);
		} catch (Exception e) { remGenerator(this); e.printStackTrace();return null; } }
	//###############################################################
	public static List<Integer> generateTransitionList(List<Integer> orderedFrames, int firstFrame, int lastFrame) {
        List<Integer> transitionList = new ArrayList<>();
        
        if (firstFrame <= lastFrame) {
            // Cas o� firstFrame est inf�rieur ou �gal � lastFrame
            for (int i = firstFrame; i <= lastFrame; i++) {
                transitionList.add(orderedFrames.get(i));
            }
        } else {
            // Cas o� firstFrame est sup�rieur � lastFrame (transition � l'envers)
            for (int i = firstFrame; i >= lastFrame; i--) {
                transitionList.add(orderedFrames.get(i));
            }
        }
        
        return transitionList;
    }
	
	
	//###############################################################
 

     
  //###############################################################
	@Override public void onFileSelected(String selectedFile) {  resumeGenerator(); }
	@Override public void onClick(int clickedX, int clickedY) { if(waitForClick) {this.clickedX=clickedX; this.clickedY=clickedY; resumeGenerator();} }
	//@Override public void onFrameSelected(int i) { frameSelected=i; resumeGenerator(); } 
	@Override public void onPressEnter() { if(enterAccepted)resumeGenerator(); }
 
	//###############################################################
	@Override public void onGenerationComplete(int id) { } 
	@Override protected CompletableFuture<Integer> generate() { return null; } 
	@Override public void onZoneDrawn(Zone z) { } 
	@Override public void onZoneSelected(ArrayList<Zone> zl) { } 
	@Override public void onObjectSelected(List <BasicObject> l) { } 
	@Override public void onStateSelected(ObjectState a) { }
	@Override public void onPressNumber(int keyCode) { } 
	@Override public void charSelected(CharacterState character) { } 
	@Override public void onInvZoneDrawn(SpecialZoneInv zone) { }
 
}
