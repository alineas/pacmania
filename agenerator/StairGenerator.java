package me.nina.agenerator;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
 

import me.nina.editionnodes.StairsNode; 
import me.nina.gameobjects.BasicObject;
import me.nina.gameobjects.Character;
import me.nina.gameobjects.CharacterState;
import me.nina.gameobjects.ObjectState;
import me.nina.gameobjects.SpecialZoneInv;
import me.nina.gameobjects.Stair;
import me.nina.gameobjects.StairList;
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Main;

public class StairGenerator extends Generator {

	boolean clickOnPriorities;
	public static Stair temp;
	StairsNode node;
	
	//####################################
	public StairGenerator(StairsNode s) { 
		super();
		
		node = s;
		
	}
	
	//####################################
	public int generateStair() { 
		new Thread(() -> {
			
		    generate();
		}).start();
		return -1;
	}
	Zone old = null;
	//####################################
	@Override protected CompletableFuture<Integer> generate() {
		temp = new Stair();
		try {
			boolean isColimacon = old!=null||!Main.getUserConfirmation("horizontal stair ?");
			
			
			if(isColimacon){Main.leftController.pencilList.setSelectedItem(ZoneType.LINE);
				if(old==null){
					say(" draw first detector line " ); 
					pauseGenerator();
					for(Zone a : node.zones) 
						temp.primaryZones.add(new StairList(a));  
					node.zones.clear();
				}
				else
					temp.primaryZones.add(new StairList(old)); //secondary of last stair become primary
		   
				say( "draw second detector line " );
				pauseGenerator(); 
				 
				for(Zone a : node.zones){ 
						old = a;
					temp.secondaryZones.add(new StairList(a)); 
				}
				node.zones.clear();
			}
			else{
				Main.leftController.pencilList.setSelectedItem(ZoneType.ARC);
				say("draw arcs representing the upper level of this stair, and press enter   " ); 
				pauseGenerator();
				for(Zone a : node.zones) 
					temp.primaryZones.add(new StairList(a));  
			 	node.zones.clear();
			 	say( "draw arcs representing the bottom level of this stair, invisible and imagined, and press enter  " );
			 	pauseGenerator();  
				for(Zone a : node.zones) 
					temp.secondaryZones.add(new StairList(a));  
				node.zones.clear();
			}
			for(StairList a : temp.primaryZones)
				a.save(Main.actualEditor.scene.id, "primaries zones of the stair");
			for(StairList a : temp.secondaryZones)
				a.save(Main.actualEditor.scene.id, "secondaries zones of the stair");
			
			
			say("click right on the priority zones who can hide the character accross this stair, then press enter " );
			clickOnPriorities = true;
			pauseGenerator();
			clickOnPriorities = false;
			node.generatorIsRunning=false;
			say(" from bottom to top, what is the level of the stair ? press 0-9 keyboard " );
			pauseGenerator(); 
			 
			for(Zone a : Stair.getByOwner(Main.actualEditor.scene.id))
				for(int b : ((Stair)a).priorities)
					temp.priorities.add(b);
			temp.save(Main.actualEditor.scene.id, "stair_"+temp.level); 
			temp=new Stair();
			node.repopulate();
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
		return null;
	}


	//####################################
	@Override public void onClick(int x, int y) {
		if( clickOnPriorities )
			if(!temp.priorities.contains(Main.actualEditor.imagePri.getRGB(x,y)))
			temp.priorities.add(Main.actualEditor.imagePri.getRGB(x,y));
		
	}

	
	//#################################### 
	@Override public void onPressEnter() {
		resumeGenerator();
	}
 
	//####################################
	@Override public void onPressNumber(int keyCode) {
		boolean resume = true;
	    switch (keyCode) {
	    
        case KeyEvent.VK_0: temp.level = 0; break;
        case KeyEvent.VK_1: temp.level = 1; break;
        case KeyEvent.VK_2: temp.level = 2; break;
        case KeyEvent.VK_3: temp.level = 3; break;
        case KeyEvent.VK_4: temp.level = 4; break;
        case KeyEvent.VK_5: temp.level = 5; break;
        case KeyEvent.VK_6: temp.level = 6; break;
        case KeyEvent.VK_7: temp.level = 7; break;
        case KeyEvent.VK_8: temp.level = 8; break;
        case KeyEvent.VK_9: temp.level = 9; break;
        default: resume = false; break;
    }
	    if(resume)resumeGenerator();
	}
	
	//####################################
	@Override public void onFileSelected(String selectedFile) { }
	@Override public void onGenerationComplete(int id) { } 
	@Override public void onZoneDrawn(Zone z) { } 
	@Override public void onZoneSelected(ArrayList<Zone> zl) { } 
	@Override public void onObjectSelected(List <BasicObject> l) { } 
	@Override public void onStateSelected(ObjectState a) { }

	@Override
	public void charSelected(CharacterState character) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onInvZoneDrawn(SpecialZoneInv zone) {
		// TODO Auto-generated method stub
		
	}
 
}
