package me.nina.agenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.swing.JOptionPane;

import me.nina.editors.Editor;
import me.nina.gameobjects.Action;
import me.nina.gameobjects.Animation;
import me.nina.gameobjects.BasicObject;
import me.nina.gameobjects.Character;
import me.nina.gameobjects.CharacterState;
import me.nina.gameobjects.ObjectState;
import me.nina.gameobjects.SpecialZoneInv;
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Animation.AnimationState;
import me.nina.gameobjects.BasicObject.ObjectType;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.gameobjects.GameObject;
import me.nina.maker.Ashe;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Main;
import me.nina.maker.Utils;
import me.nina.sqlmanipulation.SQLTable;

public class FullScreenObjectGenerator extends Generator{ 

    private String file;
	private Zone zone;
	private int clickedX;
	private int clickedY;
	private boolean waitForZone;

	//###############################################################
	public FullScreenObjectGenerator( ) { super();  }
 
	//###############################################################
	public static void generateFullscreenObject() { new Thread(() -> {
		    new FullScreenObjectGenerator( ).generate();
	}).start(); }
	 
	//###############################################################
    @Override  protected CompletableFuture<Integer> generate() { try {  
    	//get a title
    	String title = Main.getUserInput(); 
		if(title==null) 
			return CompletableFuture.completedFuture(-1); 
		
		BasicObject theFullScreenObject = new BasicObject(title).save(); 
		theFullScreenObject.setSelected(true);
		theFullScreenObject.updateZindex(98); 
		theFullScreenObject.updatePossessor(Main.actualEditor.scene); 
		theFullScreenObject.updateType(ObjectType.GROUND);  
		say("Select a file");
    	pauseGenerator();
    	 
    	file = Main.fileDisplayer.getUpdatedImage(theFullScreenObject.getFolder(), null);
   
		ObjectState state = new ObjectState("initial").save();
		state.updatePossessor(theFullScreenObject);   
		state.updatefullScreen(false);
		  
		say("Click at the top left position of the image");
		pauseGenerator(); 
		state.updatePos(clickedX,clickedY,0);  
		
		say("Click at the required player position");
		pauseGenerator(); 
		state.updateRequiredPlayerPosition(clickedX,clickedY,0);   
		theFullScreenObject.updateInitialState(state.id);
		theFullScreenObject.initialState().updateAnimID(Animation.generateAnimation(
				(GameObject)theFullScreenObject, theFullScreenObject.initialState().title(),
				AnimationState.INVISIBLE.getValeur(), file));
		//Ash.setAsh(theFullScreenObject.title()+"_is_disabled",
		//		AshType.IS_DISABLED,Main.getUserConfirmation("disabled at game start ?"),theFullScreenObject); 
		
		ObjectState invisibleState = theFullScreenObject.initialState() ; 
 
		int visibleAnimId = Animation.generateAnimation(
				(GameObject)theFullScreenObject,title+"_is_fullscreen",0,file); 
 	
		ObjectState visibleState = new ObjectState(title+"_fullscreen").save();   
		visibleState.updatePossessor(theFullScreenObject);   
		visibleState.updatefullScreen(true);  
		visibleState.updateAnimID(visibleAnimId);  
		visibleState.updatePos(theFullScreenObject.initialState().pos().x,theFullScreenObject.initialState().pos().y,0);  
		visibleState.updateRequiredPlayerPosition(theFullScreenObject.initialState().requiredPlayerPosition().x,theFullScreenObject.initialState().requiredPlayerPosition().y,0); 
		  
		//Ash.setAsh(theFullScreenObject.title()+"_fullscreen",AshType.ACTUAL_STATE,false,visibleState);
		  
		waitForZone = true;
		say("draw the clickable zone for open it");
	    pauseGenerator();
	    waitForZone=false;
	    
	    zone.setType(ZoneType.RECTANGLE);
	    int zoneId = zone.save(theFullScreenObject.initialState().id,"open");
		
	    
	    Action action = new Action(zoneId).save(); 
 		action.updateTitle(title+"_become_visible");
 		action.addAshToSet(new Ashe(visibleState.id,AshType.WILL_BE_IN_STATE,1) );    
 		action.addAshNeeded(new Ashe(invisibleState.id,AshType.IS_IN_STATE,1)); 
		 
		title="exit_button";
		BasicObject exitObject = new BasicObject(title).save();  
		exitObject.setSelected(true);
		exitObject.updateZindex(99);
		exitObject.updateType(ObjectType.GROUND);
		exitObject.updatePossessor(Main.actualEditor.scene); 
		 
		theFullScreenObject.addChild(exitObject.id);
        say("select an image for exit button");
        pauseGenerator();
        file = Main.fileDisplayer.getUpdatedImage(exitObject.getFolder(), null); 
         
          state = new ObjectState("initial").save(); 
		state.updatePossessor(exitObject );   
		state.updatefullScreen(false);
    
		say("Click at the top left position of the exit button");
		pauseGenerator(); 
		state.updatePos(clickedX,clickedY,0);  
		state.updateRequiredPlayerPosition(0,0,0); 
	  
		//Ash.setAsh(exitObject.title()+"_initial_state",AshType.ACTUAL_STATE,true, state );
		
		exitObject.updateInitialState(state.id);
		exitObject.initialState().updateAnimID(Animation.generateAnimation(
				(GameObject)exitObject, exitObject.title()+"_initial_state",AnimationState.MONO.getValeur(), file));
		   
		 Main.actualEditor.populate(); 
		
        waitForZone = true;
        theFullScreenObject.setSelectedState(visibleState);
        JOptionPane.showMessageDialog(null, "draw the zone for close it", "", JOptionPane.INFORMATION_MESSAGE);
	    pauseGenerator();waitForZone=false;
	    zone.setType(ZoneType.RECTANGLE);
	    zoneId = zone.save(visibleState.id,"close");
	    
	    action = new Action(zoneId).save(); 
 		action.updateTitle(title+"_become_invisible");
 		action.addAshToSet(new Ashe(invisibleState.id,AshType.WILL_BE_IN_STATE,1) );  
 		action.addAshNeeded(new Ashe(visibleState.id,AshType.IS_IN_STATE,1));         remGenerator(this);
} catch (Exception e) {remGenerator(this);e.printStackTrace();}
    Main.actualEditor.populate();
		return CompletableFuture.completedFuture(0);
		
		 
    }
  //###############################################################
    @Override public void onFileSelected(String selectedFile) {  resumeGenerator(); }
    
  //###############################################################
	@Override public void onClick(int clickedX, int clickedY) {
		if(waitForZone)return;
		this.clickedX=clickedX;
		this.clickedY=clickedY;
		resumeGenerator();
		} 
	
	//###############################################################
	
	@Override public void onZoneDrawn(Zone z) {this.zone= z; if(z!=null)resumeGenerator(); }
	
	//###############################################################
	@Override public void onPressEnter() { } 
	@Override public void onPressNumber(int keyCode) { } 
	@Override public void onGenerationComplete(int id) { } 
	@Override public void onZoneSelected(ArrayList<Zone> zl) { } 
	@Override public void onObjectSelected(List <BasicObject> l) { } 
	@Override public void onStateSelected(ObjectState a) { } 
	@Override public void charSelected(CharacterState character) { } 
	@Override public void onInvZoneDrawn(SpecialZoneInv zone) { }

 
}