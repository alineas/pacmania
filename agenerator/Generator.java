package me.nina.agenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
 
import me.nina.gameobjects.BasicObject;
import me.nina.gameobjects.Character;
import me.nina.gameobjects.CharacterState;
import me.nina.gameobjects.InventoryObject;
import me.nina.gameobjects.ObjectState;
import me.nina.gameobjects.SpecialZoneInv;
import me.nina.gameobjects.Zone;
import me.nina.maker.Main;

public abstract class Generator {
	
    private static List<Generator> generators = new ArrayList<>(); 
    public static void addGenerator(Generator generator) {  generators.add(generator); }   
    public static void remGenerator(Generator generator) {  generators.remove(generator); }   
    private final Lock lock = new ReentrantLock();
    private final Condition condition = lock.newCondition();   
     
    public Generator() { 
    	addGenerator(this);
    }
    protected void pauseGenerator() {
        lock.lock();
        try { condition.await(); } catch (InterruptedException e) { Thread.currentThread().interrupt();  } 
        finally { lock.unlock(); }
    }
 
    protected void resumeGenerator() {
        lock.lock(); try { condition.signal(); } finally { lock.unlock();  }
    }
    
	protected int ask(String question, String[] options) {
		 return JOptionPane.showOptionDialog(
	                null, question, "Options",
	                JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE,
	                null, options,
	                null);
	}
	protected void say(String msg) {
		JOptionPane.showMessageDialog(null, msg, "", JOptionPane.INFORMATION_MESSAGE);

	}
	
	public abstract void onClick(int clickedX, int clickedY);
	public abstract void onPressEnter();  
    public abstract void onFileSelected(String selectedFile);
    protected abstract CompletableFuture<Integer> generate();
	public abstract void onPressNumber(int keyCode);
	public abstract void onGenerationComplete(int stateId);
	public abstract void onZoneDrawn(Zone z);
	public abstract void onZoneSelected(ArrayList<Zone> zl);
	public abstract void onObjectSelected(List<BasicObject> bl);
	public abstract void onStateSelected(ObjectState a);
	public static void zoneDrawn(Zone z) {
		for(Generator a:generators)
			a.onZoneDrawn(z); 
	}

	public static void zoneSelected(ArrayList<Zone> zl) {
		for(Generator a:generators)
			a.onZoneSelected(zl); 
	}
	public static void objectSelected(List<BasicObject> bl) {
		for(Generator a:generators)
			a.onObjectSelected(bl); 
	}
	public static void stateSelected(ObjectState st) {
		for(Generator a:generators)
			a.onStateSelected(st); 
	}
	public static void pressEnter() {
		for(Generator a:generators)
			a.onPressEnter(); 
	}
	public static void fileSelected(String name) {
		for(Generator a:generators)
			a.onFileSelected(name); 
	}
	public static void click(int clickedX, int clickedY) {
		for(Generator a:generators)
			a.onClick(clickedX,clickedY); 
	}
	public static void pressNumber(int keyCode) {
		for(Generator a:generators)
			a.onPressNumber(keyCode); 
	}
	public abstract void charSelected(CharacterState character);
	public static void charSelect(CharacterState character) {
		for(Generator a:generators)
		a.charSelected(character);
		
	}
	public static void zoneDrawnInvEditor(SpecialZoneInv z) {
		for(Generator a:generators)
		a.onInvZoneDrawn(z);
		
	}
	public abstract void onInvZoneDrawn(SpecialZoneInv zone);
 
}