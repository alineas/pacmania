package me.nina.agenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
 

import me.nina.editors.Editor;
import me.nina.editors.MenuBarEditor;
import me.nina.gameobjects.BasicObject;
import me.nina.gameobjects.Character;
import me.nina.gameobjects.CharacterState;
import me.nina.gameobjects.ObjectState;
import me.nina.gameobjects.SpecialZoneInv;
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Main;
import me.nina.sqlmanipulation.DuoArray;

public class MenuBarGenerator extends Generator {
	 
	private boolean waitForZone;
	private Zone bgZone;
	private String bgName;
	private Zone tempZone;
	private Zone invZone;
	private Zone dragObjZone;
	private Zone igMenuOpenerZone; 
	private Zone invZoneTitles;

	//###############################################################
	public MenuBarGenerator() { super(); }//TODO: add vertical choice and hidden contextual
	
	//###############################################################
	public static void generateMenuBar() { new Thread(() -> {
		new MenuBarGenerator( ).generate();
	}).start();  }
 
	//###############################################################
	@Override protected CompletableFuture<Integer> generate() { try {
			waitForZone = true;
			say("draw the zone of the menu");
			pauseGenerator();waitForZone=false;
			bgZone = tempZone;
			
			say("Select a file");
			pauseGenerator();
			int i = tempZone.save(-1, "");
			bgName = Main.fileDisplayer.getUpdatedImage("games/"+Main.gameName+"/", null);
			Editor.getGameInfo().duoManager.addInArray("menubar", new DuoArray<String,String>("bgname",bgName));
			Editor.getGameInfo().duoManager.addInArray("menubar", new DuoArray<String,String>("bgzone",i+"")) ;
			Main.setActualEditor( new MenuBarEditor());
			Main.revalidate();
			
			Main.actualEditor.setImage("games/"+Main.gameName+"/"+Editor.getGameInfo().duoManager.getString("menubar", "bgname"));
   
			waitForZone = true;
			say("draw the zone of the inventory");
			pauseGenerator();waitForZone=false;
			invZone = tempZone;
			
			
			
			waitForZone = true;
			say("draw the zone who explore the object");
			pauseGenerator();waitForZone=false;
			dragObjZone = tempZone;
			
			
			
			waitForZone = true;
			say("draw the zone for open ingame menu");
			pauseGenerator();waitForZone=false;
			igMenuOpenerZone = tempZone;
			
			
			waitForZone = true;
			say("draw the zone for open ingame menu");
			pauseGenerator();waitForZone=false;
			invZoneTitles = tempZone;
			
			
 
			i = invZone.save(-1, "");
			Editor.getGameInfo().duoManager.addInArray("menubar", new DuoArray<String,String>("invzone",i+""));
			i = dragObjZone.save(-1, "");
			Editor.getGameInfo().duoManager.addInArray("menubar", new DuoArray<String,String>("dragobjzone",i+""));
			i = igMenuOpenerZone.save(-1, "");
			Editor.getGameInfo().duoManager.addInArray("menubar", new DuoArray<String,String>("igmenuopenerzone",i+""));
			i = invZoneTitles.save(-1, "");
			Editor.getGameInfo().duoManager.addInArray("menubar", new DuoArray<String,String>("invzonetitles",i+""));
			
			remGenerator(this);
			return null;
		} catch (Exception e) { remGenerator(this); e.printStackTrace();return null; } }
	
	//###############################################################
    @Override public void onFileSelected(String selectedFile) {  resumeGenerator(); }
	@Override public void onClick(int clickedX, int clickedY) { if(waitForZone)return;  resumeGenerator(); } 
	@Override public void onZoneDrawn(Zone z) { this.tempZone= z; if(z!=null)resumeGenerator();  }
	
	//###############################################################
	@Override public void onPressEnter() { }  
	@Override public void onPressNumber(int keyCode) { } 
	@Override public void onGenerationComplete(int id) { }
	@Override public void onZoneSelected(ArrayList<Zone> zl) { }
	@Override public void onObjectSelected(List <BasicObject> l) { }
	@Override public void onStateSelected(ObjectState a) {  } 
	@Override public void charSelected(CharacterState character) { } 
	@Override public void onInvZoneDrawn(SpecialZoneInv zone) { }
 
	
}