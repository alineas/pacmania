package me.nina.agenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import me.nina.editors.Editor;
import me.nina.gameobjects.Action;
import me.nina.gameobjects.Animation; 
import me.nina.gameobjects.BasicObject;
import me.nina.gameobjects.BasicObject.ObjectType;
import me.nina.gameobjects.CharacterState;
import me.nina.gameobjects.InventoryObject;
import me.nina.gameobjects.ObjectState;
import me.nina.gameobjects.SpecialZoneInv;
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Animation.AnimationState; 
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Ashe;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Main;
import me.nina.maker.Utils;

public class BasicObjectGenerator extends Generator {
  
	private BasicObject owner = null;
	private int clickedX, clickedY;
	private int type;   

	//###############################################################
	public BasicObjectGenerator(int ownerID, int type ) {
		super(); 
		this.type=type; 
		if(ownerID!=-1)
			owner  = BasicObject.get(ownerID); //used for childs objects
	}
	
	//###############################################################
	public static void generateBasicObject(int i) { new Thread(() -> {   try {
	    new BasicObjectGenerator(i,0).generate();  } catch (Exception e) { e.printStackTrace();   }  
	}).start(); }
	 
	//###############################################################
	public static void generateGroundToInvObject(int i) { new Thread(() -> {   try {
	    new BasicObjectGenerator(i,1).generate ();  } catch (Exception e) { e.printStackTrace();   }  
	}).start(); }
	
	//###############################################################
	@Override protected CompletableFuture<Integer> generate() { try {  
		BasicObject theObject = null;
		if(type==0)theObject = generateSimpleGroundObject(); 
		if(type==1)theObject = generateGroundToInventoryObject(); 
					
		remGenerator(this);
		return CompletableFuture.completedFuture(theObject.id);
	} catch (Exception e) { remGenerator(this); e.printStackTrace();return null;   } }
	
	
	
	
	
	
	
	//###############################################################
	private BasicObject generateGroundToInventoryObject() { try { 
			BasicObject theObject = generateSimpleGroundObject();
			int invisibleState =  generateInvisibleState(theObject,false);
			int idZone = Zone.getByOwner(theObject.initialState().id).iterator().next().id ; 
			Action action = new Action(idZone).save(); 
			InventoryObject obj = Utils.askForInvObj( "Select the inventory object to give");
			action.addAshToSet(new Ashe(obj.id,AshType.WILL_HAVE_IN_INVENTORY,1) );   
			action.addAshNeeded(new Ashe(theObject.initialState().id,AshType.IS_IN_STATE,1) ); 
			action.addAshToSet(new Ashe( invisibleState ,AshType.WILL_BE_IN_STATE,1));   //add inv
			action.updateTitle(theObject.title()+" "+theObject.initialState().title()+" to "+ObjectState.get(invisibleState).title());  
			remGenerator(this); 
			return theObject;
		} catch (Exception e) { remGenerator(this); e.printStackTrace(); return null;} }
	  
	//###############################################################
		private BasicObject generateSimpleGroundObject() { try { 
				String title = Main.getUserInput(); 
				if(title==null)return null;
				BasicObject theObject = new BasicObject(title).save(); 
				if(owner==null)theObject.updateZindex(1);
				else { theObject.updateZindex(98);
					   owner.addChild(theObject.id); }
				theObject.updatePossessor(Main.actualEditor.scene);
				theObject.updateType( ObjectType.GROUND);  
				int initialState = generateInitialState(theObject,true);
				theObject.updateInitialState( initialState ); 
				theObject.setSelected(true);
				Main.actualEditor.populate();  
				ObjectState state = ObjectState.get(initialState);
				Animation anim = Animation.get(state.animID()); 
				anim.loadGif();
				Zone z = new Zone(state.pos().x,state.pos().y,state.pos().x+anim.getWidth(),state.pos().y+anim.getHeight(),ZoneType.RECTANGLE);
				z.save(state.id,title+" main zone");  
				remGenerator(this); 
				return theObject;
			} catch (Exception e) { remGenerator(this); e.printStackTrace();return null; } }
	  

	//###############################################################
	private int generateInitialState(BasicObject obj, boolean initial) { try {
				String title = "initial"; 
				ObjectState state = new ObjectState (title).save(); 
				state.updatePossessor(obj);
				state.updatefullScreen(false);  
				say("Select a file");
				pauseGenerator(); 
				String file = Main.fileDisplayer.getUpdatedImage(obj.getFolder(), null);

				int choice = ask("Initially, the object will be:",  new String[]{"Invisible", "Immobile", "Infinite Loop"} );
				int selection = -999;
				switch (choice) {
				    case 0:  selection = AnimationState.INVISIBLE.getValeur(); break;
				    case 1://single frame  
				            selection = Utils.askForFrame( obj.getFolder()+file, "select frame",true );   
				        break;
				    case 2: 
				        selection = AnimationState.INFINITE_LOOP.getValeur(); break;
				    default:
				        return -1;
				}
				 
	      if(selection == -999)return -1;
	      int animTransitionId = Animation.generateAnimation(obj, title, selection, file);
	      state.updateAnimID(animTransitionId);
	      
	      say( "click for select state position" ); 
	      pauseGenerator(); 
	      state.updatePos(clickedX, clickedY,0);
	      say("click for select required player position" );
	      pauseGenerator();
	      state.updateRequiredPlayerPosition(clickedX, clickedY ,0);
	       
	       
	      return  state.id ;
	} catch (Exception e) {e.printStackTrace(); remGenerator(this); return -1;  } }
	
	//###############################################################
	private int generateInvisibleState(BasicObject obj, boolean initial) { try {
			String title = "invisible";
			ObjectState state = new ObjectState (title).save(); 
			state.updatePossessor(obj);
			state.updatefullScreen(false);  
			     
      int animTransitionId = Animation.generateAnimation(obj, title,
    		  AnimationState.INVISIBLE.getValeur(), Animation.get(obj.initialState().animID()).image());
      state.updateAnimID( animTransitionId ); 
      state.resetPlayerPosition();
        
    //  Ash.setAsh(title, AshType.ACTUAL_STATE,initial,state);
       
      return  state.id ;
		} catch (Exception e) { e.printStackTrace();remGenerator(this); return -1; }    }
	
	
	//###############################################################
	@Override public void onFileSelected(String selectedFile) {  resumeGenerator(); } 
	@Override public void onClick(int clickedX, int clickedY) {this.clickedX=clickedX;this.clickedY=clickedY;resumeGenerator(); } 
	@Override public void onPressEnter() { } 
	@Override public void onPressNumber(int keyCode) { } 
	@Override public void onGenerationComplete(int id) { }
	@Override public void onZoneDrawn(Zone z) { } 
	@Override public void onZoneSelected(ArrayList<Zone> zl) { } 
	@Override public void onObjectSelected(List <BasicObject> l) { } 
	@Override public void onStateSelected(ObjectState a) { } 
	@Override public void charSelected(CharacterState character) {  } 
	@Override public void onInvZoneDrawn(SpecialZoneInv zone) { }

 
}
