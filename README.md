# Pacmania

Pacmania is a "point and click" game maker that allows users to create interactive games with no programming knowledge required. The games are stored as SQLite databases with associated art resources. These games can be played on mobile devices using the Android interpreter or on computers using the desktop interpreter.

## Interpreters
- [Desktop Interpreter](https://gitlab.com/alineas/pacmania-desktop-interpretor)
- [Android Interpreter](https://gitlab.com/alineas/pacmania-android-interpreter)

## Example
A demonstration of what can be accomplished using Pacmania is provided with a partial recreation of King's Quest 7 (Chapters 1 & 2).

## Features
Pacmania operates on a system of "actions." Each action consists of:
- One or more detectors
- One or more tasks to perform
- One or more requirements for the action to be executed
- One or more conditions under which the action will not be executed

## How to Use
1. **Run pacmaniaBeta.jar**: Ensure you have Java 8 or later installed.
2. **Choose Resource Folder**: Select the folder containing your game resources. (bottom/left)
   - `..` represents the parent folder.
   - `.` represents the root folder.
3. **Add Game Elements**: Use the menu to add various game elements such as scenes, chapters, characters, and objects. (top left)

### Adding a Scene
1. **Name Your Scene**: Provide a name for your scene.
2. **Add Background Image**: Drag and drop your background image.
3. **Optional Elements (left menu)**:
   - Priority background image
   - Background music (.mid or .mp3)
   - Scene options like fade time for music loops and priority levels for background music. (like 500 if you want to start next loop 500ms before the last has ended)

### Scene Editing (when you select a scene use the "edition mode menu" at left)
- **Collision**: Draw geometric shapes to represent walls. These can be modified later. Click right, choose a name or create an action with this collider as detector.
- **Z-Indexes**: if you specified a priority bg in your scene, the colors will appear. Adjust the vertical position priority of elements in the scene after right click on a color. This will draw the background beside the character when he will be placed upper of the bar.
- **Scale Factor**: Define the scale of characters within the scene. Move positions of bottom bar (start of the decrease), and the top bar (end of the decrease) wheel the mouse for chose a scale.
- **Transition Points**: Create draw some geometry for changing scenes. Click right and move character will define the arrival point of this "door". (point is used too when we leave the scene, but character position specified is for the arrival)
  - you can add an action using this tpoint as a detector.
  - (disable at start is not used anymore)
  - (use "link scenes" menu for specify directs targets by drawing corresponding lines, or leave blanc and use action system described after)

- **Stair Manager**: Draw some geometries representing the top of a mountain, same for the bottom, specify a level from bot to top, right click corresponding priority (will hide character when be across this stair).

- **Objects**: Create objects with multiple states and animations. (right click)
  - Each object can have multiple "objectstates", including animations (image file: idle/invisible, GIF file: animated/idle/infinite/single loop).
  - Zero or more clickable zones can be defined for each state.
  - A desired position for the character interacting with the object.
  - The initial state zones remain enabled across all selected states, unless the state is invisible and non-fullscreenable.
  - Zones in the initial state can be manually disabled in the action system, or by setting zero zones in the initial state.
  - Other zones in subsequent states are clickable only when their corresponding state is selected.
  - The desired position of the state owner is used when a zone is clicked.
  - Objects can be z-indexed for positioning (on the scene/under character/beside character) and z-position can be adjusted for alignment with other objects.
  - **Child Objects**: Objects can have child objects that inherit visibility properties.
  - **Actions**: Define actions triggered by clickable zones, transitions, and other interactions.
  - ! state actions menu is not finished yet !

### Creating Objects
You can discover your first action by creating a fullscreen object and following the instructions. A fullscreen object consists of:
- One invisible object state with one clickable zone
- One visible object state
The fullscreen object contains a child object, which serves as the exit button with an initial state that includes a clickable zone. (No invisible state is needed for the button; child visibility is the same as the parent.)

### Action Examples
What will these actions do?
- **Detector**: The zone of the invisible fullscreen state
- **Action**: Set fullscreen selected state to visible
- **Needed**: Fullscreen state is invisible
This action will show the fullscreen state when the initial zone is clicked, but only if it is invisible.

- **Detector**: The main zone of the exit button
- **Action**: Set fullscreen selected state to invisible
- **Needed**: Fullscreen selected state is visible
This action will hide the fullscreen state when the exit button is clicked, but only if it is visible.

### Spawnpoints
Spawnpoints is a misleading name; it would be better called placement points. They are simple geometries used to specify:
- Placing a character
- Detecting a placed character
- Detecting a click
- Forcing a click

The order of click interpretation is as follows (with the upper interrupting the lower):
- Inventory Object Acted On a Character
- Inventory Object Acted In Menubar
- Scene Object Has Been Clicked
- Transition Point Has Been Clicked
- Click Has Acted On Character
- Scene Has Been Clicked In Action System
- Player Has Set a Walk Click Position

### Adding a Chapter
To add a chapter, use the menu "add game element". A chapter consists of:
- A start scene to select
- A start point
- A start action
The action will define initially disabled objects/zones, the game character to spawn, and describe the first animation. The first character spawned will be the player.

### Adding a Character
To add a character, use the menu "add game element". Characters, like objects, have an initial state and as many other states as needed.

Name the character, then drag and drop your GIF animations into the eight movement positions. Optionally, you can specify idle images in the second grid and talk animations in the third grid. Specify offsets by placing little rectangles:
- Gray rectangle: offset from (the base point used in the system is the feet position middle/bottom of the image, so if the feet are not there, place the gray on the feet)
- Blue rectangle: offset to (if the animation does not change position, align it with the gray; if, for example, the character jumps from left to right, align gray and feet before the jump, and blue and feet after the jump)

Move the scroll to play the animation. The talk animation is aligned with the idle image specified or generated. Just drag and drop the idle in the third grid to make the image and mouth/lips correspond. Optionally, in moving animations, offsets can be used as a priority difference (like the rabbit in KQ7, starting to run across the rock and finishing beside it). Use the right-click button to discover the basic animation editor.

### Basic Animation Editor
The basic animation editor allows you to:
- Choose between invisible, infinite, one loop (only one loop and go to idle), random loop (do a loop sometime and wait in idle)
- Specify a shadow color
- Specify the order of frames by switching with drag and drop
- Delete or copy a frame by right-clicking
- Reverse order by right-clicking
- Repeat animation order in the left menu
- Duplicate all frames in the left menu to make it slower
- Don't forget to click "save order"
- Empty slots are not used for the moment and can cause a crash!
- Click "original frame order" to switch back to the GIF order
- Specify two priority numbers in the left menu:
  - Top text field: the bypass (choose 100 if animation is placed at 250px from top and you want this to use the priority of 350 y position; use a negative value for reverse)
  - Other field: directly choose the y priority position to use

Press enter to apply your numbers. You can drag and drop an MP3 sound, and right-click one or more frames to sonorize them with this sound (one MP3 sound per animation + one optional MIDI music). By default, the music of an animation will replace the scene music.

### Creating Inventory Objects
To create an inventory object, use the inventory menu on the left. Choose a name, click "create object", then drag and drop images for one or more states. Right-click and choose "edit zones" if your inventory object needs a clickable zone in the inventory object viewer. Draw it and specify the corresponding frames. When your zone is ready, you can go back to the states and select "create action" in the right-click menu, then follow the instructions. Check results in the left menu "action editor". Actions can be grouped in mass action sub-folders called "megaactions". Follow instructions to create one and move actions into it with the combobox if needed.

### Action System
An action has four parts as we mentioned before. An action can be timed or randomized.

#### Detector Part
When you add something with the "+" button of one of the four columns, a list of all the game elements will appear. You can add one or more with right-click. In the detector part, you can add:
- Scenes (used if the player clicks outside of other zones, enters, or exits the scene)
- Object zones (used on object click, selected state zone, and initial state zone of an object)
- Transition points (same as objects)
- Collision zones (used on a collision; be careful because adding a collision detector will make the pathfinder ignore this zone, causing repeated collisions and repositioning of your player)
- Characters (used when clicking on the player or an NPC, with or without an item in hand)

#### Action Part
The action part is an ordered list of things to do. For example, a character will look right, a character will change state, an item will be removed from the inventory, etc. When you have multiple characters in your scene, you must specify who the actor is for some actions (a forced click, another character to look, a forced positioning). Just go to the character category, select your actor, and add "is the actor". The next action (only one) you add after will be done by the selected actor.

You can cancel a timer by choosing the timed one and selecting "remove" with right-click on "will do after timer". You can add an inventory object by adding "will have in inventory" or remove one by choosing "remove" with right-click on "has in inventory". The same applies for enabling/disabling a zone ("will be disabled" to add or "has been disabled" to remove). Tip: to disable a zone at game start, add a "will be disabled" action in the first action of a chapter. The same applies for "will have in hand" on a ground object and "has in hand". In character actions, we have "will inside this state" (do a single loop of the current animation, in the actual position) and "outside this state" (same but reversed). "Inside outside this state" does the same, one after the other. Tip: don't forget to put your character in the initial state if you finished animating it.

#### Need/Refuse Part
This is an exclusive list of things needed/refused (if one needed item is not present, the action will not be executed; if one refused item is present, the action will not be executed). You can add an "or" at the top of the game objects list. When no "or" is specified, it will be "and" by default. You can specify multiple "or" and multiple "or" lists. Here is the schema: 1 or 2 or 3456 or 78 or 9, which means 1 or 2 or 3 and 4 and 5 and 6 or 7 and 8 or 9.

You can need something said by adding "has been said" on the selected message (the action will execute only if this message has been played). You can need the player to be in a certain state by adding "is in state" in the character state part. You can need an object to be in a certain state (same method). You can specify "has been collided" or "has been clicked" or "is in zone" if the detector is a spawnpoint.

I will not put the full list here, as I do not know what is working at the moment. Look at the King’s Quest 7 example; these working actions will show you what can be done.

The "refuse" part acts the same way as the "needed" part.

## Tips
- **Pathfinder**: By default, all collision zones are treated as walls, except the geometry who act as a detector. (tip:add a blank action on a wall if it can be disabled by action system)
- **Transition Points**: even if it has no destination, a tpoint will force player to be placed at the "desired position"
- **State Management**: if an initial state zone is not disabled, it can take the priority for desired player position when another state is selected
- **Performance**: all classes are stored in memory, sometime a change need a software reboot
- **Unique Image Names**: all image name must be unique, or it can cause conflict with shadows
- **spawnpoint has been collided** is only detected if the player click was forced by an action

keep in mind its beta ! its the one woman s work done in only 5 months ( 30 000 lines of code to maintain ) so every click you will make can break the system
save your game folder sometimes and the npcma file constantly !!! feel free to contribute for make it better
