package me.nina.objects2d;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.MemoryCacheImageInputStream;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
 

public class GifDecoder {

	private String path;

	public GifDecoder(String path) {
		this.path=path;
	}

	public List<BufferedImage> getFrames() { 
		List<BufferedImage> frames = new ArrayList<>();

		try {
			ImageReader reader = ImageIO.getImageReadersBySuffix("gif").next(); 
			File imageFile = new File(path); 
			ImageInputStream stream = ImageIO.createImageInputStream(imageFile); 
			if (stream == null)  
				stream = new MemoryCacheImageInputStream( Thread.currentThread().getContextClassLoader().getResourceAsStream(path));
			reader.setInput(stream); 
			int numFrames = reader.getNumImages(true); 
			int maxWidth = 0;
			int maxHeight = 0; 
			for (int i = 0; i < numFrames; i++) {
				IIOMetadata metadata = reader.getImageMetadata(i);
				Node imageDescriptorNode = getNode(metadata.getAsTree("javax_imageio_gif_image_1.0"), "ImageDescriptor");

				if (imageDescriptorNode != null) {
					int imageWidth = getIntAttribute(imageDescriptorNode, "imageWidth");
					int imageHeight = getIntAttribute(imageDescriptorNode, "imageHeight");
					int imageLeftPosition = getIntAttribute(imageDescriptorNode, "imageLeftPosition");
					int imageTopPosition = getIntAttribute(imageDescriptorNode, "imageTopPosition");
					maxWidth = Math.max(maxWidth, imageWidth + imageLeftPosition);
					maxHeight = Math.max(maxHeight, imageHeight + imageTopPosition);
				}
			}


			for (int i = 0; i < numFrames; i++) {
				BufferedImage frame =  reader.read(i)  ;
				IIOMetadata metadata = reader.getImageMetadata(i);
				Node imageDescriptorNode = getNode(metadata.getAsTree("javax_imageio_gif_image_1.0"), "ImageDescriptor");

				if (imageDescriptorNode != null) {
					int imageLeftPosition = getIntAttribute(imageDescriptorNode, "imageLeftPosition");
					int imageTopPosition = getIntAttribute(imageDescriptorNode, "imageTopPosition");

					BufferedImage fullImage = new BufferedImage(maxWidth, maxHeight, BufferedImage.TYPE_INT_ARGB);
					Graphics2D g = fullImage.createGraphics();

					g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
					g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

					g.drawImage(frame, imageLeftPosition, imageTopPosition, null); 
					g.dispose();

					frames.add(fullImage); 
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		 
		return frames;
	}
	
	 
	 
private static Node getNode(Node parent, String nodeName) {
    NodeList children = parent.getChildNodes();
    for (int i = 0; i < children.getLength(); i++) {
        Node child = children.item(i);
        if (child.getNodeName().equals(nodeName)) {
            return child;
        }
    }
    return null;
}
//########################################################################
 private static int getIntAttribute(Node node, String attributeName) {
        NamedNodeMap attributes = node.getAttributes();
        Node attributeNode = attributes.getNamedItem(attributeName);
        if (attributeNode != null) {
            return Integer.parseInt(attributeNode.getNodeValue());
        }
        return -1;
    }
}