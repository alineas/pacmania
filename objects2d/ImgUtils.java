package me.nina.objects2d;
 
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
 
public class ImgUtils {
 
    public static Map<String, Img> mappe = new HashMap<>();
    private static final Color SHADOW_ALPHA = new Color(0, 0, 0, 120); // ombre (0-255)
  
    public static BufferedImage getPicture(String path ) {
        return getPicture(path,0, -1);  
    }
    
    public static BufferedImage getPicture(String path, int frame,boolean b) {
        return getPicture(path, frame, -1); // -1 = pas d'ombre
    }

    public static BufferedImage getPicture(String path, int frame, int shadow) {
    	path=path.replaceAll("�|�", "e");
        if (!mappe.containsKey(path)) 
           mappe.put(path, new Img(path,shadow)); 
        return mappe.get(path).frames.get(frame);
    }
	public static Img getImg(String path, int shadow) {
    	path=path.replaceAll("�|�", "e");
		if (!mappe.containsKey(path)) 
	           mappe.put(path, new Img(path,shadow)); 
		return mappe.get(path);
	}  
	//##################################################################
	 public static BufferedImage getCopy(BufferedImage original) { 
		 BufferedImage copiedImage = new BufferedImage( original.getWidth(), original.getHeight(), original.getType()  );
			copiedImage.getGraphics().drawImage(original, 0, 0, null); 
			return copiedImage;
		}
  //##################################################################
	 static void applyShadow(BufferedImage image,int shadow) {if(shadow == -1)return;
	    int imageWidth = image.getWidth();
	    int imageHeight = image.getHeight();
	    for(int i = 0 ; i < imageWidth ; i++)
	    	for(int j = 0 ; j < imageHeight ; j++)  {
	    		int pixelColor = image .getRGB(i,j);
	    		if (pixelColor == shadow) {   
	    			image .setRGB(i,j, SHADOW_ALPHA.getRGB());
	    		}
	    	}   
 
	    }
	 public static BufferedImage scal(BufferedImage original, double scale) {
		    int w = original.getWidth();
		    int h = original.getHeight();
		    int w2 = (int) (w * scale);
		    int h2 = (int) (h * scale);
		    BufferedImage after = new BufferedImage(w2, h2, original.getType());
		    AffineTransform scaleInstance = AffineTransform.getScaleInstance(scale, scale);
		    AffineTransformOp scaleOp = new AffineTransformOp(scaleInstance, AffineTransformOp.TYPE_BILINEAR);
		    scaleOp.filter(original, after);
		    return after;
		}
	 public static BufferedImage scal2(BufferedImage original, double scale) {
		    int w = original.getWidth();
		    int h = original.getHeight();
		    int w2 = (int) (w * scale);
		    int h2 = (int) (h * scale);
		    
		    // Cr�er une nouvelle image redimensionn�e
		    BufferedImage after = new BufferedImage(w2, h2, original.getType());
		    Graphics2D g2d = after.createGraphics();
		    
		    // Configurer les hints pour d�sactiver l'anti-cr�nelage
		    RenderingHints hints = new RenderingHints(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
		    hints.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
		    hints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_DEFAULT);
		    g2d.setRenderingHints(hints);
		    
		    // Dessiner l'image redimensionn�e
		    g2d.drawImage(original, 0, 0, w2, h2, null);
		    g2d.dispose();
		    
		    return after;
		}
		public static BufferedImage resiz(BufferedImage original, int width, int height) {
		    double scaleX = (double) width / original.getWidth();
		    double scaleY = (double) height / original.getHeight();
		    double scale = Math.min(scaleX, scaleY);
		    int w2 = (int) (original.getWidth() * scale);
		    int h2 = (int) (original.getHeight() * scale);
		    BufferedImage after = new BufferedImage(w2, h2, original.getType());
		    AffineTransform scaleInstance = AffineTransform.getScaleInstance(scale, scale);
		    AffineTransformOp scaleOp = new AffineTransformOp(scaleInstance, AffineTransformOp.TYPE_BILINEAR);
		    scaleOp.filter(original, after);
		    return after;
		}
     
    
 
		 public static ImageIcon createSemiTransparentImageIcon(BufferedImage img,float alpha,int w, int h) {  
		        Graphics2D g2d = img.createGraphics();
		        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha));  
		        g2d.drawImage(img, 0, 0, null);
		        g2d.dispose(); 
		        return new ImageIcon(img);
		    }
}
