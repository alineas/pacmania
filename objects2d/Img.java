package me.nina.objects2d;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
  

public class Img {
	
	  public String path;  public int shadow; public List<BufferedImage> frames = new ArrayList< >();
	  public Img(String path) {
		  this(path, -1);
	  }
	  
	//##################################################################
	  public Img(String path, int shadow) {
	        this.path = path; 
	        this.shadow = shadow;         
	        try { 
	        	GifDecoder decoder = new GifDecoder(path);
	        	 
	        	for(BufferedImage fram : decoder.getFrames())
	        		frames.add(ImgUtils.getCopy(fram));
	        	 
	        	if (frames.isEmpty()) {
	        		BufferedImage loadedImage = null; 
 					loadedImage = ImageIO.read(new File(path));
				  
	        		frames.add(loadedImage); 
	        	}
	        	
	        } catch (Exception e) {e.printStackTrace(); }
	       if(shadow !=-1)
	    	   for(BufferedImage fram:frames)
	    		   ImgUtils.applyShadow(fram, shadow);
	    		   
	    }
	//################################################## load in memory
		 
	public int getWidth() { 
		return frames.get(0).getWidth();
	}
	public int getHeight() { 
		return frames.get(0).getHeight();
	}
	

	    
}
