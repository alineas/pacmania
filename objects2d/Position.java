package me.nina.objects2d;
 
import me.nina.gameobjects.Character.PosType;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.sqlmanipulation.SQLWrapper;
 
public class Position extends Point {
	public final PosType pos;
	public Position(int x, int y,int z) {
		super(x, y);
		this.pos=PosType.from(z);
	}
	public Position(String s) { super(Integer.parseInt(s.split(",")[0]),Integer.parseInt(s.split(",")[1]));
		 
		this.pos=PosType.from(s.split(",")[2]);
		
	} 
	public String toSqlValues() {
		return x+","+y+","+pos.getValeur();
	}
	 
	 

}
