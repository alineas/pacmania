package me.nina.objects2d;public class Point{@Override
	public String toString() {
		return "Point [x=" + x + ", y=" + y + "]";
	}
public final int x,y;
public Point(int x,int y){this.x=x;this.y=y;}@Override
public int hashCode() { final int prime = 31;
int result = 1; result = prime * result + x; result = prime * result + y; return result; }
@Override public boolean equals(Object o) { Point ot = (Point) o; return x==ot.x&&y==ot.y; }}