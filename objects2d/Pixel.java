package me.nina.objects2d;

import java.util.List;
import java.util.Objects;
import java.util.Set;
 

public class Pixel {
    public int stored;
    public int rgb;
	public Set<Point> points;

    public Pixel(int stored, int rgb) {
        this.stored = stored;
        this.rgb = rgb;
    }

    // Getters pour stored et rgb

    // M�thode equals et hashCode bas�e sur stored et rgb
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Pixel)) return false;
        Pixel other = (Pixel) obj;
        return stored == other.stored && rgb == other.rgb;
    }

    @Override
    public int hashCode() {
        return Objects.hash(stored, rgb);
    }

	
}