package me.nina.maker;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.JComboBox;
import javax.swing.JFrame; 
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import me.nina.gameobjects.GameObject;
import me.nina.sqlmanipulation.SQLLine;
import me.nina.sqlmanipulation.SQLTable;
import me.nina.sqlmanipulation.SQLUtils;
  
public class SQLPanel extends Pane { private static final long serialVersionUID = 1L;
Pane flowPanel = new Pane(); 
	JFrame sqlFrame;
	private JScrollPane scroll;
	public static boolean isShown;
	
	//#################################################################
	public SQLPanel(JFrame sqlFrame) {
		super();
		scroll = new JScrollPane(this);  
		Utils.setSize(scroll,1500,768) ;
		this.sqlFrame = sqlFrame;
		sqlFrame.setTitle("sql frame");    
		setLayout(new GridLayout(0, 1));  
		flowPanel.setLayout(new FlowLayout(FlowLayout.LEFT));  
		add(flowPanel);
		sqlFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); // D�finir la fermeture par d�faut
        sqlFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                isShown = false;  
            }
        });
		setSize(1000,2700);  

		sqlFrame.getContentPane().add(scroll, BorderLayout.CENTER);  

		JTextField t = createText("name",flowPanel,100,20,null); 
		createBoutton("add table",flowPanel,100,20,new ActionListener() { @Override public void actionPerformed(ActionEvent e) {
			SQLTable.createNewTable(t.getText()); 

		}});

		sqlFrame.pack(); 
	}

	//#################################################################
	public void updateTableList(Map<String, List<String>> temp) {
if(!isShown)return;
		removeAll(); 
		newFlowLine();
		JTextField tt = createText("id to search",flowPanel,100,20,null); 
		createBoutton("search",flowPanel,100,20,new ActionListener() { @Override public void actionPerformed(ActionEvent e) {
			GameObject.getById(Integer.parseInt(tt.getText())).line.toFrame(true);

		} });
		JTextField t = createText("name",flowPanel,100,20,null); 
		createBoutton("add table",flowPanel,100,20,new ActionListener() { @Override public void actionPerformed(ActionEvent e) {
			SQLTable.createNewTable(t.getText());

		} });

		for(Entry<String, List<String>> a:temp.entrySet()) {
			newFlowLine();
			createLabel(a.getKey(),flowPanel,150,20); 

			JComboBox<String> liste = new JComboBox<String> (a.getValue().toArray(new String[0]));
			Utils.setSize(liste, 250, 20);
 			flowPanel.add(liste);


			createBoutton("X",flowPanel,60,20,new ActionListener() { @Override public void actionPerformed(ActionEvent e) {
				SQLTable.getTable(a.getKey()).remColumn((String) liste.getSelectedItem());

			} });



			JTextField b1 = createText("columnName",flowPanel,150,20,null);
			createBoutton("add col",flowPanel,100,20,new ActionListener() { @Override public void actionPerformed(ActionEvent e) {
				SQLTable.getTable(a.getKey()).addcolumn(b1.getText()+" text");

			} });

			createBoutton("insert",flowPanel,100,20,new ActionListener() { @Override public void actionPerformed(ActionEvent e) {
				SQLTable.getTable(a.getKey()).getEmptyLine().toFrame(false);

			} });


			createBoutton("del",flowPanel,100,20,new ActionListener() { @Override public void actionPerformed(ActionEvent e) {
				SQLTable.getTable(a.getKey()).drop(); 

			} });

			createBoutton("show",flowPanel,100,20,new ActionListener() { @Override public void actionPerformed(ActionEvent e) {
				showTable(a.getKey());
				sqlFrame.setTitle("table "+ a.getKey());

			} });

			createBoutton("truncate",flowPanel,60,20,new ActionListener() { @Override public void actionPerformed(ActionEvent e) {
				SQLTable.getTable(a.getKey()).truncate(); 

			} });
		}


		sqlFrame.pack(); 	


	}


	//#################################################################
	public void showTable(String table) {  try {
		removeAll(); 
		newFlowLine();
		for(String col : SQLUtils.getColumns(table))
			createLabel(col,flowPanel,100,20); 
		createLabel("",flowPanel,100,20); createLabel("",flowPanel,100,20); 
		newFlowLine();
		SQLTable t = SQLTable.getTable(table); 
		
		for(Entry<Integer, SQLLine> res : t.getLines().entrySet()) {
			
			for(String col : SQLUtils.getColumns(table)) 
				createLabel(res.getValue().getString(col),flowPanel,100,20); 

			createBoutton("modify",flowPanel,100,20, new ActionListener() {  @Override public void actionPerformed(ActionEvent arg0) {
				 res.getValue().toFrame(true);

			}  });

			createBoutton("delete",flowPanel,100,20,new ActionListener() { @Override public void actionPerformed(ActionEvent e) {
				res.getValue().delete();

				showTable(table);
			} });
			
			newFlowLine();
		}

		sqlFrame.pack(); 
	} catch (Exception e1) { e1.printStackTrace(); }}	

	//#################################################################
	private void newFlowLine() {
		flowPanel = new Pane();
		flowPanel.setLayout(new FlowLayout(FlowLayout.LEFT)); 
		add(flowPanel);
	}
}