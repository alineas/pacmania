package me.nina.maker;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import me.nina.gameobjects.Character;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JWindow;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileSystemView;

import agame.GameCharacter;
import agame.MainGame; 
import me.nina.editors.Editor;
import me.nina.gameobjects.Animation;
import me.nina.gameobjects.BasicObject;
import me.nina.gameobjects.Character.PosType;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.gameobjects.CharacterState;
import me.nina.gameobjects.GameObject;
import me.nina.gameobjects.InventoryObject;
import me.nina.gameobjects.ObjectState;
import me.nina.gameobjects.Scene;
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Animation.AnimationState;
import me.nina.objects2d.Img;
import me.nina.objects2d.ImgUtils;
import me.nina.objects2d.Pixel;
import me.nina.objects2d.Point;
import me.nina.sqlmanipulation.SQLLine;
import me.nina.sqlmanipulation.SQLTable;

public class Utils { 
   
	//##################################################
	public static void setSize(JComponent p, int i, int j) {
		p.setSize(i, j);
		p.setMinimumSize(new Dimension(i, j)); 
		p.setMaximumSize(new Dimension(i, j));
		p.setPreferredSize(new Dimension(i, j)); 
	}
	//##################################################
	static Map<Integer,List<Pixel>> mappe = new HashMap<>();


	public static List<Pixel> getMatrix( Scene scene,boolean reset) {
		BufferedImage img = scene.priorityBgImage();
		if(img==null)return null;
		if(reset)mappe.remove(scene.id);
	    if (!mappe.containsKey(scene.id)) {
	        Integer[][] m = new Integer[img.getWidth()][img.getHeight()];
	        Map<Integer, Pixel> uniquePixelMap = new HashMap<>(); // Utilisation d'une Map pour stocker les pixels uniques
	        Map<Integer, Set<Point>> pointsMap = new HashMap<>(); // Map pour stocker les coordonn�es y correspondant � chaque pixel unique
	        Map<Integer, Integer> colorCountMap = new HashMap<>(); // Compteur de couleurs
	        Map<Integer, Integer> records = new HashMap<>(); // Compteur de couleurs

	        // Premier parcours pour d�tecter la couleur la plus pr�sente
	        for (int i = 0; i < img.getWidth(); i++) {
	            for (int j = 0; j < img.getHeight(); j++) {
	                int rgb = img.getRGB(i, j);
	                m[i][j] = rgb;

	                // Mettre � jour le compteur de couleurs
	                colorCountMap.put(rgb, colorCountMap.getOrDefault(rgb, 0) + 1);
	            }
	        }

	        // Trouver la couleur la plus pr�sente
	        int mostFrequentColor = 0;
	        int maxCount = 0;
	        for (Map.Entry<Integer, Integer> entry : colorCountMap.entrySet()) {
	            if (entry.getValue() > maxCount) {
	                maxCount = entry.getValue();
	                mostFrequentColor = entry.getKey();
	            }
	        }
	        colorCountMap.clear();
	        // Deuxi�me parcours pour stocker tous les autres pixels
	        for (int i = 0; i < img.getWidth(); i++) {
	            for (int j = 0; j < img.getHeight(); j++) {
	                int rgb = m[i][j];
	                if(!records.containsKey(rgb))
	                	records.put(rgb, scene.line.duoManager.getInt("zindexes", rgb + ""));
	                int stored = records.get(rgb);

	                // Ignorer la couleur la plus pr�sente
	                if (rgb != mostFrequentColor) {
	                    Pixel pixel = new Pixel(stored, rgb);
	                    uniquePixelMap.putIfAbsent(rgb, pixel); // Ajouter le pixel � la map si absent

	                    // Ajouter les coordonn�es x et y � leurs listes respectives dans les maps
	                    pointsMap.computeIfAbsent(rgb, key -> new HashSet<>()).add(new Point(i,j));
	                    
	                }
	            }
	        }

	        // Construire la liste finale de pixels uniques avec leurs coordonn�es
	        List<Integer> uniquePixels = new ArrayList<>(uniquePixelMap.keySet());
	        for (Integer pixel : uniquePixels) {
	            Set<Point> points = pointsMap.get(pixel);
	            uniquePixelMap.get(pixel).points=points;
	        }

	        mappe.put(scene.id, new ArrayList<>(uniquePixelMap.values())); // Ajouter les pixels uniques � la mappe
	        }
	    return mappe.get(scene.id);
	}
	//##################################################
	public static List<Integer> getPriLevels(List<Pixel> matrix) {
		List<Integer > l = new ArrayList<>();

		for (Pixel arr : matrix) {
			 
				if(!l.contains(arr.rgb))
					l.add(arr.rgb);
		}


		return l;
	}

	
	//##################################################
	public static void reverseStringArray(String[] array) {
	    int length = array.length;
	    for (int i = 0; i < length / 2; i++) {
	        String temp = array[i];
	        array[i] = array[length - i - 1];
	        array[length - i - 1] = temp;
	    }
	}
	//##################################################
	public static boolean copy(File source, File dest) { try { 
            Path sourcePath = source.toPath(); 
            String destPath = removeAccents(dest.getPath()); 
            Files.copy(sourcePath, new File(destPath).toPath(), StandardCopyOption.REPLACE_EXISTING); 
            return true;
    } catch ( Exception e) { e.printStackTrace();return false; } }
	//##################################################
	public static void log(String s) {   
		System.out.println(s);
		 
	}//##################################################
	 public static String removeAccents(String input) {
	        StringBuilder result = new StringBuilder();
	        for (char c : input.toCharArray())  
	            if (accents.contains(String.valueOf(c)))  
	                result.append('e'); 
	                else result.append(c);  
	        return result.toString();
	    }
	//##################################################
    private static final String accents = "�����������������������������������������������������"; 
	//##################################################
 
	//##################################################
	public static File getWorkingFolder() { 
		try {
			SQLLine line = Editor.getGameInfo(); 
			String folder = line.getString("actualfolder");
			return new File(folder);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		return FileSystemView .getFileSystemView() .getHomeDirectory();
          
	}
 
 
	public static void setWorkingDirectory(String parent) {  
		Editor.getGameInfo().update("actualfolder", parent);
	}
 
 

	//##################################################
	
	//##################################################
	public static void deleteOldImage(String folder, String name) {
		if(name!=null&&!name.equals("")) {
			File f = new File(folder+name);
			if(f.exists())
				f.delete();
		}
	}





	public static int getTimestampId() {
		long toRemove = 1704464212846L;
		long l = System.currentTimeMillis() - toRemove;
		int res = (int) (l/100)+2308698;
		return res;
	}
	
 
	
	static InventoryObject selected = null;
	//###############################################################
	public static InventoryObject askForInvObj(String question) {
        JPanel panel = new JPanel(new GridLayout(0, 5));
        JScrollPane scrollPane = new JScrollPane(panel);
        scrollPane.setPreferredSize(new Dimension(1024,768));
        for (BasicObject o : InventoryObject.getAll()) {
            Animation a = Animation.get(o.initialState().animID());
            a.loadGif();
            if (a.image() == null) continue;
            
    		ImageIcon icon = new ImageIcon(ImgUtils.getPicture(a.getFolder()+a.image()).getScaledInstance(100, 100, Image.SCALE_SMOOTH)); 
    		 
            JLabel label = new JLabel(icon);
            label.setText(o.title());

            label.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) {
                    selected = (InventoryObject) o;
                    label.setBorder(new LineBorder(Color.RED));
                }
            });

            panel.add(label);
        }

        panel.revalidate();

        if(  select( scrollPane, question)==JOptionPane.OK_OPTION   )  return selected;
        return selected;
    }
	 
	 static int frameSelected = -998;

	    public static int askForFrame(String path, String question, boolean askForTypes) {
	        frameSelected = -998;
	        JPanel panel = new JPanel(new GridLayout(0, 5));

	        // Load frames from the provided image path
	        Img img = ImgUtils.getImg(path,-1);
	        List<BufferedImage> frames = img.frames;

	        // Add labels for each frame
	        for (BufferedImage frame : frames)
	            panel.add(createLabel(new ImageIcon(ImgUtils.resiz(frame,75, 75)), frames.indexOf(frame)));

	        // Add labels for animation types if asked
	        if (askForTypes) {
	            panel.add(createLabel(ImgUtils.createSemiTransparentImageIcon(ImgUtils.getPicture(path),0.5F,75,75), AnimationState.INVISIBLE.getValeur()));
	            panel.add(createLabel(new ImageIcon(img.path), AnimationState.INFINITE_LOOP.getValeur()));
	        }

	        // Wrap the panel with a scroll pane
	        JScrollPane scrollPane = new JScrollPane(panel);
	        scrollPane.setPreferredSize(new Dimension(600, 400));

	        panel.revalidate();

	        // Show the selection dialog
	        if (select(scrollPane, question) == JOptionPane.OK_OPTION) return frameSelected;
	        return -998;
	    }
		 
		 private static JLabel createLabel(ImageIcon img,int i) {
				JLabel l = new JLabel(img);
			    if(i == AnimationState.INFINITE_LOOP.getValeur()) l.setText("infinite");
			    if(i == AnimationState.INVISIBLE.getValeur()) l.setText("invisible");
			    l.addMouseListener(new MouseAdapter() {  @Override  public void mouseReleased(MouseEvent e) {
			            frameSelected = i;  l.setBorder(new LineBorder(Color.RED));  }  });
			    return l;
			}
		 
		 
		 static ObjectState stateFrom = null;
		  //###############################################################
		    public static ObjectState askForState(GameObject obj,String question) {stateFrom = null;
		    	JPanel panel = new JPanel(new GridLayout(0, 5));
		    	 
		    		for(ObjectState st : ObjectState.getByObject(obj.id)){
		    			if(Animation.get(st.animID())==null)continue;
		    			ImageIcon reduced = new ImageIcon(ImgUtils.resiz(Animation.get(st.animID()).getLastFrame(),75, 75));

		    			JLabel label = new JLabel(reduced);
		    			label.setText(st.title());
		    			 

		    			label.addMouseListener(new MouseAdapter() {  @Override public void mousePressed(MouseEvent e) { 
		    				stateFrom= st;
		    				label.setBorder(new LineBorder(Color.RED));  
		    			}});

		    			panel.add(label);
		    		}
		    				
		    		 
		    	 
		    	panel.revalidate(); 
		    	if(  select( panel, question)==JOptionPane.OK_OPTION   )  return stateFrom;
		    	return stateFrom;
		    }
		 
		    
		    
		    static CharacterState charState = null;
			  //###############################################################
			    public static CharacterState askForCharacterState(GameObject chare,String question) {charState = null;
			    	JPanel panel = new JPanel(new GridLayout(0, 5));
			    	 
			    	t:for(CharacterState st : CharacterState.getByPossessor(chare.id)){
			    			 for (PosType p : PosType.values())
			    				 if(st.movingAnimation(p)!=null){
						    			ImageIcon reduced = new ImageIcon(ImgUtils.resiz(st.movingAnimation(p).getLastFrame(),75, 75));

			    					 JLabel label = new JLabel(reduced);
			    					 label.setText(st.title()); 
			    					  

			    					 label.addMouseListener(new MouseAdapter() {  @Override public void mousePressed(MouseEvent e) { 
			    						 charState= st;
			    						 label.setBorder(new LineBorder(Color.RED));  
			    					 }});

			    					 panel.add(label);
			    					 continue t;
			    				 }
			    		}
			    				
			    		 
			    	 
			    	panel.revalidate(); 
			    	if(  select( panel, question)==JOptionPane.OK_OPTION   )  return charState;
			    	return charState;
			    }
			    
			    
			    
			    
			    
			    
			    
			    
			    
			    
			    static PosType charPos = null;
				  //###############################################################
				    public static PosType askForCharacterPosType(CharacterState chare ) {charPos = null;
				    	JPanel panel = new JPanel(new GridLayout(0, 3));
				    	 
				    	 
				    			 for (PosType p : PosType.values()) {
				    				 if(chare.movingAnimation(p)!=null){
				    					// ImageIcon reduced = new ImageIcon(chare.movingAnimation(p).getLastFrame().resiz(75, 75));
				    					 chare.movingAnimation(p).loadGif();
				    			            ImageIcon reduced = new ImageIcon(chare.movingAnimation(p).gifFile.path);
				    			            
				    					 JLabel label = new JLabel(reduced);
				    					 label.setText(p+""); 
				    					 Utils.setSize(label, 100, 100); 

				    					 label.addMouseListener(new MouseAdapter() {  @Override public void mousePressed(MouseEvent e) { 
				    						 charPos= p;
				    						 label.setBorder(new LineBorder(Color.RED));  
				    					 }});

				    					 panel.add(label);
				    					  
				    				 }
				    		}
				    				
				    		 
				    	 
				    	panel.revalidate(); 
				    	if(  select( panel, "position ?")==JOptionPane.OK_OPTION   )  return charPos;
				    	return charPos;
				    }
				    
				    
				    
				    
				    
				    
				    
				    
			    static Character chare = null;
				  //###############################################################
				    public static Character askForCharacter ( String question) {chare = null;
				    	JPanel panel = new JPanel(new GridLayout(0, 5));
				    	
			    		for( Character o :  Character .getAll()){
			    			BufferedImage img = o.getInitialState().movingAnimation(PosType.BOTTOM).getLastFrame()  ;
			    			 
			    			ImageIcon reduced = new ImageIcon(ImgUtils.resiz(img,75, 75));
			    			JLabel label = new JLabel(reduced);
			    			label.setText(o.title()); 

			    			label.addMouseListener(new MouseAdapter() {  

							@Override public void mousePressed(MouseEvent e) { 
								chare =  o ;
			    				label.setBorder(new LineBorder(Color.RED));  
			    			}});

			    			panel.add(label);
			    		}
			    		 
			    	panel.revalidate(); 
					 if(  select( panel, question)==JOptionPane.OK_OPTION   )  return chare;
					return chare;
				    }
				    
				    
				    
	private static int select(JPanel panel, String msg) {
		return JOptionPane.showConfirmDialog(Main.mainFrame, panel,
        		msg, JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
	}
	
	private static int select(JScrollPane panel, String msg) {
		return JOptionPane.showConfirmDialog(Main.mainFrame, panel,
        		msg, JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
	}
	
	
	
	public static void copyFolder(File sourceFolder, File destinationFolder) throws IOException {
	    if (sourceFolder.isDirectory()) {
	        if (!destinationFolder.exists()) {
	            destinationFolder.mkdirs();
	        }

	        String[] files = sourceFolder.list();
	        for (String file : files) {
	            File srcFile = new File(sourceFolder, file);
	            File destFile = new File(destinationFolder, file);
	            copyFolder(srcFile, destFile);
	        }
	    } else {
	        Files.copy(sourceFolder.toPath(), destinationFolder.toPath(), StandardCopyOption.REPLACE_EXISTING);
	    }
	}
	
	
	public static class FloatingMenu extends MouseAdapter {
	    private String floatingContent;
	    private JWindow floatingWindow;

	    public FloatingMenu(String content) {
	        this.floatingContent = content;
	    }

	    @Override
	    public void mouseEntered(MouseEvent e) {
	        showFloatingContent(e.getComponent(), e.getXOnScreen(), e.getYOnScreen());
	    }

	    @Override
	    public void mouseExited(MouseEvent e) {
	        hideFloatingContent();
	    }

	    private void showFloatingContent(Component component, int x, int y) {
	        // Obtenir le menu contextuel associ� � l'�v�nement de la souris
	        JPopupMenu popupMenu = (JPopupMenu) SwingUtilities.getAncestorOfClass(JPopupMenu.class, component);

	        // Calculer la nouvelle position pour la fen�tre flottante
	        int offsetX = (popupMenu != null) ? popupMenu.getWidth() : 0;
	        if (x + offsetX + 200 > Toolkit.getDefaultToolkit().getScreenSize().getWidth())
	            offsetX = -offsetX;
	        Point newPosition = new Point(x + offsetX, y - 10); // Ajuster selon les besoins

	        // Cr�er la fen�tre flottante
	        floatingWindow = new JWindow();
	        JLabel label = new JLabel(floatingContent);
	        label.setBorder(BorderFactory.createLineBorder(Color.BLACK));
	        floatingWindow.add(label);
	        floatingWindow.pack();
	        floatingWindow.setLocation(new java.awt.Point(newPosition.x,newPosition.y) );
	        floatingWindow.setVisible(true);

	        // Fermer le contenu flottant lorsque la souris quitte le composant associ�
	        component.addMouseListener(new MouseAdapter() {
	            @Override
	            public void mouseExited(MouseEvent e) {
	                if (floatingWindow != null)
	                    floatingWindow.dispose();
	            }
	        });
	    }

	    public void hideFloatingContent() {
	        if (floatingWindow != null) {
	            floatingWindow.dispose();
	            floatingWindow = null;
	        }
	    }}
	
	
	
	
	
    public static int getDistanceBetweenPoints(Point p1, Point p2) {
        int deltaX = p2.x - p1.x;
        int deltaY = p2.y - p1.y; 
        double distance = Math.sqrt(deltaX * deltaX + deltaY * deltaY); 
        return (int) Math.round(distance);
    }
    
	
	
    public static List<Point> getRectanglePoints(Point a, Point b) {
        List<Point> points = new ArrayList<>();

        int minX = Math.min(a.x, b.x);
        int maxX = Math.max(a.x, b.x);
        int minY = Math.min(a.y, b.y);
        int maxY = Math.max(a.y, b.y);

        for (int i = minX; i <= maxX; i++) {
            for (int j = minY; j <= maxY; j++) {
                points.add(new Point(i, j));
            }
        }

        return points;
    }//##################################################
	public static boolean isCollided(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4) {
	    int o1 = orientation(x1, y1, x2, y2, x3, y3);
	    int o2 = orientation(x1, y1, x2, y2, x4, y4);
	    int o3 = orientation(x3, y3, x4, y4, x1, y1);
	    int o4 = orientation(x3, y3, x4, y4, x2, y2);//dont work

	    if (o1 != o2 && o3 != o4) {
	        return true;  
	    }

	    return false;  
	}
	//##################################################
	private static int orientation(int x1, int y1, int x2, int y2, int x3, int y3) {
	    int val = (y2 - y1) * (x3 - x2) - (x2 - x1) * (y3 - y2);
	    if (val == 0) return 0;  
	    return (val > 0) ? 1 : 2;  
	}//##################################################
	public static List<Point> getArcPoints(int x1, int y1, int x3, int y3, int x2, int y2) {
        List<Point> points = new ArrayList<>();
 
        for (float t = 0; t <= 1; t += 0.005) {
            float x = (1 - t) * (1 - t) * x1 + 2 * (1 - t) * t * x2 + t * t * x3;
            float y = (1 - t) * (1 - t) * y1 + 2 * (1 - t) * t * y2 + t * t * y3;
            points.add(new Point((int) x, (int) y));
             
        }

        return points;
    }

	//##################################################
	public static List<Point> getOvalePoints(int x1, int y1, int x2, int y2) {
        List<Point> points = new ArrayList<>();
 
        int centerX = (x1 + x2) / 2;
        int centerY = (y1 + y2) / 2;
        int a = Math.abs(x2 - x1) / 2;
        int b = Math.abs(y2 - y1) / 2;
 
        int minX = centerX - a;
        int minY = centerY - b;
        int maxX = centerX + a;
        int maxY = centerY + b;

        for (int x = minX; x <= maxX; x++) {
            for (int y = minY; y <= maxY; y++) {
                if (isPointInsideOval(x, y, centerX, centerY, a, b)) {
                    points.add(new Point(x, y));
                }
            }
        }

        return points;
    }
	//##################################################
	private static boolean isPointInsideOval(int x, int y, int centerX, int centerY, int a, int b) { 
        double value = Math.pow((x - centerX) / (double) a, 2) + Math.pow((y - centerY) / (double) b, 2);
        return value <= 1;
    }
	//##################################################
	public static List<Point> getUnfilledOvale(int x1, int y1, int x2, int y2) {
	    List<Point> points = new ArrayList<>();
 
	    int centerX = (x1 + x2) / 2;
	    int centerY = (y1 + y2) / 2;
	    int a = Math.abs(x2 - x1) / 2;
	    int b = Math.abs(y2 - y1) / 2;

	    for (double angle = 0; angle <= 2 * Math.PI; angle += 0.01) {
	        int pointX = (int) (centerX + a * Math.cos(angle));
	        int pointY = (int) (centerY + b * Math.sin(angle));
	        points.add(new Point(pointX, pointY));
	    }

	    return points;
	}
	//##################################################
    public static List<Point> getUnfilledRectangle(Point a, Point b) {
        List<Point> points = new ArrayList<>();

        int minX = Math.min(a.x, b.x);
        int maxX = Math.max(a.x, b.x);
        int minY = Math.min(a.y, b.y);
        int maxY = Math.max(a.y, b.y);
 
        for (int i = minX; i <= maxX; i++) {
            points.add(new Point(i, minY));  
            points.add(new Point(i, maxY));  
        }

        for (int j = minY + 1; j < maxY; j++) {
            points.add(new Point(minX, j));  
            points.add(new Point(maxX, j));  
        }

        return points;
    }
  //##################################################
    public static List<Point> getLinePoints(int x1, int y1, int x2, int y2) {
        List<Point> points = new ArrayList<>();

        int deltaX = Math.abs(x2 - x1);
        int deltaY = Math.abs(y2 - y1);
        int signX = x1 < x2 ? 1 : -1;
        int signY = y1 < y2 ? 1 : -1;

        int erreur = deltaX - deltaY;

        int x = x1;
        int y = y1;

        while (true) {
            points.add(new Point(x, y));

            if (x == x2 && y == y2) {
                break;
            }

            int erreurDouble = 2 * erreur;

            if (erreurDouble > -deltaY) {
                erreur -= deltaY;
                x += signX;
            }

            if (erreurDouble < deltaX) {
                erreur += deltaX;
                y += signY;
            }
        }

        return points;
    }
    
    public static boolean isInRectangle(Point p, int x1, int y1, int x2, int y2) {
		return isInRectangle(p,x1,y1,x2,y2,0);
	}
	public static boolean isInRectangle(Point p, int x1, int y1, int x2, int y2, int tolerance) {
	    int rectX = Math.min(x1, x2) - tolerance;
	    int rectY = Math.min(y1, y2) - tolerance;
	    int rectWidth = Math.abs(x2 - x1) + 2 * tolerance;
	    int rectHeight = Math.abs(y2 - y1) + 2 * tolerance;
	    
	    return p.x >= rectX && p.x <= rectX + rectWidth && p.y >= rectY && p.y <= rectY + rectHeight;
	}

	public static boolean isInOval(Point p, int x1, int y1, int x2, int y2) {
		return isInOval(p,x1,y1,x2,y2,0);
	}
	public static boolean isInOval(Point p, int x1, int y1, int x2, int y2, int tolerance) {
	    int centerX = (x1 + x2) / 2;
	    int centerY = (y1 + y2) / 2;
	    int a = (Math.abs(x2 - x1) + 2 * tolerance) / 2;
	    int b = (Math.abs(y2 - y1) + 2 * tolerance) / 2;

	    double dx = (p.x - centerX) * 1.0 / a;
	    double dy = (p.y - centerY) * 1.0 / b;

	    return dx * dx + dy * dy <= 1;
	}
    
    
    public static Zone getZoneFromPoint(Point p, List<Zone> l, int tolerance) {
    	for(Zone line : l) {  
            if( line.isNear(p,tolerance))return line ;
        }
        return null;
    }
  
	public static Zone getProlongedLine(int clicX, int clicY, int posX, int posY) {
		int endX = clicX;
		int endY = clicY; 
		if (posY > endY) 
			endY = posY - (int) (Math.abs(endY - posY) * 1000 / Math.sqrt((endX - posX)*(endX - posX) + (endY - posY)*(endY - posY)));
		else if (posY < endY) 
			endY = posY + (int) (Math.abs(endY - posY) * 1000 / Math.sqrt((endX - posX)*(endX - posX) + (endY - posY)*(endY - posY))); 
		return new Zone(posX, posY, endX, endY, ZoneType.LINE);//prolonged line
	}
	public static BufferedImage unprioritize(BufferedImage movingImage, int addx, int addy, int posY, boolean hasStair, GameCharacter charact) {
	    List<Integer> stList = hasStair ? charact.stairManager.getHiddenRgb() : null;

	    int width = movingImage.getWidth();
	    int height = movingImage.getHeight();
	    int[] pixels = new int[width * height];
	    movingImage.getRGB(0, 0, width, height, pixels, 0, width);

	    // Utiliser un tableau pour stocker les positions des pixels non-blancs
	    boolean[] isNonWhitePixel = new boolean[width * height];
	    for (int y = 0; y < height; y++) {
	        for (int x = 0; x < width; x++) {
	            if (pixels[y * width + x] != 0x00FFFFFF) {
	                isNonWhitePixel[y * width + x] = true;
	            }
	        }
	    }

	    if (MainGame.instance.priorityArray != null) {
	        for (Pixel pixel : MainGame.instance.priorityArray) {
	            final int stored = pixel.stored;
	            final int rgb = pixel.rgb;
	            if ((hasStair && stList.contains(rgb)) || (!hasStair && stored > posY)) {
	                for (Point p : pixel.points) {
	                    int px = p.x - addx;
	                    int py = p.y - addy;
	                    if (px >= 0 && px < width && py >= 0 && py < height) {
	                        int index = py * width + px;
	                        if (isNonWhitePixel[index]) {
	                            pixels[index] = 0x00FFFFFF;
	                        }
	                    }
	                }
	            }
	        }
	    }

	    movingImage.setRGB(0, 0, width, height, pixels, 0, width);
	    return movingImage;
	}
}