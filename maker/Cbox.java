package me.nina.maker;

import java.awt.Component;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JList;

import me.nina.gameobjects.GameObject;
import me.nina.gameobjects.MegaAction;
import me.nina.gameobjects.GameObject;
import me.nina.gameobjects.SpecialZoneInv;
import me.nina.gameobjects.Stair;
import me.nina.gameobjects.TransitionPoint;
import me.nina.gameobjects.Zone;
import me.nina.sqlmanipulation.SQLLine;

public class Cbox<T> extends JComboBox<T> { private static final long serialVersionUID = 1L;

 	String column;
 	private Collection<T> liste; 
	  
	public Cbox (Collection<T> liste ) { 
		super(liste.toArray((T[]) new Object[0]));
		  this.liste = liste;
		setRenderer(new myRenderer( ));
		 
	}
	  
	public Collection<T> values(){
		liste.clear();
		for(int i = 0 ; i < getItemCount() ; i++)
			liste.add(getItemAt(i));
		return liste;
	}
	@Override public void removeAllItems() {
		liste.clear();
		super.removeAllItems();
	}
	@Override
    public T getSelectedItem() {
        return super.getSelectedItem() != null ? (T) super.getSelectedItem() : null;
    }

    @Override
    public T getItemAt(int i) {
        return super.getItemAt(i) != null ? (T) super.getItemAt(i) : null;
    }

    public void addAll(List<T> objects) {
        
        removeAllItems(); 
        for (T object : objects) {
            liste.add(object);
            addItem(object);
        } 
    }
 



 
}
 
class myRenderer extends DefaultListCellRenderer { private static final long serialVersionUID = 1L;
  

	public myRenderer() {
		// TODO Auto-generated constructor stub
	}

	@Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        if (value instanceof SQLLine) {
            SQLLine l = (SQLLine) value;
            value = l.getString("title")+l.getString("name");   
        }

        else if (value instanceof TransitionPoint){
        	Zone l = ((TransitionPoint) value);
            value = l.getP1().x+","+l.getP1().y+"->"+l.getP2().x+","+l.getP2().y; 
        }
        	
        if (value instanceof GameObject) {
        	GameObject l = (GameObject) value;
            value = l.title();   
        }
        if (value instanceof MegaAction) {
        	MegaAction l = (MegaAction) value;
            value = l.title();   
        }

        if (value instanceof Stair) {
        	Stair l = (Stair) value;
            value = l.level;   
        }
        if (value instanceof SpecialZoneInv) { 
        	SpecialZoneInv l = (SpecialZoneInv) value;
            value = l.frames;   
        }
        
        
        return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
    }
}