package me.nina.maker;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;

import agame.MainGame;
import me.nina.agenerator.Generator; 
import me.nina.gameobjects.Scene;
import me.nina.objects2d.Img;
import me.nina.objects2d.ImgUtils;
import me.nina.objects2d.Position;
import me.nina.sqlmanipulation.SQLTable;

public class FileChooser extends Pane { private static final long serialVersionUID = 1L;

	private File selectedFolder; 
	private File selectedFile = null; 
	Pane right; 
	LazyLoadingFolderExplorer left = new LazyLoadingFolderExplorer(this);
	JScrollPane scrollLeft  = new JScrollPane(left);
	JScrollPane scrollRight;
	public boolean isClicking;

	protected boolean ctrlPressed;

	private List<File> selectedFiles;
 

	//##################################################################
	public FileChooser(JFrame frame) {
    	   
		right=new Pane();right.setFocusable(true);
		scrollRight = new JScrollPane(right);
		this.setLayout(null);
		
		add(scrollLeft);
		add(scrollRight);
		scrollLeft.getVerticalScrollBar().setUnitIncrement(10);
		scrollRight.getVerticalScrollBar().setUnitIncrement(10);
       right.setLayout(new GridLayout(0,6));  
       
       right.addKeyListener(new KeyAdapter() {
           @Override
           public void keyPressed(KeyEvent e) {
               if (e.getKeyCode() == KeyEvent.VK_CONTROL) {
                   ctrlPressed = true;
               }
           }

           @Override
           public void keyReleased(KeyEvent e) {
               if (e.getKeyCode() == KeyEvent.VK_CONTROL) {
                   ctrlPressed = false;
               }
           }
       });
       
	}
  
	//##################################################################
    public void updateRight() {

    	left.setRootFolder(Utils.getWorkingFolder());
    	if(selectedFolder == null)
    		selectedFolder = Utils.getWorkingFolder();
    	File folder = selectedFolder ;
    	right.removeAll(); 
    	 selectedFiles = new ArrayList<>();
        File[] files = folder.listFiles(); if (files != null) {        			
        	Utils.setSize(right, scrollRight.getBounds().width, 200*(files.length/6)); 

        	//right.setPreferredSize(new Dimension(scrollRight.getBounds().width,140*(files.length/6) ));
        	for (File file : files) {
        		if (isImageFile(file) || isSoundFile(file)) {  

        			Pane toPaste = new Pane(); 
        			toPaste.setLayout(new GridLayout(2,1)); 
        			 
        			JComponent c = getComponent(file);
        			toPaste.add(c);
        			if (c instanceof JButton) {
        				toPaste.add(new JLabel("drag here"));
        				((JButton)c).setText(file.getName());
        			}
        			else toPaste.add(new JLabel(file.getName()));
        			Utils.setSize(c, 100, 100); 
        			right.add(toPaste);
        			
        			toPaste.addMouseListener(new MouseAdapter() {
        				@Override
        				public void mouseReleased(MouseEvent e) { 
        					 isClicking = false; 
        					 MouseEvent pressedEvent = SwingUtilities.convertMouseEvent(toPaste, e, Main.actualEditor);
     				        Main.actualEditor.dispatchEvent(pressedEvent);
        				}
        				@Override
        				public void mousePressed(MouseEvent e) { right.requestFocusInWindow();
        				
        					selectedFile = file;
        					
        					Border b = BorderFactory.createLineBorder(Color.black,10);
 
        				        if (ctrlPressed) {
        				            toPaste.setBorder(b);
        				            if (selectedFiles.contains(file)) {
        	                            selectedFiles.remove(file);
        	                            toPaste.setBorder(null);
        	                        } else {
        	                            selectedFiles.add(file);
        	                            toPaste.setBorder(b);
        	                        }
        				        } else {
        				            for (Component a : right.getComponents()) {
        				                if (a instanceof JPanel) {
        				                    ((JPanel) a).setBorder(null);
        				                }
        				            }
        				            toPaste.setBorder(b);
        				        }
        				        
        				        isClicking = true;
        				        Generator .fileSelected(selectedFile.getName());
        				        MouseEvent pressedEvent = SwingUtilities.convertMouseEvent(toPaste, e, Main.actualEditor);
        				        Main.actualEditor.dispatchEvent(pressedEvent);
        					//Main.fileDisplayer.mousePressed(e);
        				}

        			});
        			
        			
        		}
        	}
        	
        }
 
		revalidate();
        repaint();
    }
  //##################################################################

  //##################################################################
    private JComponent getComponent(File file ) {
    	JLabel label = new JLabel();
		try {
			if(isSoundFile(file)){
				return Main.rightController.createBoutton("play",null,100,20,( ActionListener)e->   {   
					if(file.getName().indexOf(".mid")!=-1)
						MainGame.soundPlayer.playMidi( file.getAbsolutePath(),true,false); 
					else
						MainGame.soundPlayer.playAudio(file.getAbsolutePath(),false);
  } ); 
			}
		
		ImageIcon icon = null;
		if(file.getName().toLowerCase().endsWith(".gif")) {
			icon = new ImageIcon(file.getPath()); 

		}
		else icon = new ImageIcon(ImgUtils.getPicture(file.getAbsolutePath()).getScaledInstance(100, 100, Image.SCALE_SMOOTH)); 
		icon = new ImageIcon(icon.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
		label.setIcon(icon); 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return label;
	}

  //##################################################################
	public boolean isImageFile(File file) {
        String name = file.getName().toLowerCase();
        return name.endsWith(".bmp") ||name.endsWith(".jpg") || name.endsWith(".jpeg") || name.endsWith(".png") || name.endsWith(".gif");
    }
	//##################################################################
 
	public boolean isSoundFile(File file) {
        String name = file.getName().toLowerCase();
        return name.endsWith(".mp3") ||name.endsWith(".wav") || name.endsWith(".midi") || name.endsWith(".mid") || name.endsWith(".ogg");
    }
	//##################################################################
 
	
	
	public String getUpdatedImage(String oldFolder, String oldName) {try {
		if(oldName!=null)
		Utils.deleteOldImage(oldFolder,oldName);
		File file = selectedFile;  
		Utils.copy(file,new File(oldFolder+file.getName()));
		return file.getName(); 
	} catch (Exception e1) { e1.printStackTrace(); } return null;}


	public void handleDirectoryChange(String string) {
		selectedFolder = new File(string);
		updateRight();
	}


	public File getSelected() {
		// TODO Auto-generated method stub
		return selectedFile;
	}
	public List<File> getSelection() {
		// TODO Auto-generated method stub
		return selectedFiles;
	}

 

	public File getFolder() {
		// TODO Auto-generated method stub
		return selectedFolder;
	}
 
 
 
    
}

















































class LazyLoadingFolderExplorer extends Pane {

    private JTree folderTree;
    private JTextField selectedFolderField;
    private File rootFolder;
    private boolean processingEvent = false;
	private DefaultMutableTreeNode root;
	private FileChooser fileChooser;
    public LazyLoadingFolderExplorer(FileChooser fileChooser) {
    	this.fileChooser=fileChooser;
        setLayout(new BorderLayout());
       
        
        selectedFolderField = new JTextField(30);
        selectedFolderField.setEditable(false);

        folderTree = new JTree();
        folderTree.addTreeSelectionListener(new TreeSelectionListener() {
             

			@Override
            public void valueChanged(TreeSelectionEvent e) {
				
				  if (!processingEvent) {
				        processingEvent = true;
                TreePath selectedPath = e.getPath();
                Object lastPathComponent = selectedPath.getLastPathComponent();
                if (lastPathComponent instanceof DefaultMutableTreeNode) {
                    DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) lastPathComponent;
                    Object userObject = selectedNode.getUserObject();
                    if (userObject instanceof File) {
                        File selectedFile = (File) userObject;
                        selectedFolderField.setText(selectedFile.getAbsolutePath());
                    }else if (userObject instanceof String && userObject.equals(".")) {
                    	setRootFolder(new File("."));
                    	for(File a :File.listRoots()){
                    		DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(a);
                    		root.add(childNode);
                    		lazyLoadChildren(childNode); 
                    	} 
                    	 lazyLoadChildren(root); 
							 
                    	
                    	
                        
                    }
                    else if (userObject instanceof String && userObject.equals("..")) {
                    	File file = rootFolder.getParentFile();
                    	if(file != null){
                    		 Main.fileDisplayer.handleDirectoryChange(file.getAbsolutePath());
                    		 setRootFolder(file);Utils.setWorkingDirectory(file .toString());
                    		 }
                    	
                    	
                    }
                }
            
                processingEvent = false;
			    }
			}
        });

        folderTree.addTreeExpansionListener(new TreeExpansionListener() {
            @Override
            public void treeExpanded(TreeExpansionEvent event) {
                TreePath path = event.getPath();
                
                Object lastPathComponent = path.getLastPathComponent();
                if(Main.sql!=null)Utils.setWorkingDirectory(path.getLastPathComponent().toString());
                Main.fileDisplayer.handleDirectoryChange(path.getLastPathComponent().toString()); 
                if (lastPathComponent instanceof DefaultMutableTreeNode) {
                    DefaultMutableTreeNode expandedNode = (DefaultMutableTreeNode) lastPathComponent;
                    lazyLoadChildren(expandedNode);
                }
            }

            @Override
            public void treeCollapsed(TreeExpansionEvent event) {
                // No action needed when the tree is collapsed
            }
        });
 
       // add(selectedFolderField, BorderLayout.NORTH);
        add(folderTree, BorderLayout.CENTER);
    }

    public void setRootFolder(File rootFolder) {
        this.rootFolder = rootFolder;
        root = new DefaultMutableTreeNode(rootFolder);
        folderTree.setModel(new DefaultTreeModel(root));
        
        
        
        DefaultMutableTreeNode goUpNode = new DefaultMutableTreeNode("..");  
	    root.add(goUpNode); 
	    DefaultMutableTreeNode goUpNode2 = new DefaultMutableTreeNode(".");  
	    root.add(goUpNode2);
	    
	    
        lazyLoadChildren(root); 
        
         
    }

    private void lazyLoadChildren(DefaultMutableTreeNode parentNode) {
         
            File parentFile = (File) parentNode.getUserObject();
            
            File[] children = parentFile.listFiles();
            int i = 200;
            if (children != null) {
                for (File child : children) { 
                    DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(child);
                    //

                    if (child.isDirectory()) {parentNode.add(childNode);i+=20;
                        childNode.add(new DefaultMutableTreeNode()); // Dummy child for lazy loading
                    }
                }
            }
            Utils.setSize(fileChooser.left, fileChooser.left.getWidth(), i);
            ((DefaultTreeModel) folderTree.getModel()).nodeStructureChanged(parentNode);
       
    }

 
}