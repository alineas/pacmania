package me.nina.maker;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.JarURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.security.*;
import java.util.*;
import java.util.zip.*;

import org.apache.commons.io.IOUtils;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cms.*;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.DigestCalculatorProvider;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.util.Store;

import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import me.nina.editionnodes.EditNode;
import me.nina.editors.ChapterEditor;
import me.nina.editors.CharacterEditor;
import me.nina.editors.Editor; 
import me.nina.editors.SceneEditor;
import me.nina.editors.SceneTpointLinker;
import me.nina.editors.VideoPanel;
import me.nina.editors.Editor.AddType;
import me.nina.editors.SceneEditor.EditType;
import me.nina.editors.InventoryObjectsEditor;
import me.nina.editors.MenuBarEditor;
import me.nina.editors.MessagesEditor;
import me.nina.gameobjects.Action;
import me.nina.gameobjects.Chapter;
import me.nina.gameobjects.Character;
import me.nina.gameobjects.InventoryObject;
import me.nina.gameobjects.Message;
import me.nina.gameobjects.Scene;
import me.nina.maker.Ashe.AshType;
import me.nina.sqlmanipulation.SQLLine;

public class Main { 
	public static JFrame mainFrame, sqlFrame; 
	public static SQLPanel sqlPanel;
	public static JPanel flowPanel = new JPanel();
	public static Connection sql = null;
	public static Dimension screenSize;
	public static String gameName;
	public static ControlLeft leftController;
	public static ControlRighte rightController; 
	public static FileChooser fileDisplayer;   
	public static JScrollPane videoScroll,scrollLeft;
	public static Editor actualEditor; 
	public static Cbox<SQLLine> chapterChooser;
	public static Cbox<SQLLine> sceneListChooser;
	public static Cbox<SQLLine> basicObjectChooser;
	public static Cbox<SQLLine> characterChooser; 
	public static Cbox<AddType> elementChooser;
	private static Timer timer; 
	public static EditType actualEdition = EditType.NOTHING;
	public static EditNode<?> actualNode; 
	public static Cbox<EditType> editList;
	
	//#########################################################################
	public static void main(String[] args) throws Exception {gameName = "game-name";
		try { //open sql and set the logger to the file
			
			if(!new File("games").exists()) {
			 String jarPath = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParent();
			 Files.copy(Thread.currentThread().getContextClassLoader().getResourceAsStream("walk.gif"), Paths.get("","walk.gif"), StandardCopyOption.REPLACE_EXISTING);
			 Files.copy( Thread.currentThread().getContextClassLoader().getResourceAsStream("sierra.jpg"), Paths.get("","sierra.jpg"), StandardCopyOption.REPLACE_EXISTING);
			 final URL configFolderURL = Thread.currentThread().getContextClassLoader().getResource("games");
			 copyJarResourcesRecursively(new File(jarPath).toPath(), (JarURLConnection) configFolderURL.openConnection());
			}
		} catch ( Exception e) { }
		screenSize = Toolkit.getDefaultToolkit().getScreenSize(); 
		videoScroll = new JScrollPane(new VideoPanel()); 
		
		videoScroll.addMouseWheelListener(actualEditor);
		fileDisplayer = new FileChooser(mainFrame); 
		rightController = new ControlRighte();
		leftController = new ControlLeft();
		scrollLeft = new JScrollPane(leftController); 
		 
		mainFrame = new JFrame("pacmania : point and click maker for android by nina intuitive and accurate");
		mainFrame.setDefaultCloseOperation(0);
		mainFrame.addWindowListener( new java.awt.event.WindowAdapter() {
			@Override public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				 if (JOptionPane.showConfirmDialog(mainFrame, "quitter ???", "fermer?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
					 try { sql.close(); } catch (Exception e) { e.printStackTrace(); }
					System.exit(0); } 
			} });
		mainFrame.setVisible(true);
        mainFrame.setLayout(null);
        mainFrame.add(scrollLeft);
        mainFrame.add(videoScroll);
        mainFrame.add(rightController);
        mainFrame.add(fileDisplayer);
        mainFrame.addComponentListener(new ComponentAdapter() {
            @Override public void componentResized(ComponentEvent e) {
            	screenSize = mainFrame.getSize();
                revalidate(); 
            }
        });
        mainFrame.setSize(screenSize.width, screenSize.height-50);
		revalidate(); 
		sqlFrame = new JFrame();    
		sqlPanel = new SQLPanel(sqlFrame); 
		timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override public void run() {
            	try { fileDisplayer.repaint(); } catch (Exception e) { e.printStackTrace(); }
            } }, 0, 100); 
	}

	//#########################################################################  
	public static void openSql(String string) {
		try { sql = DriverManager.getConnection("jdbc:sqlite:"+string);
		} catch (SQLException e) { e.printStackTrace(); }
	}
	//######################################################################### 
	public static void revalidate() {
		 int frameWidth = screenSize.width;  
	        int frameHeight = screenSize.height-50;  
	        
	        if(actualEditor!=null) {
	        	videoScroll.setViewportView(actualEditor); 
	        	
	            MouseWheelListener[] mouseWheelListeners = videoScroll.getMouseWheelListeners();
	            for (MouseWheelListener listener : mouseWheelListeners)  
	            	videoScroll.removeMouseWheelListener(listener); 

	            InputMap inputMap = videoScroll.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
	            inputMap.put(KeyStroke.getKeyStroke("RIGHT"), "do-nothing");
	            inputMap.put(KeyStroke.getKeyStroke("LEFT"), "do-nothing");
	            inputMap.put(KeyStroke.getKeyStroke("UP"), "do-nothing");
	            inputMap.put(KeyStroke.getKeyStroke("DOWN"), "do-nothing");
	        	videoScroll.addMouseWheelListener(actualEditor); 
	        	  KeyListener[] keyListeners = actualEditor.getKeyListeners();
	              for (KeyListener listener : keyListeners)  
	            	  actualEditor.removeKeyListener(listener); 
	        	
	        	actualEditor.requestFocusInWindow();
	        	actualEditor.addKeyListener(new KeyAdapter() {
	        	    @Override public void keyPressed(KeyEvent e) {  actualEditor.keyPressed(e); } });
	        }
	        
 	        int panel1Width = frameWidth / 10*2;
	        int panel1Height = frameHeight - frameHeight / 10 * 3;
	        
	        int panel2Width = frameWidth / 10 * 6;
	        int panel2Height = frameHeight / 10 * 7;
	        
	        int panel3Width = frameWidth / 10*2;
	        int panel3Height = frameHeight / 10 * 7;
	        
	        int panel4Width = frameWidth;
	        int panel4Height = frameHeight / 10 * 4-(frameHeight/40);
  
	        scrollLeft.setBounds(0, 0, panel1Width, panel1Height);
	        videoScroll.setBounds(panel1Width, 0, panel2Width, panel2Height);
	        rightController.setBounds(panel2Width+panel1Width, 0, panel3Width, panel3Height);
	        fileDisplayer.setBounds(0, frameHeight / 7 * 5, panel4Width, panel4Height); 

	        fileDisplayer.scrollLeft.setBounds( 0,0,
	        		fileDisplayer.getBounds().width/3, panel4Height-(frameHeight/40));
	        fileDisplayer.scrollRight.setBounds(fileDisplayer.getBounds().width/3, 0, 
	        		fileDisplayer.getBounds().width/3*2, panel4Height-(frameHeight/40));
 
	        mainFrame.revalidate();  
	        mainFrame.repaint();  
 
	}
	//######################################################################### 
	public static boolean getUserConfirmation(String message) {
	    int option = JOptionPane.showOptionDialog(
	            null, message, "Confirmation",
	            JOptionPane.YES_NO_OPTION,
	            JOptionPane.QUESTION_MESSAGE,
	            null, null, null);

	    return option == JOptionPane.YES_OPTION;
	}
	//######################################################################### 
	public static String getUserInput( ) {
		return getUserInput("name/title");
	}
	//######################################################################### 
    public static String getUserInput(String question) { 
    	JTextField textField = new JTextField();
    	Object[] message = {"Entrez votre texte :", textField};

    	JOptionPane optionPane = new JOptionPane(
    	    message, 
    	    JOptionPane.PLAIN_MESSAGE,
    	    JOptionPane.OK_CANCEL_OPTION
    	);

     	JDialog dialog = optionPane.createDialog(null, question);

     	dialog.addWindowListener(new WindowAdapter() {
    	    @Override
    	    public void windowOpened(WindowEvent e) {
    	        textField.requestFocusInWindow();
    	    }
    	});
 
    	dialog.setVisible(true);
 
    	Object selectedValue = optionPane.getValue();
 
    	if (selectedValue.equals(JOptionPane.OK_OPTION)) { 
    	    return textField.getText(); 
    	} return null; 
         
    }
  //######################################################################### 
    public static  void addCharacter( ) {actualNode=null;
		String title = Main.getUserInput();
		if(title==null || title.equals(""))
			return;
		Character charac = new Character(title);
		int id = charac.save();
		charac.generateInitialState(id);
		characterChooser.add(Character.get(id));
		editCharacter( id); 
	}
  //######################################################################### 
	public static void addScene( ) {actualNode=null;
		String title = Main.getUserInput();
		if(title==null || title.equals(""))
			return;
		int id = new Scene(title).save();
		sceneListChooser.add(Scene.get(id));
		editScene(id);
	}
	//######################################################################### 
	public static  void addChapter( ) { actualNode=null;
		String title = Main.getUserInput();
		if(title==null || title.equals("") || Scene.getScenes().size()==0 || Character.getAll().size()==0)
			return;
		int id = new Chapter(title).save();
		chapterChooser.add(Chapter.get(id));
		editChapter(id);
	}//######################################################################### 
	public static void editScene(int i ) {  setActualEditor(new SceneEditor(i)); } 
	public static  void editChapter(int i) {  setActualEditor(new ChapterEditor(i ));  actualNode=null; } 
	public static void editInvObj(int id) {  setActualEditor(new InventoryObjectsEditor(id)); actualNode=null;}
	public static  void editCharacter(int i) {  setActualEditor( new CharacterEditor(i ));   actualNode=null;} 
	public static void linkScenes() { setActualEditor(new SceneTpointLinker()); actualNode=null;}
	public static void editMenuBar() { setActualEditor(new MenuBarEditor()); actualNode=null; } 
	public static void editMessages() { setActualEditor(new MessagesEditor()); actualNode=null; } 
	//######################################################################### 
	 private static Stack<Editor> editorStack = new Stack<>(); 
	    public static void setActualEditor(Editor e) {
	    	editorStack.push(e); 
	        actualEditor=e;
	        revalidate();
	    } 
	    public static void goBack() {
	        if (editorStack.size() >= 2) { 
	            Editor currentEditor = editorStack.pop();
	            Editor previousEditor = editorStack.peek();
	            editorStack.remove(currentEditor); 
	            setActualEditor(previousEditor);
	            leftController.removeAll();
	            actualEditor.updateLeftEditor();
	            
	            revalidate(); 
	             
	        } 
	    }
	  //######################################################################### 
	public static void addInvObject() {actualNode=null;
		String title = getUserInput();
		if(title==null || title.equals(""))
			return;
		InventoryObject obj = (InventoryObject) new InventoryObject(title).save(); 
		obj.generateInitialState( ) ;
		setActualEditor(new InventoryObjectsEditor(obj.id));
	}
	//######################################################################### 
	private static void copyJarResourcesRecursively(Path destination, JarURLConnection jarConnection) throws IOException {
        JarFile jarFile = jarConnection.getJarFile();
        for (Enumeration<JarEntry> it = jarFile.entries() ; it.hasMoreElements();) {
            JarEntry entry = it.nextElement();
            if (entry.getName().startsWith(jarConnection.getEntryName())) {
                if (!entry.isDirectory()) {
                    try (InputStream entryInputStream = jarFile.getInputStream(entry)) {
                        Files.copy(entryInputStream, Paths.get(destination.toString(), entry.getName()));
                    }
                } else {
                    Files.createDirectories(Paths.get(destination.toString(), entry.getName()));
                }
            }
        }
    }
	//######################################################################### 
	public static void export() {
		ControlRighte panel = rightController;
		panel.removeAll();
		panel.setLayout(new GridLayout(0,2));
		panel.createLabel("android", panel, 150, 30);
		panel.createLabel("desktop", panel, 150, 30); 
		panel.createBoutton("default language", panel, 150, 30,(ActionListener)e->{export("default",0);});
		panel.createBoutton("default language", panel, 150, 30,(ActionListener)e->{export("default",1);});
		for (String lang : Editor.getGameInfo().getArray("languages" ) ) {
			panel.createBoutton(lang, panel, 150, 30,(ActionListener)e->{export(lang,0);});
			panel.createBoutton(lang, panel, 150, 30,(ActionListener)e->{export(lang,1);});
		}
		panel.revalidate();
		
	}
	//######################################################################### 
	    private static void export(String lang, int i) {try {
	    	Message.translate(lang);
	    	  
	    	  for(Action action : Action.getAll())
	    		  for(Ashe ash : action.ashToSet())
	    			  if(ash.type == AshType.WILL_SAY||ash.type == AshType.WILL_SAY_UNFREEZE){
	    				  Message message = (Message) ash.possessor();
	    				 String name = message.getFileName();
	    				  String folder = "games/"+Main.gameName+"/dialogs/"+lang+"/dialogs/";
	    					if(! Utils.copy(new File( folder+name), new File("games/"+Main.gameName+"/dialogs/"+name)))
	    					 Utils.log("error copy "+name+" "+message.text());
	    				 
	    			  }
	    	
	            if (i == 0) { // android
	            	  
	                      InputStream apkStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("android.apk");
	                      Path tempApkPath = Files.createTempFile("tempAndroid", ".apk");
	                      File tempApkFile = tempApkPath.toFile();
	                      tempApkFile.deleteOnExit(); 
	                      try (OutputStream out = new FileOutputStream(tempApkFile)) {
	                          byte[] buffer = new byte[1024];
	                          int bytesRead;
	                          while ((bytesRead = apkStream.read(buffer)) != -1)  
	                              out.write(buffer, 0, bytesRead); 
	                      }//apk is now temp file
 
	                      modifyApk(tempApkFile.getAbsolutePath(), "new-android.apk"); 
	                    
	                      SignedJar bcApk = new SignedJar( new FileOutputStream(Main.gameName+"-"+lang+".apk"));
	                    		JarFile jsApk = new JarFile(new File("new-android.apk"));
	                    		Enumeration<JarEntry> entries = jsApk.entries();
	                    		while (entries.hasMoreElements()) {
	                    		    JarEntry entry = entries.nextElement();
	                    		    String name = entry.getName();
	                    		    if (!entry.isDirectory() && !name.startsWith("META-INF/")) {
	                    		        InputStream eis = jsApk.getInputStream(entry);
	                    		        bcApk.addFileContents(name, IOUtils.toByteArray(eis));
	                    		        eis.close();
	                    		    }
	                    		}
	                    		jsApk.close();
	                    		bcApk.close(); 
	                  
	            } else { // desktop 
	                    InputStream str = Thread.currentThread().getContextClassLoader().getResourceAsStream("desktop.jar");  
	                    File folderToAdd = new File("games");  
	                    File fileToAdd = new File("games/"+Main.gameName+"/game.npcma"); 
	                    Path tempJarPath = Files.createTempFile("tempDesktop", ".jar");
	                    File tempJarFile = tempJarPath.toFile();
	                    tempJarFile.deleteOnExit(); 
	                    copyJarWithAdditions(str, tempJarFile, folderToAdd, fileToAdd); 
	                    Files.move(tempJarPath, Paths.get(Main.gameName+"-"+lang+".jar"), StandardCopyOption.REPLACE_EXISTING);
	                 
	            }
	         
	    } catch (Exception e) { e.printStackTrace(); }}
	  //######################################################################### 
	    private static void copyJarWithAdditions(InputStream originalJarStream, File newJarFile, File folderToAdd, File fileToAdd) throws IOException {
	        
	    	File tempDir = new File("temp");
	        tempDir.mkdir();

	        // Extract org/sqlite resources to temporary directory
	        final URL configFolderURL = Thread.currentThread().getContextClassLoader().getResource("org/sqlite");
	        if (configFolderURL != null) {
	            copyJarResourcesRecursively(tempDir.toPath(), (JarURLConnection) configFolderURL.openConnection());
	        }
	    	
	    	try (JarInputStream jarInputStream = new JarInputStream(originalJarStream);
	             JarOutputStream jarOutputStream = new JarOutputStream(new FileOutputStream(newJarFile), jarInputStream.getManifest())) {

	            // Copy the original JAR entries to the new JAR
	            JarEntry entry;
	            while ((entry = jarInputStream.getNextJarEntry()) != null) {
	                jarOutputStream.putNextEntry(entry);
	                if (!entry.isDirectory()) {
	                    byte[] buffer = new byte[1024];
	                    int bytesRead;
	                    while ((bytesRead = jarInputStream.read(buffer)) != -1) {
	                        jarOutputStream.write(buffer, 0, bytesRead);
	                    }
	                }
	                jarInputStream.closeEntry();
	                jarOutputStream.closeEntry();
	            }

	            // Add new files/folders to the JAR
	            addFileToJar(jarOutputStream, folderToAdd, "");
	            addFileToJar(jarOutputStream, fileToAdd, "");
	            addFileToJar(jarOutputStream, handleFile("intro.mp4"), "");
	            addFileToJar(jarOutputStream, new File(tempDir, "org"), "");
	        }finally {deleteDirectory(tempDir);}
	    }
 

	  private static File handleFile(String name) throws IOException {
	        File introFile = new File(name);
	        if (!introFile.exists()) {
	            boolean userConfirmed = Main.getUserConfirmation("Select "+name+" file?");
	            if (userConfirmed) {
	                JFileChooser fileChooser = new JFileChooser(Utils.getWorkingFolder());
	                fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
	                int res = fileChooser.showOpenDialog(null);
	                if (res == JFileChooser.APPROVE_OPTION) {
	                    File selectedFile = fileChooser.getSelectedFile();
	                    Utils.copy(selectedFile, introFile);
	                }
	            } else {
		  InputStream introStr = Thread.currentThread().getContextClassLoader().getResourceAsStream(name);
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        byte[] buffer = new byte[8192];
	        int bytesRead;
	        while ((bytesRead = introStr.read(buffer)) != -1)  
	            baos.write(buffer, 0, bytesRead); 
	        byte[] soundfontBytes = baos.toByteArray(); 
	        introFile = new File(name);  
	        introFile.createNewFile();
	        try (FileOutputStream fos = new FileOutputStream(introFile)) {
	            fos.write(soundfontBytes);
	        } 
	        
	        baos.close();
	        introStr.close();
	            }
	        }
	        return introFile;
	}

 
	 
	     
	  //######################################################################### 
	    private static void addFileToJar(JarOutputStream jarOutputStream, File source, String parentEntryName) throws IOException {
	        String entryName = parentEntryName + source.getName();
	        if (source.isDirectory()) {
	            if (source.getName().equals("dialogs") && !parentEntryName.isEmpty()) {
	                // dont take language folders, only mp3 files
	                for (File file : Objects.requireNonNull(source.listFiles())) {
	                    if (file.isFile()) {
	                        addFileToJar(jarOutputStream, file, entryName + "/");
	                    }
	                }
	            } else {
	                if (!entryName.endsWith("/")) {
	                    entryName += "/";
	                }
	                JarEntry entry = new JarEntry(entryName);
	                jarOutputStream.putNextEntry(entry);
	                jarOutputStream.closeEntry();

	                for (File file : Objects.requireNonNull(source.listFiles())) {
	                    addFileToJar(jarOutputStream, file, entryName);
	                }
	            }
	        } else {
	            JarEntry entry = new JarEntry(entryName);
	            jarOutputStream.putNextEntry(entry);

	            try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(source))) {
	                byte[] buffer = new byte[1024];
	                int bytesRead;
	                while ((bytesRead = in.read(buffer)) != -1) {
	                    jarOutputStream.write(buffer, 0, bytesRead);
	                }
	            }
	            jarOutputStream.closeEntry();
	        }
	    }
	  //######################################################################### 
	    public static void modifyApk(String inputApk, String outputApk) throws Exception {
	    	 // Décompilation de l'APK
	        String[] decompileArgs = {"d", inputApk, "-o", "temp_directory","-f"};
	        brut.apktool.Main.main(decompileArgs);
 
	        File soundfontFile =handleFile("soundfont.sf2");
	        File folderToAdd = new File("games");  
	        File fileToAdd = new File("games/"+Main.gameName+"/game.npcma");  
	         
	        addFileToApk("temp_directory/assets/", folderToAdd);
	        addFileToApk("temp_directory/assets/", fileToAdd);
	        addFileToApk("temp_directory/assets/", soundfontFile); 
	        File introFile = handleFile("intro.mp4");
	        addFileToApk("temp_directory/assets/", introFile); 
	        String[] compileArgs = {"b", "temp_directory", "-o", outputApk};
	        modifyApkToolYml("temp_directory");//do not compress my assets
	        brut.apktool.Main.main(compileArgs); 
	        deleteDirectory(new File("temp_directory"));
	         
	    } 
		private static void modifyApkToolYml(String directoryPath) throws IOException {
	        File apkToolYmlFile = new File(directoryPath, "apktool.yml");
	        List<String> lines = Files.readAllLines(apkToolYmlFile.toPath(), StandardCharsets.UTF_8);
	        List<String> modifiedLines = new ArrayList<>();

	        boolean doNotCompressFound = false;
	        for (String line : lines) {
	            modifiedLines.add(line);
	            if (line.trim().startsWith("doNotCompress:")) {
	                doNotCompressFound = true;
	            }
	            if (doNotCompressFound && (line.trim().isEmpty() || line.trim().startsWith("- "))) {
	                if (!modifiedLines.contains("- mp4")) {
	                    modifiedLines.add("- mp4");
	                }
	                if (!modifiedLines.contains("- gif")) {
	                    modifiedLines.add("- gif");
	                }
	                if (!modifiedLines.contains("- mp3")) {
	                    modifiedLines.add("- mp3");
	                }
	            }
	        }

	        Files.write(apkToolYmlFile.toPath(), modifiedLines, StandardCharsets.UTF_8, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
	    }
		//######################################################################### 
	    private static void deleteDirectory(File directory) {
	        File[] files = directory.listFiles();
	        if (files != null) {
	            for (File file : files) {
	                if (file.isDirectory()) {
	                    deleteDirectory(file);
	                } else {
	                    file.delete();
	                }
	            }
	        }
	        directory.delete();
	    }
	  //######################################################################### 
	    private static void addFileToApk(String targetDir, File source) throws IOException {
	        Path targetPath = Paths.get(targetDir);
	        new File(targetDir).mkdirs();

	        if (source.isDirectory()) {
	            //dont copy all languages
	            if (source.getName().equals("dialogs")) {
	                for (File file : Objects.requireNonNull(source.listFiles())) {
	                    if (file.isFile()) { //so  take only the mp3 files, not folders
	                        addFileToApk(targetDir + source.getName() + "/", file);
	                    }
	                }
	            } else {
	                for (File file : Objects.requireNonNull(source.listFiles())) {
	                    addFileToApk(targetDir + source.getName() + "/", file);
	                }
	            }
	        } else {
	            Files.copy(source.toPath(), targetPath.resolve(source.getName()), StandardCopyOption.REPLACE_EXISTING);
	        }
	    }
}
//######################################################################### 
//######################################################################### 
class SignedJar {//@author Michal Rydlo, Maciek Muszkowski

        private static final String DIG_ALG = "SHA1";
        private static final String SIG_ALG = "SHA1withRSA";
        private static final String CREATED_BY = System.getProperty("java.version")
                + " (" + System.getProperty("java.vendor") + ")";
        private static final String SIG_FN = "META-INF/INTERMED.SF";
        private static final String SIG_RSA_FN = "META-INF/INTERMED.RSA";

        private final Collection<X509Certificate> mChain;
        private final X509Certificate mCert;
        private final PrivateKey mSignKey;
        private final MessageDigest mHashFunction;

        private final Map<String, String> mManifestAttributes;
        private final Map<String, String> mFileDigests;
        private final Map<String, String> mSectionDigests;
        private String mManifestHash;
        private String mManifestMainHash;

        private JarOutputStream mJarOut; 
        
    	private static final String KEYSTORE_PASSWORD = "password";
        private static final String KEY_ALIAS = "mykey";
        private static final String KEY_PASSWORD = "android";
         
        public SignedJar(OutputStream out )
                throws NoSuchAlgorithmException, IOException, CertificateException, KeyStoreException, UnrecoverableKeyException {
        	 
            KeyStore keyStore = KeyStore.getInstance("JKS");
            InputStream keystoreStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("my-release-key.jks");
            keyStore.load(keystoreStream, KEYSTORE_PASSWORD.toCharArray());
 
            PrivateKey signKey = (PrivateKey) keyStore.getKey(KEY_ALIAS, KEY_PASSWORD.toCharArray());
            Certificate[] certificateChain = keyStore.getCertificateChain(KEY_ALIAS);
 
            Collection<X509Certificate> chain = new ArrayList<>();
            for (Certificate certe : certificateChain) {
                if (certe instanceof X509Certificate) {
                    chain.add((X509Certificate) certe);
                }
            }
            
            X509Certificate cert = (X509Certificate) certificateChain[0];
            mJarOut = new JarOutputStream(out);
            mChain = chain;
            mCert = cert;
            mSignKey = signKey;
            mManifestAttributes = new LinkedHashMap<>();
            mFileDigests = new LinkedHashMap<>();
            mSectionDigests = new LinkedHashMap<>();
            mHashFunction = MessageDigest.getInstance(DIG_ALG);
        } 
        public void addManifestAttribute(String name, String value) {
            mManifestAttributes.put(name, value);
        } 
        public void addFileContents(String filename, byte[] contents)
                throws IOException {
        	if(filename.contains(".mp4")||filename.contains(".gif")||filename.contains(".mp3")) {
        		addUncompressedFileContents(filename, contents);//do not compress my assets files
        		return;
        	}
            mJarOut.putNextEntry(new JarEntry(filename));
            mJarOut.write(contents);
            mJarOut.closeEntry();

            byte[] hashCode = mHashFunction.digest(contents);
            mFileDigests.put(filename, toBase64String(hashCode));
        }
 
        public void addUncompressedFileContents(String filename, byte[] contents) throws IOException {
            // Calcul du hachage du contenu du fichier pour la signature
            byte[] hashCode = mHashFunction.digest(contents);
            mFileDigests.put(filename, toBase64String(hashCode)); 
            JarEntry entry = new JarEntry(filename);
            entry.setSize(contents.length);  
            entry.setCompressedSize(-1);   
            entry.setCrc(calcChecksum(contents));  
            entry.setMethod(JarEntry.STORED); 
            mJarOut.putNextEntry(entry);
            mJarOut.write(contents);
            mJarOut.closeEntry();
        } 
        private long calcChecksum(byte[] bytes) {
            CRC32 crc = new CRC32();
            crc.update(bytes);
            return crc.getValue();
        }
         
        public void finish() throws IOException {
            writeManifest();
            byte sig[] = writeSigFile();
            writeSignature(sig);
        }
 
        public void close() throws IOException {
            finish();
            mJarOut.close();
        }
 
        private CMSSignedDataGenerator createSignedDataGenerator() throws Exception {
            Security.addProvider(new BouncyCastleProvider());

            Store certStore = new JcaCertStore(mChain);
            ContentSigner signer = new JcaContentSignerBuilder(SIG_ALG)
                    .setProvider("BC").build(mSignKey);
            CMSSignedDataGenerator generator = new CMSSignedDataGenerator();
            DigestCalculatorProvider dcp = new JcaDigestCalculatorProviderBuilder()
                    .setProvider("BC").build(); 
            SignerInfoGenerator sig = new JcaSignerInfoGeneratorBuilder(dcp)
                    .setDirectSignature(true)
                    .build(signer, mCert);

            generator.addSignerInfoGenerator(sig);
            generator.addCertificates(certStore);
            return generator;
        } 
        private byte[] signSigFile(byte[] sigContents) throws Exception {
            CMSSignedDataGenerator gen = createSignedDataGenerator();
            CMSTypedData cmsData = new CMSProcessableByteArray(sigContents);
            CMSSignedData signedData = gen.generate(cmsData, false); 
            ASN1Primitive asn1Primitive = ASN1Primitive.fromByteArray(signedData.getEncoded());
            return asn1Primitive.getEncoded();
        }
      
        private void writeSignature(byte[] sigFile) throws IOException {
            mJarOut.putNextEntry(new JarEntry(SIG_RSA_FN));
            try {
                byte[] signature = signSigFile(sigFile);
                mJarOut.write(signature);
            } catch (IOException e) {
                throw e;
            } catch (Exception e) {
                throw new RuntimeException("Signing failed.", e);
            } finally {
                mJarOut.closeEntry();
            }
        } 
        private byte[] writeSigFile() throws IOException {
            mJarOut.putNextEntry(new JarEntry(SIG_FN));
            Manifest man = new Manifest(); 
            Attributes mainAttributes = man.getMainAttributes();
            mainAttributes.put(Attributes.Name.SIGNATURE_VERSION, "1.0");
            mainAttributes.put(new Attributes.Name("Created-By"), CREATED_BY);
            mainAttributes.put(new Attributes.Name(
                    DIG_ALG + "-Digest-Manifest"),
                    mManifestHash);
            mainAttributes.put(new Attributes.Name(
                    DIG_ALG + "-Digest-Manifest-Main-Attributes"),
                    mManifestMainHash);
 
            Attributes.Name digestAttr = new Attributes.Name(
                    DIG_ALG + "-Digest");
            for (Map.Entry<String, String> entry : mSectionDigests.entrySet()) {
                Attributes attributes = new Attributes();
                man.getEntries().put(entry.getKey(), attributes);
                attributes.put(digestAttr, entry.getValue());
            }

            man.write(mJarOut);
            mJarOut.closeEntry();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            man.write(baos);
            return baos.toByteArray();
        } 
        private String hashEntrySection(String name, Attributes attributes)
                throws IOException {
            // crate empty manifest
            Manifest manifest = new Manifest();
            manifest.getMainAttributes().put(
                    Attributes.Name.MANIFEST_VERSION, "1.0");
            ByteArrayOutputStream o = new ByteArrayOutputStream();
            manifest.write(o);
            int emptyLen = o.toByteArray().length;

            // get hash of entry without manifest header
            manifest.getEntries().put(name, attributes);
            o.reset();
            manifest.write(o);
            byte[] ob = o.toByteArray();
            o.close();
            ob = Arrays.copyOfRange(ob, emptyLen, ob.length);
            return toBase64String(mHashFunction.digest(ob));
        } 
        private String hashMainSection(Attributes attributes) throws IOException {
            Manifest manifest = new Manifest();
            manifest.getMainAttributes().putAll(attributes);
            return toBase64String(getManifestHash(manifest));
        } 
        private void writeManifest() throws IOException {
            mJarOut.putNextEntry(new JarEntry(JarFile.MANIFEST_NAME));
            Manifest man = new Manifest();
 
            Attributes mainAttributes = man.getMainAttributes();
            mainAttributes.put(Attributes.Name.MANIFEST_VERSION, "1.0");
            mainAttributes.put(new Attributes.Name("Created-By"), CREATED_BY);

            for (Map.Entry<String, String> entry : mManifestAttributes.entrySet()) {
                mainAttributes.put(new Attributes.Name(entry.getKey()),
                        entry.getValue());
            }
 
            Attributes.Name digestAttr = new Attributes.Name(
                    DIG_ALG + "-Digest");
            for (Map.Entry<String, String> entry : mFileDigests.entrySet()) {
                Attributes attributes = new Attributes();
                man.getEntries().put(entry.getKey(), attributes);
                attributes.put(digestAttr, entry.getValue());
                mSectionDigests.put(entry.getKey(),
                        hashEntrySection(entry.getKey(), attributes));
            }
            man.write(mJarOut);
            mJarOut.closeEntry();

            mManifestHash = toBase64String(getManifestHash(man));
            mManifestMainHash = hashMainSection(man.getMainAttributes());
        } 
        private byte[] getManifestHash(Manifest manifest) throws IOException {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            manifest.write(baos);
            baos.close();
            return mHashFunction.digest(baos.toByteArray());
        } 
        private static String toBase64String(byte[] data) {
        	return Base64.getEncoder().encodeToString(data);
            
        }
    }