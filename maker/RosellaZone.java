package me.nina.maker;

import java.awt.AlphaComposite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.List;

import me.nina.editors.Editor;
import me.nina.gameobjects.Animation;
import me.nina.gameobjects.Character;
import me.nina.gameobjects.Character.PosType;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.gameobjects.Scene;
import me.nina.gameobjects.Zone;
import me.nina.objects2d.Img;
import me.nina.objects2d.ImgUtils;
import me.nina.objects2d.Pixel;
import me.nina.objects2d.Point;

public class RosellaZone extends Pane{
	
	BufferedImage originalImage;
	private BufferedImage movingImage; 
	Point feetPosition = new Point(0,0);   
	protected int oldBlitX,oldBlitY ; 
	
	private double scal = 1.0; 
	private Scene scene; 
 	public PosType posType = PosType.BOT_LEFT;
	
	public static Character actualCharacter = Character.getAll().iterator().next();
	public Animation currentAnim ;
	//#################################################################################### 
	public RosellaZone(Scene scene ) {
		 this.setOpaque(false);
		currentAnim = actualCharacter.getInitialState().movingAnimation(posType);
		  
		try { 
			actualCharacter=Editor.actualCharacter();
			if(actualCharacter==null) {
				Editor.getGameInfo().update("actualcharacter",Character.getAll().iterator().next().id+"");
				
			} 
		} catch (Exception e) {actualCharacter = Character.getAll().iterator().next(); }
		
		this.originalImage =ImgUtils.getPicture("walk.gif");
		this.movingImage = originalImage; 
		this.scene=scene; 
		calculateScale(feetY());
		  }
	
	//####################################################################################
	public void paint(Graphics g, int feetX, int feetY,PosType t) { try { 
		
		feetPosition=new Point(feetX,feetY);
		setBounds(blitX(),blitY(),getScalledW(), getScalledH());
		generateMovingImage( t); 
		
		 
		 calculateScale(feetY()); 
	} catch (Exception e) { 	e.printStackTrace();} }
	 protected void paintComponent(Graphics g) {
		 super.paintComponent(g);
		 g.drawImage(movingImage, 0 , 0 , getScalledW(), getScalledH(), null);
	 }
	//####################################################################################
	public void generateMovingImage( PosType t) { 
		if(t!=null)posType=t;
		else if(Math.abs(oldBlitX-blitX())>15||Math.abs(oldBlitY-blitY())>15){
		PosType newPosType = PosType.calculatePosType(oldBlitX, oldBlitY, blitX(), blitY());
		if(newPosType!=PosType.UNUSED)posType = newPosType;
		oldBlitX = blitX(); 
		oldBlitY = blitY();
		}
		
		setWalk(posType);  
			movingImage = ImgUtils.getCopy(originalImage); 
			movingImage =ImgUtils.scal(movingImage,scal);
			  try { Graphics2D g2 = movingImage.createGraphics();  
				g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f)); 
				if(Utils.getMatrix( scene,false) != null)
				for (Pixel pixel : Utils.getMatrix( scene,false)) {
				    int stored = pixel.stored; // R�cup�rer la valeur stored du pixel
				    
				    // V�rifier si le pixel doit �tre transparent
				    if (stored > feetY()) {
				       

				        // Parcourir les listes de coordonn�es x et y
				        for (Point p : pixel.points) {
				            int x = p.x - blitX();
				            int y = p.y - blitY();
				            
				            // V�rifier si les coordonn�es sont valides
				            if (x >= 0 && y >= 0 && x < movingImage.getWidth() && y < movingImage.getHeight()) {
				                movingImage.setRGB(x, y, 0x00FFFFFF); // Pixel transparent
				            }
				        }
				    }
				} } catch (Exception e) {   } }

	//####################################################################################
	public void calculateScale(int feetY) { 
	    double factor = ((double)scene.scalePercents())/100;
	    int HlimiteTop = scene.scalefactorTopLimit() ;
	    int HlimiteBot = scene.scalefactorBotLimit(); 
	    if (feetY < HlimiteTop) {   scal = factor;  return;  } 
	    else if (feetY > HlimiteBot) {    scal = 1.0;   return;    }  
	    double m = (1.0 - factor) / (HlimiteBot - HlimiteTop);
	    double b = factor - m * HlimiteTop; 
	    scal = m * feetY + b; 
	} 
 
 
	//################################################################
	public void setWalk(PosType pos ) {if(pos!=PosType.UNUSED) try { 
		posType = pos; 
		currentAnim= actualCharacter.getInitialState().idleAnimation(pos);
			originalImage =  ImgUtils.getCopy(currentAnim.getActualFrame());
	} catch (Exception e) { e.printStackTrace();Utils.log(pos+"caused an error in rosella zone"); }  }
	//################################################################
	public boolean isNear(Point p) {
		Zone z = new Zone(blitX() , blitY(), blitX()+getScalledW(), blitY()+getScalledH(), ZoneType.RECTANGLE); 
		
		if( z.isNear(p,   10))
			return true;
		return false;
	}
	//################################################################
	public int getScalledH() { return (int) (movingImage.getHeight() ); }
	public int getScalledW() { return (int) (movingImage.getWidth() ); } 
	 
	int blitY() { return feetPosition.y - getScalledH(); } 
	int blitX() { return feetPosition.x - getScalledW()/2; }
	public int feetY() { return feetPosition.y; } 
	public int feetX() { return feetPosition.x; }
	public void setPos(Point pos) {feetPosition=pos;}
	
	public int getPosType() {  return posType.getValeur(); }
	
	
	 	
}
