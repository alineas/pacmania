package me.nina.maker;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import me.nina.gameobjects.GameObject;
import me.nina.maker.Ashe.AshType; 

public class Ashe  {
	
	private boolean jtreeSelected;
	private String title;
	public int possessor=-1;
	AshType type;
	int nbr;
	
	public static  enum AshType {
		HAS_BEEN_COLLIDED(0),/////////////////not tested
	     HAS_BEEN_DISABLED(1),WILL_BE_DISABLED(2),//
	     IS_IN_STATE(3),  WILL_BE_IN_STATE(4), //
	     HAS_IN_HAND(5),//
	     SCENE_HAS_BEEN_VISITED(6),//////////////////////not tested
	     HAS_IN_INVENTORY(7),WILL_HAVE_IN_INVENTORY(8),//
	     IS_CLICKED_ON(9),//
	     ACTUAL_POSITION(10),WILL_BE_IN_POSITION(11),//
	     WILL_DO_AFTER_TIMER(12),//
	     WILL_DO_WITH_RANDOM(13),//
	     RECORD_ACTUAL_POSITION(14), WILL_BE_IN_POSITION_RECORDED(15), 
	     OR(16),
	     HAS_BEEN_SAID(20),WILL_SAY(21),//
	     WILL_HAVE_IN_HAND(22),
	     SCENE_WAS_EXITED(24),
	     WILL_BE_SPAWNED(25),
	     WILL_BE_CLICKED(26),
	     SCENE_HAS_BEEN_ENTERED(27),
	     WILL_BE_DESPAWN(28),
	     HAS_BEEN_CLICKED(29),
	     IS_THE_ACTOR(30),
	     ACTION_TO_DO_WITH_BEFORE_CONDITION_CHECK(31),
	     IS_ARRIVED(32),
	     HAS_BEEN_SPAWNED(33),
	     WILL_LOOK_RIGHT(34),
	     WILL_LOOK_LEFT(35),
	     WILL_LOOK_TOP(36),
	     WILL_LOOK_BOTTOM(37),
	     WILL_BE_LOOKED(38),
	     WILL_INSIDE_OUTSIDE_THIS_STATE(39),
	     WILL_INSIDE_THIS_STATE(40),
	     WILL_OUTSIDE_THIS_STATE(41) ,
	     WILL_INSIDE_WAIT_AND_OUTSIDE_STATE(17),
	     
	     WILL_LOOK_BOT_RIGHT(42),
	     WILL_LOOK_BOT_LEFT(43),
	     WILL_LOOK_TOP_RIGHT(44),
	     WILL_LOOK_TOP_LEFT(45),
	     IS_ACTUAL_SCENE(46),
	     HAS_NOTHING_IN_HAND(47),
	     SAVE_BEFORE_DEAD(48),
	     RESTORE_BEFORE_DEAD(49),
	     WILL_SAY_UNFREEZE(50),
	     ACTION_TO_DO_WITH_AFTER_CONDITION_CHECK(51),ACTION_CAN_BE_EXECUTED(52),  ENTER_CHAPTER(53),
	     SCENE_HAS_BEEN_CLICKED(54),IS_IN_ZONE(55), ZONE_HAS_BEEN_CLICKED (56)
	     ;
		
	    private final int valeur; 
	  
	    AshType(int valeur) { this.valeur = valeur;  } 
	    public int getValeur() {  return valeur; } 
	    //@Override public String toString() { return String.valueOf(getValeur()); }
	    public static AshType from(String s) { 
	    	return from(Integer.parseInt(s));
	    }
	    public static AshType from(int valeur) {
	        for (AshType type : values()) {
	            if (type.getValeur() == valeur) {
	                return type;
	            }
	        }
	        throw new IllegalArgumentException("Aucun AshType correspondant � la valeur : " + valeur);
	    }
	    
	     
	    
	}

	
	
	public boolean isJTreeSelected() {
	    return jtreeSelected;
	}

	public void setJTreeSelected(boolean jtreeSelected) {
	    this.jtreeSelected = jtreeSelected;
	}
	public Ashe (int g, AshType v, int n ){ 
		possessor = g ;
		type = v;
		nbr = n;
	}
	public Ashe (GameObject g, AshType v, int n,String perso ){ 
		if(g!=null)possessor = g.id;
		type = v;
		nbr = n;
		title=perso;
	}
	public Ashe (int g, int v, int n ){ 
		possessor = g;
		type = AshType.from(v);
		nbr = n;
	}
	
	public static Ashe fromSqlValue(String s){
		String[] split = s.split(":");
		 
		return new Ashe( Integer.parseInt(split[0] ),
				AshType.from(Integer.parseInt(split[1])),
				Integer.parseInt(split[2]));
	}
	public String toSqlValue(){ 
		return possessor +":"+type.getValeur()+":"+nbr;
	}
	
	
	public String title(){
		if(possessor!=-1)
		  return possessor().title()+" "+type;
		 
			 return type+"";
		 
	}
	public static LinkedList <Ashe> getlist(String string) {
		LinkedList <Ashe> temp = new LinkedList<>(); 
		for(String a : string.split(";"))if(!a.equals(""))
			temp.add(fromSqlValue(a));
		return temp;
	}
	public AshType type() {  return type; }
	public GameObject possessor() {  return GameObject.getById(possessor); }
	public void setNbr(int n) { nbr=n; }
	public int getNbr() {  return nbr; }
	
	@Override public boolean equals(Object obj) {
	    if (!(obj instanceof Ashe)) return false;
	    Ashe other = (Ashe) obj;
	    return Objects.equals(possessor, other.possessor) &&
	           Objects.equals(title, other.title) &&
	           type == other.type;
	}
 

	@Override
	public String toString() {
		return title();
	}
	
	
	
	
	
	
	
	
	
}
