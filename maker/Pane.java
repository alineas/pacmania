package me.nina.maker;

import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;

import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Zone.ZoneType;

public class Pane extends JPanel { private static final long serialVersionUID = 1L;


	//public String title() {return "";}
	
	public Pane() {
		super();
	}
	
	
	
	
	public void log(String s) {
		System.out.println( s);
	}
	
	//##################################################################################################################################
	public JButton createBoutton(String s, JComponent c,int w, int h, EventListener ac) {
		JButton j = new JButton();
		j.setText(s);
		Utils.setSize(j, w, h); 
		if(ac!=null&&ac instanceof ActionListener)
			j.addActionListener((ActionListener) ac);
		if(c!=null)c.add(j);
		return j;
	}
	//##################################################################################################################################
	public JCheckBox createCheckBox(String s, JComponent c,int w, int h, EventListener ac) {
		JCheckBox j = new JCheckBox();
		j.setText(s);
		Utils.setSize(j, w, h); 
		if(ac!=null&&ac instanceof ActionListener)
			j.addActionListener((ActionListener) ac);
		c.add(j);
		return j;
	}
	//######################################################################### 
	public JLabel createLabel(String s, JComponent c,int w, int h) {
		JLabel j = new JLabel(s, JLabel.LEFT); 
		j.setText(s);
		Utils.setSize(j, w, h);  
		c.add(j);
		return j;
	}
	//######################################################################### 
	public JTextField createText(String s, JComponent c,int w, int h, EventListener ac) {
		JTextField j = new JTextField(s, JLabel.CENTER); 
		j.setText(s);
		Utils.setSize(j, w, h); 
		if(ac!=null&&ac instanceof ActionListener)
			j.addActionListener((ActionListener) ac); 
		c.add(j);
		return j;
	}
	//######################################################################### 
 
	public <T> Cbox<T>  createCbox(T[] list, JComponent c,int w, int h, EventListener ac) {
		return createCbox(Arrays.asList(list) ,c,w,h,ac);
	}
 
	public <T> Cbox<T> createCbox(Collection<T> list, JComponent c, int w, int h, EventListener ac) {
		Cbox<T> box = new Cbox<>(list);
		Utils.setSize(box, w, h);
		if(ac!=null&&ac instanceof ActionListener)
			box.addActionListener((ActionListener) ac); 
		if(c!=null)c.add(box);
		return box;
	}




	public JMenuItem createJMenu(JPopupMenu popupMenu, String s, ActionListener a) { 
		JMenuItem i = new JMenuItem(s); i.addActionListener(a); popupMenu.add(i); return i ;
	}
	public JMenuItem createJMenu(JMenuItem popupMenu, String s, ActionListener a) { 
		JMenuItem i = new JMenuItem(s); i.addActionListener(a); popupMenu.add(i); return i ;
	}
}
