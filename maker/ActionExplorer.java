package me.nina.maker;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;

import me.nina.editors.Editor;
import me.nina.gameobjects.Action;
import me.nina.gameobjects.Animation;
import me.nina.gameobjects.BasicObject;
import me.nina.gameobjects.Chapter;
import me.nina.gameobjects.CharacterState; 
import me.nina.gameobjects.GameObject;
import me.nina.gameobjects.InventoryObject;
import me.nina.gameobjects.MegaAction;
import me.nina.gameobjects.Message;
import me.nina.gameobjects.ObjectState;
import me.nina.gameobjects.Scene;
import me.nina.gameobjects.SpawnPoint;
import me.nina.gameobjects.SpecialZoneInv;
import me.nina.gameobjects.Stair;
import me.nina.gameobjects.TransitionPoint;
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Action.AddWat;
import me.nina.maker.Ashe.AshType;

public class ActionExplorer  extends JFrame  implements TreeExpansionListener{

	private JTree gameObjectTree;
	private DefaultMutableTreeNode root;
	private boolean processingEvent = false;
	private AddWat wat;
	private ArrayList<Message> ls;

	public ActionExplorer(Action a, AddWat wat) {
		super("explore and add with clic right");
		ls = new ArrayList<Message>();
		for(Action aa : Action.getAll())
			for(Ashe b : aa.ashToSet())
				if(b.possessor() instanceof Message)
					ls.add((Message) b.possessor());
		this.wat=wat;
		CheckBoxNodeListener.selecteds = new ArrayList<>();
		root = new DefaultMutableTreeNode("Root"); 
		// Cr�er l'arbre avec le n�ud racine
		gameObjectTree = new JTree(root);
		gameObjectTree.addTreeExpansionListener(new TreeExpansionListener() {
			@Override public void treeExpanded(TreeExpansionEvent event) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) event.getPath().getLastPathComponent();
				if (!processingEvent && node.getUserObject() instanceof GameObject||node.getUserObject()instanceof String && ((String)node.getUserObject()).contains("essage")) {
					node.removeAllChildren();
					processingEvent = true; 
					lazyLoadChildren(node);
					processingEvent = false;
				}
			}

			@Override public void treeCollapsed(TreeExpansionEvent event) { }
		});

		JScrollPane scrollPane = new JScrollPane(gameObjectTree);
		getContentPane().add(scrollPane, BorderLayout.CENTER);

		addCheckBoxes(gameObjectTree, a ,wat);

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(800, 600);
		setLocationRelativeTo(null);
		populate(); 
	}
	//###############################################
	public void populate() {
		root.removeAllChildren(); 

		if(wat!=AddWat.DETECTOR) {
			

			 
			
			DefaultMutableTreeNode elementNode = new DefaultMutableTreeNode( 
					new Ashe(null,AshType.OR,1,"OR" ));
			root.add(elementNode);
		}
		createNodesForType("megaAction", MegaAction.getAll());   
		createNodesForType("action", Action.getAll());   
		createNodesForType("animation", Animation.getAll());   
		createNodesForType("chapters", Chapter.getAll());  
		createNodesForType("Basic Objects", BasicObject.getAll());
		createNodesForType("Characters", me.nina.gameobjects.Character.getAll());
		createNodesForType("Character states", CharacterState.getAll());
		createNodesForType("Inventory objects", InventoryObject.getAll());
		createNodesForType("objectStates", ObjectState.getAll());  
		createNodesForType("Scenes", Scene.getScenes());
		createNodesForType("Zone", Zone.getAll());
		createNodesForType("tpoint", TransitionPoint.getAll());
		createNodesForType("specialzone", SpecialZoneInv.getAll());
		createNodesForType("stair", Stair.getAll()); 
		createNodesForType("spawnPoints", SpawnPoint.getAll()); 
		createNodesForType("message", Message.getAlls()); 
		DefaultTreeModel model = (DefaultTreeModel)gameObjectTree.getModel();
		DefaultMutableTreeNode root = (DefaultMutableTreeNode)model.getRoot(); 
		model.reload(root);
	}
	//###############################################
	private void createNodesForType(String typeName, Collection<? extends GameObject> elements) {
		DefaultMutableTreeNode typeNode = new DefaultMutableTreeNode(typeName);
		root.add(typeNode);
		if(typeName.equals("message")) { 
			typeNode.add(new DefaultMutableTreeNode());
		}
		else
			for (GameObject element : elements) {element.setJTreeSelected(false);
			DefaultMutableTreeNode elementNode = new DefaultMutableTreeNode(element);
			typeNode.add(elementNode);
			// Ajouter un n�ud factice pour le chargement paresseux
			elementNode.add(new DefaultMutableTreeNode());
			}
		// Mettre � jour le mod�le de l'arbre apr�s l'ajout des n�uds
		((DefaultTreeModel) gameObjectTree.getModel()).nodeStructureChanged(typeNode);
	}
	//###############################################
	private void addCheckBoxes(JTree tree, Action a, AddWat wat) {
		tree.setCellRenderer(new CheckBoxNodeRenderer(wat,ls));
		tree.addMouseListener(new CheckBoxNodeListener(tree, a, wat,this));
	}
	//###############################################
	public void setRootNode(DefaultMutableTreeNode rootNode) {
		this.root = rootNode;
		gameObjectTree.setModel(new DefaultTreeModel(rootNode));
	}
	//###############################################
	private void lazyLoadChildren(DefaultMutableTreeNode parentNode) {


		Object userObject = parentNode.getUserObject();

		if (userObject instanceof String && ((String) userObject).equals("message")) { Utils.log("string message");

		for (String a : Message.getSids()) {
			DefaultMutableTreeNode messageNode = new DefaultMutableTreeNode("Message " + a);
			parentNode.add(messageNode);
			// Ajoutez un n�ud factice pour le chargement paresseux
			messageNode.add(new DefaultMutableTreeNode());
		}


		} else if(userObject instanceof String && ((String) userObject).contains("Message")) {Utils.log("string MMMessage");
		// R�cup�rer l'identifiant "a" � partir du nom du n�ud
		String a = ((String) userObject).substring(8);

		// Charger l'ArrayList pour "Message + a"
		List<Message> temp = new ArrayList<>();
		for (Message msg : Message.getAlls()) {
			if (msg.sierraId() == Message.getSelected(a)) {
				temp.add(msg);
			}
		}

		// Remplacer le n�ud factice par les vrais enfants
		parentNode.removeAllChildren();
		for (Message message : temp) {
			DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(message);
			parentNode.add(childNode);
			// Ajoutez des n�uds enfants factices ici si n�cessaire
			childNode.add(new DefaultMutableTreeNode());
		}

		// Mettez � jour le mod�le de l'arbre apr�s l'ajout des n�uds
		((DefaultTreeModel) gameObjectTree.getModel()).nodeStructureChanged(parentNode);

		}else {

			List<GameObject> children = getChildrenForNode(parentNode);
			if (children != null && !children.isEmpty()) {
				for (GameObject child : children) {
					DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(child);
					parentNode.add(childNode);
					// Ajouter un n�ud factice pour le chargement paresseux
					childNode.add(new DefaultMutableTreeNode());
				}
			}

			Set<AshType> getters = getGettersForNode(parentNode);
			Set<AshType> setters = getSettersForNode(parentNode);
			Set<AshType> removers = getRemoversForNode(parentNode);

			if(wat == AddWat.ASHTOSET) {
				for (AshType a : setters) {
					DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(a);
					parentNode.add(childNode);
				}
				for (AshType a : removers) {
					DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(a);
					parentNode.add(childNode);
				}
			} else if (wat != AddWat.DETECTOR) {
				for (AshType a : getters) {
					DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(a);
					parentNode.add(childNode);
				}
			}
		}
		((DefaultTreeModel) gameObjectTree.getModel()).nodeStructureChanged(parentNode);
	}
	//###############################################
	private List<GameObject> getChildrenForNode(DefaultMutableTreeNode node) {
		// R�cup�rer l'objet associ� au n�ud
		Object userObject = node.getUserObject();

		// V�rifier si l'objet est une instance de GameObject
		if (userObject instanceof GameObject) {
			GameObject gameObject = (GameObject) userObject;
			gameObject.setJTreeSelected(false);
			// R�cup�rer les enfants du gameObject
			List<GameObject> children = gameObject.getChilds();

			// Retourner les enfants si la liste n'est pas vide
			if (!children.isEmpty()) {
				return children;
			}
		}

		return null; // Retourner null si le n�ud n'a pas d'enfants
	}
	//###############################################
	private Set<AshType> getGettersForNode(DefaultMutableTreeNode node) { 
		GameObject gameObject = (GameObject) node.getUserObject();
		return gameObject.possibleGetters();  
	}
	//###############################################
	private Set<AshType> getRemoversForNode(DefaultMutableTreeNode node) { 
		GameObject gameObject = (GameObject) node.getUserObject();
		return gameObject.possibleRemovers();  
	}
	//###############################################
	private Set<AshType> getSettersForNode(DefaultMutableTreeNode node) { 
		GameObject gameObject = (GameObject) node.getUserObject();
		return gameObject.possibleSetters();  
	}
	//###############################################
	@Override public void treeExpanded(TreeExpansionEvent event) {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) event.getPath().getLastPathComponent();
		lazyLoadChildren(node);
	}

	@Override public void treeCollapsed(TreeExpansionEvent event) { }
}

//###############################################
//###############################################
//###############################################
class CheckBoxNodeListener extends MouseAdapter {
	private JTree tree;
	private AddWat wat;
	private Action action;
	private ActionExplorer explorer;
	public static List<Ashe> selecteds = new ArrayList<>();

	//###############################################
	public CheckBoxNodeListener(JTree tree, Action action, AddWat wat, ActionExplorer explorer) {
		this.tree = tree;
		this.action = action;
		this.wat = wat;
		this.explorer = explorer;
	}
	//###############################################
	@Override public void mouseReleased(MouseEvent e) {
		int row = tree.getRowForLocation(e.getX(), e.getY());
		TreePath path = tree.getPathForRow(row);

		if (path != null) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();

			if (node != null) {
				Object userObject = node.getUserObject();

				if (userObject instanceof AshType) {
					handleAshTypeNode((AshType) userObject, node, e);
				} else if (userObject instanceof GameObject && (wat == AddWat.DETECTOR )) {
					handleDetectorNode((GameObject) userObject, node, e);
				} else if (userObject instanceof Ashe && wat != AddWat.DETECTOR) {
					handleAsheNode((Ashe) userObject, e);
				} 
			}
		}
	}
	//###############################################
	private void handleAshTypeNode(AshType ashType, DefaultMutableTreeNode node, MouseEvent e) {
		if (e.getButton() == 3 || e.getButton() == 2) {
			DefaultMutableTreeNode p = (DefaultMutableTreeNode) node.getParent();
			GameObject parent= (GameObject) p.getUserObject();  
			JPopupMenu popupMenu = new JPopupMenu();
if(wat != AddWat.TEST_IN_GAME) {
			action.createJMenu(popupMenu, "add and dont care about number " + parent.title() + " " + ashType, ee -> {
				Ashe ash = new Ashe(parent.id, ashType.getValeur(), 0);
				handleAshAction(ash);
				selecteds.add(ash);
			});
			
			action.createJMenu(popupMenu, "remove 1 " + parent.title() + " " + ashType, ee -> {
				Ashe ash = new Ashe(parent.id, ashType.getValeur(), -1);
				handleAshAction(ash);
				selecteds.add(ash);
			});

			action.createJMenu(popupMenu, "remove precise quantity " + parent.title() + " " + ashType, ee -> {
				int quantity = Integer.parseInt(Main.getUserInput("how many ?"));
				Ashe ash = new Ashe(parent.id, ashType.getValeur(), -quantity);
				handleAshAction(ash);
				selecteds.add(ash);
			});
			
			action.createJMenu(popupMenu, "add precise quantity " + parent.title() + " " + ashType, ee -> {
				int quantity = Integer.parseInt(Main.getUserInput("how many ?"));
				Ashe ash = new Ashe(parent.id, ashType.getValeur(), quantity);
				handleAshAction(ash);
				selecteds.add(ash);
			});

			 
}else {
	Main.leftController.createJMenu(popupMenu, "add achievment in this game", ee -> {
		
		Ashe ash = new Ashe(parent.id, ashType.getValeur(), 0);
		 Main.leftController.actualGame.actionMgr.doTheJobAshToSet(ash, Main.leftController.actualGame.player); 
	});
	Main.leftController.createJMenu(popupMenu, "rem achievment in this game", ee -> {
		Ashe ash = new Ashe(parent.id, ashType.getValeur(), -1);
		 Main.leftController.actualGame.actionMgr.doTheJobAshToSet(ash, Main.leftController.actualGame.player); 

	});
}
			popupMenu.show(tree, e.getX(), e.getY());
		}
	}
	//###############################################
	private void handleDetectorNode(GameObject gameObject, DefaultMutableTreeNode node, MouseEvent e) {
		if (e.getButton() == 3) {
			JPopupMenu popupMenu = new JPopupMenu(); 

			gameObject.createJMenu(popupMenu, "add detector " + gameObject.title(), ee -> {
				action.addDetector(gameObject.id);
				action.populate(false);
				gameObject.setJTreeSelected(true);
				tree.repaint();
			});
			if(gameObject instanceof Action)
				gameObject.createJMenu(popupMenu, "open action " + gameObject.title(), ee -> {
					((Action)gameObject).populateRight();
				});

			popupMenu.show(tree, e.getX(), e.getY());
		}
	}
	//###############################################
	private void handleAsheNode(Ashe ashe, MouseEvent e) {
		if (e.getButton() == 3 || e.getButton() == 2) {
			JPopupMenu popupMenu = new JPopupMenu();

			action.createJMenu(popupMenu, "add single " + ashe.title(), ee -> {
				ashe.setNbr(0);
				handleAshAction(ashe);
			});

			action.createJMenu(popupMenu, "add multi " + ashe.title(), ee -> {
				int quantity = Integer.parseInt(Main.getUserInput("how many ?"));
				ashe.setNbr(quantity);
				handleAshAction(ashe);
			});
			action.createJMenu(popupMenu, "remove 1 " + ashe.title(), ee -> {
				ashe.setNbr(-1);
				handleAshAction(ashe);
			});

			action.createJMenu(popupMenu, "remove multi " + ashe.title(), ee -> {
				int quantity = Integer.parseInt(Main.getUserInput("how many ?"));
				ashe.setNbr(-quantity);
				handleAshAction(ashe);
			});
			 

			popupMenu.show(tree, e.getX(), e.getY());
		}
	} 
	//###############################################
	private void handleAshAction(Ashe ash) {
		if (wat == AddWat.ASHNEEDED) action.addAshNeeded(ash);
		if (wat == AddWat.ASHREFUSED) action.addAshRefused(ash);
		if (wat == AddWat.ASHTOSET) action.addAshToSet(ash);
		tree.repaint();
		ash.setJTreeSelected(true);
		action.populate(false);
	}
}
//###############################################
//###############################################
class CheckBoxNodeRenderer implements TreeCellRenderer {
	private JCheckBox checkBox = new JCheckBox();
	private AddWat wat;
	private ArrayList<Message> ls;
	//###############################################
	public CheckBoxNodeRenderer(AddWat wat, ArrayList<Message> ls) {
		this.ls=ls;
		this.wat = wat;
		checkBox.setOpaque(false);
	}
	//###############################################
	@Override public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean selected, boolean expanded,
			boolean leaf, int row, boolean hasFocus) {
		if (value instanceof DefaultMutableTreeNode) {
			Object userObject = ((DefaultMutableTreeNode) value).getUserObject();
			 
			if (userObject instanceof GameObject) {
				GameObject gameObject = (GameObject) userObject;
				if(wat != AddWat.DETECTOR) {
					if(userObject  instanceof Message) { 
					if(ls.contains((Message)userObject))
						return new JLabel("_________used______________"+gameObject.title());//already used
					 
				} 
					return new JLabel(gameObject.title());
					
				}
					
				else {checkBox.setText(  gameObject.title());
				checkBox.setSelected(gameObject.isJTreeSelected());
				
				
				
				}
			} else if (userObject instanceof AshType && wat != AddWat.DETECTOR ) {
				AshType ashType = (AshType) userObject;
				checkBox.setText(ashType  +"");
				DefaultMutableTreeNode p = (DefaultMutableTreeNode) ((DefaultMutableTreeNode) value).getParent();
				GameObject parent= (GameObject) p.getUserObject();

				
				
				checkBox.setSelected(CheckBoxNodeListener.selecteds.contains(new Ashe(parent.id,ashType,1))); 
			} else if (userObject instanceof Ashe && wat != AddWat.DETECTOR ) {
				Ashe ashe = (Ashe) userObject; 
				checkBox.setText(ashe.title());
				checkBox.setSelected(ashe.isJTreeSelected()); // or set selection state based on your logic
				
			} 

			else {

				return new JLabel( value.toString());
			}

			checkBox.setEnabled(tree.isEnabled());

			if (selected) {
				checkBox.setForeground(UIManager.getColor("Tree.selectionForeground"));
				checkBox.setBackground(UIManager.getColor("Tree.selectionBackground"));
			} else {
				checkBox.setForeground(UIManager.getColor("Tree.textForeground"));
				checkBox.setBackground(UIManager.getColor("Tree.textBackground"));
			}

			return checkBox;
		}

		return new JLabel(value.toString());
	}
}
