package me.nina.maker;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.LongFunction;

import javax.swing.ComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import me.nina.gameobjects.GameObject;

public class JCbox extends JComboBox<GameObject>  { private static final long serialVersionUID = 1;
	private boolean empty=true,mustShow=false;  
	
    public JCbox(LongFunction<String> message, List<GameObject> checkBoxes) {
        super(checkBoxes.toArray(new GameObject[0])); 
        addActionListener(event -> {
	    try { 
	        GameObject checkBox = (GameObject) getSelectedItem();
	        checkBox.setSelected(!checkBox.isSelected());
	        empty = howManySelected() == 0;
	        fireCustomEvent();
	    } catch (Exception e) {
	        // G�rer l'exception si n�cessaire
	    }
	});

        JLabel countLabel = new JLabel();
        setRenderer((list, value, index, isSelected, cellHasFocus) -> {
            if (index != -1) {
                GameObject element = (GameObject) value;
                JCheckBox checkBox = new JCheckBox(element.title());
                checkBox.setSelected(element.isSelected());
                return checkBox;
            }
            if (mustShow) {
                showPopup();
                mustShow = false;
            }

            countLabel.setText(message.apply(howManySelected()));

            return countLabel;
        });
    } 
 
    
    @Override public void addItem(GameObject item) {
    	 
        super.addItem(item);  
        item.setSelected(false);
       this.setSelectedItem(item);
        
    }
    
    
   public  interface CustomComboBoxListener {
        void customEventOccurred();
    }
   private List<CustomComboBoxListener> listeners = new ArrayList<>();

   // �tape 2 : M�thode pour enregistrer des �couteurs
   public void addCustomComboBoxListener(CustomComboBoxListener listener) {
       listeners.add(listener);
   }
   private void fireCustomEvent() { 
       for (CustomComboBoxListener listener : listeners) {
           listener.customEventOccurred();
       }
   }
    
   
   public boolean isSelected(String s) {
       ComboBoxModel<GameObject> model = getModel();
       for (int i = 0, n = model.getSize(); i < n; i++) {
           if (model.getElementAt(i).title().equals(s)) {
               return model.getElementAt(i).isSelected();
           }
       }
       return false;
   }

   public boolean isEmpty() {
       return empty;
   }

   public int howManySelected() {
       int count = 0;
       ComboBoxModel<GameObject> model = getModel();
       for (int i = 0, n = model.getSize(); i < n; i++) {
           if (model.getElementAt(i).isSelected())
               count++;
       }
       return count;
   }

   public void reset() {
       ComboBoxModel<GameObject> model = getModel();
       for (int i = 0, n = model.getSize(); i < n; i++) {
           if (model.getElementAt(i).isSelected())
               model.getElementAt(i).setSelected(false);
           empty = true;
       }
   }
 
    
    
    
    
    
    
     
    	
}/*
public class JCbox<T extends GameObject> extends JComboBox<T> {
    private static final long serialVersionUID = 1;

    public JCbox(LongFunction<String> message, List<T> checkBoxes) {
        DefaultComboBoxModel<T> model = new DefaultComboBoxModel<>();
        for (T checkBox : checkBoxes) {
            model.addElement(checkBox);
            Utils.log(checkBox.getText()+"test");
        }
        setModel(model);
    }

    public boolean isSelected(String s) {
        for (int i = 0; i < getModel().getSize(); i++) {
            GameObject checkBox = getModel().getElementAt(i);
            if (checkBox.getText().equals(s)) {
                return checkBox.isSelected();
            }
        }
        return false;
    }

    public int howManySelected() {
        int count = 0;
        for (int i = 0; i < getModel().getSize(); i++) {
            GameObject checkBox = getModel().getElementAt(i);
            if (checkBox.isSelected()) {
                count++;
            }
        }
        return count;
    }
}*/
