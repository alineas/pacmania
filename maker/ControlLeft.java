package me.nina.maker;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import agame.MainGame; 
import me.nina.objects2d.Img;
import me.nina.editionnodes.CollisionsNode;
import me.nina.editionnodes.ObjectsNode;
import me.nina.editionnodes.ScaleFactor;
import me.nina.editionnodes.Spawns;
import me.nina.editionnodes.StairsNode;
import me.nina.editionnodes.Tpoints;
import me.nina.editionnodes.Zindexes;
import me.nina.editors.ActionEditor;
import me.nina.editors.AnimationEditor;
import me.nina.editors.CursorEditor;
import me.nina.editors.Editor.AddType;
import me.nina.editors.SceneEditor.EditType;
import me.nina.editors.VideoEditor;
import me.nina.editors.InventoryObjectsEditor;
import me.nina.gameobjects.Action;
import me.nina.gameobjects.Action.AddWat;
import me.nina.gameobjects.BasicObject.ObjectType;
import me.nina.gameobjects.Chapter;
import me.nina.gameobjects.Message;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Ashe.AshType;
import me.nina.sqlmanipulation.SQLLine;
import me.nina.sqlmanipulation.SQLTable;
import me.nina.sqlmanipulation.SQLUtils;

public class ControlLeft extends Pane{ private static final long serialVersionUID = 1L; 

Boolean isGameOpened = false;  
private Pane privateTopButtons = new Pane(),  privateTopLabels = new Pane();
public Pane panneauBoutons = new Pane(),  panneauLabels = new Pane();  

protected MainGame actualGame;

private EditType actualEdition;

public static Cbox<EditType> editList;

public static Cbox<ZoneType> pencilList; 



public ControlLeft() {
	super(); 
	setLayout(new GridLayout(2, 2));  
Utils.setSize(this, 300, 700);
    privateTopLabels = new Pane();
    privateTopButtons = new Pane();
    panneauLabels = new Pane();
    panneauBoutons = new Pane();

    add(privateTopLabels);
    add(privateTopButtons);
    add(panneauLabels);
    add(panneauBoutons); 
    Main.openSql("games/"+Main.gameName+"/game.npcma");
	isGameOpened = true ; 
	instanciate();
		/*JTextField gn = createText("game name",privateTopLabels,100,20,null); 
		createBoutton("new game",privateTopButtons,100,20,(ActionListener) e->  {
			new File("games/").mkdirs();
			Main.gameName = gn.getText().replaceAll(" ", "-");
			if(!new File("games/"+Main.gameName+"/game.npcma").exists()) {
				Main.openSql("games/"+Main.gameName+"/game.npcma");
				isGameOpened = true ;
				instanciate();
				 
			}
			

		} );

		JTextField og = createText("game name",privateTopLabels,100,20,null); 
		createBoutton("open game",privateTopButtons,100,20,(ActionListener) e-> {
			Main.gameName = og.getText().replaceAll(" ", "-");
			if(new File("games/"+Main.gameName+"/game.npcma").exists()) {
				Main.openSql("games/"+Main.gameName+"/game.npcma");
				isGameOpened = true ; 
				instanciate();
				 
			}
			
		 }); */
     
}



public void instanciate() {
	privateTopButtons.removeAll();
	privateTopLabels.removeAll(); 
	Main.fileDisplayer.updateRight(); //we have now a path, browse the folders 
	
	createLabel("add",privateTopLabels,150,20); 
	Main.elementChooser = createCbox(AddType.values(),privateTopButtons,150,20,( ActionListener)e-> {  
		if(Main.elementChooser.getSelectedItem() == AddType.CHARACTER) { Main.addCharacter( ); } 
		else if(Main.elementChooser.getSelectedItem() == AddType.SCENE) { Main.addScene( ); } 
		else if(Main.elementChooser.getSelectedItem() == AddType.CHAPTER) { Main.addChapter( );  }
		else if(Main.elementChooser.getSelectedItem() == AddType.INVENTORY_OBJECT) { Main.addInvObject(  );  }
	});
	  
	createLabel("edit character",privateTopLabels,150,20);  
	Main.characterChooser = createCbox(SQLTable.getTable("character").getLines().values(),privateTopButtons,150,20,(ActionListener)e-> { 
		Main.editCharacter( Main.characterChooser.getSelectedItem().id);  } );

	createLabel("edit chapter",privateTopLabels,150,20); 
	Main.chapterChooser = createCbox(SQLTable.getTable("chapters").getLines().values(),privateTopButtons,150,20,(ActionListener)e-> { 
		Main.editChapter( Main.chapterChooser.getSelectedItem().id); } );
	createLabel("edit scene",privateTopLabels,150,20); 
	Main.sceneListChooser = createCbox(SQLTable.getTable("scene").getLines().values(),privateTopButtons, 150, 20,(ActionListener)e-> {
		Main.editScene( Main.sceneListChooser.getSelectedItem().id );  reinstanciate(); } ); 
	createLabel("pencil",privateTopLabels,150,20); pencilList = createCbox(ZoneType.values(),privateTopButtons,150,20,null );
	createLabel("edition mode",privateTopLabels,100,20); editList = createCbox(EditType.values(),privateTopButtons,150,20,( ActionListener)e-> {  
		actualEdition = editList.getSelectedItem(); reinstanciate(); } );
	createLabel("",privateTopButtons,150,20); createLabel("",privateTopLabels,150,20);   //separator
 
	createBoutton("inventory",privateTopLabels, 150, 20,new ActionListener() {
		public void actionPerformed(ActionEvent e) {  
			Main.setActualEditor(new InventoryObjectsEditor(-1));
			//Main.revalidate();
	} });   
	createBoutton("link scenes",privateTopButtons,150,20,( ActionListener)e-> {  Main.linkScenes(); }); 
		createBoutton("messages editor",privateTopLabels,150,20,( ActionListener)e-> {  Main.editMessages();  }); 
	createBoutton("menubar",privateTopButtons,150,20,( ActionListener)e-> {  Main.editMenuBar();  }); 
	createBoutton("actions editor",privateTopLabels,150,20,( ActionListener)e-> {  new ActionEditor(); }); 
	createBoutton("test game",privateTopButtons,150,20,( ActionListener)e-> { 
 

		if(aexp==null)aexp = new ActionExplorer(null,AddWat.TEST_IN_GAME) ;
		if(!aexp.isShowing())aexp.setVisible(true); 
		actualGame = new MainGame(Chapter.get(Main.chapterChooser.getSelectedItem().id));
		 
	 }); 
	createBoutton("show SQL",privateTopLabels,150,20,( ActionListener)e-> {  
		SQLPanel.isShown=true;
		SQLUtils.updateSqlViewer(); 
		Main.sqlFrame.setVisible(true);
	 });
	createBoutton("cursors",privateTopButtons,150,20,( ActionListener)e-> {  
		 
		Main.setActualEditor(new CursorEditor());
	 });
	createBoutton("video editor",privateTopButtons,150,20,( ActionListener)e-> { 
		Main.setActualEditor(new VideoEditor());
	 });
	createBoutton("back",privateTopLabels,150,20,( ActionListener)e-> {  
		 
		Main.goBack();
	 });
	
	createBoutton("export game",privateTopLabels,150,20,( ActionListener)e-> {  
		 
		Main.export();
	 });
	super.revalidate();
}

ActionExplorer aexp = null;

public void reinstanciate() {
	Main.actualEditor.rosella = new RosellaZone(Main.actualEditor.scene ); 
	Main.actualEditor.add(Main.actualEditor.rosella);
	if(actualEdition ==EditType.COLLISIONS)  Main.actualNode = new CollisionsNode(Main.actualEditor ); 
	if(actualEdition ==EditType.TRANSITIONPOINTS) Main.actualNode = new Tpoints(Main.actualEditor ); 
	if(actualEdition ==EditType.SCALEFACTOR) Main.actualNode = new ScaleFactor(Main.actualEditor );
	if(actualEdition ==EditType.ZINDEXES) Main.actualNode = new Zindexes(Main.actualEditor );
	if(actualEdition ==EditType.STAIRS) Main.actualNode = new StairsNode(Main.actualEditor );
	if(actualEdition ==EditType.OBJECTS) Main.actualNode = new ObjectsNode(Main.actualEditor );  
	if(actualEdition ==EditType.SPAWNS) Main.actualNode = new Spawns(Main.actualEditor ); 
	if(actualEdition ==EditType.NOTHING) Main.actualNode = null; 
	
	if(Main.actualNode!=null) {
		Main.actualNode.repopulate();
		Main.actualEditor.repaint();
	}
}

@Override public void removeAll() {  //dont touch the main buttons
	panneauLabels.removeAll(); panneauBoutons.removeAll(); 

	revalidate();
}
@Override public void revalidate() { if(panneauLabels != null) {
	panneauLabels.revalidate();  panneauLabels.repaint();
	panneauBoutons.revalidate(); panneauBoutons.repaint(); } 
	super.revalidate();
}
 
} 
