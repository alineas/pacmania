package me.nina.editors;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import me.nina.gameobjects.Animation;
import me.nina.gameobjects.Scene;
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Cbox; 
import me.nina.maker.Main;
import me.nina.maker.Pane;
import me.nina.maker.RosellaZone;
import me.nina.maker.Utils;
import me.nina.maker.Utils;
import me.nina.agenerator.Generator; 
import me.nina.editionnodes.EditNode; 
import me.nina.editors.Editor.AddType;
import me.nina.objects2d.Img;
import me.nina.objects2d.Pixel;
import me.nina.objects2d.Point;
import me.nina.objects2d.Position;
import me.nina.sqlmanipulation.SQLLine;
import me.nina.sqlmanipulation.SQLTable;
 

public class VideoPanel extends JLayeredPane implements MouseListener, MouseMotionListener,MouseWheelListener,KeyListener { private static final long serialVersionUID = 1L;
	
	public Zone lastZone;
	public BufferedImage image,imagePri;
	private int clickedX, clickedY, actualX, actualY=0; 
	private boolean isClicking ;
	private Cbox<AddType> contextual;  
	private Point cornerClicked; 	public Zone modifiedZone;
	private JButton button; 	
	public boolean isChoosingAfile;
	public Zone offsetZone;
	public Zone corner1;
	public Zone corner2; 
	//####################################################################################
	public VideoPanel( ) {
		super(); 
		setLayout(null); 
		addMouseListener(this);
		addMouseMotionListener(this);
		try { image = ImageIO.read(new File("sierra.jpg")); } catch (IOException e) { e.printStackTrace(); }  
		
	}
	
	//####################################################################################
	@Override protected void paintComponent(Graphics g) { 
		 super.paintComponent(g); 
		  
		if(image==null)return;  
		
		g.drawImage(image, 0, 0, null);  
		
		 if(Main.actualEditor==null)return;
		 if (Main.actualNode!=null)
		 Main.actualNode.paint(g );
	 
		if(modifiedZone != null)
			draw(modifiedZone,g); //draw a zone to be modified/moved if needed
		
		if(lastZone != null && modifiedZone == null)//continue to show it after clic
			draw(lastZone,g); 
		
		
    }
	
	//#################################################################################### 
	private void draw(Zone z, Graphics g) { 
	    int XOFF = -(clickedX - actualX); 
	    int YOFF = -(clickedY - actualY);
	    int offSetX = 0;
	    int offSetY = 0;
	     
	    boolean isP1 = false, isP2 = false, isP3 = false;
	    if (cornerClicked == null) {
	        offSetX = XOFF;
	        offSetY = YOFF;
	    }else if (cornerClicked.equals(z.getCorner1())) isP1 = true;
	    else if (cornerClicked.equals(z.getCorner2())) isP2 = true;
	    else if (cornerClicked.equals(z.getP3()))  isP3 = true; 
 
	    if (modifiedZone == null) {
	        offSetX = 0;
	        offSetY = 0;
	    }
	    offsetZone = z.xp(offSetX).yp(offSetY);
	    if (isP1) offsetZone.setP1(new Point(offsetZone.getP1().x+XOFF,offsetZone.getP1().y+YOFF));
	    if (isP2) offsetZone.setP2(new Point(offsetZone.getP2().x+XOFF,offsetZone.getP2().y+YOFF));
	    if ((modifiedZone != null && isP3) || (modifiedZone != null && cornerClicked == null))
	    	offsetZone.setP3(new Point(offsetZone.getP3().x+XOFF,offsetZone.getP3().y+YOFF));
	    
	    offsetZone.draw(true, g,Color.GREEN);
 
	    corner1 = new Zone(offsetZone.getCorner1().x-5 , offsetZone.getCorner1().y-5,offsetZone.getCorner1().x+5 , offsetZone.getCorner1().y+5, ZoneType.RECTANGLE);
	    corner2 = new Zone(offsetZone.getCorner2().x-5 , offsetZone.getCorner2().y-5,offsetZone.getCorner2().x+5 , offsetZone.getCorner2().y+5, ZoneType.RECTANGLE);
	    Zone corner3 = new Zone(offsetZone.getP3().x-5 , offsetZone.getP3().y-5 ,offsetZone.getP3().x+5 , offsetZone.getP3().y+5 , ZoneType.RECTANGLE);

	    corner1.draw(false, g,Color.GRAY);
	    corner2.draw(false, g,Color.GRAY);
	    if (offsetZone.getType() == ZoneType.ARC) corner3.draw(false, g,Color.GRAY);
	}

	
	//#################################################################################### 
	@Override public void mousePressed(MouseEvent e) {
		requestFocus();requestFocusInWindow();
		clickedX = e.getX(); 	clickedY = e.getY(); 
		actualX = e.getX(); 	actualY = e.getY();
		
		if(e.getButton()==3) {   if (Main.actualNode!=null)
				Main.actualNode.clickR(e.getX(),e.getY());
			return;
		}
		if(e.getButton()==2) { if (Main.actualNode!=null)
				Main.actualNode.clickC(e.getX(),e.getY());
			return;
		} 
		isClicking=true; 
		Point p = new Point(e.getX(),e.getY());
		
		if(Main.actualNode!=null) {
			Zone temp = Main.actualNode.clicked(e.getX(),e.getY());
			if(temp!=null){modifiedZone = temp.copie();
			if(!temp.isModifiableX())modifiedZone.setUnmodifiableX();
			if(!temp.isModifiable())modifiedZone.setUnmodifiable();}
			}
					
		if(modifiedZone != null && modifiedZone.isModifiable()) { //someone use this clic ? tcheck for corner clic to know if its a zone to modify 

			if(Utils.getDistanceBetweenPoints(p, modifiedZone.getCorner1())<10 ) {
				cornerClicked = modifiedZone.getCorner1();  
				clickedX = modifiedZone.getP1().x; clickedY = modifiedZone.getP1().y;  
			}	
			if(Utils.getDistanceBetweenPoints(p, modifiedZone.getCorner2())<10 ) {
				cornerClicked = modifiedZone.getCorner2(); 
				clickedX = modifiedZone.getP2().x; clickedY = modifiedZone.getP2().y;
			}
			  if (modifiedZone.getType() == ZoneType.ARC) 
			if(Utils.getDistanceBetweenPoints(modifiedZone.getP3(), p )<10) {
				cornerClicked = modifiedZone.getP3(); 
				clickedX = modifiedZone.getP3().x; clickedY = modifiedZone.getP3().y;
				 
			}
		}
		
		if(modifiedZone == null && Main.actualEditor!=null) {
			modifiedZone = new Zone(e.getX(),e.getY(),actualX,actualY,Main.leftController.pencilList.getSelectedItem());
			cornerClicked = modifiedZone.getCorner2();  
		}
		repaint();
	}
	//#################################################################################### 
	@Override public void mouseReleased(MouseEvent e) {
		 //if(e.getButton()==3)return;
			isClicking=false; 
		if(Main.actualEditor==null)return;
		if(e.getButton()==1&&modifiedZone != null)   {
			if(cornerClicked != null) { //moving a point
				if(cornerClicked.equals(modifiedZone.getCorner1())) { 
					int diffx = modifiedZone.getCorner1().x - modifiedZone.getP1().x; 
					int diffy = modifiedZone.getCorner1().y - modifiedZone.getP1().y;//ovals corners are differents
					modifiedZone.setP1(actualX-diffx,actualY-diffy ); 
				}
				else if(cornerClicked.equals(modifiedZone.getCorner2())) { 
					int diffx = modifiedZone.getCorner2().x - modifiedZone.getP2().x; 
					int diffy = modifiedZone.getCorner2().y - modifiedZone.getP2().y;//ovals corners are differents
					modifiedZone.setP2(actualX-diffx,actualY-diffy );
				}else { //arc p3
					modifiedZone.setP3(actualX,actualY );  
				}
				
			}else { //moving the zone 
				if(!modifiedZone.isModifiableX()) {
					clickedX=actualX;
				}
				modifiedZone.setP1(new Point(modifiedZone.getP1().x -(clickedX-actualX),modifiedZone.getP1().y -(clickedY-actualY))) ;
				modifiedZone.setP2(new Point(modifiedZone.getP2().x -(clickedX-actualX),modifiedZone.getP2().y -(clickedY-actualY))) ;
				modifiedZone.setP3(new Point(modifiedZone.getP3().x -(clickedX-actualX),modifiedZone.getP3().y -(clickedY-actualY))) ;
				 
			}
 
			if (Main.actualNode!=null)
				Main.actualNode.release(modifiedZone,actualX,actualY);
			  
			 
			lastZone = modifiedZone;
			lastZone.setP3(modifiedZone.getP3() );
			modifiedZone = null;
			cornerClicked = null ;
		}
		 
		repaint();
	}
	//#################################################################################### 
	@Override public void mouseMoved(MouseEvent e) {   
		if(isClicking) {  
			actualX = e.getX();
			actualY = e.getY();
			repaint();
		}
		if(Main.actualEditor==null)return;
			
			if (Main.actualNode!=null)
				Main.actualNode.moved(clickedX,clickedY,actualX,actualY);
			 
	}
	//####################################################################################
	@Override public void mouseEntered(MouseEvent e) {
 
		if(Main.fileDisplayer.isClicking) {
			Main.fileDisplayer.isClicking = false;
			
			if(Main.actualEditor!=null&&!Main.actualEditor.handleImgDragged()) { // if actual editor not select the file, maybe add a new object?
			JTextField txt = Main.leftController.createText("name/title for add object", this, e.getX(), e.getY(), null);
			
			txt.setBounds(e.getX(), e.getY()-120, 150, 20);
			contextual = Main.leftController.createCbox(AddType.values(), this, 150, 20, new ActionListener() { 
				public void actionPerformed(ActionEvent e) {   
						if(contextual.getSelectedItem() == AddType.CHARACTER)  
							Main.addCharacter( ); 
						  
						else if(contextual.getSelectedItem() == AddType.SCENE)  
							Main.addScene( ); 
						else if(contextual.getSelectedItem() == AddType.CHAPTER) 
							Main.addChapter( );  
						 
				}  });
			contextual.setBounds(e.getX(), e.getY()-100, 150, 20);
			button = Main.leftController.createBoutton("cancel", this, e.getX(), e.getY(), new ActionListener() { 
				public void actionPerformed(ActionEvent e) {  contextual.setVisible(false);txt.setVisible(false); button.setVisible(false);}});
			button.setBounds(e.getX(), e.getY()-80, 150, 20);
			 
			}
	
		}
	}
	 
	@Override public void mouseClicked(MouseEvent e) { } 
	@Override public void mouseDragged(MouseEvent e) {mouseMoved(e); }
	@Override public void mouseExited(MouseEvent e) { }
	@Override public void mouseWheelMoved(MouseWheelEvent e) { 
		if (Main.actualNode!=null) {
			Main.actualNode.wheel(e.getWheelRotation(),e.getX(),e.getY());
			 
		}
		
	}
	//################################################################
		public void setPriorityImage(String img2,Scene scene ) {
			try { imagePri = ImageIO.read(new File(img2));  
		} catch (IOException e) {Utils.log("cant open videopanel img2 "+img2);} }
		
		//################################################################
		public void setImage(String img ) {if(img==null) {
			image=null;return;
		}
			try { image = ImageIO.read(new File(img));  
			Utils.setSize(this, image.getWidth(), image.getHeight()*2); 
			} catch (IOException e) {Utils.log("cant open videopanel img "+img);}
		
		}  
		protected void setImage(BufferedImage bgImage) {
			try { image = bgImage;  
			Utils.setSize(this, image.getWidth(), image.getHeight()*2); 
			} catch ( Exception e) {Utils.log("cant open videopanel img " );}
		}
		public void populate() { if (Main.actualNode!=null)
			Main.actualNode.repopulate(); 
		 };
		@Override
	    public void keyPressed(KeyEvent e) {
			if(Main.actualNode!=null)
			 Main.actualNode.keyPressed(e.getExtendedKeyCode());
			
	       
	    }

	    @Override
	    public void keyReleased(KeyEvent e) { 
		
	    	    }

	    @Override
	    public void keyTyped(KeyEvent e) {
	    	 
			    }
		public int getClickedX() { return clickedX; }
		public int getClickedY() { return clickedY; }
		public int getActualX() { return actualX; }
	public int getActualY() { return actualY; }
	public boolean isClicking() { return isClicking; }
}
