package me.nina.editors;

import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
 
import me.nina.gameobjects.Scene;
import me.nina.gameobjects.Stair;
import me.nina.gameobjects.Zone;
import me.nina.maker.Main;
import me.nina.maker.RosellaZone;
import me.nina.maker.Utils;
import me.nina.objects2d.Img;
import me.nina.objects2d.ImgUtils;

public class SceneEditor extends Editor{ private static final long serialVersionUID = 1L;
private JTextField titleOfScene;
private JTextField loopTime;
	 
	
	
	public SceneEditor(int id) {
		super();  
		scene = Scene.get(id); 
		rosella = new RosellaZone(scene ); super.add(rosella); 
		
		try {
			if(scene.bgName().contains("/")){
				image = ImgUtils.scal(ImgUtils.getPicture( scene.bgName()),zoom); 
				setPriorityImage( scene.priorityBg(),scene);
			}else{
			image = ImgUtils.scal(ImgUtils.getPicture(scene.getFolder()+scene.bgName()),zoom); 
			setPriorityImage(scene.getFolder()+scene.priorityBg(),scene);}
		} catch (Exception e1) {
			Utils.log("scene images not set or error !");
		}
		Utils.setSize(this, image.getWidth(), image.getHeight()*2);
		
		
		updateLeftEditor();
		
		
	}
	//################################################################
	//################################################################
	//################################################################
	public void updateLeftEditor() {  
  
		lab.createLabel("rename scene",lab,150,20);  
		  titleOfScene = but.createText("new name",but,150,20,(ActionListener)e->{  
				scene.updateTitle(titleOfScene.getText(),true);  } ); 
		  
		  lab.createLabel("the music will ",lab,150,20);  
		   but.createBoutton("replace the other",but,150,20,(ActionListener)e->{  
				scene.updateReplaceSong(true);  } ); 
		  lab.createLabel(" ",lab,150,20);  
		  but.createBoutton("not replace the other",but,150,20,(ActionListener)e->{  
				scene.updateReplaceSong(false);  } );
		  
		  lab.createLabel("loop="+scene.loopMusic()+" -998=noloop",lab,150,20);  
		  loopTime = but.createText("value",but,150,20,(ActionListener)e->{  
			  if(scene.music().contains(".mid"))
				  Main.getUserConfirmation("midi pre-looping not available, but we will replay it");
				scene.updateLoopMusic(Integer.parseInt(loopTime.getText()));   } );
		  
		  
		lab.createLabel("copy scene",lab,150,20);  
		  but.createBoutton("clic",but,150,20,(ActionListener)e->{  
				copyScene();  } ); 
		  lab.createLabel("delete scene",lab,150,20);  
		  but.createBoutton("clic",but,150,20,(ActionListener)e->{  
				scene.delete();  } ); 
	}
	
 
	
	
	private void copyScene() {
		String title = Main.getUserInput();
		Scene newScene = new Scene(title);
		Scene old = this.scene;
		int id = newScene.save();
		newScene=Scene.get(id);
		newScene.updateBackground(old.getFolder()+old.bgName()); 
		newScene.updatePriorityBg(old.getFolder()+old.priorityBg()); 
		newScene.line.update("zindexes", old.line.getString("zindexes"));
		newScene.updateScaleFactors(old.scalefactorTopLimit(), old.scalefactorBotLimit(), old.scalePercents()); 
		for (Zone z:Stair.getByOwner(old.id)){
			Stair stair = (Stair)z;
			Stair copie = (Stair) stair.copy();
			copie.save(newScene.id,stair.title());
		}
		
		for (Zone z:Zone.getByOwner(scene.id)){
			Zone collision = z.copie();
			collision.save(newScene.id, collision.title());
		}
	}
	//################################################################
	@Override protected boolean handleImgDragged() {
		boolean returned = false;
				if(Main.actualNode!=null&& Main.actualNode.handleImgDragged()) returned = true;
			
		 if(returned) return true;
		if(Main.leftController.editList.getSelectedItem() != EditType.NOTHING)return true;
		 
		Object[] options = { "background", "priority background","sound" };
 
        ImageIcon icon = null;
        if(Main.fileDisplayer.isImageFile(Main.fileDisplayer.getSelected()))icon=new ImageIcon(ImgUtils.resiz(ImgUtils.getPicture(Main.fileDisplayer.getSelected().getAbsolutePath()),300, 200));

        int result = JOptionPane.showOptionDialog(
                null,
                "use it as : ",
                "image received",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                icon,
                options,
                options[2]);  

        
        switch (result) {
            case 0: 
            	scene.updateBackground(Main.fileDisplayer.getUpdatedImage(scene.getFolder(),scene.bgName()));  
				setImage(scene.getFolder()+scene.bgName());
				return true;
                
            case 1:  
				scene.updatePriorityBg(Main.fileDisplayer.getUpdatedImage(scene.getFolder(),scene.priorityBg()));    
				setPriorityImage(scene.getFolder()+scene.priorityBg(),scene);
				return true;
                
            case 2:  
            	File file = Main.fileDisplayer.getSelected();
            	String fileName = file.getName();
            	Utils.copy(file, new File(scene.getFolder() + fileName)); 
            	scene.updateMusic(fileName);
                return true;
        } return true;
	}  

//################################################################
	public enum EditType {
	    NOTHING(-1),COLLISIONS(0), ZINDEXES(1),SCALEFACTOR(2),TRANSITIONPOINTS(3), STAIRS(4),OBJECTS(5),SPAWNS(7);
	      
	    private final int valeur; 
	    EditType(int valeur) { this.valeur = valeur;  } 
	    public int getValeur() {  return valeur; } 
	    //@Override public String toString() { return String.valueOf(getValeur()); }
	    public static EditType from(String s) { 
	    	return from(Integer.parseInt(s));
	    }
	    public static EditType from(int valeur) {
	        for (EditType type : values()) {
	            if (type.getValeur() == valeur) {
	                return type;
	            }
	        }
	        throw new IllegalArgumentException("Aucun Edit correspondant � la valeur : " + valeur);
	    }
	}
	public void setZoom(double zoom) {
		this.zoom=zoom;
		image = ImgUtils.scal(ImgUtils.getPicture(scene.getFolder()+scene.bgName()),zoom); 
	}
	
}