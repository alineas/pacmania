package me.nina.editors;
 
import javax.swing.*;
import java.awt.*;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import me.nina.gameobjects.Action;
import me.nina.gameobjects.MegaAction;
import me.nina.maker.Cbox;
import me.nina.maker.Main;
import me.nina.maker.Utils;

public class ActionEditor extends JFrame {

    private JPanel actionsPanel;
    private JScrollPane scrollPane;
    private JComboBox<MegaAction> megaComboBox;
    private JTextField megaActionTextField;
	private boolean condensed = true;

    public ActionEditor() {
        setTitle("Action Panel");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLayout(new BorderLayout());

        // Character Combo Box
        megaComboBox = new Cbox(MegaAction.getAll());
        megaComboBox.addActionListener(e -> populateActionsGrid((MegaAction) megaComboBox.getSelectedItem()));
        add(megaComboBox, BorderLayout.NORTH);

        // MegaAction Creation Panel
        JPanel creationPanel = new JPanel();
        JLabel megaActionLabel = new JLabel("MegaAction Title:");
        megaActionTextField = new JTextField(20);
        JButton createButton = new JButton("Create MegaAction");
        createButton.addActionListener(e -> createMegaAction());
        JButton deleteButton = new JButton("Delete MegaAction");
        deleteButton.addActionListener(e -> deleteMegaAction());
        JButton renameButton = new JButton("Rename MegaAction");
        renameButton.addActionListener(e -> renameMegaAction());
        
        JButton edit = new JButton("edition mode");
        edit.addActionListener(e -> {condensed=!condensed;populateActionsGrid((MegaAction) megaComboBox.getSelectedItem()); });
        
        creationPanel.add(megaActionLabel);
        creationPanel.add(megaActionTextField);
        creationPanel.add(createButton);
        creationPanel.add(renameButton);
        creationPanel.add(deleteButton);
        creationPanel.add(new JLabel("         "));
        creationPanel.add(edit);
        add(creationPanel, BorderLayout.SOUTH);

        // Actions Panel
        actionsPanel = new JPanel();
        actionsPanel.setOpaque(false);
        actionsPanel.setLayout(new GridLayout(0, 4)); // Set grid size

        // Add actionsPanel to a JScrollPane
        scrollPane = new JScrollPane(actionsPanel);
        scrollPane.setOpaque(false);
        add(scrollPane, BorderLayout.CENTER);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        
        populateActionsGrid(null);
    }

 
    private void populateActionsGrid(MegaAction megaAction) {
        actionsPanel.removeAll();
        actionsPanel.revalidate();
        actionsPanel.repaint();

        // Check if no megaAction is selected
        if (megaAction == null) {
        	
            for (Action action : Action.getAll()) {
                if (action.megaOwner() == -998) {
                    // Create a JPanel to hold the action and delete button
                    JPanel actionPanel = new JPanel(new BorderLayout()); 
                    // Add the action to a JScrollPane
                    Action panelAction = new Action(action,condensed);
                    JScrollPane actionScrollPane = new JScrollPane(panelAction);
                    Utils.setSize(actionScrollPane, 300, 500);
                    actionPanel.add(actionScrollPane );

                    JPanel grid = new JPanel();
                    Utils.setSize(grid, 300, 40);
                    grid.setLayout(new GridLayout(1, 4));

                    // Add the delete button
                    JButton deleteButton = new JButton("Delete this action");
                    deleteButton.addActionListener(e -> {
                        action.delete(); // Supprimer l'action
                        populateActionsGrid(null); // Repopuler le grid
                    });
                    grid.add(deleteButton);

                    JButton copyButton = new JButton("Copy this action");
                    copyButton.addActionListener(e -> {
                        Action newe = Action.copy(action);
                      if(megaAction!=null)  newe.updateMegaOwner(megaAction.id);
                        populateActionsGrid(null); // Repopuler le grid
                    });
                    grid.add(copyButton);

                    grid.add(new JLabel("move to:"));
                    Cbox cbox = new Cbox(MegaAction.getAll());
                    cbox.addActionListener(e -> {
                        action.updateMegaOwner(((MegaAction) cbox.getSelectedItem()).id);
                        populateActionsGrid(null);
                    });
                    grid.add(cbox, BorderLayout.NORTH);

                    actionPanel.add(grid, BorderLayout.SOUTH); 
                    actionsPanel.add(actionPanel);
                }
            }
        } else { 
            List<Action> liste = megaAction.getActions();
        	Collections.sort(liste, Comparator.comparing(Action::title));
            for (Action action : liste) {
                // Create a JPanel to hold the action and delete button
                JPanel actionPanel = new JPanel(new BorderLayout());

                Action panelAction = new Action(action,condensed);
                
                JScrollPane actionScrollPane = new JScrollPane(panelAction);
                Utils.setSize(actionScrollPane, 300, 500);
                actionPanel.add(actionScrollPane );
                JPanel grid = new JPanel();
                Utils.setSize(grid, 300, 40);
                grid.setLayout(new GridLayout(1, 4));
                // Add the delete button
                JButton deleteButton = new JButton("Delete this action");
                deleteButton.addActionListener(e -> {
                    action.delete(); // Supprimer l'action
                    populateActionsGrid(megaAction); // Repopuler le grid
                });
                grid.add(deleteButton);

                JButton copyButton = new JButton("Copy this action");
                copyButton.addActionListener(e -> {
                    Action newe = Action.copy(action);
                    newe.updateMegaOwner(megaAction.id);
                    populateActionsGrid(megaAction); // Repopuler le grid
                });
                grid.add(copyButton);

                grid.add(new JLabel("move to:"));
                Cbox cbox = new Cbox(MegaAction.getAll());
                cbox.addActionListener(e -> {
                    action.updateMegaOwner(((MegaAction) cbox.getSelectedItem()).id);
                    populateActionsGrid((MegaAction) megaComboBox.getSelectedItem());
                });
                grid.add(cbox, BorderLayout.NORTH);

                actionPanel.add(grid, BorderLayout.SOUTH);
                 
                actionsPanel.add(actionPanel);
            }
        }

        // Add button to create child actions
        JButton createActionButton = new JButton("Create Child Action");
        createActionButton.addActionListener(e -> createChildAction(megaAction));
        actionsPanel.add(createActionButton);

        // Revalidate the scroll pane to update the scroll bars
        scrollPane.revalidate();
        scrollPane.repaint();
    }
    private void createMegaAction() {
        String title = megaActionTextField.getText();
        if (!title.isEmpty()) {
            MegaAction megaAction = new MegaAction(title);
            megaAction = megaAction.save();
            megaComboBox.addItem(megaAction);
            megaActionTextField.setText("");
            populateActionsGrid((MegaAction) megaComboBox.getSelectedItem());
        } else {
            JOptionPane.showMessageDialog(this, "Please enter a title for the MegaAction.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    private void renameMegaAction() {
        String title = megaActionTextField.getText();
        if (!title.isEmpty()) {
        	MegaAction actual = (MegaAction) megaComboBox.getSelectedItem();
        	actual.updateTitle(title);
        	 megaComboBox = new Cbox(MegaAction.getAll());
             megaComboBox.addActionListener(e -> populateActionsGrid((MegaAction) megaComboBox.getSelectedItem()));
            populateActionsGrid((MegaAction) megaComboBox.getSelectedItem());
        } else {
            JOptionPane.showMessageDialog(this, "Please enter a title for the MegaAction.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    private void deleteMegaAction() { 
        MegaAction actual = (MegaAction) megaComboBox.getSelectedItem();
        actual.line.delete();
        megaComboBox.removeItem(actual);
        populateActionsGrid((MegaAction) megaComboBox.getSelectedItem());
    }
    private void createChildAction(MegaAction megaAction) {
        String title = JOptionPane.showInputDialog(this, "Enter title for child action:");
        if (title != null && !title.isEmpty()) {
            Action newAction = new Action(title).save();
            if(megaAction!=null)newAction.updateMegaOwner(megaAction.id);
            populateActionsGrid(megaAction); // Refresh the actions grid
        }
    }
 
}