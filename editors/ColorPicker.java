package me.nina.editors;

import javax.swing.*;

import me.nina.gameobjects.Animation;
import me.nina.gameobjects.Character.PosType;
import me.nina.maker.Main;
import me.nina.maker.Utils;
import me.nina.gameobjects.CharacterState;
import me.nina.gameobjects.GameObject;
import me.nina.objects2d.Img;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class ColorPicker extends JFrame {

    private BufferedImage image;
    private double zoomScale = 1.0;
	private CharacterState actualState;
	private Animation original;
	private JPanel imagePanel;
	protected JFrame frame;

    public ColorPicker(Animation original, CharacterState actualState) {
    	this.original=original;
    	this.actualState = actualState;
        this.image = original.getActualFrame();
        setTitle("Color Picker");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setPreferredSize(new Dimension(400, 400));

        imagePanel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                if (image != null) {
                    int scaledWidth = (int) (image.getWidth() * zoomScale);
                    int scaledHeight = (int) (image.getHeight() * zoomScale);
                    g.drawImage(image, 0, 0, scaledWidth, scaledHeight, this);
                }
            }
        }; 
        JButton button = new JButton("next frame");
        imagePanel.add(button);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               original.run();image = original.getActualFrame();
               repaint();
            }
        });
        imagePanel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) { 
            	if(e.getButton()==3)frame = getFrame(pickColorAt(e.getX(), e.getY(),false));
                pickColorAt(e.getX(), e.getY(),true);
            }
        });

        imagePanel.addMouseWheelListener(new MouseWheelListener() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
            	 
                if (e.getWheelRotation() < 0) {
                    zoomIn(e.getPoint());
                } else {
                    zoomOut(e.getPoint());
                }
            }
        });

        getContentPane().add(imagePanel);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private int pickColorAt(int x, int y,boolean update) {
        if (image != null) {
            // Obtenez les coordonn�es actuelles du pointeur de la souris
            Point mousePoint = MouseInfo.getPointerInfo().getLocation();
            SwingUtilities.convertPointFromScreen(mousePoint, this); // Convertit les coordonn�es de l'�cran � celles de la fen�tre

            // Calculez les coordonn�es du clic en fonction du zoom
            int zoomedX = (int) (x / zoomScale);
            int zoomedY = (int) (y / zoomScale);

            // V�rifiez si le clic est � l'int�rieur de l'image
            if (zoomedX >= 0 && zoomedX < image.getWidth() && zoomedY >= 0 && zoomedY < image.getHeight()) {
                int rgb = image.getRGB(zoomedX, zoomedY);
                if(update) {
                	update(rgb);
                	if(frame!=null)frame.dispose();
                }
                return rgb;
            }
        }return -1;
    }

    private void update(int rgb) {
    	original.updateShadow(rgb);
        if (actualState != null) {
            for (PosType p : PosType.values()) {
                if(actualState.movingAnimation(p)!=null)actualState.movingAnimation(p).updateShadow(rgb);
                if(actualState.idleAnimation(p)!=null)actualState.idleAnimation(p).updateShadow(rgb);
                if(actualState.talkAnimation(p)!=null)actualState.talkAnimation(p).updateShadow(rgb);
            }
        }
        GameObject.reset(); 
        Main.actualEditor.populate();
        dispose();
	}

	public void zoomIn(Point point) {
        double zoomFactor = 1.1; // Increase zoom by 10%
        zoomImage(zoomFactor, point);
    }

    public void zoomOut(Point point) {
        double zoomFactor = 0.9; // Decrease zoom by 10%
        zoomImage(zoomFactor, point);
    }

    private void zoomImage(double zoomFactor, Point mousePoint) {
        // Anciennes dimensions de l'image
        int oldWidth = imagePanel.getWidth();
        int oldHeight = imagePanel.getHeight();

        // Nouvelles dimensions de l'image apr�s le zoom
        int newWidth = (int) (oldWidth * zoomFactor);
        int newHeight = (int) (oldHeight * zoomFactor);

        // Convertir les coordonn�es de la souris en coordonn�es locales du panneau d'image
        Point localMousePoint = new Point(mousePoint);
        SwingUtilities.convertPointFromScreen(localMousePoint, imagePanel);
        // Calcul du d�calage entre la position de la souris et le coin sup�rieur gauche de l'image
        int dx = localMousePoint.x - imagePanel.getX();
        int dy = localMousePoint.y - imagePanel.getY();

        // Calcul de la nouvelle position du coin sup�rieur gauche de l'image
        int newX = localMousePoint.x - (int) (dx * zoomFactor);
        int newY = localMousePoint.y - (int) (dy * zoomFactor);

        // Mettre � jour les dimensions et la position de l'image
        imagePanel.setSize(newWidth, newHeight);
        imagePanel.setLocation(newX, newY);

        // Mettre � jour le facteur de zoom
        zoomScale *= zoomFactor;

        // Redessiner l'image avec le nouveau zoom et la nouvelle position
        imagePanel.revalidate();
        imagePanel.repaint();
    }
    public static JFrame getFrame(int color) {
        JFrame frame = new JFrame("Color Frame");
        frame.setSize(400, 400);
         

        JPanel panel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.setColor(new Color(color));
                g.fillRect(0, 0, getWidth(), getHeight());
            }
        };

        frame.add(panel);
        frame.setVisible(true);
        return frame;
    }
}