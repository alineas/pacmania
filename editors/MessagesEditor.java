package me.nina.editors;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.BoxLayout;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import agame.MainGame;
import me.nina.gameobjects.Action;
import me.nina.gameobjects.BasicObject;
import me.nina.gameobjects.Character;
import me.nina.gameobjects.GameObject;
import me.nina.gameobjects.InventoryObject;
import me.nina.gameobjects.Message;
import me.nina.gameobjects.ObjectState;
import me.nina.maker.Ashe;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Cbox;
import me.nina.maker.ControlRighte;
import me.nina.maker.Main;
import me.nina.maker.Utils;
import me.nina.sqlmanipulation.SQLTable;

public class MessagesEditor  extends Editor{
JScrollPane pane ;JPanel panel;
	static Map<Integer,Message > mappe = new HashMap<>();
	private Cbox<String> boxNoun;
	private Cbox<String> boxVerb;
	private Cbox<String> boxTalk;
	private int selectedNoun=-2;
	private int selectedVerb=-2;
	private int selectedTalker=-2;
	private String selected;
	private JTextField renamer;
	private Cbox<String> boxSid;
	private int selectedSid=-2;
	List<String> pathes = new ArrayList<>();
	private JTextField search;
	private static List<Message> liste;
	private ArrayList<Message> ls; 
	
	
	public MessagesEditor(){ 
		ls = new ArrayList<Message>();
		for(Action a : Action.getAll())
			for(Ashe b : a.ashToSet())
				if(b.possessor() instanceof Message)
					ls.add((Message) b.possessor());
					
		panel=new JPanel();
		panel.setLayout(new BoxLayout(  panel,1)); 
		pane = new JScrollPane(panel);Utils.setSize(pane, Main.actualEditor.getWidth(), Main.actualEditor.getHeight()); 
		add(pane);
		panel.setVisible(true);
		pane.getVerticalScrollBar().setUnitIncrement(10);
		Main.leftController.removeAll();
		updateLeftEditor();
		 setBackground(Color.white);  
		 panel.add(Message.getFormPanel());
	}
	

	public void populate(){
		panel.removeAll(); 
		List<Message> todo = Message.getAlls( );
		if(liste!=null&&liste.size()!=0)
			todo=liste;
		for(GameObject gm : todo){ 
			Message m = (Message)gm; 
			if(m.line==null)continue;
			if(liste==null||liste.size()==0)
			if(m.talker()==-1
					||m.noun() != selectedNoun 
					&& m.talker() != selectedTalker 
					&& m.verb() != selectedVerb
					&& m.sierraId() != selectedSid) { 
				continue;
			}
			JPanel line = new JPanel();
			line.setLayout(new GridLayout (1,8));
if(ls.contains(m))line.setBackground(Color.blue);
			but.createBoutton("play",line,50,20,( ActionListener)e->   {   
				MainGame.soundPlayer.playAudio(m.getSound(),false); 
			} );
			but.createBoutton("del",line,50,20,( ActionListener)e->   {   
				m.delete();populate();
			} );
			Character defined = null;
			for(Character a:Character.getAll())
				if(a.talker()==m.talker())
					defined = a;

			 Cbox<Character> charChooser = but.createCbox(Character.getAll(),line,150,20,null);
			 charChooser.setSelectedItem(defined);
			 charChooser.addActionListener(new ActionListener() { 
					public void actionPerformed(ActionEvent e) {  
						charChooser.getSelectedItem().updateTalker(m.talker()); populate();
					} });
			line.add(new JLabel("sierraid="+m.sierraId() +" ("+Message.getTitles("sid",m.sierraId())+")"));
			line.add(new JLabel("cond="+m.cond() ));
			line.add(new JLabel("noun="+m.noun() ));
			line.add(new JLabel("sequence="+m.seq() ));
			JTextField talker = new JTextField("talker="+m.talker()+"");
			talker.addActionListener((ActionListener)e->{ 
				gm.line.update("talker", talker.getText().replaceAll("talker=", ""));});
			line.add(talker);
			line.add(new JLabel("verb="+m.verb()+""));
			
			ObjectState trad = null;
			List<ObjectState> statList = new ArrayList<>();
			for(BasicObject obj : InventoryObject.getAll())
				for(ObjectState state : ObjectState.getByObject(obj.id)) {
					if(state.translateId() == m.id)
						trad = state;
					statList.add(state);
				}
			Collections.sort(statList, new Comparator<ObjectState>() {
                @Override
                public int compare(ObjectState o1, ObjectState o2) {
                    return o1.toString().compareTo(o2.toString());
                }
            });
			Cbox<ObjectState> box = but.createCbox(statList,line,300,20,null);
			 if(trad!=null)box.setSelectedItem(trad);
			 box.addActionListener(new ActionListener() { 
					public void actionPerformed(ActionEvent e) {  
						box.getSelectedItem().updatetranslateId(m.id);
					} });
			
			line.add(box);
			
			
			
			
			
			
			
			
			
			JTextArea area = new JTextArea(1,2);
			area.setText(m.text());
			area.setLineWrap(true);
			area.setWrapStyleWord(true);
			panel.add(line);
			panel.add(area);
			panel.add(new JLabel("____________________________________________"));
		}
		if(liste!=null)liste.clear();
		panel.revalidate();
	}

	@Override protected boolean handleImgDragged() { return false; }

	@Override public void updateLeftEditor() {Main.leftController.removeAll();  
		but.createBoutton("import sierra xml",lab,150,20,( ActionListener)e->   {
			 JFileChooser fileChooser = new JFileChooser(Utils.getWorkingFolder()); 
			 fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int res = fileChooser.showOpenDialog(null);
		     if (res == JFileChooser.APPROVE_OPTION) { 
		      File file = fileChooser.getSelectedFile();
		      Message .importFromFolder(file.getAbsolutePath());
		      SQLTable. doTheQueue();}
		} );
		 
		 
		but.createBoutton("import sound folder",but,150,20,( ActionListener)e->   {
			 JFileChooser fileChooser = new JFileChooser(Utils.getWorkingFolder()); 
			 fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			 String language ="default";
			 if(new File("games/" + Main.gameName + "/dialogs/").exists())
				 language = Main.getUserInput("language?");
				  
			int res = fileChooser.showOpenDialog(null);
		     if (res == JFileChooser.APPROVE_OPTION) { 
		    	 File sourceFolder = fileChooser.getSelectedFile();
		         File destinationFolder=new File("games/"+Main.gameName+"/dialogs/"+language+ "/dialogs/");
		         if (!destinationFolder.exists())  
		             destinationFolder.mkdirs(); 
		         try {  Utils.copyFolder(sourceFolder, destinationFolder);
		         } catch (IOException ex) {  ex.printStackTrace();  }
		         
		         destinationFolder=new File("games/" + Main.gameName + "/dialogs/");
		         if (!destinationFolder.exists())  {
		             destinationFolder.mkdirs();
		             try {  Utils.copyFolder(sourceFolder, destinationFolder);
			         } catch (IOException ex) {  ex.printStackTrace();  }
		         }
		     }
		} );
		lab.createLabel("verb",lab,150,20); 
		boxVerb = but.createCbox(Message.getVerbs(),but,150,20,( ActionListener)e->   {selected=null;selectedNoun=-2;selectedVerb=-2;selectedSid=-2;selectedTalker=-2;
			  selectedVerb = Message.getSelected(boxVerb.getSelectedItem());selected = "verb"+Message.getSelected(boxVerb.getSelectedItem());populate();
		} );
		boxVerb.addItem("");
		boxVerb.setSelectedItem("-1");
		 
		lab.createLabel("noun",lab,150,20); 
		boxNoun = but.createCbox(Message.getNouns(),but,150,20,( ActionListener)e->   {selected=null;selectedNoun=-2;selectedVerb=-2;selectedSid=-2;selectedTalker=-2;
			  selectedNoun = Message.getSelected(boxNoun.getSelectedItem());selected = "noun"+Message.getSelected(boxNoun.getSelectedItem());populate();
		} );
		boxNoun.addItem("");
		boxNoun.setSelectedItem("-1");
		
		lab.createLabel("sierraID",lab,150,20); 
		boxSid = but.createCbox(Message.getSids(),but,150,20,( ActionListener)e->   {selected=null;selectedNoun=-2;selectedVerb=-2;selectedSid=-2;selectedTalker=-2;
			  selectedSid = Message.getSelected(boxSid.getSelectedItem());selected = "sid"+Message.getSelected(boxSid.getSelectedItem());populate();
		
		} );
		boxSid.addItem("");
		boxSid.setSelectedItem("-1");
		
		lab.createLabel("talkers",lab,150,20); 
		boxTalk = but.createCbox(Message.getTalkers(),but,150,20,( ActionListener)e->   {selected=null;selectedNoun=-2;selectedVerb=-2;selectedSid=-2;selectedTalker=-2;
			  selectedTalker = Message.getSelected(boxTalk.getSelectedItem());selected = "talker"+Message.getSelected(boxTalk.getSelectedItem());populate();
		} );
		boxTalk.addItem("");
		boxTalk.setSelectedItem("-1");
		
		
		lab.createLabel("name the selected",lab,150,20); 
		renamer = but.createText(selected,but,150,20,( ActionListener)e->   {
			Editor.getGameInfo().duoManager.setValue("msgtitles", selected,renamer.getText()); 
		} );
 		
		
		lab.createLabel("search",lab,150,20); 
		search = but.createText("go",but,150,20,( ActionListener)e->   {
			liste = new ArrayList<>();
			for(Message a : Message.getAlls( )) { 
				if(a.text().contains(search.getText())) {
					liste.add(a);
					
				}
			}
			populate();
		} );
		
		
		
		lab.createLabel("translate",lab,150,20); 
		but.createBoutton("go",but,150,20,( ActionListener)e->   {
			translate(); 
		} );
		
		
		
		Main.leftController.revalidate();
		
	}
	public static void translate() {
		ControlRighte panel = Main.rightController;
		panel.removeAll();
		panel.setLayout(new GridLayout(0,2)); 
		panel.createBoutton("default language", panel, 150, 30,(ActionListener)e->{Message.translate("default");GameObject.reset(); Main.actualEditor.populate();}); 
		for (String lang : Editor.getGameInfo().getArray("languages" ) ) {
			panel.createBoutton(lang, panel, 150, 30,(ActionListener)e->{Message.translate(lang);GameObject.reset(); Main.actualEditor.populate();}); 
		} 
		panel.revalidate();
		
	}
} 