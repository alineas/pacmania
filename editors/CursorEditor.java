package me.nina.editors;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import me.nina.editors.CharacterEditor.AnimationType;
import me.nina.gameobjects.Animation;
import me.nina.gameobjects.CharacterState;
import me.nina.gameobjects.Animation.AnimationState;
import me.nina.gameobjects.Character.PosType;
import me.nina.maker.Cbox;
import me.nina.maker.Main;
import me.nina.maker.Utils;
import me.nina.objects2d.Img;
import me.nina.objects2d.ImgUtils;

public class CursorEditor extends Editor{
	
	
	 
	Cbox<CursorType> lst;
	private CursorType actualCursor;
	private JPanel grid;
	private boolean clickedRight; 
	public enum CursorType { TELEPORT,  OTHER, ANOTHER  }
	
	
	
	
	
	//#################################################################################### 
	public CursorEditor(){
		super();  
		
		setLayout(new GridLayout(1,3));  
		Main.leftController.removeAll();
		updateLeftEditor();
		populate(); 
	}
	
	
	
	
	
	//#################################################################################### 
	public void populate(){
		   removeAll();
		    
		    grid = new JPanel(); grid.setLayout(new GridLayout(3, 3)); add(grid );  grid.setOpaque(false);  
		    grid.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		     
		    for (int row = 0; row < 3; row++)  for (int col = 0; col < 3; col++) {
		            if (row == 1 && col == 1) {//useless center
		            	JLabel j = new JLabel("null here pls");
		            	j.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY)); 
		            	grid.add(j, row * 3 + col); 
		                continue;
		            }

		            addItemToGrid(  row, col ) ;
		             
		    }
		  
		    Main.leftController.revalidate();
		    revalidate(); 
	}
	 
	//#################################################################################### 
	@Override  public void mouseReleased(MouseEvent e) { int x = e.getX(); int y = e.getY();  
			if(clickedRight) {clickedRight=false; return; }
			
			Rectangle bounds = grid .getBounds(); 
		     
		    	
			CursorType item = lst.getSelectedItem(); 
		    String fileName=""; 
				File file = Main.fileDisplayer.getSelected();
				fileName = file.getName();
				Utils.copy(file, new File(getFolder() + fileName));
			  
		     
			PosType posType = calculatePosTypeForBounds(grid.getBounds(),x,y);
			 
			if(posType!=PosType.UNUSED) {
				 getGameInfo().duoManager.setValue( "cursors", item+""+posType,fileName);
	    	} 
		    populate();
		    super.mouseReleased(e);
		}
	
	
	//#################################################################################### 
	@Override protected boolean handleImgDragged() {  return true; }
	String getFolder() {  return "games/"+Main .gameName+"/"; } 
	@Override public void mouseEntered(MouseEvent e) {   super.mouseEntered(e); }
	
	
	@Override public void updateLeftEditor() {
		lab.createLabel("type",lab,150,20);
		lst = but.createCbox(CursorType.values(),but,150,20,(ActionListener) e->{ 
			actualCursor = lst.getSelectedItem();
			 populate(); }); 
	}
	
	//#################################################################################### 
	private void addItemToGrid( int row, int col ) {
        try {
        	
        	CursorType item = lst.getSelectedItem(); 
        	PosType posType = getPosTypeForGridIndex(row, col);
        	String fileName = getGameInfo().duoManager.getString("cursors", item+""+posType);
             
            grid.add(new JLabel( ImgUtils.createSemiTransparentImageIcon(ImgUtils.getPicture(getFolder()+fileName),1,75,75)), row * 3 + col);
            
        } catch (Exception e) { 
        	JLabel j = new JLabel( "to do");
        	j.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
            grid.add(j, row * 3 + col);
        }
		 
	}
	//#################################################################################### 
	private PosType getPosTypeForGridIndex(int row, int col) {
	    switch (row) {
	        case 0: switch (col) { 
	        		case 0: return PosType.TOP_LEFT;
	                case 1: return PosType.TOP;
	                case 2: return PosType.TOP_RIGHT;
	                default: break;
	            } break;
	        case 1:
	            switch (col) {
	                case 0: return PosType.LEFT;
	                case 1: return PosType.UNUSED;
	                case 2: return PosType.RIGHT;
	                default: break;
	            } break;
	        case 2:
	            switch (col) {
	                case 0: return PosType.BOT_LEFT;
	                case 1: return PosType.BOTTOM;
	                case 2: return PosType.BOT_RIGHT;
	                default: break;
	            } break;
	        default:
	            break;
	    }
	    return null;  
	}
	//#################################################################################### 
	private Point calculateGridIndexForPoint(Rectangle bounds, int x, int y) {
		    int cellWidth = bounds.width / 3;
	    int cellHeight = bounds.height / 3;

		    int row = (y - bounds.y) / cellHeight;
	    int col = (x - bounds.x) / cellWidth;

		    row = Math.min(Math.max(row, 0), 2);
	    col = Math.min(Math.max(col, 0), 2);

		    return new Point(row, col);
	}
	//#################################################################################### 
	private PosType calculatePosTypeForBounds(Rectangle r,int x, int y) {
		Point gridIndex = calculateGridIndexForPoint(r, x, y);
        return getPosTypeForGridIndex(gridIndex.x, gridIndex.y);
	}
}
