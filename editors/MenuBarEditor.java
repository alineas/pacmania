package me.nina.editors;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import agame.MenuBar;
import agame.MenuBar.MenuType;
import me.nina.agenerator.Generator;
import me.nina.agenerator.MenuBarGenerator; 
import me.nina.gameobjects.Zone; 
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Cbox;
import me.nina.maker.Main;
import me.nina.maker.Utils;
import me.nina.maker.Utils;
import me.nina.objects2d.Point;

public class MenuBarEditor extends Editor { private static final long serialVersionUID = 1L;
	private MenuBar menuBar;
	public Cbox<MenuType> editType;
	private boolean movingCloseInv;
	private boolean movingCloseMenu;
	Zone actualZoneToModify;
	private boolean modifMenuZone;
	private Zone invZone;
	private Zone menuZone;
	private Zone explorerZone;
	private Zone menuButtonZone;
	private Zone titles;
	//##################################################################
	public MenuBarEditor() {Utils.setSize(this, 1000,1000) ;
		menuBar = new MenuBar(null); 
		if(Editor.getGameInfo().duoManager.readArray("menubar").size() ==0)
		 MenuBarGenerator.generateMenuBar( );
		editType = but.createCbox(MenuType.values(),but,150,20,(ActionListener)e->{  repaint();  } ); 
		actualZoneToModify = menuBar.menuZone();
		menuZone = menuBar.menuZone();
		invZone = menuBar.invZone();
		explorerZone = menuBar.dragObjZone();
		menuButtonZone = menuBar.igMenuOpenerZone();
		titles = menuBar.invZoneTitles();
	}
	//##################################################################
	@Override public void updateLeftEditor() {
		// TODO Auto-generated method stub
		
	}
	//##################################################################
	@Override protected boolean handleImgDragged() {   
			Object[] options = { "background", "object explorer", "ingame menu","exit button inv explorer","exit menu button","cancel"};

	        // Custom icon
	        ImageIcon icon = new ImageIcon(Main.fileDisplayer.getSelected().getAbsolutePath());

	        // Show the custom dialog
	        int result = JOptionPane.showOptionDialog(
	                null,
	                "use it as : ",
	                "image received",
	                JOptionPane.YES_NO_CANCEL_OPTION,
	                JOptionPane.QUESTION_MESSAGE,
	                icon,
	                options,
	                options[2]); // Default option (Annuler)

	        // Check the user's choice
	        switch (result) {
	            case 0: 
	            	String f = Main.fileDisplayer.getUpdatedImage("games/"+Main.gameName+"/",menuBar.bgFolder());
	            	menuBar.updateBg(f);
	            	return true;
	                
	            case 1:  
	            	f = Main.fileDisplayer.getUpdatedImage("games/"+Main.gameName+"/",menuBar.invExplorerBg());
	            	menuBar.updateInvExplorerBg(f);
					return true;
	            case 2:
	            	f = Main.fileDisplayer.getUpdatedImage("games/"+Main.gameName+"/",menuBar.igMenuBg());
	            	menuBar.updateMenuIgBg(f);
					return true;
					
	            case 3:
	            	f = Main.fileDisplayer.getUpdatedImage("games/"+Main.gameName+"/",menuBar.exitButtonInvExplorerBg());
	            	menuBar.updateExitButInvExplorer(f);
	            	menuBar.updateCloseInvExplorerZone(new Zone(0,0,icon.getIconWidth(),icon.getIconHeight(),ZoneType.RECTANGLE).save(-1, ""));
					return true;
					
	            case 4:
	            	f = Main.fileDisplayer.getUpdatedImage("games/"+Main.gameName+"/",menuBar.exitButtonMenuBg());
	            	menuBar.updateExitButMenuBg(f);
	            	menuBar.updateCloseMenuZone(new Zone(0,0,icon.getIconWidth(),icon.getIconHeight(),ZoneType.RECTANGLE).save(-1, ""));

					return true;
					
					
	            default: 
	                return true;
	        } 
	        
	         
			 
	}
	//##################################################################
	@Override protected void paintComponent(Graphics g) { try {
			 super.paintComponent(g);  
			 if(editType.getSelectedItem() == MenuType.EDIT_BG && menuBar.bgFolder()!=null) {   
				g.drawImage( ImageIO.read(new File(menuBar.bgFolder())),0,0,null);
				menuZone.draw(true, g,Color.GREEN);
				invZone.draw(true, g,Color.GREEN);
				explorerZone.draw(true, g,Color.GREEN);
				menuButtonZone.draw(true, g,Color.GREEN);
			 }
			 if(editType.getSelectedItem() == MenuType.EDIT_IG_MENU && menuBar.igMenuBg()!=null)  
					g.drawImage( ImageIO.read(new File( menuBar.igMenuBg())),0,0,null);
			 if(editType.getSelectedItem() == MenuType.EDIT_IG_MENU && menuBar.exitButtonMenuBg()!=null)  
					g.drawImage( ImageIO.read(new File( menuBar.exitButtonMenuBg())),menuBar.closeMenuZone().x(),menuBar.closeMenuZone().y(),null);
				 
			 if(editType.getSelectedItem() == MenuType.EDIT_INV_EXPLORER && menuBar.invExplorerBg()!=null){  
				 if(titles==null){
					 int z = new Zone(0,0,100,100,ZoneType.RECTANGLE).save(-1, "");
					 Editor.getGameInfo().duoManager.setValue("menubar","invzonetitles",""+z);
				 }
					g.drawImage( ImageIO.read(new File(menuBar.invExplorerBg())),0,0,null);
					titles .draw(true, g,Color.GREEN);
			 }
			 if(editType.getSelectedItem() == MenuType.EDIT_INV_EXPLORER && menuBar.exitButtonInvExplorerBg()!=null)  
					g.drawImage( ImageIO.read(new File( menuBar.exitButtonInvExplorerBg())),menuBar.closeInvExplorerZone().x(),menuBar.closeInvExplorerZone().y(),null);
			 
			 if(lastZone!=null)
				 lastZone.draw(true, g,Color.GREEN);
			 if(actualZoneToModify!=null)
				 actualZoneToModify.draw(true, g,Color.GREEN);
			 
			 
			 
				
				
	} catch (IOException e) { e.printStackTrace(); } }
	

	//##################################################################
	@Override public void mouseReleased(MouseEvent e) {
		super.mouseReleased(e);
		if(modifMenuZone){
			modifMenuZone=false;
			actualZoneToModify.update(actualZoneToModify.toSqlValues());
		}
        	Generator.zoneDrawn(lastZone);
        if(movingCloseInv) {
        	Zone old = menuBar.closeInvExplorerZone();
        	int w = old.w();
        	int h = old.h();
        	old.setP1(new Point(e.getX(),e.getY()));
        	old.setP2(new Point(e.getX()+w,e.getY()+h));
        	old.update(old.toSqlValues());
        	movingCloseInv=false;
        }
        if(movingCloseMenu) {
        	Zone old = menuBar.closeMenuZone();
        	int w = old.w();
        	int h = old.h();
        	old.setP1(new Point(e.getX(),e.getY()));
        	old.setP2(new Point(e.getX()+w,e.getY()+h));
        	old.update(old.toSqlValues());
        	movingCloseMenu=false;
        }
	    
	    
	    
	}
	@Override public void mousePressed(MouseEvent e) {
 
	        if( invZone.isNear(new Point(e.getX(),e.getY()),  10)) 
	        	actualZoneToModify = invZone;
	        if( explorerZone.isNear(new Point(e.getX(),e.getY()),  10))
	        	actualZoneToModify = explorerZone;
	        if( menuButtonZone.isNear(new Point(e.getX(),e.getY()),  10))
	        	actualZoneToModify = menuButtonZone; 	         
	        if( menuZone.isNear(new Point(e.getX(),e.getY()),  10))
	        	actualZoneToModify = menuZone; 	
	        if( titles.isNear(new Point(e.getX(),e.getY()),  10))
	        	actualZoneToModify = titles; 
		
		
		
		if(actualZoneToModify!=null && actualZoneToModify.isNear(new Point(e.getX(),e.getY()), 10)){
			 modifiedZone=actualZoneToModify;
			 modifMenuZone=true;super.mousePressed(e);
		 }else
		 try {
			if(editType.getSelectedItem() == MenuType.EDIT_INV_EXPLORER&&
					 menuBar.closeInvExplorerZone().isInZone(new Point(e.getX(),e.getY())))/////////////////////////
				 movingCloseInv=true;
			
			 
			 if(editType.getSelectedItem() == MenuType.EDIT_IG_MENU&&menuBar.closeMenuZone().isInZone(new Point(e.getX(),e.getY())))////////////////////////
				 movingCloseMenu=true;
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    
		 
	}

}
