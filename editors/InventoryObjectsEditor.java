package me.nina.editors;
 
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.MouseInfo;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import me.nina.agenerator.ActionGenerator;
import me.nina.agenerator.Generator;
import me.nina.editionnodes.EditNode;
import me.nina.gameobjects.Action;
import me.nina.gameobjects.Animation;
import me.nina.gameobjects.BasicObject;
import me.nina.gameobjects.InventoryObject;
import me.nina.gameobjects.ObjectState;
import me.nina.gameobjects.SpecialZoneInv;
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Animation.AnimationState;
import me.nina.gameobjects.BasicObject.ObjectType;
import me.nina.gameobjects.Character;
import me.nina.gameobjects.Character.PosType;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.gameobjects.CharacterState;
import me.nina.gameobjects.GameObject;
import me.nina.maker.Cbox;
import me.nina.maker.Main;
import me.nina.maker.Utils;
import me.nina.objects2d.Img;
import me.nina.sqlmanipulation.SQLTable;

public class InventoryObjectsEditor extends Editor{ private static final long serialVersionUID = 1L;

	public Cbox<BasicObject> objectList; 
	private ObjectState selectedState; 
	private int idleFrame; 
	private Cbox<Zone> specialZonesList; 
	private JPanel controls; 
	private Color actualColor;
	 public boolean editZones;

	private JTextField textField;

	private Animation actualAnim;
	 
	
	//##################################################################
	public InventoryObjectsEditor(int i ) { 
		controls = new JPanel();
		controls.setLayout(new GridLayout(0,1)); 
		addKeyListener(this); 
		setLayout(new FlowLayout());
		updateLeftEditor(); 
		 
		populate();
	}
	 
	 
 
	//##################################################################
	@Override protected void paintComponent(Graphics g) { 
		 super.paintComponent(g); 
		 if(editZones) {
			 g.drawImage( Animation.get(selectedState.animID()).getActualFrame(),0,0,null); 
			 
		 }
		 if(lastZone!=null) {if(actualColor == null) actualColor = g.getColor();
		        Color transparentColor = new Color(actualColor.getRed(), actualColor.getGreen(), actualColor.getBlue(), 100); // Rouge avec une transparence de 100 (0 � 255)
		        g.setColor(transparentColor);
		        lastZone.draw(true, g,Color.GREEN);
		        g.setColor(Color.BLACK);
		 }
		
	}
	//##################################################################
	public void populate() {  removeAll();controls.removeAll(); 
		if(selectedState==null||!editZones) {
			InventoryObject item = (InventoryObject) objectList.getSelectedItem();
			
			for (ObjectState state : ObjectState.getByObject(item.id)) { 
					actualAnim = Animation.get(state.animID());
					 if(actualAnim==null){add(new JLabel("drag image here"));continue;}
					
					 actualAnim.loadGif();
					 actualAnim.setSize(50);
					add(actualAnim); 
				}
			revalidate();
			return; //show all the states
		}
		

		if(selectedState!=null) {
			this.requestFocusInWindow(); 
			Animation.get(selectedState.animID()).setIdleFrame(idleFrame);
			lastZone=null; actualColor = Color.black; 
			for(Zone a:SpecialZoneInv.getByOwner(selectedState.id))
				if(((SpecialZoneInv)a).frames.contains(idleFrame)||((SpecialZoneInv)a).frames.contains(-1)) {
					lastZone = a;
					actualColor = Color.YELLOW; 
				} 
			add(controls); 
			JPanel p = new JPanel(new GridLayout(1,2));
			 
			JButton plus = lab.createBoutton("-->", null, 250,20, (ActionListener)e->{
				actualColor = Color.black;this.requestFocusInWindow();
				if(idleFrame==Animation.get(selectedState.animID()).frameOrder().size()-1)
					idleFrame=0;
				else Animation.get(selectedState.animID()).setIdleFrame(++idleFrame);

				for(Zone a:SpecialZoneInv.getByOwner(selectedState.id))
					if(((SpecialZoneInv)a).frames.contains(idleFrame)||((SpecialZoneInv)a).frames.contains(-1)) {
						lastZone = a;
						actualColor = Color.YELLOW; 
					} 
				repaint();
			});p.add(plus);
			
			JButton minus = lab.createBoutton("<--", null, 250,20, (ActionListener)e->{
				actualColor = Color.black;this.requestFocusInWindow();
				if(idleFrame==0)
					idleFrame=Animation.get(selectedState.animID()).frameOrder().size()-1;
				else Animation.get(selectedState.animID()).setIdleFrame(--idleFrame);

				for(Zone a:SpecialZoneInv.getByOwner(selectedState.id))
					if(((SpecialZoneInv)a).frames.contains(idleFrame)||((SpecialZoneInv)a).frames.contains(-1)) {
						lastZone = a;
						actualColor = Color.YELLOW; 
					} 
				repaint();
			});p.add(minus);
			
			controls.add(p);
			lab.createBoutton( "save this zone in actual frame", controls, 250,20, (ActionListener)e->{
				SpecialZoneInv sp = new SpecialZoneInv(lastZone,idleFrame);
				int id=InventoryObject.addSpecialZone(selectedState,sp);
				Utils.log(selectedState.title()+" "+Zone.getById(id).possessorObj().title());
				specialZonesList.addItem((SpecialZoneInv) Zone.getById(id)); this.requestFocusInWindow();
				actualColor = Color.YELLOW; repaint();
			});
			lab.createBoutton("save this zone in all frames", controls, 250,20, (ActionListener)e->{ 
				SpecialZoneInv sp = new SpecialZoneInv(lastZone,-1);
				int id = InventoryObject.addSpecialZone(selectedState,sp);
				specialZonesList.addItem((SpecialZoneInv) Zone.getById(id));this.requestFocusInWindow();
				actualColor = Color.YELLOW; repaint();
			});
			controls.add(new JLabel(""));//separator

			specialZonesList = lab.createCbox(SpecialZoneInv.getByOwner(selectedState.id), controls, 250,20, (ActionListener)e->{ lastZone=specialZonesList.getSelectedItem();repaint(); });
			lab.createBoutton("delete this zone", controls, 250,20, (ActionListener)e->{ 
				Zone zone = specialZonesList.getSelectedItem();
				 
				zone.delete();
				specialZonesList.removeItem(zone);
				actualColor = Color.YELLOW; repaint();
			});
			lab.createBoutton("delete this frame", controls, 250,20, (ActionListener)e->{ 
				SpecialZoneInv zone = (SpecialZoneInv) specialZonesList.getSelectedItem();
				 
				zone.frames.remove(new Integer(idleFrame));
				zone.update(zone.toSqlValues()); 
				actualColor = Color.BLACK; repaint();
			});
			lab.createBoutton("return to the states", controls, 250,20, (ActionListener)e->{ 
				 
				editZones=false;
				populate();
			});
			 
		}
 

		repaint();revalidate(); 





	}

	//##################################################################
	@Override protected boolean handleImgDragged() { 
		InventoryObject item = (InventoryObject) objectList.getSelectedItem();
		 if(item==null)return true;
		Object[] options = { item.title()+" initial state", item.title()+" new state","new object"};

        // Custom icon
        ImageIcon icon = new ImageIcon(Main.fileDisplayer.getSelected().getAbsolutePath());

        // Show the custom dialog
        int result = JOptionPane.showOptionDialog(
                null,
                "use it as : ",
                "image received",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                icon,
                options,
                null); // Default option (Annuler)
 
		String title;
		// Check the user's choice
        switch (result) {
            case 0: String title1=item.title()+"_initial_state";
            	
            	String name = Main.fileDisplayer.getUpdatedImage(item.getFolder(),null);
        		item.updateInitialAnim(title1,name,AnimationState.LOOP);
            	populate();
				return true;
                
            case 1:  String title2=item.title()+"_"+Main.getUserInput();
            	name = Main.fileDisplayer.getUpdatedImage(item.getFolder(),null);
            	item.generateNewState(title2,name);
        		populate();
				return true; 
            case 2: 
            	  name = Main.fileDisplayer.getUpdatedImage(item.getFolder(),null);
            	title= Main.getUserInput();
            	if(title==null||title.equals(""))return true;
            	InventoryObject obj = (InventoryObject) new InventoryObject(title).save();
            	 
            	obj.generateInitialState( ); 
            	obj.updateInitialAnim(title,name,AnimationState.LOOP);
            	populate();
            	 
            default: 
                return true;
        } 
        
          
	}
	//##################################################################
	@Override public void updateLeftEditor() {Main.leftController.removeAll();
		objectList = but.createCbox(InventoryObject.getAll(),but,150,20,( ActionListener)e-> {  
			 
			 populate();
			 textField.setText(objectList.getSelectedItem( ).title());
			 
		} );
		
		
		but.createBoutton("createObject",but,150,20,( ActionListener)e-> {     
			String title3 = Main.getUserInput();
        	if(title3==null||title3.equals(""))
        		return;
        	InventoryObject obj = (InventoryObject) new InventoryObject(title3).save(); 
        	obj.generateInitialState( );  
        	objectList.addItem(obj);
        	objectList.setSelectedItem(obj);
        	
		} );
		
		lab.createBoutton("delete "+objectList.getSelectedItem( ).title(),lab,150,20,( ActionListener)e-> {     
        	objectList.getSelectedItem( ).delete();
        	populate();
    		updateLeftEditor(); 
		} );
		textField = lab.createText("",lab,150,20,( ActionListener)e-> {     
        	objectList.getSelectedItem( ).updateTitle(textField.getText());
        	populate();
    		updateLeftEditor(); 
		} );
	}
  

	//##################################################################
 @Override public void mousePressed(MouseEvent e) {
	 if(selectedState!=null)
	super.mousePressed(e);
	JPopupMenu popupMenu = new JPopupMenu(); 
	java.awt.Point dropPosition = MouseInfo.getPointerInfo().getLocation();
    SwingUtilities.convertPointFromScreen(dropPosition, this);
    Component component = SwingUtilities.getDeepestComponentAt(this, dropPosition.x, dropPosition.y);
     
	if(e.getButton()==3){ 
        if(component instanceof Animation) {
        	Animation animation = ((Animation)component);
        	ObjectState state = ObjectState.get(animation.possessor());
        	GameObject possessor = state.possessorObj();

        	selectedState = state;
        	objectList.setSelectedItem(possessor);

        	lab.createJMenu(popupMenu,"edit zones" ,ee -> {  
        		idleFrame=0; 
        		editZones=true;
        		populate();
        	});  
        	lab.createJMenu(popupMenu,"delete state" ,ee -> {  
        		idleFrame=0; 
        		populate();
        		updateLeftEditor(); 
        	});  
        	
        	 
			if(possessor!=null) {	
				for(Action c: Action.getByDetector(GameObject.getById(state.id)))   
						lab.createJMenu(popupMenu,c.title(),ee -> {  
							new Action(c,false ).populateRight();
			        	});
				for(Action c: Action.getByDetector(GameObject.getById(possessor.id)))   
					lab.createJMenu(popupMenu,c.title(),ee -> {  
						new Action(c,false ).populateRight();
					});	
			}
			
        	lab.createJMenu(popupMenu,"create action" ,ee -> {  
        		idleFrame=0;  
        		populate(); 
        		ActionGenerator.generateInventoryAction( ); 
        	});  
        	lab.createJMenu(popupMenu,"selected file=this state cursor image" ,ee -> {   
        		String name = Main.fileDisplayer.getUpdatedImage(selectedState.getFolder(),null);
        		selectedState.updateCursorimage(name);
        		populate(); 
        	}); 
        	lab.createJMenu(popupMenu,"delete "+state.title() ,ee -> {  
        		state.delete();
        		populate();
        	}); 
        	popupMenu.show(Main.actualEditor, e.getX(), e.getY());
        }
	} 
	if(specialZonesList!=null && specialZonesList.getSelectedItem() !=null)
		Generator.zoneDrawnInvEditor( (SpecialZoneInv) specialZonesList.getSelectedItem());
		 
		 
		
	}
 
	
	
	@Override public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) { 
		case KeyEvent.VK_ENTER:

			Generator.pressEnter();

			break;
		}
       
    }
	 
}





