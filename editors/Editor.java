package me.nina.editors;
 
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.filechooser.FileSystemView;

import me.nina.editionnodes.EditNode;
import me.nina.editors.SceneEditor.EditType;
import me.nina.gameobjects.Character;
import me.nina.gameobjects.Scene;
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Ashe;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Cbox; 
import me.nina.maker.Main;
import me.nina.maker.Pane;
import me.nina.maker.RosellaZone;
import me.nina.sqlmanipulation.DuoArray;
import me.nina.sqlmanipulation.SQLLine;
import me.nina.sqlmanipulation.SQLTable;

public abstract class Editor extends VideoPanel { private static final long serialVersionUID = 1L;
double zoom = 1.0;
	protected Pane but,lab; 
	
	public Scene scene;

	public RosellaZone rosella;
	
	public Editor( ) {
		Main.leftController.removeAll();  
		but = Main.leftController.panneauBoutons;
		lab = Main.leftController.panneauLabels;  
		Main.actualEditor = this;
		Main.revalidate(); 
		 
		
		
	}
	//######################################################################### 

	protected abstract boolean handleImgDragged();

	

	
	//######################################################################### 
	public static SQLLine getGameInfo() { 
        if(SQLTable.getTable("gameinfo").getLines().size() == 0) {
        	Map <String,String>mappe = new HashMap<>();
        	mappe.put("actualfolder", FileSystemView .getFileSystemView() .getHomeDirectory().getAbsolutePath());
			SQLTable.getTable("gameinfo").insert(mappe); 
			  
			SQLLine line = SQLTable.getTable("gameinfo").getLines().values().iterator().next();
        	 
        	return line;
		}
		return SQLTable.getTable("gameinfo").getLines().values().iterator().next();
	}
	//######################################################################### 
	public static void saveOrderOfTpointsGrid(List<Case> allTheCases) { 
		SQLLine line = Editor.getGameInfo();//save the order
    	List<DuoArray<String,String>> toReturn = new ArrayList<DuoArray<String,String>>(); 
    	for(Case a : allTheCases) 
    		toReturn.add(new DuoArray(a.scene.id,a.point.x+"."+a.point.y)); 
    	line.duoManager.updateArray("tpointsorder", toReturn);
	}
	//######################################################################### 
	public static List<DuoArray<String, String>> getOrderedTpointsGrid() { 
		SQLLine line = getGameInfo();
		return line.duoManager.readArray("tpointsorder");
	}
 
	
	//######################################################################### 
	public static void recordLinkerPlacement(int scene , int x, int y) {
		if(x<200 || x >10000) x = 200; if (y<200 || y >10000) y = 200;
		SQLLine line = getGameInfo();
		line.duoManager.setValue("tpointsorder", scene+"", x+"."+y);
		 
	}
	//######################################################################### 
	public static List<DuoArray<String, String>> getOrderedObjectStateGrid() { 
		SQLLine line = getGameInfo();
		return line.duoManager.readArray("objectstateorders");
	}
	public static List<DuoArray<String, String>> getOrderedActionStateGrid() {
		SQLLine line = getGameInfo();
		return line.duoManager.readArray("actionstateorders");
	}
	
	//######################################################################### 
	public static void recordObjectStatePlacement(int id , int x, int y) {
		if(x<0 || x >10000) x = 0; if (y<0 || y >10000) y = 0;
		SQLLine line = getGameInfo();
		line.duoManager.setValue("objectstateorders", id+"", x+"."+y);
		 
	}
	public static void recordActionStatePlacement(int id , int x, int y) {
		if(x<0 || x >10000) x = 0; if (y<0 || y >10000) y = 0;
		SQLLine line = getGameInfo();
		line.duoManager.setValue("actionstateorders", id+"", x+"."+y);
		 
	}
	//######################################################################### 
	public enum AddType {
	    add_game_element(-1), BASIC_OBJECT(0), ANIMATION(1),SCENE(2),CHAPTER(3),CHARACTER(4), INVENTORY_OBJECT(5);
	      
	    private final int valeur; 
	    AddType(int valeur) { this.valeur = valeur;  } 
	    public int getValeur() {  return valeur; } 
	    //@Override public String toString() { return String.valueOf(getValeur()); }
	    public static AddType from(String s) { 
	    	return from(Integer.parseInt(s));
	    }
	    public static AddType from(int valeur) {
	        for (AddType type : values()) {
	            if (type.getValeur() == valeur) {
	                return type;
	            }
	        }
	        throw new IllegalArgumentException("no Edit corresponding to value : " + valeur);
	    }
	    
	     
	    
	    
	}
	
	public abstract void updateLeftEditor();

	public void setZoom(double zoom) {
		this.zoom=zoom;
		
	}

	public static boolean hasInitialAsh (int id, AshType a) {
		List<Ashe> liste = Ashe.getlist(getGameInfo().getString("initialashes" ));  
		return  liste.contains(new Ashe(id,a.getValeur(),1));
	}
	public static void setInitialAshes(int id, AshType a,int nbr) {
		getGameInfo().addInArray("initialashes", new Ashe(id,a.getValeur(),nbr).toSqlValue());  		 
	}
	public static void setInitialAshes(int id, AshType a) {
		getGameInfo().addInArray("initialashes", new Ashe(id,a.getValeur(),1).toSqlValue());  
		 
	}

	public static void remInitialAsh(int id, AshType a) {
		getGameInfo().remFromArray("initialashes", new Ashe(id,a.getValeur(),1).toSqlValue());  
	};
	public static Character actualCharacter() {
		return Character.get(Editor.getGameInfo().getInt("actualcharacter"));
	}

	public static void handleDelete(int id) {
		List<Ashe> list = Ashe.getlist(Editor.getGameInfo().getString("initialashes"));//update the initials sometime
		boolean contains = false;
		for(Ashe a:list)
			if(a.possessor().id == id)
				contains=true;
				if(contains) {
		List<Ashe> updated = new ArrayList<>();
		for(Ashe a:list)
			if(a.possessor().id != id && a.type() != AshType.WILL_BE_IN_STATE)
				updated.add(a);
		Editor.getGameInfo().update("initialashes","");//clear array
		
		for(Ashe a : updated)
			setInitialAshes(a.possessor().id,a.type());//stay only the existing
		
	}
	}
}
