package me.nina.editors;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import me.nina.gameobjects.Animation;
import me.nina.gameobjects.Animation.AnimationState;
import me.nina.gameobjects.CharacterState;
import me.nina.gameobjects.GameObject;
import me.nina.maker.Cbox;
import me.nina.maker.JCbox;
import me.nina.maker.Main;
import me.nina.maker.Utils; 
import me.nina.objects2d.Img;
import me.nina.objects2d.ImgUtils;

public class AnimationEditor extends Editor { private static final long serialVersionUID = 1L;
	private List<BufferedImage> frames = new ArrayList<>();
    private List<Integer> initialIndices = new ArrayList<>();
    private JPanel animationPanel;
    private List<JLabel> frameLabels=new ArrayList<>();
	protected int draggedIndex;
	private Animation animation;
	private JTextField prioBypass;
	private Animation original;
	private JTextField groundBypass; 
	private static Animation copie;    

	
	 
	public AnimationEditor( Animation original) { 
    	this.original = original;
		this.animation = original;  
    	
    	setLayout(new FlowLayout(FlowLayout.CENTER));
        
        copie=new Animation(original.line); 
        //for(Img a:original.getFrames())
        	//frames.add(Img.getCopy(a)); 
        for (int i = 0; i < copie.frameOrder().size(); i++) {
            initialIndices.add(copie.frameOrder().get(i));
            frames.add(i, ImgUtils.getCopy(copie.getFrames().get(copie.frameOrder().get(i))));
        }
        animationPanel = new JPanel(new GridLayout(0, 4, 10, 10)); 
        initializeFramesPanel();
 updateLeftEditor();
  
    }
    //############################################################
    private void updateFramesPanel() {
        animationPanel.removeAll();
        frameLabels.clear(); // Nettoie la liste des labels
        
        for (int i = 0; i < initialIndices.size(); i++) {
            BufferedImage frame = frames.get(i);
            int originalIndex = initialIndices.get(i);
            JLabel frameLabel = createFrameLabel(frame, i, originalIndex);
            frameLabels.add(frameLabel);
            animationPanel.add(frameLabel);
        }

        animationPanel.revalidate();
        animationPanel.repaint();
    }
  //############################################################
    private void initializeFramesPanel() {
        animationPanel.removeAll();

        for (int i = 0; i < initialIndices.size(); i++) {
            try {
				JLabel frameLabel = createFrameLabel(frames.get(i), i, initialIndices.get(i));
				frameLabels.add(frameLabel);
				animationPanel.add(frameLabel);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }

        add(animationPanel, BorderLayout.CENTER);
    }
  //############################################################
    private JLabel createFrameLabel(BufferedImage frame, int currentIndex, int originalIndex) {
        JLabel frameLabel = new JLabel();
        if (frame != null) {
            frameLabel.setIcon(new ImageIcon(ImgUtils.resiz(frame,100, 100)));
        }
        setFrameLabelText(frameLabel, currentIndex, originalIndex);
        addFrameLabelMouseListeners(frameLabel);
        return frameLabel;
    }
  //############################################################
    private void setFrameLabelText(JLabel frameLabel, int currentIndex, int originalIndex) {
        frameLabel.setText(String.format("%d (Original: %d)", currentIndex, originalIndex));
        if(original.sonorizedFrames().contains(currentIndex))
        	frameLabel.setText(frameLabel.getText()+" sonorised");
        frameLabel.setHorizontalTextPosition(JLabel.CENTER);
        frameLabel.setVerticalTextPosition(JLabel.TOP);
        frameLabel.setHorizontalAlignment(JLabel.CENTER);
    }
  //############################################################
    private void addFrameLabelMouseListeners(JLabel frameLabel) {
        frameLabel.addMouseListener(new MouseAdapter() {
             

            @Override
            public void mousePressed(MouseEvent e) {
            	 draggedIndex = frameLabels.indexOf(frameLabel);
                if (e.getButton() == 3) {
                    showPopup(e);
                    return;
                }
                
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.getButton() == 3) {
                    return;
                }
                Point dropPosition = MouseInfo.getPointerInfo().getLocation();
                SwingUtilities.convertPointFromScreen(dropPosition, animationPanel);
                Component component = SwingUtilities.getDeepestComponentAt(animationPanel, dropPosition.x, dropPosition.y);
                int dropIndex = frameLabels.indexOf(component);
                if (dropIndex != -1 && dropIndex != draggedIndex) {
                    moveFrame(draggedIndex, dropIndex);
                }
            }
        });
    }
  //############################################################
    private void moveFrame(int draggedIndex, int dropIndex) {
        BufferedImage draggedFrame = frames.remove(draggedIndex);
        int originalDraggedIndex = initialIndices.remove(draggedIndex);

        frames.add(dropIndex, draggedFrame);
        initialIndices.add(dropIndex, originalDraggedIndex);
         

        updateFrameLabels();
    }
  //############################################################
    private void updateFrameLabels() {
        frameLabels.clear();
        animationPanel.removeAll();
        for (int i = 0; i < initialIndices.size(); i++) {
            JLabel frameLabel = createFrameLabel(frames.get(i), i, initialIndices.get(i));
            frameLabels.add(frameLabel);
            animationPanel.add(frameLabel);
        }

        animationPanel.revalidate();
        animationPanel.repaint();
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    private void showPopup(MouseEvent e) {
        JPopupMenu menu = new JPopupMenu();

        JMenuItem insertEmptySlotItem = new JMenuItem("Insert Empty Slot");
        insertEmptySlotItem.addActionListener(event -> insertEmptySlot());
        menu.add(insertEmptySlotItem);

        JMenuItem copyFrameItem = new JMenuItem("Copy Frame");
        copyFrameItem.addActionListener(event -> copyFrame());
        menu.add(copyFrameItem);

        JMenuItem del = new JMenuItem("Delete Frame");
        del.addActionListener(event -> delFrames());
        menu.add(del);

        JMenuItem reverseFramesItem = new JMenuItem("Reverse Frames");
        reverseFramesItem.addActionListener(event -> reverseFrames());
        menu.add(reverseFramesItem);


        int clickedIndex = frameLabels.indexOf((JLabel) e.getSource());
        if(!original.sounde().equals("")){
        JMenuItem adds = new JMenuItem("sonorize this frame");
        adds.addActionListener(event -> original.addSonorized(clickedIndex));
        menu.add(adds);
        JMenuItem rems = new JMenuItem("dont sonorize this frame anymore");
        rems.addActionListener(event -> original.remSonorized(clickedIndex));
        menu.add(rems);
        JMenuItem res = new JMenuItem("reset to default sonorisation");
        res.addActionListener(event -> original.resetSono());
        menu.add(res);
        JMenuItem un = new JMenuItem("remove sonorisation");
        un.addActionListener(event -> original.unsonorize());
        menu.add(un);
        }
        JMenu duplicateMenu = new JMenu("Duplicate Single Frame");
        for (int i = 1; i <= 10; i++) {
            JMenuItem item = new JMenuItem(Integer.toString(i));
            int count = i;
            item.addActionListener(event -> duplicateFrame(clickedIndex, count));
            duplicateMenu.add(item);
        }
        menu.add(duplicateMenu);
        
        JMenuItem deleteAllBeforeItem = new JMenuItem("Delete All Frames Before");
        deleteAllBeforeItem.addActionListener(event -> deleteAllFramesBefore(clickedIndex));
        menu.add(deleteAllBeforeItem);

        JMenuItem deleteAllAfterItem = new JMenuItem("Delete All Frames After");
        deleteAllAfterItem.addActionListener(event -> deleteAllFramesAfter(clickedIndex));
        menu.add(deleteAllAfterItem);
        
        JTextField textField = new JTextField("1");
        JMenuItem duplicateFramesItem = new JMenuItem("Duplicate All Frames");
        duplicateFramesItem.addActionListener(event -> {
            try {
                int count = Integer.parseInt(textField.getText());
                duplicateFrames(count);
            } catch (NumberFormatException ex) {
                // G�rer l'erreur de conversion
                JOptionPane.showMessageDialog(this, "Invalid input. Please enter a valid number.");
            }
        });
        menu.add(duplicateFramesItem);
        menu.add(textField);

        menu.show(e.getComponent(), e.getX(), e.getY());
    } 
  //############################################################
    private void insertEmptySlot() {
    	 
        frames.add(draggedIndex, null); // Ajoute la copie � l'index
        initialIndices.add(draggedIndex, -1); // Garde le m�me index initial
        updateFramesPanel(); // Met � jour l'affichage
    }
  //############################################################
    private void reverseFrames() {
        Collections.reverse(frames); // Inverse l'ordre des frames
        Collections.reverse(initialIndices);
        updateFramesPanel(); // Met � jour l'affichage avec le nouvel ordre invers�
    }
  //############################################################
    private void delFrames() {
    	 
        frames.remove(draggedIndex);  
        initialIndices.remove(draggedIndex);  
        updateFramesPanel(); 
    }
  //############################################################
    private void copyFrame() {
    	 Point dropPosition = MouseInfo.getPointerInfo().getLocation();
         SwingUtilities.convertPointFromScreen(dropPosition, animationPanel);
         Component component = SwingUtilities.getDeepestComponentAt(animationPanel, dropPosition.x, dropPosition.y);
         int index = frameLabels.indexOf(component);
        BufferedImage copiedFrame = frames.get(index); // Copie la frame
        frames.add(index, copiedFrame); // Ajoute la copie � l'index
        initialIndices.add(index, initialIndices.get(index)); // Garde le m�me index initial
        updateFramesPanel(); // Met � jour l'affichage
    }
    private void duplicateFrames() {
        List<BufferedImage> duplicatedFrames = new ArrayList<>();
        List<Integer> duplicatedIndices = new ArrayList<>();

        for (int i = 0; i < frames.size(); i++) {
            duplicatedFrames.add(frames.get(i)); // Ajoute la frame originale
            duplicatedFrames.add(frames.get(i)); // Ajoute la copie de la frame

            duplicatedIndices.add(initialIndices.get(i)); // Garde l'index initial
            duplicatedIndices.add(initialIndices.get(i)); // Garde l'index initial
        }

        frames = duplicatedFrames; // Remplace les frames par les frames dupliqu�es
        initialIndices = duplicatedIndices; // Remplace les indices par les indices dupliqu�s

        updateFramesPanel(); // Met � jour l'affichage
    }
    
    private void deleteAllFramesBefore(int index) {
        if (index <= 0 || index >= frames.size()) {
            return; // Pas d'action si l'index est invalide
        }

        frames.subList(0, index).clear(); // Supprime toutes les frames avant l'index sp�cifi�
        initialIndices.subList(0, index).clear(); // Supprime tous les indices initiaux correspondants
        updateFramesPanel(); // Met � jour l'affichage
    }

    private void deleteAllFramesAfter(int index) {
        if (index < 0 || index >= frames.size() - 1) {
            return; // Pas d'action si l'index est invalide
        }

        frames.subList(index + 1, frames.size()).clear(); // Supprime toutes les frames apr�s l'index sp�cifi�
        initialIndices.subList(index + 1, initialIndices.size()).clear(); // Supprime tous les indices initiaux correspondants
        updateFramesPanel(); // Met � jour l'affichage
    }
    
    private void duplicateFrames(int count) {
        List<BufferedImage> duplicatedFrames = new ArrayList<>();
        List<Integer> duplicatedIndices = new ArrayList<>();

        for (int i = 0; i < frames.size(); i++) {
            for (int j = 0; j < count; j++) {
                duplicatedFrames.add(frames.get(i)); // Ajoute la frame originale
                duplicatedIndices.add(initialIndices.get(i)); // Garde l'index initial
            }
        }

        frames = duplicatedFrames; // Remplace les frames par les frames dupliqu�es
        initialIndices = duplicatedIndices; // Remplace les indices par les indices dupliqu�s

        updateFramesPanel(); // Met � jour l'affichage
    }
    private void repeatFrames(int count) {
        List<BufferedImage> duplicatedFrames = new ArrayList<>();
        List<Integer> duplicatedIndices = new ArrayList<>();
for (int j = 0; j < count; j++) {
        for (int i = 0; i < frames.size(); i++) {
            
                duplicatedFrames.add(frames.get(i)); // Ajoute la frame originale
                duplicatedIndices.add(initialIndices.get(i)); // Garde l'index initial
            }
        }

        frames = duplicatedFrames; // Remplace les frames par les frames dupliqu�es
        initialIndices = duplicatedIndices; // Remplace les indices par les indices dupliqu�s

        updateFramesPanel(); // Met � jour l'affichage
    }
    private void duplicateFrame(int clickedIndex, int count) {
        if (clickedIndex >= 0 && clickedIndex < frames.size()) {
            BufferedImage clickedFrame = frames.get(clickedIndex);
            int originalIndex = initialIndices.get(clickedIndex);

            for (int i = 0; i < count; i++) {
                frames.add(clickedIndex, clickedFrame); // Ajoute la copie � l'index cliqu�
                initialIndices.add(clickedIndex, originalIndex); // Garde l'index initial
            }

            updateFramesPanel(); // Met � jour l'affichage
        }
    }
@Override protected boolean handleImgDragged() {
	  
	
	Object[] options = { "image file", "sound","music"};

    // Custom icon
   // ImageIcon icon = new ImageIcon(Img.getPicture(Main.fileDisplayer.getSelected().getAbsolutePath()).resiz(300, 200));
    // Show the custom dialog
    int result = JOptionPane.showOptionDialog(
            null,
            "use it as : ",
            "image received",
            JOptionPane.YES_NO_CANCEL_OPTION,
            JOptionPane.QUESTION_MESSAGE,
            null,
            options,
            options[2]); // Default option (Annuler)

    
	// Check the user's choice
    switch (result) {
        case 0: 
        	File file = Main.fileDisplayer.getSelected();
        	String fileName = file.getName();
        	ImgUtils.mappe.remove(original.gifFile.path);
        	 
        	Utils.copy(file, new File(original.getFolder() + fileName));
        	original.updateImage(fileName);
        	ArrayList<Integer> order=new ArrayList<Integer>();
        	order.add(0);
        	original.updateOrder(order); 
        	original=new Animation(original.line); 
        	frames.clear(); frameLabels.clear();
    		for(BufferedImage a:original.getFrames()) frames.add(ImgUtils.getCopy(a)); 
    		initialIndices.clear();
    		for (int i = 0; i < frames.size(); i++)  initialIndices.add( i );
    		updateFramesPanel(); revalidate(); repaint(); 
        	return true;
 
        case 1:  
        	file = Main.fileDisplayer.getSelected();
        	fileName = file.getName();
        	Utils.copy(file,  new File( original.possessorObj().possessorObj().getFolder()+fileName));
        	original.updateSound(fileName) ;
        	return true;

        case 2:  
        	file = Main.fileDisplayer.getSelected();
        	fileName = file.getName();
        	Utils.copy(file,  new File( original.possessorObj().possessorObj().getFolder()+fileName));
        	original.updateMusic(fileName) ;
        	return true;
        	
    }  return true;
}
@Override public void updateLeftEditor() {  
	JCheckBox jinf = new JCheckBox("infinite");lab.add(jinf);
	JCheckBox jinv = new JCheckBox("invisible");but.add(jinv);
	JCheckBox jloop = new JCheckBox("one loop");lab.add(jloop);
	JCheckBox aloop = new JCheckBox("aleatory loop");but.add(aloop);
	jloop.addActionListener( (ActionListener)e->{ original.updateState(AnimationState.LOOP); } );
	jinv.addActionListener( (ActionListener)e->{ original.updateState(AnimationState.INVISIBLE);  } );
	aloop.addActionListener( (ActionListener)e-> { original.updateState(AnimationState.ALEATORY_LOOP); }  );
	jinf.addActionListener( (ActionListener)e->{  original.updateState(  AnimationState.INFINITE_LOOP ); } );

	lab.createLabel("prioBypass=",lab, 150, 20 ); 
	prioBypass = but.createText(""+original.priorityToAddForBypass(), but, 150, 20, (ActionListener)e->{
		original.updatePriorityToAdd(Integer.parseInt(prioBypass.getText())); });
	
	lab.createLabel("ground priority lvl=",lab, 150, 20 ); 
	groundBypass = but.createText(""+original.priorityGroundPoint(), but, 150, 20, (ActionListener)e->{
		original.updatePriorityGroundPoint(Integer.parseInt(groundBypass.getText())); });
	
	lab.createLabel("reset",lab, 150, 20 ); 
	but.createBoutton("original frame order", but, 150, 20, (ActionListener)e -> { 
		copie= new Animation(original.line);  frames.clear(); frameLabels.clear();
		for(BufferedImage a:original.getFrames()) frames.add(ImgUtils.getCopy(a)); 
		initialIndices.clear();
		for (int i = 0; i < frames.size(); i++)  initialIndices.add( i );
		updateFramesPanel(); revalidate(); repaint();  });
	
		if(!original.sounde().equals("") ){
		lab.createLabel("sound=",lab, 150, 20 ); 
		but.createLabel(original.sounde(),but, 150, 20 ); 
		
		lab.createLabel("remove sonorisation",lab, 150, 20 ); 
		but.createBoutton("clic",but, 150, 20,(ActionListener)event -> original.unsonorize()); 
		}
	if(!original.music().equals("")){
		lab.createLabel("music=",lab, 150, 20 ); 
		but.createLabel(original.music(),but, 150, 20 );
		
		lab.createLabel("remove sonorisation",lab, 150, 20 ); 
		but.createBoutton("clic",but, 150, 20,(ActionListener)event -> original.unsonorize()); 
		}
	 
		
	
	lab.createBoutton("duplicate", lab, 150, 20, (ActionListener)e->{duplicateFrames();});
	lab.createBoutton("repeat", lab, 150, 20, (ActionListener)e->{repeatFrames(2);});
	
	but.createBoutton("shadow selector", but, 150, 20, (ActionListener)e->{new ColorPicker(original,null);});
	 
	lab.createBoutton("save order", lab, 150, 20, (ActionListener)e->{Main.goBack(); original.updateOrder(initialIndices);
	
	 GameObject.reset();
     ImgUtils.mappe.clear();Utils.log("clearde"); 
     Main.actualEditor.populate();
     
	});

}
}