package me.nina.editors;
import javax.swing.*;
 
import me.nina.gameobjects.Character.PosType;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.gameobjects.Scene;
import me.nina.gameobjects.TransitionPoint;
import me.nina.gameobjects.Zone;
import me.nina.maker.Main;
import me.nina.maker.RosellaZone;
import me.nina.maker.Utils; 
import me.nina.objects2d.Img;
import me.nina.objects2d.ImgUtils;
import me.nina.objects2d.Point;
import me.nina.sqlmanipulation.DuoArray;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.MouseInfo;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class SceneTpointLinker extends Editor { private static final long serialVersionUID = 1L;
	private Case caseIpressedFirst;
	public static TransitionPoint tpIpressedFirst;
    private boolean isMovingCase;
    public static List<Case> allTheCases;   
	public static Map <TransitionPoint,Point> locationsOfTpoints = new HashMap<>(); 
	public static Map <TransitionPoint,TransitionPoint> linkedTpoints = new HashMap<>(); 
	boolean listenersHasBeenSet; 
	static boolean pressed;
	public  TransitionPoint caseIdroppedMouseOn;   
	double zoom = 0.25; 
	PosType posType;
	public static int caseHeight=100;
	private ArrayList<Case> selectedLabels = new ArrayList<>();
	private int clikX;private Point oldMouseLocation=new Point(0,0);
	//##################################################################
	private int clikY;
	private java.awt.Point clickedCenter;
	
    public SceneTpointLinker( ) {   
    	super();
        allTheCases = new ArrayList<>();
        Utils.setSize(this, 10000, 10000);
        caseHeight = (int) ( Scene.getScenes().iterator().next().bgImage().getHeight() ); 
        
        
        
       if(Editor.getOrderedTpointsGrid().size() == 0) { //1st time i open the link editor
        	int nbr =  0; int xx = 0 ; int yy = 0 ;//create the first location for components
        	for (Scene scene : Scene.getScenes())  {  
        		nbr++;
        		allTheCases.add(new Case(scene, new Point(xx,yy),this,zoom)); 
        		xx+=caseHeight;
        	 if(nbr > 5) {yy+=caseHeight; xx=0;nbr = 0 ;}
        	}
        	 
    		   
        	Editor.saveOrderOfTpointsGrid(allTheCases); //save positions
        	 
    	}else { //already opened the link editor, put all at the last place
    		for (DuoArray<String, String> a : Editor.getOrderedTpointsGrid()) {
    		    try {
					int scen = Integer.parseInt(a.getKey());
					String pos = a.getValue();
					int xx = Integer.parseInt(pos.split("\\.")[0]);
					int yy = Integer.parseInt(pos.split("\\.")[1]); 
					allTheCases.add(new Case(Scene.get(scen), new Point(xx, yy), this, zoom));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
    		}
    	}
        setLayout(null);
        populateThumbnailPanel(); 
        repaint(); 
    } 
	//##################################################################
    private void populateThumbnailPanel() { 
    	 
    	removeAll(); 
    	
        for (Case littleLabel : allTheCases) { 
        	littleLabel.setZoom(zoom);
        	 
        	littleLabel.setBounds((int)(littleLabel.point.x*zoom),(int)( littleLabel.point.y*zoom),littleLabel.resizedImage.getWidth(),littleLabel.resizedImage.getHeight());
        	littleLabel.label.setBounds((int)(littleLabel.point.x*zoom),(int)( littleLabel.point.y*zoom-80*zoom),littleLabel.resizedImage.getWidth(),(int) (80*zoom));
        	add(littleLabel);
        	add(littleLabel.label);
            if(!listenersHasBeenSet) {
            	littleLabel.addMouseWheelListener(new MouseWheelListener() { 
            		

					@Override public void mouseWheelMoved(MouseWheelEvent e) { 
            			  
            			((SceneTpointLinker)e.getComponent().getParent()).mouseWheelMoved(e);  } 
            		 
            		  }); 
            	
            	littleLabel.addMouseMotionListener(new MouseAdapter() { 
            		@Override public void mouseMoved(MouseEvent e) { 
            			((SceneTpointLinker)e.getComponent().getParent()).mouseMoved(e);  } 
            		@Override public void mouseDragged(MouseEvent e) { mouseMoved(e); } }); 
            	littleLabel.addMouseListener(new MouseAdapter() {
            		@Override public void mousePressed(MouseEvent e) { 
            			((SceneTpointLinker)e.getComponent().getParent()).mousePressed(e); }  
            		@Override public void mouseMoved(MouseEvent e) { 
            			((SceneTpointLinker)e.getComponent().getParent()).mouseMoved(e); } 
            		@Override public void mouseReleased(MouseEvent e) { 
            			 MouseEvent convertMouseEvent = SwingUtilities.convertMouseEvent(e.getComponent(), e, getParent());
                         getParent().dispatchEvent(convertMouseEvent);
            			((SceneTpointLinker)e.getComponent().getParent()).mouseReleased(e); 
                } });} 
        }listenersHasBeenSet=true;
         
        revalidate();
        repaint();
    	 
 	    try {
			Main.videoScroll.getViewport().setViewPosition(new java.awt.Point((int) (oldMouseLocation.x*zoom-200), (int)(oldMouseLocation.y*zoom-200)));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
     
    
 
    
  //######################################################################### 
    public static void addScenePosition(int id) { //if i create a new scene, i need to give it a position in grid
    	 
				Editor.recordLinkerPlacement(id, 0,0); //go to top left
			
    } 
  //##################################################################
    @Override protected void paintComponent(Graphics g) {  
    	super.paintComponent(g);	

         if(zoom>0.20 && zoom<0.50) {
     		for (Entry<TransitionPoint, TransitionPoint> tp :  SceneTpointLinker.linkedTpoints.entrySet()) { 
     			 
 					TransitionPoint pt1 = tp.getKey();if(!SceneTpointLinker.locationsOfTpoints.containsKey(pt1))continue; 
 					TransitionPoint pt2 = tp.getValue(); if(!SceneTpointLinker.locationsOfTpoints.containsKey(pt2))continue;
 					Point p1 = SceneTpointLinker.locationsOfTpoints.get(pt1); Point p2 = SceneTpointLinker.locationsOfTpoints.get(pt2);
 					
 					g.drawLine(p1.x, p1.y, p2.x, p2.y); 
 					new Zone(p1,p2,ZoneType.LINE).draw(true, g,Color.GREEN); //draw all the recorded link lines
 				 
     		}  
     		 
     	
         }
        
         if (isClicking()) {
             Graphics2D g2d = (Graphics2D) g;
             g2d.setColor(new Color(0, 0, 255, 50)); // draw selection rectangle 
             int x = Math.min(getClickedX(), getActualX());
             int y = Math.min(getClickedY(), getActualY());
             int width = Math.abs(getClickedX() - getActualX());
             int height = Math.abs(getClickedY() - getActualY());
             g2d.fill(new Rectangle(x, y, width, height)); 
             repaint();
         }
         
         
   }
  //##################################################################
    @Override public void mousePressed(MouseEvent e) {Component comp = getComponent();
    if(e.getButton()==2) {
    	 clickedCenter = MouseInfo.getPointerInfo().getLocation();
  	    SwingUtilities.convertPointFromScreen(clickedCenter, this);//zoom on mouse pos
return;
    }
    	java.awt.Point location = MouseInfo.getPointerInfo().getLocation(); 
	    SwingUtilities.convertPointFromScreen(location, this); 
	    clikX = location.x;//for lines
	    clikY=location.y;
    	if(comp instanceof Case) { Case label = ((Case) comp);
    		TransitionPoint rect = label.getRectangle(e.getX(),e.getY());
    		if(rect == null ) {//click on a case
    			isMovingCase = true;
    	 		caseIpressedFirst = label; 
    	 		selectedLabels.add(label);
    	 		remove(caseIpressedFirst); 
                add(caseIpressedFirst);
    	 	}
    		
    		else { //clic on a rect
    			if(zoom<0.50) {
    				TransitionPoint linked = CrossSceneLinkManager.getLinkedPoint(rect);
    				if(linked != null) {//cliked rect is linked
    					CrossSceneLinkManager.deleteCrossSceneLink(linked, rect);
    					tpIpressedFirst = linked;  
    					Point point = locationsOfTpoints.get(linked);
    				 	clikX = point.x;
    				 	clikY=point.y; 
    				}
    				else //cliked rect isnt linked
    					tpIpressedFirst = rect; 
    				pressed=true; 
    			}
    			else { //zoomed in ? lets set the point were player arrive
    				label.handleMousePressed(e,rect);
    				
    			}
    		} 
    	}
    	repaint();
    	super.mousePressed(e);
    }
  //##################################################################
    @Override public void mouseMoved(MouseEvent e) { repaint(); 
    if(  clickedCenter != null) {
   	 java.awt.Point newMouseLocation = MouseInfo.getPointerInfo().getLocation();
	    SwingUtilities.convertPointFromScreen(newMouseLocation, this);//centerclic
   	    int dax = newMouseLocation.x-clickedCenter.x;
		int day = newMouseLocation.y-clickedCenter.y; 
	    Main.videoScroll.getViewport().setViewPosition(
	    		new java.awt.Point( Main.videoScroll.getViewport().getViewPosition().x-dax,Main.videoScroll.getViewport().getViewPosition().y-day));
 return;
   }
    java.awt.Point mouseLocation = MouseInfo.getPointerInfo().getLocation();
    SwingUtilities.convertPointFromScreen(mouseLocation, this);//zoom on mouse pos
    oldMouseLocation = new Point ((int)(mouseLocation.x/zoom),(int)(mouseLocation.y/zoom)); 
     
    	java.awt.Point top = SwingUtilities.convertPoint(e.getComponent(), e.getX(),e.getY(), this); 
    	Component comp = getComponent(); // get the component at the current mouse position
    	if (isMovingCase) { 
    		int theOldX = caseIpressedFirst.getBounds().x;
    		int theOldY = caseIpressedFirst.getBounds().y;
    		java.awt.Point newLocation = new java.awt.Point(top.x - caseIpressedFirst.getWidth() / 2,
                    top.y - caseIpressedFirst.getHeight() / 2);
    		caseIpressedFirst.setLocation(newLocation);
    		caseIpressedFirst.label.setLocation(new java.awt.Point(top.x - caseIpressedFirst.getWidth() / 2,
                    (int) (top.y - caseIpressedFirst.getHeight() / 2-80*zoom)));
    		
    		int theNewX = caseIpressedFirst.getBounds().x;
    		int theNewY = caseIpressedFirst.getBounds().y;
    		caseIpressedFirst.point = new Point(newLocation.x,newLocation.y);
    		
    		for(Case a : selectedLabels) {if(a.equals(caseIpressedFirst))continue;
    			java.awt.Point l = new java.awt.Point(a.getBounds().x+(theNewX-theOldX),
    					(int) (a.getBounds().y+(+theNewY-theOldY)-80*zoom));
        		a.setLocation(l);
        		a.label.setLocation(l);
        		a.point = new Point(l.x,l.y);
    		}
    		
    		
    		
    	}
    	if(comp instanceof Case) { Case label = ((Case) comp);  
    		label.handleMouseMoved(e);
    	}
    	 
    	if(pressed) {
			new Zone( clikX , clikY ,  top.x ,  top.y ,ZoneType.LINE).draw(true, getGraphics(),Color.GREEN);
			repaint();
    	}
    	super.mouseMoved(e);
    }
  //##################################################################
    @Override public void mouseReleased(MouseEvent e) {Component comp = getComponent(); 
    if(e.getButton()==2 && clickedCenter != null) {
    	 java.awt.Point newMouseLocation = MouseInfo.getPointerInfo().getLocation();
 	    SwingUtilities.convertPointFromScreen(newMouseLocation, this);//centerclic
    	    int dax = newMouseLocation.x-clickedCenter.x;
		int day = newMouseLocation.y-clickedCenter.y; 
 	    Main.videoScroll.getViewport().setViewPosition(
 	    		new java.awt.Point( Main.videoScroll.getViewport().getViewPosition().x-dax,Main.videoScroll.getViewport().getViewPosition().y-day));

    	clickedCenter = null;return;
    }

    if (!isMovingCase) {  
    	int x = Math.min(getClickedX(), getActualX());
        int y = Math.min(getClickedY(),  getActualY());
        int width = Math.abs(getClickedX() - getActualX());
        int height = Math.abs(getClickedY() -  getActualY());
        Rectangle selectionRectangle = new Rectangle(x, y, width, height) ;
        for(Case a : selectedLabels)
        	a.setBorder(null);
         selectedLabels.clear();

         for (Component component : getComponents()) {
             if (component instanceof Case) {
                 Case label = (Case) component;
                 Rectangle labelBounds = label.getBounds();
                 if (selectionRectangle.intersects(labelBounds)) { 
                     selectedLabels.add(label);
                     label.setBorder(BorderFactory.createLineBorder(Color.YELLOW,10));
                 }
             }
         }

         repaint(); 
    }
    	if(comp instanceof Case) { Case label = ((Case) comp);  
    	    if (isMovingCase) {  
    	    	for (Case a : selectedLabels) {
    	    	    a.setBorder(null);
    	    	    int zoomedX = (int) (a.getLocation().x / zoom);
    	    	    int zoomedY = (int) (a.getLocation().y / zoom);
    	    	    Utils.log("record "+zoomedX+" "+zoomedY );
    	    	    Editor.recordLinkerPlacement(a.scene.id, zoomedX, zoomedY);
    	    	    a.point= new Point((int)(a.getBounds().x/zoom),(int)(a.getBounds().y/zoom));
    	    	}
        			
    		
        		selectedLabels.clear();
    	        isMovingCase = false;  
    	    }
    	    java.awt.Point converted = SwingUtilities.convertPoint(e.getComponent(), e.getX(),e.getY(), label);
    	    TransitionPoint rect = label.getRectangle(converted.x,converted.y);
    		if(rect != null && tpIpressedFirst !=null) {
    	    	caseIdroppedMouseOn = rect;
    	    	label.handleMouseReleased(e);
    	    }
    		if(rect == null && tpIpressedFirst !=null) {
    			TransitionPoint linked = CrossSceneLinkManager.getLinkedPoint(tpIpressedFirst);
    			if(linked != null)  
    				CrossSceneLinkManager.deleteCrossSceneLink(linked, tpIpressedFirst);
    			else
    				CrossSceneLinkManager.deleteCrossSceneLink(tpIpressedFirst, tpIpressedFirst);
    			tpIpressedFirst = null; 
    		}
    		if(zoom>0.50)label.handleMouseReleased(e);
    	}
    	    
    	pressed = false; 
    	repaint();
    	 super.mouseReleased(e);
    }
  //##################################################################
    @Override public void mouseWheelMoved(MouseWheelEvent e) {  
    	double old = zoom; 
    	zoom += ((e.getWheelRotation() > 0) ? -0.02 : 0.02);
		if(zoom>1)zoom = 1;
		if(zoom<0)zoom = 0;
		if(old != zoom) {
			  
			populateThumbnailPanel();
		}
		
	  }
  //##################################################################
    private Component getComponent() {
    	java.awt.Point location = MouseInfo.getPointerInfo().getLocation();
    	 
	    SwingUtilities.convertPointFromScreen(location, this); 
	    
	    return findComponentAt(location.x, location.y); 
    }
	@Override
	protected boolean handleImgDragged() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void updateLeftEditor() {
		// TODO Auto-generated method stub
		
	}

 
  
 
}





































//##################################################################
//##################################################################
//##################################################################
class Case extends JLabel { private static final long serialVersionUID = 1L;
 
	final Scene scene;   BufferedImage resizedImage; RosellaZone rosellaImMoving=null;
	private SceneTpointLinker frame; 
	private double zoom;
int actualX, actualY,clickedX,clickedY,oldx,oldy; 
	private PosType posType = PosType.BOT_LEFT; 
	List<TransitionPoint> allTpoints;
	Point point; public JTextArea label=new JTextArea();
	//#################################################################
    public Case(Scene s,  Point point, SceneTpointLinker pane,double zoom) {
        super( ); this.zoom=zoom;
        scene = s; 
        label.setText(s.title());
        frame = pane;
        this.point= point; 
         
        resizedImage =  ImgUtils.scal(scene.bgImage() ,zoom);  
       // Utils.setSize(this, resizedImage.getWidth(null), resizedImage.getHeight(null));
        
        allTpoints = new ArrayList<>();
        for(Zone a : TransitionPoint.getByOwner(scene.id)) 
        	allTpoints.add((TransitionPoint) a);
        
        for (TransitionPoint rect : allTpoints) { 
        	TransitionPoint dest = CrossSceneLinkManager.getLinkedPoint(rect); 
        	if(dest != null //link already recorded in base ?
        			&& dest.getDestinationScene()!=-1 
        			&& rect.getDestinationScene()!=-1
        			&& ! SceneTpointLinker.linkedTpoints.containsKey(rect) ) {
        		SceneTpointLinker.linkedTpoints.put(dest, rect); 
     	        SceneTpointLinker.linkedTpoints.put(rect,dest ); 
        	} 
        } 
    }
  //#################################################################
  public void handleMouseMoved(MouseEvent e) {
	  actualX=e.getX();actualY=e.getY();
	}
//#################################################################
    public void handleMousePressed(MouseEvent e, TransitionPoint rect) { 
    	clickedX = e.getX();clickedY=e.getY();oldx = e.getX();
		oldy = e.getY();
    	  
    	 
	}
	//#################################################################
    public void setZoom(double z) {
    	this.zoom=z;      
         resizedImage =  ImgUtils.scal(scene.bgImage() ,zoom);  

	}
    
    //#################################################################
    public void handleMouseReleased(MouseEvent e) {   
    	TransitionPoint rect = frame.caseIdroppedMouseOn;
    	 if(  SceneTpointLinker.tpIpressedFirst != null && !SceneTpointLinker.tpIpressedFirst.equals(rect)) {  
	        CrossSceneLinkManager.addCrossSceneLink(SceneTpointLinker.tpIpressedFirst, rect);
	        SceneTpointLinker.linkedTpoints.put(SceneTpointLinker.tpIpressedFirst, rect);
	        SceneTpointLinker.linkedTpoints.put(rect,SceneTpointLinker.tpIpressedFirst );  
	        SceneTpointLinker.tpIpressedFirst = null;
	    }  
	}
 
	//#################################################################
    public TransitionPoint getRectangle(int mouseX,int mouseY) { 
    	if(zoom<0.25)return null;//too little act like i clicked a case event if i clic rectangle
        for (TransitionPoint rect :allTpoints) {
            int x1 = (int) ((rect.getP1().x) * zoom );
            int y1 = (int) ((rect.getP1().y) * zoom );
            int x2 = (int) ((rect.getP2().x) * zoom );
            int y2 = (int) ((rect.getP2().y) * zoom );

            int minX = Math.min(x1, x2);
            int minY = Math.min(y1, y2);
            int maxX = Math.max(x1, x2);
            int maxY = Math.max(y1, y2);

            if (mouseX >= minX && mouseX <= maxX && mouseY >= minY && mouseY <= maxY) 
            	return rect; 
             
        }
        return null;
    }
    //##################################################################
    @Override protected void paintComponent(Graphics g) {
    	  
        g.setColor(Color.blue);   
        g.drawImage(resizedImage, 0, 0, null);//draw scene background
         
        for (TransitionPoint rect : allTpoints) { //draw all rectangles 
            int x1 = (int)( rect.getP1().x * zoom );
            int y1 = (int)( rect.getP1().y * zoom );
            int x2 = (int)( rect.getP2().x * zoom );
            int y2 = (int)( rect.getP2().y * zoom ); 
            int width = Math.abs(x2 - x1);
            int height = Math.abs(y2 - y1); 
            int x = Math.min(x1, x2);
            int y = Math.min(y1, y2); 
            if(zoom >=0.20 )g.drawRect(x, y, width, height); 
            
             java.awt.Point convertedCenter = SwingUtilities.convertPoint(this, //convert to general java frame
                    new java.awt.Point(x + width / 2, y + height / 2), getParent());
 
            SceneTpointLinker.locationsOfTpoints.put( rect,  new Point( convertedCenter.x   , convertedCenter.y   )  );
           
            
           
       
        if(zoom>0.20 ) {
    		for (Entry<TransitionPoint, TransitionPoint> tp :  SceneTpointLinker.linkedTpoints.entrySet()) { 
    			 
					TransitionPoint pt1 = tp.getKey();if(!SceneTpointLinker.locationsOfTpoints.containsKey(pt1))continue; 
					TransitionPoint pt2 = tp.getValue(); if(!SceneTpointLinker.locationsOfTpoints.containsKey(pt2))continue;
					Point p1 = SceneTpointLinker.locationsOfTpoints.get(pt1); Point p2 = SceneTpointLinker.locationsOfTpoints.get(pt2);
					  java.awt.Point po1 = SwingUtilities.convertPoint(frame, //convert to general java frame
			                    new java.awt.Point(p1.x,p1.y), this);
					  java.awt.Point po2 = SwingUtilities.convertPoint(frame, //convert to general java frame
			                    new java.awt.Point(p2.x,p2.y), this);
			 
					new Zone(po1.x, po1.y, po2.x, po2.y,ZoneType.LINE).draw(true, g,Color.GREEN); 
				 
    		}  
    		 
    	
        }}
         
    }
   
}

//##################################################################
//##################################################################
//##################################################################
class CrossSceneLinkManager { 
    public static void deleteCrossSceneLink(TransitionPoint point1, TransitionPoint point2) {
    	SceneTpointLinker.linkedTpoints.remove(point1);
		SceneTpointLinker.linkedTpoints.remove(point2);
		 
		point1.setDestinationId(-1);
		point1.setDestinationScene(-1); //set destinations to no one (-1)
		point2.setDestinationId(-1);
		point2.setDestinationScene(-1);
		point1.update(point1.toSqlValues());
		point2.update(point2.toSqlValues());
		
    }
    //##################################################################
    public static void addCrossSceneLink(TransitionPoint point1, TransitionPoint point2) {
    	if(SceneTpointLinker.linkedTpoints.containsKey(point1)||SceneTpointLinker.linkedTpoints.containsKey(point2)) 
    		return; //we clicked on a already linked tpoint ? do nothing
    		  
		point1.setDestinationScene(point2.getSourceScene());  //modify
		point1.setDestinationId(point2.getSourceId()); 
		point2.setDestinationScene(point1.getSourceScene());
		point2.setDestinationId(point1.getSourceId()); 

		point1.update(point1.toSqlValues());
		point2.update(point2.toSqlValues());
    }
    
    //##################################################################
    public static TransitionPoint getLinkedPoint(TransitionPoint pointFrom) {  
			   
			 List<TransitionPoint> allTpoints = new ArrayList<>();
		        for(Zone a : TransitionPoint.getByOwner(pointFrom.getDestinationScene())) 
		        	allTpoints.add((TransitionPoint) a);
			 
				for(TransitionPoint pointInDestination : allTpoints)  
					if(pointInDestination.getSourceId() == pointFrom.getDestinationId())
						return pointInDestination;
					 
        return null;
    }
     
}