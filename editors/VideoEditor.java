package me.nina.editors;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import me.nina.gameobjects.Action;
import me.nina.gameobjects.Animation;
import me.nina.gameobjects.Animation.AnimationState;
import me.nina.gameobjects.Character;
import me.nina.gameobjects.Scene;
import me.nina.gameobjects.SpawnPoint;
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.gameobjects.Character.PosType;
import me.nina.gameobjects.CharacterState;
import me.nina.gameobjects.MegaAction;
import me.nina.gameobjects.Message;
import me.nina.maker.Ashe;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Cbox;
import me.nina.maker.Main;
import me.nina.maker.Utils;
import me.nina.objects2d.ImgUtils;
import me.nina.objects2d.Point;
import me.nina.sqlmanipulation.SQLTable;

public class VideoEditor extends Editor { private static final long serialVersionUID = 1L; 
	public Cbox<Scene> sceneSelector ;  
	private Cbox<Action> actionSelector; 
	static LinkedList<Step> stepList=new LinkedList<Step>(); 
	int actualStep = -1; 
	static Map<Character,Zone> startPoints = new HashMap<Character,Zone>();
	static Map<Character,CharacterState> statesCreated = new HashMap<Character,CharacterState>();
	boolean changePosFrom,changePosTo,isSettingPoint = false;
	private int startX,startY;
	Map<Character, Map<String, JMenu>> characterMessageSubMenus;
	private MegaAction megaAction;
	static Map<String, Point> characterPositions = new HashMap<>();
	//#################################################################################### 

	public VideoEditor( ) { 
		super();
		 
		for(MegaAction a:MegaAction.getAll())
			if(a.title().equals("generated"))
				megaAction=a;
		if(megaAction==null)
			megaAction =  new MegaAction("generated").save();
		actualStep = -1;startPoints.clear();stepList.clear();
		sceneSelector = but.createCbox(Scene.getScenes(),  but, 150, 20, (ActionListener)e-> {  
				setImage(sceneSelector.getSelectedItem().bgImage());
				revalidate();
				repaint();
				 
		} ); 
		actionSelector = but.createCbox(Action.getAll(),  but, 150, 20, (ActionListener)e-> {  
				actionSelector.getSelectedItem().populateRight();
				 
		} );
 		 characterMessageSubMenus = new HashMap<>();
 /*for(String sid : Message.getSids())
		for (Message msg : Message.getAlls()) { 
			if(msg.sierraId() == Message.getSelected(sid))
		    for (Character character : Character.getAll()) { 
		        if (msg.talker()==character.talker() ) { 
		            Map<String, JMenu> messageSubMenus = characterMessageSubMenus.get(character);
		            if (messageSubMenus == null) {
		                messageSubMenus = new HashMap<>();
		                characterMessageSubMenus.put(character, messageSubMenus);
		            }
		             
		            JMenu messageSubMenu = messageSubMenus.get(sid);
		            if (messageSubMenu == null) {
		                messageSubMenu = new JMenu(sid);
		                messageSubMenus.put(sid, messageSubMenu);
		            }
		             
		            JMenuItem msgItem = new JMenuItem(msg.title()); 
		            msgItem.addActionListener((ActionListener) e -> { 
	                       if(actualStep == -1 )  actualStep=0;
	                       else actualStep+=1;
	                       Step st = new StepMessage(actualStep,sceneSelector.getSelectedItem(), character,msg); 
	                       stepList.add(st); 
	                });
		            messageSubMenu.add(msgItem);
		        }
		    } 
		}*/
 
		
	}
	//###############################################################
	public void updateLeftEditor() { }
	//#################################################################################### 
	@Override protected boolean handleImgDragged() {  
		Character character = Utils.askForCharacter("charact ?");
		if(!statesCreated.containsKey(character)) {
			Map<String,String> val = new HashMap<>(); 
			String title = character.title()+"_"+Main.getUserInput("statename")+"_state";
			val.put("title", title); 
			val.put("possessor", character.id+""); 
		  
			SQLTable tabl = SQLTable.getTable("characterstate");
			int id = tabl.insert(val); 
			statesCreated.put(character, CharacterState.get(id));
		}
		else {boolean full = true;
			for(PosType p : PosType.values())if(p!=PosType.UNUSED)
				if(statesCreated.get(character).movingAnimation(p)==null)
					full=false;
			if(full) {statesCreated.remove(character);return true;}
		}
		File file = Main.fileDisplayer.getSelected();
		String fileName = file.getName();
		Utils.copy(file, new File(statesCreated.get(character).getFolder() + fileName));
		
		for(PosType p : PosType.values())if(p!=PosType.UNUSED)
			if(statesCreated.get(character).movingAnimation(p)==null) {
				statesCreated.get(character).updateMovingAnimation(
						Animation.get(Animation.generateAnimation(statesCreated.get(character), 
								"moving"+p, AnimationState.INFINITE_LOOP.getValeur(), fileName)), p);
		return true;}return true; }
	//#################################################################################### 
	@Override protected void paintComponent(Graphics g) {
		super.paintComponent(g);  
		if(image!=null)
			g.drawImage(image, 0, 0, null); 
		if(stepList.size()!=0) {
			getStep().draw(g);
		List<Character>used=new ArrayList<Character>();
		j:for(Character chare : startPoints.keySet()) {
			if(!chare.equals(getStep().chare)&&!used.contains(chare)) { 
				used.add(chare);
				for(int i = actualStep ; i >=0 ; i--) {
					Step step = stepList.get(i);
					if(!(step instanceof StepMessage)&&step.chare.equals(chare)) {
						step.draw(g);
						continue j;
					}
				}
			}
		}
		
		
		
		
		if(getStep() instanceof StepMessage)
			 for(int i = actualStep ; i >=0 ; i--) {
						Step step = stepList.get(i);
						if(step.chare.equals(getStep().chare) && !(step instanceof StepMessage)) {
							step.draw(g);
							 
						}
				 
			}
		
		
		
		
		
		}
		
		
		for(Zone z : SpawnPoint.getByOwner(sceneSelector.getSelectedItem().id) ){
			SpawnPoint item = (SpawnPoint) z;  
			item .draw(false, g,Color.GRAY);
			new Zone(item.mid().x-10,item.mid().y-10,item.mid().x+10,item.mid().y+10,ZoneType.RECTANGLE).draw(g);
				
			 
		}
		repaint();
	} 
	//#################################################################################### 
	
	private void showMenu(int x, int y) {
		JPopupMenu popupMenu = new JPopupMenu(); 
        JMenu characterMenu = new JMenu("new step");
        
        for (Character character : Character.getAll()) { 
        	JMenu subMenu = new JMenu("messages");
        	JMenuItem setStartPointItem = new JMenuItem("Set Start Point");
            setStartPointItem. addActionListener((ActionListener) e->{ 
            	Point p = new Point(x,y);
        		Zone found = Utils.getZoneFromPoint(p, SpawnPoint.getByOwner(sceneSelector.getSelectedItem().id), 10); 
            	if(found!=null && Main.getUserConfirmation("use zone "+found.title()+" ?"))
            		startPoints.put(character, found); 
            	else if(Main.getUserConfirmation("create spawnPoint here ?")){
            		SpawnPoint spawn = new SpawnPoint(x-5,y-5,x+5,y+5,ZoneType.UNFILLED_RECT);
            		int id = spawn.save(sceneSelector.getSelectedItem().id, "point "+character.title());
            		startPoints.put(character, SpawnPoint.get(id));  
            	}
            }); 
            JMenu characterSubMenu = new JMenu(character.title());
            List<CharacterState> states = CharacterState.getByPossessor(character.id); 
            JMenu secondColumnSubMenu = new JMenu(); JMenu thirdColumnSubMenu = new JMenu();
            characterSubMenu.add(setStartPointItem); 
            
            
            
            
            
            
            
            
            
            
            
            
            try { 
                Map<String, JMenu> messageSubMenusMap = characterMessageSubMenus.get(character);  
                JMenu secondColumn = new JMenu();
                JMenu thirdColumn = new JMenu(); 
                int count = 0;
                for (Map.Entry<String, JMenu> entry : messageSubMenusMap.entrySet()) {
                    if (count <= 20) {
                        subMenu.add(entry.getValue());
                    } else if (count >20 && count <=40) {
                        secondColumn.add(entry.getValue());
                    } else if (count >40) {
                        thirdColumn.add(entry.getValue());
                    }
                    count++;
                } 
                 
                 subMenu.add(secondColumn);
            subMenu.add(thirdColumn);
            } catch (Exception e1) { }
            
            
            
            characterSubMenu.add(subMenu);
            
            
            
            
            
            
            
            
            
            
            
            
            int i = 0 ;
            for (CharacterState state : states) { 
                JMenuItem stateMenuItem = new JMenuItem(state.title());
                stateMenuItem.  addActionListener((ActionListener) e->{ 
                       PosType pos = Utils.askForCharacterPosType(state);
                       Character chare = (Character) state.possessorObj();
                       Animation anim = state.movingAnimation(pos);
                       Step st = null;
                       if(!Main.getUserConfirmation("be in state ?" )) {
                    	   boolean inside = Main.getUserConfirmation("inside this state ? "); 
                    	   if(actualStep == -1 )  actualStep=0;
                    	   else actualStep+=1;
                    	   st = new StepFreeze(actualStep, pos,chare,anim,inside,state,sceneSelector.getSelectedItem()); 
                    	   
                       }else {
                    	   if(Main.getUserConfirmation("idle reversed ?"))
                    		   anim.setReversed(true);
                    	   st = new StepAnim(actualStep, pos,chare,anim,state,sceneSelector.getSelectedItem()); 
                    	   
                       }
                       if(Main.getUserConfirmation("insert before actual position ? else, the end of list"))
                    	   stepList.add(actualStep,st);  
                       else
                    	   stepList.add(st);  
                }); 
                if (i<=20) characterSubMenu.add(stateMenuItem);
                else if (i >20&&i<=40) secondColumnSubMenu.add(stateMenuItem);
                else if(i>40) thirdColumnSubMenu.add(stateMenuItem); 
           i++; } 
            characterSubMenu.add(secondColumnSubMenu);
            characterSubMenu.add(thirdColumnSubMenu); 
            characterMenu.add(characterSubMenu);
        }
        popupMenu.add(characterMenu);
        

        popupMenu.addSeparator();
        
        if(actualStep != -1 && getStep() instanceof StepFreeze) {
        	JMenuItem setPosFrom = new JMenuItem("change offset from");
        	setPosFrom.addActionListener((ActionListener) e->{ changePosFrom=true; startX=-1; getStepFreeze().anim.rewindAll(); });
        
        	JMenuItem setPosTo = new JMenuItem("change offset to");
        	setPosTo.addActionListener((ActionListener) e->{
                changePosTo=true;
                startX=getStepFreeze().anim.posFrom().x;
                startY=getStepFreeze().anim.posFrom().y;
                getStepFreeze().anim.forwardAll();
             
        	});
        	popupMenu.add(setPosFrom);
        	popupMenu.add(setPosTo);
        }
          
        
        
        JMenuItem delStep = new JMenuItem("delete step");
        delStep.addActionListener((ActionListener) e->{
           stepList.remove(actualStep);
    	});
    	popupMenu.add(delStep);
        
        
        
        
        
        
        JMenuItem toAction = new JMenuItem("to action");
        toAction.addActionListener((ActionListener) e->{
           Action action = new Action(Main.getUserInput("title?")).save();
           action.updateMegaOwner(megaAction.id);
           
           for(Character chare : startPoints.keySet()) {
        	   action.addAshToSet(new Ashe(chare.id ,AshType.WILL_BE_SPAWNED,1));
        	   action.addAshToSet(new Ashe(chare.id ,AshType.IS_THE_ACTOR,1));
        	   action.addAshToSet(new Ashe(startPoints.get(chare).id ,AshType.WILL_BE_IN_POSITION,1));
           }
         for(Step step : stepList)
        	 step.populateAction(action);
    	});
    	popupMenu.add(toAction);
         
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	 JMenuItem fromAction = new JMenuItem("from selected action");
    	 fromAction.addActionListener((ActionListener) e->{
            Action action =this.actionSelector.getSelectedItem();
            
            action.updateMegaOwner(megaAction.id);
            startPoints.clear();stepList.clear();
            Character temp=null;Scene sc = null;
            PosType pos = null;int i = 0 ;
            for(Ashe toSet : action.ashToSet()) {
            	if(toSet.type() == AshType.IS_THE_ACTOR)
            		temp=(Character) toSet.possessor();
            	if(toSet.type() == AshType.WILL_BE_IN_POSITION) {
            		startPoints.put(temp, (Zone) toSet.possessor());
            		sc = (Scene) toSet.possessor().possessorObj();
            	}
            	if(toSet.type() == AshType.WILL_LOOK_BOT_LEFT) pos =PosType.BOT_LEFT;
        		if(toSet.type() == AshType.WILL_LOOK_BOTTOM  )pos  =PosType.BOTTOM;
        		if(toSet.type() == AshType.WILL_LOOK_TOP )pos =PosType.TOP;
        		if(toSet.type() == AshType.WILL_LOOK_LEFT )pos =PosType. LEFT;
        		if(toSet.type() == AshType.WILL_LOOK_RIGHT )pos =PosType.RIGHT;
        		if(toSet.type() == AshType.WILL_LOOK_BOT_RIGHT )pos =PosType.BOT_RIGHT;
        		if(toSet.type() == AshType.WILL_LOOK_TOP_LEFT )pos =PosType.TOP_LEFT;
        		if(toSet.type() == AshType.WILL_LOOK_TOP_RIGHT )pos =PosType.TOP_RIGHT;
        		
        		if(toSet.type() == AshType.WILL_BE_IN_STATE) {
        			stepList.add(
        					new StepAnim(i, 
        							pos,
        							(Character) toSet.possessor().possessorObj(),
        							((CharacterState)toSet.possessor()).idleAnimation(pos), 
        							(CharacterState)toSet.possessor(), sc  ) );
        			i++;actualStep=0;
        		}
        		if(toSet.type() == AshType.WILL_INSIDE_THIS_STATE) {
        			stepList.add(
        					new StepFreeze(i, 
        							pos,
        							(Character) toSet.possessor().possessorObj(),
        							((CharacterState)toSet.possessor()).movingAnimation(pos), 
        							true,
        							(CharacterState)toSet.possessor(), sc  ) );
        			i++;actualStep=0;
        		}
        		if(toSet.type() == AshType.WILL_OUTSIDE_THIS_STATE) {
        			stepList.add(
        					new StepFreeze(i, 
        							pos,
        							(Character) toSet.possessor().possessorObj(),
        							((CharacterState)toSet.possessor()).movingAnimation(pos), 
        							false,
        							(CharacterState)toSet.possessor(), sc  ) );
        			i++;actualStep=0;
        		}
        		if(toSet.type() == AshType.WILL_SAY) {
        			Character whoSay = null;
        			for(Character c : Character.getAll())
        				if(c.talker() == ((Message)toSet.possessor()).talker())
        					whoSay=c;
        			stepList.add(
        					new StepMessage(i, sc , whoSay, (Message)toSet.possessor()));
        		}
            }
            if(temp == null)Main.getUserConfirmation("dont forget to place the actors!");
             
            
           
     	});
      	popupMenu.add(toAction);
     	popupMenu.add(fromAction);
     	
     	
     	
     	
     	
     	
        popupMenu.addSeparator();
         
        int i = 0 ;
        
        for (Step step : stepList) {
            JMenuItem stepMenuItem = new JMenuItem("step "+i);final int j = i;
            stepMenuItem.addActionListener((ActionListener)e->{ 
                   this.actualStep = j ;
                    
                   step.replay();
                		  
            });
            popupMenu.add(stepMenuItem);
            i++;
        } 
        popupMenu.show(this, x,y); 
	}
	//#################################################################################### 
	private StepFreeze getStepFreeze() { return (StepFreeze)stepList.get(actualStep); } 
	private Step getStep () { return  stepList.get(actualStep); } 
	//#################################################################################### 
	@Override public void mousePressed(MouseEvent e) {  
		if(changePosTo) {
			changePosTo=false; 
			int relativeX = (int) ((e.getX()-getStep().pt().x)/(getStepFreeze().scale+getStepFreeze().state.scaleCorrection()));
			int relativeY = (int) ((e.getY()-getStep().pt().y)/(getStepFreeze().scale+getStepFreeze().state.scaleCorrection())); 
			getStepFreeze().anim.updatePosTo( relativeX ,  relativeY );
			characterPositions.clear();
		}
		startX=e.getX();
		startY=e.getY();
	}
	//#################################################################################### 
	@Override public void mouseMoved(MouseEvent e) { 
		if(startX!=-1 && changePosFrom) {
			getStepFreeze().anim.updatePosFrom(0, 0);//no increment please
			getStepFreeze().anim.updatePosFrom(startX-e.getX(), startY-e.getY()); 
			
		}
	}
	//####################################################################################
	@Override public void mouseReleased(MouseEvent ee) { 
		if(changePosFrom) {
			if(Main.getUserConfirmation("align the offset TO ? (no move")) {
				((StepFreeze)getStep()).anim.updatePosTo(0,0);
				((StepFreeze)getStep()).anim.updatePosTo(((StepFreeze)getStep()).anim.posFrom().x, ((StepFreeze)getStep()).anim.posFrom().y);
			}
			changePosFrom=false; 
			characterPositions.clear();
		}
		if(ee.getButton() == 3) 
			showMenu(ee.getX(),ee.getY());
		 
	}

	//#################################################################################### 
	public static Point getPoint(Character character, int stepNumber) {  
	    if (characterPositions.containsKey(character.title()+stepNumber))  
	        return characterPositions.get(character.title()+stepNumber); 
	   
	    Point position = startPoints.get(character).mid();
	     
	    try { for (int i = 0; i <= stepNumber; i++) {
	    	if(i==stepNumber) return position;
	    	if (stepList.get(i).chare.equals(character)) 
	    		position = stepList.get(i).resultingPosition(position); 
	    }
			    
			     
		} catch (Exception e) { Utils.log("steplist dont contain it for the moment");}
	         
	    characterPositions.put(character.title()+stepNumber, position);
	    return position;
	} 

}
//####################################################################################
//####################################################################################
//####################################################################################
class StepFreeze extends StepAnim { 
	 boolean inside;  
	   
	public StepFreeze(int number2,PosType pos2, Character chare2, Animation anim2, boolean inside2, CharacterState state2,Scene scene) {
		super(number2,pos2, chare2, anim2,  state2, scene); 
		inside=inside2; anim.setReversed(!inside); if(!inside)anim.forwardAll(); else anim.rewindAll();  
	} 
	//##################################
	public void replay() {anim.unsetIdle(); if(!inside)anim.forwardAll(); else anim.rewindAll(); }
	//##################################
	
	public void populateAction(Action action) {
		 
		if(inside)action.addAshToSet(new Ashe(state.id,AshType.WILL_INSIDE_THIS_STATE,1));
		else action.addAshToSet(new Ashe(state.id,AshType.WILL_OUTSIDE_THIS_STATE,1));
		super.populateAction(action);
	}
	public Point resultingPosition(Point pt) {
		int offsetX = (int) ((anim.posTo().x - anim.posFrom().x)*(scale+state.scaleCorrection()));
		int offsetY = (int) ((anim.posTo().y - anim.posFrom().y)*(scale+state.scaleCorrection())); 
		Point pos = new Point(pt .x+offsetX,pt .y+offsetY);
		if(anim.isReversed())pos = new Point(pt .x-offsetX,pt .y-offsetY);

	   return pos;
	} 
	
	public void draw(Graphics g) { 
		if(System.currentTimeMillis()-paintTime>100) {  
			paintTime = System.currentTimeMillis();
			if(anim.loopFinished()) anim.setIdleFrame(anim.currentIndex());
			else anim.run(); 
		}
	   super.draw(g);
	}  	
}
//####################################################################################
//####################################################################################
class StepAnim extends Step {
	long paintTime = System.currentTimeMillis(); CharacterState state; PosType pos;  Animation anim; double scale; 
	
	public StepAnim(int number2,PosType pos2, Character chare2, Animation anim2,  CharacterState state2,Scene scene) {
		super(number2,scene,chare2);
		pos=pos2;  chare=chare2;   state=state2;  anim= anim2 ;   
		calculateScale(pt().y);
		  anim.setIdleFrame(0);
		  if(anim.isReversed())
			  anim.forwardAll();
	}
	public void populateAction(Action action) {
		if(pos==PosType.BOT_LEFT) action.addAshToSet(new Ashe(chare.id,AshType.WILL_LOOK_BOT_LEFT,1));
		if(pos==PosType.BOTTOM ) action.addAshToSet(new Ashe(chare.id,AshType.WILL_LOOK_BOTTOM,1));
		if(pos==PosType.TOP) action.addAshToSet(new Ashe(chare.id,AshType.WILL_LOOK_TOP,1));
		if(pos==PosType. LEFT) action.addAshToSet(new Ashe(chare.id,AshType.WILL_LOOK_LEFT,1));
		if(pos==PosType.RIGHT) action.addAshToSet(new Ashe(chare.id,AshType.WILL_LOOK_RIGHT,1));
		if(pos==PosType.BOT_RIGHT) action.addAshToSet(new Ashe(chare.id,AshType.WILL_LOOK_BOT_RIGHT,1));
		if(pos==PosType.TOP_LEFT) action.addAshToSet(new Ashe(chare.id,AshType.WILL_LOOK_TOP_LEFT,1));
		if(pos==PosType.TOP_RIGHT) action.addAshToSet(new Ashe(chare.id,AshType.WILL_LOOK_TOP_RIGHT,1));
		 if(this instanceof StepAnim)action.addAshToSet(new Ashe(state.id,AshType.WILL_BE_IN_STATE,1));
	}
	//##################################
	public void replay() { 
	}
	//##################################
	public void draw(Graphics g) {  
	    int offsetX = (int) (anim.isReversed() ? anim.posTo().x : anim.posFrom().x);
	    int x = (int) (blitX() - offsetX * (scale + state.scaleCorrection())); 
	    int offsetY = (int) (anim.isReversed() ? anim.posTo().y : anim.posFrom().y);
	    int y = (int) (blitY() - offsetY * (scale + state.scaleCorrection()));

		g.drawImage(ImgUtils.scal(anim.getActualFrame(),scale+state.scaleCorrection()),x,y,null); 
		 
	}
	//####################################################################################emulate the game
	public int getScalledH() { return (int) (anim.getActualFrame().getHeight()*(scale+state.scaleCorrection())); }
	public int getScalledW() { return (int) (anim.getActualFrame().getWidth()*(scale+state.scaleCorrection())); } 
	int blitY() { return pt().y - getScalledH(); } 
	int blitX() { return pt().x - getScalledW()/2; } 
	public void calculateScale(int feetY) {   
	    double factor = ((double)actualScene.scalePercents())/100;
	    int HlimiteTop = actualScene.scalefactorTopLimit() ;
	    int HlimiteBot = actualScene.scalefactorBotLimit(); 
	    if (feetY < HlimiteTop) {  scale = factor;   return;  } 
	    else if (feetY > HlimiteBot) { scale = 1.0; return;  } 
	    double m = (1.0 - factor) / (HlimiteBot - HlimiteTop);
	    double b = factor - m * HlimiteTop;  
	    scale = m * feetY + b;
	    
	}
	//##################################

}
//####################################################################################
//####################################################################################
class StepMessage extends Step {  
	private Message msg; 
	
	public StepMessage(int number2, Scene scene, Character chare2,Message msg2) {
		 super(number2,scene,chare2); msg=msg2;
	}
	public void populateAction(Action action) {
		 action.addAshToSet(new Ashe(msg.id,AshType.WILL_SAY,1));
	}
	//##################################
	public void replay() {   }
	//##################################
	public void draw(Graphics g) {  
		g.setColor(Color.YELLOW);
        g.fillRect(pt().x, pt( ) .y, 200,20);
        
        // Dessine le texte sur l'arri�re-plan
        g.setColor(Color.BLACK);
        g.drawString(msg.title(), pt().x, pt( ) .y + 20); 
    } 
	//################################## 
	 
	@Override
    public Point resultingPosition(Point pt) {
        return pt;
    }
}
//####################################################################################
//####################################################################################
class Step { 
	 Scene actualScene;  int number; Character chare; 
	
	public Step(int number2, Scene scene, Character chare2) {
		 number=number2; actualScene = scene;  chare = chare2; 
	}
	public void populateAction(Action action) {
		 
	}
	Point pt() { return VideoEditor.getPoint(chare ,number); }
	//##################################
	public void replay() {  }
	//##################################
	public void draw(Graphics g) {   } 
	//##################################
	public Point resultingPosition(Point pt) {  return pt();  }
	
	
	
}