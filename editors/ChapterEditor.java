package me.nina.editors;

import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import javax.swing.JTextField;

import me.nina.gameobjects.Character.PosType;
import me.nina.gameobjects.Action;
import me.nina.gameobjects.Chapter;
import me.nina.gameobjects.Scene;
import me.nina.maker.Cbox;
import me.nina.maker.Main;
import me.nina.maker.RosellaZone;
import me.nina.objects2d.Position;

public class ChapterEditor extends Editor { private static final long serialVersionUID = 1L;

	public boolean isSettingPoint;

	public Cbox<Scene> sceneSelector ; 
	public Chapter chapter;   
	private JTextField title;
	private JTextField number; 

	private Cbox<Action> actionSelector;
 
	//#################################################################################### 
	
	public ChapterEditor(int id) { 
		super();
		Main.leftController.removeAll();
		this.chapter = Chapter.get(id);
		
		 scene = chapter.startScene();
		 rosella = new RosellaZone(scene); 
		 add(rosella);
		 lab.createLabel("scenestart" , lab,100,20);
		 
			sceneSelector = but.createCbox(Scene.getScenes(),  but, 150, 20, (ActionListener)e-> {   
					chapter.updateStartScene (sceneSelector.getSelectedItem());
					setImage( chapter.startScene().bgImage());
					revalidate();
					repaint();
					 
			} );
			lab.createLabel("first action" , lab,100,20);
			 
			actionSelector = but.createCbox(Action.getAll(),  but, 150, 20, (ActionListener)e-> {   
					chapter.updateFirstAction (actionSelector.getSelectedItem().id);
					actionSelector.getSelectedItem().populateRight();
					 
			} );
			
	 
			
			
			 if(chapter.startScene()!=null)
			sceneSelector.setSelectedItem(chapter.startScene());
			 if(chapter.firstAction()!=null)
			 actionSelector.setSelectedItem(chapter.firstAction());
			 
			lab.createLabel("title" , lab,100,20);
				title = but.createText(chapter.title(),but,150,20,  (ActionListener)e-> {
				chapter.updateTitle(title.getText()); 
			}  );
				 
			lab.createLabel("number" , lab,100,20);
			number = but.createText(chapter.number()+"",but,150,20,(ActionListener)e-> {
				chapter.updateNumber(number.getText()); 
			} );
			lab.createLabel("delete this chapter" , lab,100,20);
			but.createBoutton("clic",but,150,20,(ActionListener)e-> {
				chapter.delete(); 
			} );
			
			lab.createLabel("clic on scene" , lab,100,20);
			but.createLabel("for start position" , but,100,20);
			 
	}
 
	//###############################################################
	public void updateLeftEditor() {
		 
		

 
	}



	//#################################################################################### 
	@Override protected boolean handleImgDragged() { return false; }
	//#################################################################################### 
	@Override protected void paintComponent(Graphics g) {
		super.paintComponent(g); 
		
		if(image!=null)
			g.drawImage(image, 0, 0, null);
		 
		 rosella.paint(g, chapter.positionStart().x, chapter.positionStart().y, PosType.BOTTOM);
		repaint();
	}

	//#################################################################################### 
	@Override public void mouseReleased(MouseEvent e) { 
		super.mouseReleased(e);
		 
		 
			  
			
	}
	//#################################################################################### 
	@Override public void mousePressed(MouseEvent e) { 
		super.mousePressed(e);
		 
		 chapter.updateStartPosition(new Position(e.getX(),e.getY(),0));
		
	}
	
	
 

}
 