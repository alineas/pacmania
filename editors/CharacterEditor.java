package me.nina.editors;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;
 
import me.nina.maker.Cbox; 
import me.nina.maker.Main;
import me.nina.maker.Utils;
import me.nina.maker.Utils;
import me.nina.objects2d.Img;
import me.nina.objects2d.Position;
import me.nina.sqlmanipulation.SQLTable;
import me.nina.gameobjects.Animation;
import me.nina.gameobjects.Character;
import me.nina.gameobjects.Character.PosType;
import me.nina.gameobjects.CharacterState;
import me.nina.gameobjects.GameObject;
import me.nina.gameobjects.ObjectState;
import me.nina.gameobjects.Zone;
import me.nina.gameobjects.Animation.AnimationState;

public class CharacterEditor extends Editor { private static final long serialVersionUID = 1L;

	Character  character;
	JTextField name;   
	Cbox<CharacterState> stateListe; 
	private CharacterState actualState; 
	private JPanel gridMove,gridIdle,gridTalk;  
	private JTextField stateName; 
	private JButton deleteButton; 
	private boolean movingTheOffsetFrom,clickedRight,movingTheOffsetTo; 
	private Point convertedPoint; 
	private Animation clickedAnim; 
	public enum AnimationType { MOVING,  IDLE, TALK  }
	
	//#################################################################################### 
	public CharacterEditor(int id) { 
		super(); 
		
		setLayout(new GridLayout(1,3));
		character = Character.get(id); 
		/*List<File> liste = new ArrayList<>();
		for(CharacterState c : CharacterState.getByPossessor(character.id))
			liste.add(new File(c.getFolder()));
		
		for(File f : new File(character.getFolder()).listFiles())
			if(f.isDirectory()&&!liste.contains(f)) {
				for(File ff:f.listFiles())
					ff.delete();
				f.delete();
			}*/
		Main.leftController.removeAll();
		updateLeftEditor();
		populate(); 
		
	}
	
	//#################################################################################### 
	public void updateLeftEditor() {
		lab.createLabel("name",lab,150,20); 
		name = but.createText(character.title(),but,150,20,(ActionListener) e->{  
			 character.updateTitle(name.getText()); 
		} ); 
		lab.createLabel("delete "+character.title(),lab,150,20); 
		but.createBoutton("delete",but,150,20,(ActionListener) e->{  
			 character.delete(); 
			 Main.leftController.removeAll();
				updateLeftEditor();
				populate();
		} ); 
 
		lab.createLabel("state",lab,150,20);
		stateListe = but.createCbox(CharacterState.getByPossessor(character.id),but,150,20,(ActionListener) e->{ 
			actualState = stateListe.getSelectedItem();
			stateName.setText(actualState.title());
			deleteButton.setText("delete "+actualState.title());
			populate(); }); 
		if(actualState!=null)
			stateListe.setSelectedItem(actualState);
		else actualState = stateListe.getSelectedItem();
		lab.createLabel("state name",lab,150,20); 
		stateName = but.createText(character.title(),but,150,20,(ActionListener) e->{  
			stateListe.getSelectedItem().updateTitle(stateName.getText()); 
		} ); 
		
		lab.createLabel("add a new state",lab,150,20);
		but.createBoutton("add",but,150,20,(ActionListener)e->{
			Map<String,String> val = new HashMap<>(); 
			String title = character.title()+"_"+Main.getUserInput()+"_state";
			val.put("title", title); 
			val.put("possessor", character.id+""); 
			  
			SQLTable tabl = SQLTable.getTable("characterstate");
			int id = tabl.insert(val); 
			Main.leftController.removeAll();
			updateLeftEditor();
			populate();
			stateListe.setSelectedItem(CharacterState.get(id));
			
		});
		 
		lab.createLabel("add scale",lab,150,20); 
		name = but.createText("1.0",but,150,20,(ActionListener) e->{  
			 actualState.updateScaleCorrection(Double.parseDouble(name.getText()));
		} ); 
		
		
		lab.createLabel("delete ",lab,150,20); 
		deleteButton = but.createBoutton("delete "+stateListe.getSelectedItem().title(),but,150,20,(ActionListener) e->{  
			stateListe.getSelectedItem().delete();
			Main.leftController.removeAll();
			updateLeftEditor();
			populate();
		} ); 
		
		lab.createLabel("drag and drop images",lab,150,20);
		but.createLabel("for walkTop,right,left...",but,150,20);
	}
	 
	//#################################################################################### 
	@Override public void populate() {
	    removeAll();
	    CharacterState item = stateListe.getSelectedItem();
	    List<File> liste = new ArrayList<>();
	    gridMove = new JPanel(); gridMove.setLayout(new GridLayout(3, 3)); add(gridMove); 
	    gridIdle = new JPanel(); gridIdle.setLayout(new GridLayout(3, 3)); add(gridIdle); 
	    gridTalk = new JPanel(); gridTalk.setLayout(new GridLayout(3, 3)); add(gridTalk); 
	    gridTalk.setOpaque(false); gridIdle.setOpaque(false); gridMove.setOpaque(false);

	    gridMove.setBorder(BorderFactory.createLineBorder(Color.BLACK));
	    gridIdle.setBorder(BorderFactory.createLineBorder(Color.BLACK));
	    gridTalk.setBorder(BorderFactory.createLineBorder(Color.BLACK));
	    for (int row = 0; row < 3; row++)  for (int col = 0; col < 3; col++) {
	            if (row == 1 && col == 1) {//useless center
	            	JLabel j = new JLabel("drag here for explode a gif");
	            	j.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
	            	JLabel h = new JLabel("drag here for explode a gif");
	            	h.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
	            	JLabel i = new JLabel("drag here for explode a gif");
	            	i.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
	                gridMove.add(j, row * 3 + col);
	                gridIdle.add(h, row * 3 + col);
	                gridTalk.add(i, row * 3 + col);
	                continue;
	            }

	            liste.add(addItemToGrid(gridMove, item, row, col, AnimationType.MOVING));
	            liste.add( addItemToGrid(gridIdle, item, row, col, AnimationType.IDLE));
	            liste.add( addItemToGrid(gridTalk, item, row, col, AnimationType.TALK)); 
	    }
	   // for(File f : new File(item.getFolder()).listFiles())//no wave n mp3 anymore!
	    	//if(!liste.contains(f) && !f.getName().contains(".wav")&&!f.getName().contains(".mp3") )
	    		//f.delete();
	    Main.leftController.revalidate();
	    revalidate(); 
	}
	
	//#################################################################################### 
	//#################################################################################### 
	//#################################################################################### 
	@Override  public void mouseReleased(MouseEvent e) {  
		if(clickedRight) {clickedRight=false; return; }
	    int x = e.getX(); int y = e.getY(); 
	    
	    CharacterState state = stateListe.getSelectedItem(); 
	   
	    String fileName="";
	    if(!movingTheOffsetFrom && !movingTheOffsetTo && clickedAnim == null) {
			File file = Main.fileDisplayer.getSelected();
			fileName = file.getName();
			Utils.copy(file, new File(state.getFolder() + fileName));
		}  
	     
	    Rectangle boundsMove = gridMove.getBounds();
	    if (boundsMove.contains(x, y))
	    	handleRelease(boundsMove, x,y,fileName, state,CharacterState::updateMovingAnimation,CharacterState::movingAnimation,"move");
	     
	    Rectangle boundsIdle = gridIdle.getBounds();
	    if (boundsIdle.contains(x, y)) 
	    	handleRelease(boundsIdle, x,y,fileName, state,CharacterState::updateIdleAnimation,CharacterState::idleAnimation,"idle");
	     
	    Rectangle boundsTalk = gridTalk.getBounds();
	    if (boundsTalk.contains(x, y)) {
	    	handleRelease(boundsTalk, x,y,fileName, state,CharacterState::updateTalkAnimation,CharacterState::talkAnimation,"talk");
	    	handleTalkSpecialStuff(state,boundsTalk,x,y );
	    }
	    clickedAnim=null;
	    populate();
	    super.mouseReleased(e);
	}
	//#################################################################################### 
	private void handleTalkSpecialStuff(CharacterState state, Rectangle boundsTalk, int x, int y ) {
		PosType posType = calculatePosTypeForBounds(boundsTalk,x,y);
		Animation talkAnim = state.talkAnimation(posType);
		if(state.idleAnimation(posType)==null) {
			Animation movingCorresponding = state.movingAnimation(posType);
			int nbr = movingCorresponding.frameOrder().size();
			String animName = state.title()+"_"+posType.toString()+"_idle";
			int id = Animation.generateAnimation(state, animName,  movingCorresponding.frameOrder().get(nbr-1), movingCorresponding.image());
			state.updateIdleAnimation(Animation.get(id), posType); 
		}
		
		Animation idleCorresponding = state.idleAnimation(posType);
		talkAnim.setCorrespondingAnim(idleCorresponding);
	}

	//#################################################################################### 
	private void handleRelease(Rectangle bounds,int x, int y, String fileName, CharacterState state, Updater updater, Getter getter,String type) {
		PosType posType = calculatePosTypeForBounds(bounds,x,y);
		if(recordedAnOffset(getter.getAnimation(state, posType),x,y))
    		return;
		if(posType==PosType.UNUSED)
    		generateExplodedAnimations(fileName,updater);  
    	else {
    		if(reversedFrames(getter.getAnimation(state, posType)))
    			return;
    		 String animName = state.title()+"_"+posType.toString()+"_"+type;
    		 int frameIndex = AnimationState.INFINITE_LOOP.getValeur();
    		 if(gridIdle.getBounds().contains(x,y))
    			 frameIndex = Utils.askForFrame(Main.fileDisplayer.getSelected().getPath(), "select frame",false);
    		 int id = Animation.generateAnimation(state, animName,  frameIndex, fileName);
			 updater.performAction(state,Animation.get(id), posType);
    	} 
	}
    /*Animation idle = item.idleAnimation(getPosTypeForGridIndex(row, col));
	anim.updateTalk(idle.id);
	idle.updateTalk(anim.id);
	anim.setTalkOn(true);*/
     
	//#################################################################################### 
	//#################################################################################### 
	//#################################################################################### 
	@Override public void mousePressed(MouseEvent e) {  
		int scrollX = Main.videoScroll.getHorizontalScrollBar().getValue();
        int scrollY = Main.videoScroll.getVerticalScrollBar().getValue();
        int visibleWidth = Main.videoScroll.getWidth();
        int visibleHeight = Main.videoScroll.getHeight();

        // Calculer les limites de la zone visible
        Rectangle visibleArea = new Rectangle(scrollX, scrollY, visibleWidth, visibleHeight);

        
		if(!visibleArea.contains(e.getPoint()))return;
		Rectangle boundsMove = gridMove.getBounds();
	    Rectangle boundsIdle = gridIdle.getBounds(); 
	    Rectangle boundsTalk = gridTalk.getBounds(); 
	    int x = e.getX();
	    int y = e.getY();
	    Animation anim = null;
	    if (boundsIdle.contains(x, y)) {
	    	PosType posType = calculatePosTypeForBounds(boundsIdle,x,y);
	    	anim = stateListe.getSelectedItem().idleAnimation(posType) ;
	    	tcheckOffsetClick(boundsIdle,x,y,anim,e.getButton()==3);
	    	 
	    }
	    if (boundsMove.contains(x, y)) {  
	    	PosType posType = calculatePosTypeForBounds(boundsMove,x,y);
	    	anim = stateListe.getSelectedItem().movingAnimation(posType) ;
	    	tcheckOffsetClick(boundsMove,x,y,anim,e.getButton()==3);
	    	 
	    }
	    if (boundsTalk.contains(x, y)) { 
	    	PosType posType = calculatePosTypeForBounds(boundsTalk,x,y);
    		anim = stateListe.getSelectedItem().talkAnimation(posType) ;
	    	if(e.getButton()!=3) { 
	    		convertedPoint = SwingUtilities.convertPoint(this, x,y, anim); 
	    		this.movingTheOffsetFrom=true;
	    	}
	    }
	    if(e.getButton()==3){
	    	
	    	
	    	clickedRight=true;
    		JPopupMenu popupMenu = new JPopupMenu(); 
    		
    		Animation anime = anim;
    		lab.createJMenu(popupMenu,"selected sound in all anims" ,ee -> { 
    			
    			File file = Main.fileDisplayer.getSelected();
            	String fileName = file.getName();
            	Utils.copy(file,  new File( anime.possessorObj().possessorObj().getFolder()+fileName));

    			for (PosType p : PosType.values()) {
    				if(!fileName.contains(".mid")){
                    if(actualState.movingAnimation(p)!=null)actualState.movingAnimation(p).updateSound(fileName);
                    if(actualState.idleAnimation(p)!=null)actualState.idleAnimation(p).updateSound(fileName);
                    if(actualState.talkAnimation(p)!=null)actualState.talkAnimation(p).updateSound(fileName);
    				}else{
    					 if(actualState.movingAnimation(p)!=null)actualState.movingAnimation(p).updateMusic(fileName);
    	                    if(actualState.idleAnimation(p)!=null)actualState.idleAnimation(p).updateMusic(fileName);
    	                    if(actualState.talkAnimation(p)!=null)actualState.talkAnimation(p).updateMusic(fileName);
    				}
                }

    		
    		}); 
    		
    		
    		lab.createJMenu(popupMenu,"unsonorize all anims" ,ee -> { 
    			 
    			for (PosType p : PosType.values()) {
    				
                    if(actualState.movingAnimation(p)!=null){
                    	actualState.movingAnimation(p).updateSound("");
                    	actualState.movingAnimation(p).unsonorize();}
                    if(actualState.idleAnimation(p)!=null){
                    	actualState.idleAnimation(p).updateSound("");
                    	actualState.idleAnimation(p).unsonorize();}
 
                }

    		
    		}); 
    		
    		
    		lab.createJMenu(popupMenu,"all anims are a single loop (not infinite)" ,ee -> { 
    			 
    			for (PosType p : PosType.values()) {
    				
                    if(actualState.movingAnimation(p)!=null)actualState.movingAnimation(p).updateState(AnimationState.LOOP); 
                }

    		
    		}); 


    		lab.createJMenu(popupMenu,"set color for all shadows" ,ee -> {   new ColorPicker(anime,stateListe.getSelectedItem()); }); 
    		lab.createJMenu(popupMenu,"set shadow color for selected position" ,ee -> {   new ColorPicker(anime,null); }); 
    		lab.createJMenu(popupMenu,"edit animation" ,ee -> {  
    			Main.setActualEditor( new AnimationEditor(anime)); });  
    		lab.createJMenu(popupMenu,"reset offsets" ,ee -> {  
    	         anime.updatePosFrom(0, 0);  anime.updatePosTo(0, 0); });  
    		
    		lab.createJMenu(popupMenu,"use offsets as priority difference" ,ee -> { 
    			int from = anime.posFrom().y;
    			int to = anime.posTo().y;
    			anime.updatePrioFrom(from);
    			anime.updatePrioTo(to);
   	         anime.updatePosFrom(0, 0);  anime.updatePosTo(0, 0); });  
    		
    	    popupMenu.show(Main.actualEditor, x, y);
	    }
		super.mousePressed(e);
	}
	//#################################################################################### 
	private void tcheckOffsetClick(Rectangle r, int x, int y, Animation anim, boolean rightClic) { 
	        convertedPoint = SwingUtilities.convertPoint(this, x,y, anim); 
	        if(rightClic) {
	        	
	    	}
	        else {
	    		if(anim.offsetFrom.isNear(new me.nina.objects2d.Point(convertedPoint.x,convertedPoint.y), 10))
	    			this.movingTheOffsetFrom=true;
	    		else if(anim.offsetTo.isNear(new me.nina.objects2d.Point(convertedPoint.x,convertedPoint.y),  10))
	    			this.movingTheOffsetTo=true;
	    		else clickedAnim = anim;
	    		 
	    	}
	}
	//#################################################################################### 
	private boolean recordedAnOffset(Animation anim, int x, int y) {
		if( ! movingTheOffsetFrom && ! movingTheOffsetTo) 
			return false;
		if(anim==null) {
			movingTheOffsetTo = false;
			movingTheOffsetFrom = false;
			clickedAnim=null;
			return false;
		}
		Point point2 = SwingUtilities.convertPoint(this, x,y, anim); 
    	int offsetX = point2.x - convertedPoint.x;
		 int offsetY = point2.y - convertedPoint.y;
        if(movingTheOffsetFrom)
			 anim.updatePosFrom(offsetX,offsetY);  
        if(movingTheOffsetTo)  
			 anim.updatePosTo(offsetX,offsetY); 
        movingTheOffsetTo = false;
		movingTheOffsetFrom = false;
        return true;
	}
	//#################################################################################### 
	private void generateExplodedAnimations(String fileName, Updater updater) {
		 CharacterState state = stateListe.getSelectedItem(); 
			for(PosType pos:PosType.values()) {
				if(pos == PosType.UNUSED)continue;
				try { 
					String animName = state.title()+"_"+pos.toString();
					int id = Animation.generateAnimation(state,animName,  pos.getValeur(),fileName);
					updater.performAction(state,Animation.get(id), pos);
				 
				} catch (Exception e) {  }
			} 
	}
	//#################################################################################### 
	private boolean reversedFrames(Animation releasedAnimation) {//intervert frames
		if(clickedAnim == null)return false;
		String clickedAnimImg = clickedAnim.image();
		List<Integer> clickedFrameOrder = clickedAnim.frameOrder();
		String releasedAnimImg = releasedAnimation.image();
		List<Integer> releasedFrameOrder = releasedAnimation.frameOrder();
		
		clickedAnim.updateImage(releasedAnimImg);
		clickedAnim.updateOrder(releasedFrameOrder); 
		
		releasedAnimation.updateImage(clickedAnimImg);
		releasedAnimation.updateOrder(clickedFrameOrder); 
		 populate();
		return true;
	}
	//#################################################################################### 
			private Point calculateGridIndexForPoint(Rectangle bounds, int x, int y) {
 			    int cellWidth = bounds.width / 3;
			    int cellHeight = bounds.height / 3;

 			    int row = (y - bounds.y) / cellHeight;
			    int col = (x - bounds.x) / cellWidth;

 			    row = Math.min(Math.max(row, 0), 2);
			    col = Math.min(Math.max(col, 0), 2);
	 
 			    return new Point(row, col);
			}
			//#################################################################################### 
			private PosType getPosTypeForGridIndex(int row, int col) {
			    switch (row) {
			        case 0: switch (col) { 
			        		case 0: return PosType.TOP_LEFT;
			                case 1: return PosType.TOP;
			                case 2: return PosType.TOP_RIGHT;
			                default: break;
			            } break;
			        case 1:
			            switch (col) {
			                case 0: return PosType.LEFT;
			                case 1: return PosType.UNUSED;
			                case 2: return PosType.RIGHT;
			                default: break;
			            } break;
			        case 2:
			            switch (col) {
			                case 0: return PosType.BOT_LEFT;
			                case 1: return PosType.BOTTOM;
			                case 2: return PosType.BOT_RIGHT;
			                default: break;
			            } break;
			        default:
			            break;
			    }
			    return null;  
			}
	//#################################################################################### 
			private File addItemToGrid(JPanel grid, CharacterState item, int row, int col, AnimationType animationType) {
		        try {
		            Animation anim = null; 
		            if(animationType == AnimationType.MOVING) 
		                    anim = item.movingAnimation(getPosTypeForGridIndex(row, col));
		            if(animationType == AnimationType.IDLE)  
		                    anim = item.idleAnimation(getPosTypeForGridIndex(row, col)); 
		            if(animationType == AnimationType.TALK)  {
		                    anim = item.talkAnimation(getPosTypeForGridIndex(row, col));
		                    Animation idleCorresponding = stateListe.getSelectedItem().idleAnimation(getPosTypeForGridIndex(row, col));
		            		anim.setCorrespondingAnim(idleCorresponding);
		            }
		              
		            if(animationType == AnimationType.MOVING) {
		            	anim.rewindAll();anim.unsetIdle();}
		            anim.loadGif();
		            anim.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
		            grid.add(anim, row * 3 + col);
		            return new File((item.getFolder()+anim.image()));
		        } catch (Exception e) {  
		        	JLabel j = new JLabel(animationType+" "+getPosTypeForGridIndex(row, col)+"");
		        	j.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
		            grid.add(j, row * 3 + col);
		        }
				return null; 
		} 
			
	private PosType calculatePosTypeForBounds(Rectangle r,int x, int y) {
		Point gridIndex = calculateGridIndexForPoint(r, x, y);
        return getPosTypeForGridIndex(gridIndex.x, gridIndex.y);
	}
	@Override public void mouseEntered(MouseEvent e) {   super.mouseEntered(e); }
	@FunctionalInterface interface Getter {  Animation getAnimation(CharacterState state, PosType posType); }
	@FunctionalInterface interface Updater { void performAction(CharacterState state, Animation animation, PosType posType); }
	@Override protected void paintComponent(Graphics g) { }
	@Override protected boolean handleImgDragged() {  return true; }
}
 