package me.nina.gameobjects;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import me.nina.editors.Editor;
import me.nina.gameobjects.GameObject;
import me.nina.maker.Ashe;
import me.nina.maker.Main;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Pane;
import me.nina.maker.Utils;
import me.nina.sqlmanipulation.SQLLine;
import me.nina.sqlmanipulation.SQLTable;

public abstract class GameObject extends Pane { private static final long serialVersionUID = 1L;
 
	public final static Map< GameObject , Map<Integer, GameObject>> mappe = new HashMap<>(); 
	public final SQLLine line; public final int id;  
	private boolean selected,jtreeSelected;
	public GameObject(SQLLine line){   this.line = line; this.id=line.id;   } 
	
	public GameObject() { line=null;id=-1;}
	 
	
	
	
	public static void reset() {
		for(Map<Integer, GameObject> a:mappe.values())
			a.clear();
		mappe.clear();
		MegaAction.mappe.clear();
	}
	
	
    public static <T extends GameObject> T get(GameObject clazz, SQLLine l, int id) { try {
		if(!mappe.containsKey(clazz)) 
			mappe.put(clazz, new HashMap<Integer,GameObject>());
		 
		if (!mappe.get(clazz).containsKey(id)) {  
			        if(l!=null&&clazz.isValidElement(l))
			        	mappe.get(clazz).put(id, clazz.instanciate(l));
		}
		return (T) mappe.get(clazz).get(id);
	} catch (Exception e) { e.printStackTrace(); return null; }  }
    
     
	public static  <T extends GameObject> List<T> getAll(GameObject clazz) { try { 
		List<GameObject> allGameObjects = new ArrayList<>(); 
		String tableName = clazz.getTable();
		for (Entry<Integer, SQLLine> l : SQLTable.getTable(tableName).getLines().entrySet()) 
			if(clazz.isValidElement(l.getValue()))
				allGameObjects.add(get(clazz, l.getValue(),l.getKey()));
		 
		return (List<T>) allGameObjects;
	} catch (Exception e) { e.printStackTrace();return null; } }
	
	
	public static  <T extends GameObject> List<T> getbyPossessor(GameObject clazz, GameObject possessor ) { try {
		List<GameObject> allGameObjects = new ArrayList<>(); 
		String tableName = clazz.getTable();
		for (Entry<Integer, SQLLine> l : SQLTable.getTable(tableName).getLines().entrySet()) 
			if(clazz.isValidPossessor(l.getValue(), possessor) && clazz.isValidElement(l.getValue()) )
				allGameObjects.add(get(clazz, l.getValue(),l.getKey()));
		return (List<T>) allGameObjects;
	} catch (Exception e) { e.printStackTrace(); return null;}  }

	
	 public static <T extends GameObject> T get(GameObject clazz, int id) { try {
			if(!mappe.containsKey(clazz)) 
				mappe.put(clazz, new HashMap<Integer,GameObject>());
			 
			if (!mappe.get(clazz).containsKey(id)) { 
				String tableName = clazz.getTable();
				 SQLLine l = SQLTable.getTable(tableName).getLines().get(id); 
				        if(l!=null&&clazz.isValidElement(l))
				        	mappe.get(clazz).put(id, clazz.instanciate(l));
			}
			return (T) mappe.get(clazz).get(id);
		} catch (Exception e) { e.printStackTrace(); return null; }  }
  
	public static GameObject getById(int int1) {
		GameObject result; 
	    // Store the result of each get call in a variable and check if it is not null
	    result = Scene.get(int1);
	    if (result != null) return result;

	    result = CharacterState.get(int1);
	    if (result != null) return result;

	    result = InventoryObject.get(int1);
	    if (result != null) return result;

	    result = ObjectState.get(int1);
	    if (result != null) return result;

	    result = Character.get(int1);
	    if (result != null) return result;

	    result = Chapter.get(int1);
	    if (result != null) return result;

	    result = BasicObject.get(int1);
	    if (result != null) return result;

	    result = Message.get(int1);
	    if (result != null) return result;

	    result = Action.get(int1);
	    if (result != null) return result;

	    result = Zone.get(int1);
	    if (result != null) return result;

	    result = TransitionPoint.get(int1);
	    if (result != null) return result;

	    result = Stair.get(int1);
	    if (result != null) return result;

	    result = StairList.get(int1);
	    if (result != null) return result;

	    result = SpawnPoint.get(int1);
	    if (result != null) return result;

	    result = SpecialZoneInv.get(int1);
	    if (result != null) return result;
	    result = Animation.get(int1);
	    if (result != null) return result;
	    // Return null if no object is found
	    return result;
	
	}
	public void updateTitle(String text){
		updateTitle(text,false);
	}
	public void updateTitle(String text,boolean renameFolder){ 
		if(renameFolder && new File (getFolder()).exists())
		try {
			File parent = new File (getFolder()).getParentFile(); 
			new File (getFolder()).renameTo(new File(parent.getAbsolutePath()+File.separator+text+File.separator));
		} catch (Exception e) { 
			 
		}
		line.update("title", text); 
	}
	public String title(){
		String s = line.getString("title");
		if(s.equals(""))s="no title defined";
		return s;}
	public boolean isJTreeSelected() {
	    return jtreeSelected;
	}

	public void setJTreeSelected(boolean jtreeSelected) {
	    this.jtreeSelected = jtreeSelected;
	}
	public boolean isSelected() {return selected;}
	public void setSelected(boolean b) {selected = b;}
    abstract GameObject instanciate(SQLLine l); 
	abstract boolean isValidPossessor(SQLLine l, GameObject possessor);
	abstract boolean isValidElement(SQLLine l);
    abstract String getTable( );
    abstract GameObject default_instance();
    public int possessor() { return  line.getInt("possessor") ; }  
    public GameObject possessorObj() { return GameObject.getById(line.getInt("possessor")); }  
	public void updatePossessor(GameObject s) { line.update("possessor", s.id+"");  } 
	public void updatePossessor(int i) { line.update("possessor", i+"");  }  
	abstract public String getFolder(); 
	public abstract Set<AshType> possibleGetters() ;  
	public abstract Set<AshType> possibleSetters() ;   
	public abstract Set<AshType> possibleRemovers() ;   
	abstract public List<GameObject> getChilds();
	public String image() { return line.getString("img"); } 
 	
	 
	@Override public boolean equals(Object obj) { if(!(obj instanceof GameObject))return false;
	GameObject other = (GameObject) obj; if (id != other.id) return false; return true;
}

	public void delete(){ if(!Main.getUserConfirmation("are you sure ?"))return;
	for(Action action : Action.getAll()){
		   for(Ashe s : action.ashToSet())
			   if( s.possessor().equals(this) )
				   action.remAshToSet(s,-2);//-2 = rem all occurences
		   for(Ashe n : action.ashNeeded())
			   if( n.type()!=AshType.OR&&n.possessor().equals(this) )
				   action.remAshNeeded(n,-2);//-2 = rem all occurences
		   for(Ashe r : action.ashRefused())
			   if( r.type()!=AshType.OR&&r.possessor().equals(this) )
				   action.remAshRefused(r,-2);//-2 = rem all occurences
		   for(Integer d : action.detectors())
			   if( d==id) 
				   action.remDetector(d,-2);//-2 = rem all occurences
		   }
		line.delete(); 
		Editor.handleDelete(id);

		for (GameObject a:mappe.keySet())
			mappe.get(a).remove(new Integer(id)); 
	}

	protected GameObject reinstanciate() {
		return instanciate(line);
	}


	 
}
