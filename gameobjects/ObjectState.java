package me.nina.gameobjects;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import me.nina.editors.AnimationEditor;
import me.nina.gameobjects.GameObject;
import me.nina.gameobjects.BasicObject.ObjectType;
import me.nina.gameobjects.Character.PosType;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Main;
import me.nina.maker.Utils;
import me.nina.objects2d.Position;
import me.nina.sqlmanipulation.DuoArray;
import me.nina.sqlmanipulation.DuoArrayManager;
import me.nina.sqlmanipulation.SQLLine;
import me.nina.sqlmanipulation.SQLTable;

public class ObjectState extends GameObject{ private static final long serialVersionUID = 1L; 
    public Animation image3,image2,image1; 
	public JPanel panel; 
	private JTextField textField;
	private double zoom; 
	boolean horizontal =true;
	private JButton displayButton;
	private String title;
	
	private ObjectState(SQLLine line ) {
		super(line);  
		setLayout(null);
        setOpaque(false);
	}
	public ObjectState() { }
	public ObjectState(String title) { this.title=title; }
	
	public ObjectState save() {
		Map<String,String> val = new HashMap<>(); 
		val.put("title",title);
		return get(SQLTable.getTable(getTable()).insert(val)); 
	}
	 
		//####################################################################################
	    public void populate(double zoom) {
	    	this.zoom=zoom;
	    	removeAll(); 
	    	int size = (int) (100*zoom); 
	    	Animation anim = Animation.get(animID()); 
	    	int fontSize = (int) (30 * zoom); 
			Font font = new Font("Arial", Font.PLAIN, fontSize); 
	    	
	    	 if( BasicObject.get(this.possessor()) instanceof InventoryObject) {
	    		 
	    		 
	    		 image1 = new Animation(anim.line) ; image1.setSize(size);
	    		 
	    		 textField = new JTextField();
		         textField.setBounds(0,20+size*3,(int) (600*zoom),(int) (40*zoom));
		         textField.setText(title());
		         textField.setFont(font);

		         textField.addActionListener(new ActionListener() {
		             public void actionPerformed(ActionEvent e) {
		                 String input = textField.getText(); 
		                 updateTitle(input);
		                 Main.actualEditor.populate();
		             }
		         });
		         
		         
		         Main.actualEditor. add(image1); 
		         Main.actualEditor. add(textField); 
		         setBounds(0,0,  size , size +40 );
	        	 image1.setBounds(0, 0, size, size );  
				 textField.setBounds(0,20+size,getWidth(),(int) (getHeight()*zoom));
				 setBorder(new LineBorder(Color.black));
	    		 return;
	    	 }
	    	
	    	 
	        image3 = new Animation(anim.line) ; image3.setIdleFrame(anim.frameOrder().size()-1); image3.setSize(size); 
 	        image2 = new Animation(anim.line) ; image2.setSize(size);
	        image1 = new Animation(anim.line) ; image1.setIdleFrame(0); image1.setSize(size);
	        displayButton = new JButton("turn");
	       
			displayButton.setFont(font);
	         displayButton.addActionListener(new ActionListener() {
	             public void actionPerformed(ActionEvent e) {
	                horizontal=!horizontal;
	                Main.actualEditor.populate();
	             }
	         });
	        
	         textField = new JTextField();
	         textField.setBounds(0,20+size*3,(int) (600*zoom),(int) (40*zoom));
	         textField.setText(title());
	         textField.setFont(font);

	         textField.addActionListener(new ActionListener() {
	             public void actionPerformed(ActionEvent e) {
	                 String input = textField.getText();
	                  
	                 updateTitle(input);
	                 Main.actualEditor.populate();
	             }
	         });
	         
	         
	         Main.actualEditor. add(image2); 
	         Main.actualEditor.add(image1); 
	         Main.actualEditor.add(image3);  
	         Main.actualEditor.add(displayButton); 
	         Main.actualEditor. add(textField);
	         
	         if(horizontal) {
	        	 setBounds(0,0,  size*3, size +40 );
	        	 image1.setBounds(0, 0, size, size );  
				    image2 .setBounds( size*2,20, size, size );  
				    image3 .setBounds( size*3, 20, size, size ); 
			         textField.setBounds(0,20+size,getWidth(),(int) (getHeight()*zoom));
			         displayButton.setBounds(0,0,(int) (50*zoom),(int) (40*zoom));
			 }else {
				 setBounds(0,0,  size, size*3 +40 );
	        	 image1.setBounds(0, 20, size, size );  
				    image2 .setBounds(0, 20+size*2, size, size );  
				    image3 .setBounds(0, 20+size*3, size, size ); 
			         textField.setBounds(0,size,getWidth(),(int) (getHeight()*zoom));
			         displayButton.setBounds(0,0,(int) (50*zoom),(int) (40*zoom));
			 }
	        
	         
	          
	         setBorder(new LineBorder(Color.RED));
	         
	       
	    }
		@Override public void setLocation(int x,int y) {
			super.setLocation(x, y); 
			 try { 
				 int size = (int) (100*zoom);
				 int deltaX = 0;
				 int deltaY = 0;
				 BasicObject possessor = BasicObject.get(possessor());
				 if( possessor instanceof InventoryObject ){
					 textField.setBounds(x,y ,getWidth(),(int) (40*zoom));
					 image1.setBounds(x, y+20, size, size );  
					 return;
				 }
				 ObjectState stateFrom = possessor.initialState();
				 
				 if(stateFrom!=null && !stateFrom.pos().equals(pos())){
					 deltaX = (int) ((stateFrom.pos().x - pos().x)*zoom);
					 deltaY = (int) ((stateFrom.pos().y - pos().y)*zoom);
				 }
				 if(horizontal) {setBounds(getX(),getY(),  size*3, size  +40 );
				image1.setBounds(x+deltaX, 20+y+deltaY, size, size );  
				    image2 .setBounds(x+size, 20+y, size, size );  
				    image3 .setBounds(x-deltaX+size*2, 20+y-deltaY, size, size ); 
			         textField.setBounds(x,y+getHeight()-20,getWidth(),(int) (40*zoom));
			         
				 }else {setBounds(getX(),getY(),  size, size*3  +40 );
					 image1.setBounds(x+deltaX, 20+y+deltaY, size, size );  
					    image2 .setBounds(x, 20+y+size, size, size );  
					    image3 .setBounds(x-deltaX, 20+y-deltaY+size*2, size, size ); 
				         textField.setBounds(x,y+getHeight()-20,getWidth(),(int) (40*zoom));
				 }
				 displayButton.setBounds(x,y,(int) (50*zoom),(int) (40*zoom));
				 
				 } catch (Exception e) { e.printStackTrace();
			}
		        
		}
	//########################################################################
 
 	public void delete() { 
 		if(!Main.getUserConfirmation("are you sure ?"))return;
 		for(Zone z: Zone.getByOwner(id))
 			z.delete();
 		
 		Animation.get(animID()).line.delete(); 
 		super.delete(); }
 	//@Override protected void paintComponent(Graphics g) { super.paintComponent(g);  } 
	public Position pos() { return new Position(line.getString("pos"));  } 
	public void updatePos(int x, int y,int z) { line.update("pos", new Position(x,y,z).toSqlValues()); } 
	public Position requiredPlayerPosition() { return new Position(line.getString("requiredplayerposition"));   }  
	public void updateRequiredPlayerPosition(int x, int y,int z) { line.update("requiredplayerposition", new Position(x,y,z).toSqlValues()); }
 	public int animID() { return line.getInt("animtransitionid"); }  
	public void updateAnimID(int i) { line.update("animtransitionid", i+"");  } 
 	public boolean isFullscreen() { return line.getInt("fullscreen")==1; }  
	public void updatefullScreen(boolean b) { line.update("fullscreen", b?"1":"0");  }  
	public String cursorImg() { 
		String s = line.getString("cursorimage");
		if(this.animID()==-998)return null;
		if(s==null||s.equals(""))return Animation.get(this.animID()).image();  
		return s;}  
	public void updateCursorimage(String img) { line.update("cursorimage", img);  }  
	public void updateStateFrom(ObjectState stateFrom) { line.update("statefrom", stateFrom.id+"" );  } 
	
	@Override public String toString() { return title(); } 
	@Override public String getFolder() {  return possessorObj().getFolder(); } 


	public int translateId() { return line.getInt("translateid") ; }  
	public void updatetranslateId(int i) { line.update("translateid", i+"");  }
 
	@Override
	public List<GameObject> getChilds() {
		List<GameObject> liste = new ArrayList<>() ;
		/*for(AshType a : AshType.values()) {
				//if(z.ash(a) != -1) 
					//liste.add(Ash.get(z.ash(a)));
				if(ash(a) != -1) 
					liste.add(Ash.get(ash(a)));
			}*/ 
			liste.addAll(Zone.getByOwner(id));
		  	 
		return liste;
	}
	@Override GameObject instanciate(SQLLine l) {  return new ObjectState(l); }
	@Override boolean isValidPossessor(SQLLine l, GameObject possessor) { 
		return l.getString("possessor").equals(possessor.id+""); }
	@Override boolean isValidElement(SQLLine l) {  return true; }
	@Override String getTable() {  return "objectstate"; }
	static ObjectState instance= new ObjectState();
	@Override GameObject default_instance() { return instance; } 
	public static List<ObjectState> getByObject(int objectID) {
		return getbyPossessor(instance,GameObject.getById(objectID));  }  
	public static ObjectState get(int id) {  return get(instance,id); }  
	public static Collection<? extends GameObject> getAll() { return getAll(instance); }

	public void resetPlayerPosition() {
		BasicObject owner = (BasicObject) this.possessorObj();
		int x = owner.initialState().requiredPlayerPosition().x;
		int y = owner.initialState().requiredPlayerPosition().y;
		PosType pos = owner.initialState().requiredPlayerPosition().pos;
		this.updateRequiredPlayerPosition(x, y, pos.getValeur());
		x = owner.initialState().pos().x;
		y = owner.initialState().pos().y;
		pos = owner.initialState().pos().pos;
		this.updatePos(x, y, pos.getValeur());

	}
	 
	
	
	
	@Override  public 	Set<AshType> possibleGetters() {
		Set<AshType> temp = new HashSet<>();
		temp.add(AshType.IS_IN_STATE ); 
		 
		return temp;
	}
	@Override  public 	Set<AshType> possibleSetters() {
		Set<AshType> temp = new HashSet<>();
		 
		temp.add(AshType.WILL_BE_IN_POSITION );  
		temp.add(AshType.WILL_BE_IN_STATE ); 
		return temp;
	}
	@Override  public 	Set<AshType> possibleRemovers() {
		Set<AshType> temp = new HashSet<>();
		return temp;
	}
	
	@Override public String title() {
		try {
			return this.possessorObj().title()+"_state_"+super.title();
		} catch (Exception e) {
			 
			return super.title();
		}
	}
}
