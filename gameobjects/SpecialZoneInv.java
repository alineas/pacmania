package me.nina.gameobjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import me.nina.maker.Utils;
import me.nina.sqlmanipulation.SQLLine;
   
public class SpecialZoneInv extends Zone{ 

	public List<Integer> frames=new ArrayList<>();
	 
	public SpecialZoneInv(int x, int y, int i, int j, ZoneType type,int frame) {
		super(x, y, i, j, type);
		frames.add(frame);
		 
	}
	public SpecialZoneInv(String sql, int id) {
		super(sql,id);
		
		String[] frams = sql.split(",")[5].split("@");
		for(String s : frams)
			frames.add(Integer.parseInt(s));
		if(!sql.split(",")[5].contains("@") && sql.split(",")[5].length()!=0)
			frames.add(Integer.parseInt(sql.split(",")[5]));
	}
	public SpecialZoneInv(Zone z, int i) {
		this(z.x(),z.y(),z.i(),z.j(),z.type,i);
	}
 

	@Override public String toSqlValues() { 
		String sql = "";
		for (int a :frames)
			sql=sql+"@"+a;
		return super.toSqlValues()+","+sql.replaceAll("^@", "");
	}
 
 
	@Override public SpecialZoneInv instanciate(String sql,int id) { return new SpecialZoneInv(sql,id); }
	@Override public SpecialZoneInv getObject() { return this; } 
	@Override
	public List<GameObject> getChilds() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	
	
	
	 
	@Override
	boolean isValidPossessor(SQLLine l, GameObject owner) { 
		return  l.getInt("possessor") == owner.id  ;
	}

	@Override
	boolean isValidElement(SQLLine l) { 
		 if (l.getInt("type") == ZoneSort.SPECIAL.getValeur())
     		return true;
		return false;
	}

static SpecialZoneInv instance = new SpecialZoneInv(0,0, 0, 0, ZoneType.LINE, 0);
	@Override
	GameObject default_instance() { 
		return instance;
	}
 


	public static List<Zone> getByOwner(int ownerId){
		 
		 return getbyPossessor(instance,GameObject.getById(ownerId));
	}
 
	
	public static List<Zone> getAll( ) {
	    return getAll(instance);
	}
	
	public static Zone get( int id) {
	    return get(instance,id);
	}
	
	
	@Override public boolean equals(Object obj) {
		if(!(obj instanceof SpecialZoneInv)) return false;
		if (this == obj)
			return true; 
	
		Zone other = (Zone) obj;
		if (p1 == null) {
			if (other.p1 != null)
				return false;
		} else if (!p1.equals(other.p1))
			return false;
		if (p2 == null) {
			if (other.p2 != null)
				return false;
		} else if (!p2.equals(other.p2))
			return false;
		return true;
	}
	
	
	
	
	
	

}
