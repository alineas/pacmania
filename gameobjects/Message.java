package me.nina.gameobjects;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
 
import javazoom.jl.decoder.JavaLayerException;
import me.nina.editors.Editor;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Main;
import me.nina.maker.Utils;
import me.nina.sqlmanipulation.DuoArray;
import me.nina.sqlmanipulation.SQLLine;
import me.nina.sqlmanipulation.SQLTable;

public class Message extends GameObject {
	private String sierraid;
	private String text;
	private String talker;
	private String seq;
	private String cond;
	private String verb;
	private String noun; 
	
	public Message (String sierraid,String noun,String verb,String cond,String seq,String talker,String text) {
		super();
		this.sierraid=sierraid;
		this.noun=noun;
		this.verb=verb;
		this.cond=cond;
		this.seq=seq;
		this.talker=talker;
		this.text=text; 
	}
	public String getSound(){
		return getFolder()+ getFileName();
	}
	public String getFileName(){
		return "Audio"+sierraid+"-"+noun+"-"+verb+"-"+cond+"-"+seq+".mp3";
	}
	public Message (SQLLine l) {
		super(l);
		this.sierraid=l.getString("sierraid");
		this.noun=l.getString("noun");
		this.verb=l.getString("verb");
		this.cond=l.getString("cond");
		this.seq=l.getString("seq");
		this.talker=l.getString("talker");
		this.text=l.getString("text"); 
	}

	public Message() {
		// TODO Auto-generated constructor stub
	}

	
	
	
	
	
	
	
	
	public static JPanel getFormPanel() {
		return new MessagePanel();
	}
	
	
	
	
	
	
	
	
	
	
	static class MessagePanel extends JPanel {
	    private JTextField sierraidField;
	    private JTextField nounField;
	    private JTextField verbField;
	    private JTextField condField;
	    private JTextField seqField;
	    private JTextField talkerField;
	    private JTextField textField;
	    private JTextField soundField;

	    private JComboBox<String> sierraidComboBox;
	    private JComboBox<String> nounComboBox;
	    private JComboBox<String> verbComboBox;
	    private JComboBox<String> talkerComboBox;
	    
	    public MessagePanel() {
	    	 setLayout(new GridLayout(10, 3));

	         JLabel sierraidLabel = new JLabel("Sierra ID:");
	         add(sierraidLabel);
	         sierraidField = new JTextField();
	         add(sierraidField);
	         sierraidComboBox = Main.leftController.createCbox(Message.getSids(),this,150,20,(ActionListener)e->{ 
	                     sierraidField.setText(getSelected((String) sierraidComboBox.getSelectedItem())+"");
	                 
	         } ); 

	         JLabel nounLabel = new JLabel("Noun:");
	         add(nounLabel);
	         nounField = new JTextField();
	         add(nounField);
	         nounComboBox = Main.leftController.createCbox(Message.getNouns(),this,150,20,(ActionListener)e->{ 
	                     nounField.setText(getSelected((String) nounComboBox.getSelectedItem())+""); 
	         } );
	          

	         JLabel verbLabel = new JLabel("Verb:");
	         add(verbLabel);
	         verbField = new JTextField();
	         add(verbField);
	         verbComboBox = Main.leftController.createCbox(Message.getVerbs(),this,150,20,(ActionListener)e->{
	                     verbField.setText(getSelected((String) verbComboBox.getSelectedItem())+""); 
	         } );
	          

	         JLabel condLabel = new JLabel("Condition:");
	         add(condLabel);
	         condField = new JTextField();
	         add(condField);
	         add(new JLabel(""));
	         JLabel seqLabel = new JLabel("Sequence:");
	         add(seqLabel);
	         seqField = new JTextField();
	         add(seqField);
	         add(new JLabel(""));
	         JLabel talkerLabel = new JLabel("Talker:");
	         add(talkerLabel);
	         talkerField = new JTextField();
	         add(talkerField);
	         talkerComboBox = Main.leftController.createCbox(Message.getTalkers(),this,150,20,(ActionListener)e->{ 
	                     talkerField.setText(getSelected((String) talkerComboBox.getSelectedItem())+""); 
	         });
	         add(talkerComboBox);

	         JLabel textLabel = new JLabel("Text:");
	         add(textLabel);
	         textField = new JTextField();
	         add(textField);
	         add(new JLabel(""));
	         
	         JLabel soundLabel = new JLabel("Sound:");
	         add(soundLabel);
	         soundField = new JTextField();
	         add(soundField);
	         
	         JButton but = new JButton("use selected");
		        but.addActionListener((ActionListener) e-> {
		        	soundField.setText(Main.fileDisplayer.getSelected().getAbsolutePath());
		     	       but.setVisible(false);
		        } );
		        add(but);
	        JButton saveButton = new JButton("Save");
	        saveButton.addActionListener((ActionListener) e-> {
	            	 String sierraid = sierraidField.getText();
	     	        String noun = nounField.getText();
	     	        String verb = verbField.getText();
	     	        String cond = condField.getText();
	     	        String seq = seqField.getText();
	     	        String talker = talkerField.getText();
	     	        String text = textField.getText();

	     	        Message message = new Message(sierraid, noun, verb, cond, seq, talker, text);
	     	        
	     	       if (!soundField.getText().equals("")) {
	     	            File sourceFile = new File(soundField.getText());
	     	            String destinationFolder = "games/"+Main.gameName+"/dialogs/";
	     	            String fileName = "Audio" + sierraid + "-" + noun + "-" + verb + "-" + cond + "-" + seq + ".mp3";
	     	            File destinationFile = new File(destinationFolder + fileName);

	     	            try {
	     	                Files.copy(sourceFile.toPath(), destinationFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
	     	               message.save();
	     	               SQLTable.doTheQueue();
	    	     	       saveButton.setVisible(false);
	     	            } catch (IOException ee) {
	     	                ee.printStackTrace();
	     	            }
	     	        }
	     	       
	     	       
	     	        
	        } );
	        add(saveButton);
	    }

	 

	    
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public void save() {
		Map<String,String> val = new HashMap<>();  
		val.put("sierraid", sierraid);     
		val.put("noun", noun);  
		val.put("verb", verb);  
		val.put("cond", cond);  
		val.put("seq", seq);  
		val.put("talker", talker);  
		val.put("text", text); 
		SQLTable tabl = SQLTable.getTable("message");
		tabl.queue(val);
	}
	
	
	
	
	public static void updateTitles(String title,String type, int number){
		
	}
	public static String getTitles( String type, int number){ 
		return Editor.getGameInfo().duoManager.getString("msgtitles", type+number);
	}
	
	
	public static void importFromFolder(String folderPath) {
	    File folder = new File(folderPath);
	    File[] files = folder.listFiles();

	    if (files == null) {
	        System.out.println("Folder is empty or does not exist.");
	        return;
	    }
	    boolean addOnly = !Main.getUserConfirmation("replace existing ? no=addOnly");
	     
	    	String language = "default";
	    	if(new File("games/" + Main.gameName + "/dialogs/").exists())
				 language = Main.getUserInput("language?");
	    	File destinationFolder = new File( "games/"+Main.gameName+"/dialogs/"+language+"/");
	    	destinationFolder.mkdir();
	    	try { Utils.copyFolder(new File(folderPath), destinationFolder); } catch (IOException e) { e.printStackTrace(); }
	    	Editor.getGameInfo().addInArray("languages", language);
	    
	     
	    for (File file : files) {
	        if (file.isFile() && file.getName().toLowerCase().endsWith(".xml")) {
	            importFromFile(file.getPath(),addOnly);
	        }
	    } 
	    Main.getUserConfirmation("work finished!");
	}
	
	
	
	public static void translate(String lang) {
		File folder = new File("games/"+Main.gameName+"/dialogs/"+lang);
	    File[] files = folder.listFiles();
		 for (File file : files) {
		        if (file.isFile() && file.getName().toLowerCase().endsWith(".xml")) {
		            importFromFile(file.getPath(),false);
		        }
		    } 
	}
	public static void importFromFile(String path,boolean addOnly) {try {
	    List<Integer> presentId = new ArrayList<>(); 
	    for (Message m : getAlls())  
	        if (!presentId.contains(m.sierraId()))  
	            presentId.add(m.sierraId());
	         
	    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	    
	        dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
	        DocumentBuilder db = dbf.newDocumentBuilder();
	        Document doc = db.parse(new File(path));
	        doc.getDocumentElement().normalize();

	        NodeList messageNodes = doc.getElementsByTagName("Message");
	        for (int i = 0; i < messageNodes.getLength(); i++) {
	            Node messageNode = messageNodes.item(i);
	            if (messageNode.getNodeType() == Node.ELEMENT_NODE) {
	                Element messageElement = (Element) messageNode;

	                String id = messageElement.hasAttribute("ID") ? messageElement.getAttribute("ID") : null;
boolean idExists = false;
	                if (id != null) {
	                    
	                    for (int existingId : presentId)  
	                        if (existingId == Integer.parseInt(id)) {
	                            idExists = true;
	                            break;
	                        }
	                    
	                    if (idExists && addOnly) {
	                        System.out.println("ID already exists: " + id);
	                        continue;   
	                    } else if (!idExists) {
	                        presentId.add(Integer.parseInt(id));
	                        Utils.log("Adding new message with ID: " + id + " from " + path);
	                    }
	                }

	                NodeList childNodes = messageElement.getChildNodes();
	                for (int j = 0; j < childNodes.getLength(); j++) {
	                    Node childNode = childNodes.item(j);
	                    if (childNode.getNodeType() == Node.ELEMENT_NODE) {
	                        Element childElement = (Element) childNode;

	                        String noun = childElement.getAttribute("Noun");
	                        String verb = childElement.getAttribute("Verb");
	                        String cond = childElement.getAttribute("Cond");
	                        String seq = childElement.getAttribute("Seq");
	                        String talker = childElement.getAttribute("Talker");
	                        String text = childElement.getTextContent();

	                        Message existingMessage = findMessageByAttributes(id, noun, verb, cond, seq, talker);
	                        if (existingMessage != null && !addOnly)  
	                            existingMessage.line.update("text", text); 
	                         else if (!idExists)  
	                            new Message(id, noun, verb, cond, seq, talker, text).save();
	                         
	                    }
	                }
	            }
	        }

	    } catch (Exception e) { e.printStackTrace(); } }

	private static Message findMessageByAttributes(String id, String noun, String verb, String cond, String seq, String talker) {
	    for (Message m : getAlls())  
	        if (m.sierraid.equals(id) && m.noun .equals(noun) && m.verb .equals(verb) && m.cond .equals(cond)
	                && m.seq .equals(seq) )  
	            return m;
	        
	    return null;
	}

	public int talker() { 
		try {
			return Integer.parseInt(talker);
		} catch (NumberFormatException e) {
			return -1;
		}
	}
	public int noun() { 
		try {
			return Integer.parseInt(noun);
		} catch (NumberFormatException e) {
			return -1;
		} 
	}
	public int seq() {
		try {
			return Integer.parseInt(seq);
		} catch (NumberFormatException e) {
			return -1;
		} 
	}
	public int verb() {
		try {
			return Integer.parseInt(verb);
		} catch (NumberFormatException e) {
			return -1;
		} 
	}
	public int sierraId() {
		try {
			return Integer.parseInt(sierraid);
		} catch (NumberFormatException e) {
			return -1;
		} 
	}
	public int cond() {
		try {
			return Integer.parseInt(cond);
		} catch (NumberFormatException e) {
			return -1;
		} 
	}
	public String text() { 
		
		return text;
	}


	public static List<Message> getAlls( ) {return getAll(instance);}
	
	


@Override public List<GameObject> getChilds() {
	// TODO Auto-generated method stub
	return new ArrayList();
}

@Override GameObject instanciate(SQLLine l) {  return new Message(l); }

@Override boolean isValidPossessor(SQLLine l, GameObject possessor) {
	// TODO Auto-generated method stub
	return false;
}

@Override boolean isValidElement(SQLLine l) { return true; }

@Override
public String getTable() { return "message"; }
public static Message instance = new Message();
@Override GameObject default_instance() { 
	return instance;
}
public static Message get(int id){
	return get(instance, id);
}
@Override public String getFolder() { 
	return "games/"+Main.gameName+"/dialogs/";
}
 
 
public static Integer getSelected(String s) {
	 
	try {
		return Integer.parseInt(s);
	} catch (NumberFormatException e) {
		for(DuoArray<String, String> d:Editor.getGameInfo().duoManager.readArray("msgtitles"))
			if(d.getValue().equals(s)){
				if(d.getKey().contains("noun"))
					return Integer.parseInt(d.getKey().replaceAll("noun", ""));
				if(d.getKey().contains("verb"))
					return Integer.parseInt(d.getKey().replaceAll("verb", "")); 
				if(d.getKey().contains("sid"))
					return Integer.parseInt(d.getKey().replaceAll("sid", ""));
				if(d.getKey().contains("talker"))
					try {
						return Integer.parseInt(d.getKey().replaceAll("talker", ""));
					} catch (NumberFormatException e1) {
						for(Character chare : Character.getAll())
							if(chare.title().equals(s ))
								return chare.talker();
					}
		}
		 
	}
	for(Character chare : Character.getAll())
		if(chare.title().equals(s ))
			return chare.talker();
	return null;
}
public static List<String> getNouns() {
	List<String> temp = new ArrayList<>();
	  
	for( GameObject  m : getAll(instance)){
		Message message = (Message)m;
		if(message.talker()==-1)continue;
		String rename = Editor.getGameInfo().duoManager.getString("msgtitles", "noun"+message.noun());
		 if(rename == null)
			 rename = message.noun()+"";
		 if(!temp.contains(rename) )
				temp.add( rename);
	}
	return temp;
}
public static List<String> getVerbs() {
	List<String> temp = new ArrayList<>();
	  
	for( GameObject  m : getAll(instance)){
		Message message = (Message)m;
		if(message.talker()==-1)continue;
		String rename = Editor.getGameInfo().duoManager.getString("msgtitles", "verb"+message.verb());
		 if(rename == null){
			 rename = message.verb()+"";
		 }
		 if(!temp.contains(rename) )
		 temp.add( rename);
	}
	return temp;
} 
public static List<String> getSids() {
	List<String> temp = new ArrayList<>();
	  
	for( GameObject  m : getAll(instance)){
		Message message = (Message)m;
		if(message.talker()==-1)continue;
		String rename = Editor.getGameInfo().duoManager.getString("msgtitles", "sid"+message.sierraId());
		 if(rename == null){
			 rename = message.sierraId()+"";
		 }
		 if(!temp.contains(rename) )
		 temp.add( rename);
	}
	return temp;
}


public static List<String> getTalkers() {
	List<String> temp = new ArrayList<>();
	  
	for( GameObject  m : getAll(instance)){
		Message message = (Message)m;
		if(message.talker()==-1)continue;
		String rename = Editor.getGameInfo().duoManager.getString("msgtitles", "talker"+message.talker());
		for(Character chare : Character.getAll())
			if(chare.talker() == message.talker())
				rename = chare.title();
		 if(rename == null){
			 rename = message.talker()+"";
		 }
		 if(!temp.contains(rename) )
		 temp.add( rename);
	}
	return temp;
}


@Override public String title() {
	if(text.length()>100)
		return text.substring(0, 100).replaceAll("\"", "");
	return text();
}
 
@Override public void updateTitle(String text) {
	// TODO Auto-generated method stub
	
}
 

@Override  public 	Set<AshType> possibleGetters() {
	Set<AshType> temp = new HashSet<>();
 
	temp.add(AshType.HAS_BEEN_SAID ); 
	return temp;
}
@Override  public 	Set<AshType> possibleRemovers() {
	Set<AshType> temp = new HashSet<>();
 
	temp.add(AshType.HAS_BEEN_SAID ); 
	return temp;
}
@Override  public 	Set<AshType> possibleSetters() {
	Set<AshType> temp = new HashSet<>();
 
	temp.add(AshType.WILL_SAY ); 
	temp.add(AshType.WILL_SAY_UNFREEZE ); 
	return temp;
}
}
