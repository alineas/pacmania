package me.nina.gameobjects;

import java.util.List;
import java.util.Set;

import me.nina.sqlmanipulation.SQLLine;

public class StairList extends Zone {

	public StairList(int x, int y, int i, int j, ZoneType type) {
		super(x, y, i, j, type);
	}

	
	

	public StairList(Zone z) {
		super(z.x(),z.y(),z.i(),z.j(),z.getType()); 
		setP3(z.p3);
	}




	public StairList(String sql, int id) {
		super(sql,id);
	}




	@Override
	boolean isValidPossessor(SQLLine l, GameObject owner) { 
		return l.getInt("type") == ZoneSort.STAIRLIST.getValeur() && l.getInt("possessor") == owner.id;
	}

	@Override
	boolean isValidElement(SQLLine l) { 
		 if (l.getInt("type") == ZoneSort.STAIRLIST.getValeur())
     		return true;
		return false;
	}

static StairList instance = new StairList(0, 0, 0, 0, ZoneType.LINE);
	@Override
	GameObject default_instance() { 
		return instance;
	}
 


	public static List<Zone> getByOwner(int ownerId){
		 return getbyPossessor(instance,GameObject.getById(ownerId));
	}
 
	
	public static List<Zone> getAll( ) {
	    return getAll(instance);
	}
	public static Zone get( int id) {
	    return get(instance,id);
	}
	@Override public void delete(){
		for(Zone s : Stair.getAll()){
			Stair stair = (Stair) s;
			stair.primaryZones.remove(this);
			stair.secondaryZones.remove(this); 
		}
		super.delete();
	}
}
