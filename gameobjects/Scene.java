package me.nina.gameobjects;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import me.nina.editors.SceneTpointLinker;
import me.nina.gameobjects.GameObject;
import me.nina.gameobjects.Character.PosType; 
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Main;
import me.nina.maker.Utils;
import me.nina.objects2d.Img;
import me.nina.objects2d.ImgUtils;
import me.nina.sqlmanipulation.DuoArray;
import me.nina.sqlmanipulation.SQLLine;
import me.nina.sqlmanipulation.SQLTable;

public class Scene extends GameObject { private static final long serialVersionUID = 1L;
private String title; 
	public Scene(SQLLine line) { super(line); }

	
	
	public void delete() { 
		if(!Main.getUserConfirmation("are you sure ?"))return;
		for(Zone z: Zone.getByOwner(id))
 			z.delete();
		for(Zone z: TransitionPoint.getByOwner(id))
 			z.delete();
		for(GameObject a : BasicObject.getObjects(this))
			a.delete(); 
		super.delete();} 
	
	public Scene(String title) {
		 
		this.title=title;
	}
	public int save() {
		Map<String,String> val = new HashMap<>(); 
		val.put("title", title);  
		int id =  SQLTable.getTable("scene").insert(val);
		//Ash.setAsh(title, Ash.AshType.HAS_BEEN_VISITED, false, get(id));
		SceneTpointLinker.addScenePosition(id);
		return id;
	}
	@Override public void updateTitle(String text,boolean renameFolder){ 
		for(Scene scene : Scene.getScenes())
			if(scene.image().contains(title())) {
				scene.updateImage(scene.image().replaceAll(title(), text));
				scene.updateImage2(scene.image().replaceAll(title(), text));
			}
		super.updateTitle(text, renameFolder); 
	}
	private void updateImage(String s) { 
		line.update("img", s);
	}
	private void updateImage2(String s) { 
		line.update("img2", s);
	}

	public boolean replaceSong() { 
		return   line .getString("replacesong").equals("true") ;
	} 
	public void updateReplaceSong(boolean replace) { 
		  line .update("replacesong",replace+"")   ;
	} 
	public int scalefactorBotLimit() {return line.duoManager.getInt("scalefactor", "scalefactorbotlimit");}
	public int scalefactorTopLimit() {return line.duoManager.getInt("scalefactor", "scalefactortoplimit");}
	public int scalePercents() {return line.duoManager.getInt("scalefactor", "scalepercents");}
	public List<DuoArray<String, String>> zindexes(){return line.duoManager.readArray("zindexes");}
	public BufferedImage bgImage(){
		String name = line.getString("img");
		if(! name.contains("/"))
			return ImgUtils.getPicture(getFolder()+name);
		return ImgUtils.getPicture( name);//copieds scenes call orininal image path
	}
	public BufferedImage priorityBgImage(){
		String name = line.getString("img2");
		if(! name.contains("/"))
			return ImgUtils.getPicture(getFolder()+name);
		return ImgUtils.getPicture( name);//copieds scenes call orininal image path
	}
	 public String bgName() { return line.getString("img"); }
 

	public void updateScaleFactors(int top, int bot, int factor) {
		line.duoManager.setValue("scalefactor", "scalepercents", factor+"");
		line.duoManager.setValue("scalefactor", "scalefactorbotlimit", bot+"");
		line.duoManager.setValue("scalefactor", "scalefactortoplimit", top+"");
	}



	

	public void updateBackground(String name) { line .update( "img", name); } 
	 public String priorityBg() {  return line .getString("img2"); } 
	public void updatePriorityBg(String name) { line .update( "img2", name); } 
	public int getZindex(int rgb) { return line.duoManager.getInt("zindexes",rgb+""); }
	public void updateZindex(int selectedItem, String s) { line.duoManager.setValue("zindexes",selectedItem+"",s); }
	
	@Override public String getFolder() {
		String path = "games/"+Main.gameName+"/scenes/"+title()+"/";
		new File(path).mkdirs();
		return  path ; 
	}
	public String music() {  return line .getString("music"); } 
	public void updateMusic(String name) { line .update( "music", name); } 

  
	public int loopMusic() {  return line .getInt("loopmusic") ; } 
	public void updateLoopMusic(int b) { line .update( "loopmusic", b+""); }  


	 
@Override public List<GameObject> getChilds() {
	List<GameObject> liste = new ArrayList<>() ;
	
	List<Zone> lst = Zone.getByOwner(id);
	for(Zone z : lst) {
		/*for(AshType a : AshType.values()) {
			if(z.ash(a) != -1) 
				liste.add(Ash.get(z.ash(a)));
			if(ash(a) != -1) 
				liste.add(Ash.get(ash(a)));
		}*/
		liste.add(z);
			
	}
	
	lst = TransitionPoint.getByOwner(id);
	for(Zone z : lst) {
		/*for(AshType a : AshType.values()) {
			if(z.ash(a) != -1) 
				liste.add(Ash.get(z.ash(a)));
			
		}*/
		liste.add(z);
			
	}
	
	lst = Stair.getByOwner(id);
	for(Zone z : lst) {
		/*for(AshType a : AshType.values()) {
			if(z.ash(a) != -1) 
				liste.add(Ash.get(z.ash(a)));
			
		}*/
		liste.add(z);
			
	}
	 
	liste.addAll(BasicObject.getObjects((Scene)this));
	return liste;
}
 
@Override
GameObject instanciate(SQLLine l) { 
	return new Scene(l);
}

@Override
boolean isValidPossessor(SQLLine l, GameObject possessor) { 
	return false;
}

@Override
boolean isValidElement(SQLLine l) {
	return true;
}

@Override
String getTable() {
	return "scene";
}
static Scene instance= new Scene("");
@Override
GameObject default_instance() { 
	return instance;
}

public static Scene get(int id) { 
	return get(instance,id);
} 

public static List<Scene> getScenes() {  
	return getAll(instance);
}



@Override public void updateTitle(String text) { 
	for(GameObject a : Zone.getAll())
		if(a.title().contains(title())) 
			a.updateTitle(a.title().replaceAll(title(), text));
	for(GameObject a : TransitionPoint.getAll())
		if(a.title().contains(title())) 
			a.updateTitle(a.title().replaceAll(title(), text));
	super.updateTitle(text,true); 
}
 


@Override  public 	Set<AshType> possibleGetters() {
	Set<AshType> temp = new HashSet<>();
	temp.add(AshType.SCENE_HAS_BEEN_VISITED ); 
	temp.add(AshType.SCENE_HAS_BEEN_ENTERED ); 
	temp.add(AshType.SCENE_WAS_EXITED ); 
	temp.add(AshType.IS_ACTUAL_SCENE ); 
	temp.add(AshType.SCENE_HAS_BEEN_CLICKED );
	return temp;
}
@Override  public 	Set<AshType> possibleSetters() {
	Set<AshType> temp = new HashSet<>();
	temp.add(AshType.SCENE_HAS_BEEN_VISITED ); 
	 
	return temp;
}
@Override  public 	Set<AshType> possibleRemovers() {
	Set<AshType> temp = new HashSet<>();
	temp.add(AshType.SCENE_HAS_BEEN_VISITED ); 
	return temp;
}
}
