package me.nina.gameobjects;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
 
import me.nina.editors.Editor;
import me.nina.gameobjects.Animation.AnimationState;
import me.nina.gameobjects.BasicObject.ObjectType;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Main;
import me.nina.maker.Utils;
import me.nina.sqlmanipulation.SQLLine;
import me.nina.sqlmanipulation.SQLTable;

public class InventoryObject extends BasicObject { private static final long serialVersionUID = 1L;
public String title;
private int type;
public String fileName;
static SQLTable table = SQLTable.getTable("basicobject");
	 
	public InventoryObject(String title) {
		super( );
		this.title=title;
		this.type=ObjectType.INVENTORY.getValeur();
		
	} 
	public InventoryObject() {
		// TODO Auto-generated constructor stub
	}
	public InventoryObject(SQLLine l) {
		super(l);
	}
	public BasicObject save() {
		final Map<String,String> val = new HashMap<>(); 
		val.put("title", title);  
		val.put("type", type+"");  
		int id =  table.insert(val);
		//Ash.setAsh( title,AshType.HAS_IN_HAND,false,get(id));
		//Ash.setAsh( title,AshType.HAS_IN_INVENTORY,false,get(id));
		//Ash.setAsh( title,AshType.IS_CLICKED_ON,false,get(id));
		val.clear(); 
		
		return get(id);
	}
	public void populate() {   //in game context 
		 
 		Utils.log("populate called in inventory object");
		 
	}
	
	public void setSelectedState(ObjectState selectedState) {
		this.selectedState = selectedState;
		}
	public void generateInitialState( ) {
		Map<String,String> val = new HashMap<>(); 
		val.put("title", title()+"_initial_state");  
		val.put("possessor", id+""); 
		 
		int id2=SQLTable.getTable("objectstate").insert(val);
		get(id).updateInitialState(id2);
		
	}
	public ObjectState initialState() {return ObjectState.get(line.getInt("initialstate"));}

	
	
	@Override public String getFolder() {
		if(type()!= ObjectType.INVENTORY )return null;
		String path = "games/"+Main.gameName+"/invobjects/";
		new File(path).mkdirs();
		return  path ; 
	}
 
 
	public static List<BasicObject> getAll() {return getAll(instance); } 
	 
 
	@Override boolean isValidElement(SQLLine l) { 
		return l.getInt("type") == ObjectType.INVENTORY.getValeur();
	}
	@Override boolean isValidPossessor(SQLLine l, GameObject possessor) {
		return false; //inv have no possessor
	}
	public static InventoryObject instance  = new InventoryObject();
	public Zone zone;//in game context
	@Override GameObject default_instance() { 
		return instance;
	}
	@Override GameObject instanciate(SQLLine l) {
		if( l.getInt("type") == ObjectType.INVENTORY.getValeur())  
			return new InventoryObject(l);  
		 
		return new BasicObject(l);
		
	}
	public static BasicObject get(int id) {return get(instance,id);}
	public static int addSpecialZone(ObjectState selectedState, SpecialZoneInv t) {
		 
		List<Zone> array = SpecialZoneInv.getByOwner(selectedState.id);
		 
		if(t.frames.get(0) == -1) { 
			for(Zone a:array)
				a.delete();
			return t.save(selectedState.id,selectedState.title()+"_zone_"+SQLLine.getUniqueID()); 
		}
		for(Zone a : array) {  
			if(a.equals(t)) {
				((SpecialZoneInv)a).frames.add(t.frames.get(0));
				 
				a.update(a.toSqlValues());
				return a.id;
			}}
		 
		return t.save(selectedState.id,selectedState.title()+"_zone_"+SQLLine.getUniqueID()); 
		 
		 
		
		
		
		
	}

	public void generateNewState(String title, String fileName) {
		Map<String,String> val = new HashMap<>();  
		val.put("title", title);  
		val.put("possessor", id+"");   
		int stateId = SQLTable.getTable("objectstate").insert(val);
		//Ash.setAsh(title,AshType.ACTUAL_STATE,false,ObjectState.get(stateId)); 
		int a = Animation.generateAnimation(this, title, AnimationState.LOOP.getValeur(), fileName);
		ObjectState state = ObjectState.get(stateId);
		state.updateAnimID(a);  
		Animation.get(state.animID()).updatePossessor(stateId);
	}

	public void updateInitialAnim(String title, String fileName, AnimationState st) {
		int id = Animation.generateAnimation(this, title, st.getValeur(), fileName);
    	initialState().updateAnimID(id);  
    	Animation.get(initialState().animID()).updatePossessor(initialState().id);
	}
	public void setZone(Zone zone2) {
		this.zone=zone2; //in game context
	}

	
	
	@Override public Set<AshType> possibleGetters() {
		Set<AshType> temp = new HashSet<>();
		temp.add(AshType.HAS_IN_HAND ); 
		temp.add(AshType.IS_CLICKED_ON ); 
		temp.add(AshType.HAS_IN_INVENTORY); 
		return temp;
	}
	@Override public Set<AshType> possibleRemovers() {
		Set<AshType> temp = new HashSet<>(); 
		temp.add(AshType.HAS_IN_INVENTORY); 
		return temp;
	}
	@Override public Set<AshType> possibleSetters() {
		Set<AshType> temp = new HashSet<>(); 
		temp.add(AshType.WILL_HAVE_IN_INVENTORY); 
		return temp;
	}
	
}
