package me.nina.gameobjects;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import me.nina.maker.Ashe.AshType;
import me.nina.sqlmanipulation.SQLLine;
import me.nina.sqlmanipulation.SQLTable;

public class MegaAction extends GameObject {

	static MegaAction instance = new MegaAction("");
	 

	private String title; 
	
	public MegaAction(SQLLine line) {
		super(line);
	}
	public MegaAction(String title) {
		this.title=title;
	}
	
	public MegaAction save ( ) {
		Map<String,String> val = new HashMap<>();  
		val.put("title", title);  
		SQLTable tabl = SQLTable.getTable("megaaction");
		int i = tabl.insert(val); 
		return GameObject.get(default_instance(),i);
	}
	
	
 
	public String title(){
		String s = line.getString("title");
		if(s.equals(""))s="no title defined";
		return s;}
	
	
	public void updateTitle(String text ){  
		line.update("title", text); 
	}
	public List<Action> getActions(){
		List<Action> temp = new ArrayList<>();
		for(Action a : Action.getAll())
			if(a.megaOwner() == this.id)
				temp.add(a);
				
		return temp;
		
	}
	public static List<MegaAction> getAll() {
		 
		return getAll(instance);
	}
	 
	public static MegaAction get(int i) {
		 
		return get(instance,i);
	}
	
	
	@Override
	GameObject instanciate(SQLLine l) { 
		return new MegaAction(l);
	}
	@Override
	boolean isValidPossessor(SQLLine l, GameObject possessor) { 
		return  l.getInt("megaowner") == possessor.id;
	}
	@Override
	boolean isValidElement(SQLLine l) {
		// TODO Auto-generated method stub
		return true;
	}
	@Override
	String getTable() {
		// TODO Auto-generated method stub
		return "megaaction";
	}
	@Override
	GameObject default_instance() { 
		return instance;
	}
	@Override
	public String getFolder() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Set<AshType> possibleGetters() {
		// TODO Auto-generated method stub
		return new HashSet<>();
	}
	@Override
	public Set<AshType> possibleSetters() {
		return new HashSet<>();
	}
	@Override
	public Set<AshType> possibleRemovers() {
		return new HashSet<>();
	}
	@Override
	public List<GameObject> getChilds() {
		List<GameObject> temp = new ArrayList<>();
		for(Action a : Action.getAll())
			if(a.megaOwner()==id)
				temp.add(a);
		return temp;
	}
}
