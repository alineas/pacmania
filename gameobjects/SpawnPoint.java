package me.nina.gameobjects;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import me.nina.gameobjects.Zone.ZoneSort;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Ashe.AshType;
import me.nina.sqlmanipulation.SQLLine;

public class SpawnPoint extends Zone {
//bad naming, its juste a point for the actions
	public SpawnPoint(int x, int y, int i, int j, ZoneType type) {
		super(x, y, i, j, type); 
	}

	
	



	public SpawnPoint(String s, int id) {
		super(s,id);
	}






	@Override
	boolean isValidPossessor(SQLLine l, GameObject owner) { 
		return l.getInt("type") == ZoneSort.SPAWN_POINT.getValeur() && l.getInt("possessor") == owner.id;
	}

	@Override
	boolean isValidElement(SQLLine l) { 
		 if (l.getInt("type") == ZoneSort.SPAWN_POINT.getValeur())
     		return true;
		return false;
	}

static SpawnPoint instance = new SpawnPoint(0, 0, 0, 0, ZoneType.LINE); 
	@Override
	GameObject default_instance() { 
		return instance;
	}
 


	public static List<Zone> getByOwner(int ownerId){
		 return getbyPossessor(instance,GameObject.getById(ownerId));
	}
 
	
	public static List<Zone> getAll( ) {
	    return getAll(instance);
	}
	public static Zone get( int id) {
	    return get(instance,id);
	}
	

	
}
