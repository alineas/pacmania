package me.nina.gameobjects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import me.nina.maker.Ashe;
import me.nina.maker.Ashe.AshType;
import me.nina.editors.Editor;
import me.nina.gameobjects.Character.PosType;
import me.nina.maker.Main;
import me.nina.objects2d.Position;
import me.nina.sqlmanipulation.SQLLine;
import me.nina.sqlmanipulation.SQLTable;

public class Chapter extends GameObject{
	 
	  
	public Animation animationBeforeStart;
	public Animation animationWhenStart;
	public Animation animationWhenEnd;
	public List<InventoryObject> neededItems; 
	int characterId; 

	private String title; 
	
	 
	public Chapter(SQLLine l) {
		super(l);
	}
 
	public Chapter(String title) {
		this.title=title;
	}

	
	
	public int save() {
		Map<String,String> val = new HashMap<>(); 
		val.put("title", title); 
		int sceneId = Scene.getScenes().iterator().next().id; 

 		val.put("positionstart", new Position(100,100,0).toSqlValues());
		val.put("scenestart", sceneId+"");
		val.put("number", ""+(Main.chapterChooser.getItemCount()+1));
		SQLTable tabl = SQLTable.getTable("chapters");
		return tabl.insert(val);
	}
	
	
	public Scene startScene() {
		return Scene.get(line.getInt("scenestart"));
	}
	public Position positionStart() { 
		try {
			return new Position(line.getString("positionstart"));
		} catch (Exception e) {
			return new Position(200,200,PosType.BOTTOM.getValeur());
		}
	}
 
	public int number() {
		return line.getInt("number");
	}
	
 
	
	public static List<Chapter> getAll() { 
		return getAll(instance);
			
	}
	public static Chapter get(int id) {
		return get(instance,id);
	} 
	public void updateStartPosition(Position p) {
		line.update("positionstart",p.toSqlValues());
	}

	public void updateStartScene(Scene scene) {
		line.update("scenestart",scene.id+"");
	}
 
	public void updateNumber(String nbr) {
		line.update("number",nbr); 
	}
	public void updateFirstAction(int nbr) {
		line.update("firstaction",nbr+""); 
	}
	public Action firstAction() {
		return Action.get(line.getInt("firstaction"));
	}
	
	
	
	public void updateCharacter(int id) {
		line.update("character",id+""); 
	}
 
	
	
	
	@Override
	public List<GameObject> getChilds() {
		List<GameObject> liste = new ArrayList<>() ;
		liste.add(startScene()); 
		//for(a:neededItems())
		//for ash : position 
		return liste;
	}

	@Override
	public String getFolder() {
		// TODO Auto-generated method stub
		return null;
	}

 

	@Override
	GameObject instanciate(SQLLine l) { 
		return new Chapter(l);
	}

 

	@Override
	boolean isValidElement(SQLLine l) { 
		return true;
	}

	@Override
	String getTable() { 
		return "chapters";
	}
static Chapter instance = new Chapter("");
	@Override
	GameObject default_instance() { 
		return instance;
	}

	@Override
	boolean isValidPossessor(SQLLine l, GameObject possessor) {
		// TODO Auto-generated method stub
		return false;
	}




	@Override  public 	Set<AshType> possibleGetters() {
		Set<AshType> temp = new HashSet<>();
		return temp;
	}
	@Override  public 	Set<AshType> possibleSetters() {
		Set<AshType> temp = new HashSet<>();
		temp.add(AshType.ENTER_CHAPTER);
		return temp;
	}
	@Override  public 	Set<AshType> possibleRemovers() {
		Set<AshType> temp = new HashSet<>();
		return temp;
	}

	public List<Ashe> getInitialAshes() {
		// TODO Auto-generated method stub
		return Ashe.getlist(Editor.getGameInfo().getString("initialashes"));
	}


}
