package me.nina.gameobjects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import me.nina.gameobjects.Zone.ZoneSort;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Utils;
import me.nina.objects2d.Point;
import me.nina.sqlmanipulation.SQLLine;
import me.nina.sqlmanipulation.SQLTable;
import me.nina.sqlmanipulation.SQLWrapper;

public class Stair extends Zone {

	
	
	public void delete() { 
		//for(AshType a:AshType.values())
			//if(ash(a) != -1)
				//Ash.get(ash(a)).line.delete();
		
		for(StairList a : primaryZones)
			a.delete();
		
		for(StairList a : secondaryZones)
			a.delete();
		line.delete();} 
	
	public Zone copy() {
		Stair temp = new Stair();
		temp.primaryZones.addAll(primaryZones) ;
		temp.secondaryZones.addAll(secondaryZones) ;
		temp.priorities.addAll(priorities) ;
		 temp.level= level;
		return temp;
	} 
	public List<StairList> primaryZones; 
	public List<StairList> secondaryZones;
	public List<Integer> priorities;
	public int level = 0;
	  
	
	public Stair(String sql,int id) {
		super("0,0,0,0,-1",id);
		primaryZones = new ArrayList<>(); 
		secondaryZones = new ArrayList<>();
		priorities = new ArrayList<>();
		String pzones = sql.split("@")[0];
		String szones = sql.split("@")[1];
		String pris = sql.split("@")[2];
		String lev = sql.split("@")[3]; 
		for(String a : pris.split(","))if(!a.equals(""))
			priorities.add(Integer.parseInt(a));
		for(String a : pzones.split("\\*"))if(!a.equals(""))
			primaryZones.add((StairList) StairList.get(Integer.parseInt(a)));
		for(String a : szones.split("\\*"))if(!a.equals(""))
			secondaryZones.add((StairList) StairList.get(Integer.parseInt(a)));
		level = Integer.parseInt(lev);  
	}
	
	
	
	public Stair() {
		super(0,0,0,0,ZoneType.LINE);
		primaryZones = new ArrayList<>(); 
		secondaryZones = new ArrayList<>();
		priorities = new ArrayList<>();
	}



	@Override public String toSqlValues() {
		String ret = "";
		for(StairList a : primaryZones)
			ret=ret+"*"+a.line.id; 
		ret=ret+"@";
		
		for(StairList a : secondaryZones)
			ret=ret+"*"+a.line.id; 
		ret=ret+"@";
		
		for(int a : priorities)
			ret=ret+","+a ; 
		ret=ret+"@"+level ;
		
		return ret;
	}
	
	public void update(String sql) {
		primaryZones = new ArrayList<>(); 
		secondaryZones = new ArrayList<>();
		priorities = new ArrayList<>();
		String pzones = sql.split("@")[0];
		String szones = sql.split("@")[1];
		String pris = sql.split("@")[2];
		String lev = sql.split("@")[3]; 
		for(String a : pris.split(","))if(!a.equals(""))
			priorities.add(Integer.parseInt(a));
		for(String a : pzones.split("\\*"))if(!a.equals(""))
			primaryZones.add((StairList) Zone.get(Integer.parseInt(a)));
		for(String a : szones.split("\\*"))if(!a.equals(""))
			secondaryZones.add((StairList) Zone.get(Integer.parseInt(a)));
		level = Integer.parseInt(lev);  
		line.update("value", sql);} 
 


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + level;
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Stair other = (Stair) obj;
		if (level != other.level)
			return false;
		return true;
	} 
	public static Stair DEFAULT_INSTANCE=new Stair();
	
	public int getLevel() {
		return level;
	}


 
	@Override public Stair instanciate(String sql, int id) { return new Stair(sql,id); } 
	@Override public Stair getObject() { return this; }  
	
	
	
	
	
	
	
	
	
	
	
	
	
 

	@Override
	boolean isValidPossessor(SQLLine l, GameObject owner) { 
		return l.getInt("type") == ZoneSort.STAIR.getValeur() && l.getInt("possessor") == owner.id;
	}

	@Override
	boolean isValidElement(SQLLine l) { 
		 if (l.getInt("type") == ZoneSort.STAIR.getValeur())
     		return true;
		return false;
	}

static Stair instance = new Stair();
	@Override
	GameObject default_instance() { 
		return instance;
	}
 


	public static List<Zone> getByOwner(int ownerId){
		 return getbyPossessor(instance,GameObject.getById(ownerId));
	}
 
	
	public static List<Zone> getAll( ) {
	    return getAll(instance);
	}
	public static Zone get( int id) {
	    return get(instance,id);
	}
	
	
	
	
}
