package me.nina.gameobjects; 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import me.nina.gameobjects.Zone.ZoneSort;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Utils;
import me.nina.objects2d.Point;
import me.nina.objects2d.Position;
import me.nina.sqlmanipulation.SQLLine;
import me.nina.sqlmanipulation.SQLTable;
import me.nina.sqlmanipulation.SQLWrapper; 

public class TransitionPoint extends Zone  {  
 
	private Position desiredPlacement; 		// column = "transitionpoints" 
	private final int sourceScene;
	private int destinationScene;
	private final int sourceId;
	private int destinationId;
	private int direction;  
	
 	
	
	//##########################################################
	public TransitionPoint(int x, int y, int i, int j, ZoneType type, //the rectangle
			int desiredPlacementX, int desiredPlacementY, //the position in the rectangle for the transition
			int sourceScene, int destinationScene,int sourceId,int destinationId,int direction) {  //the scene where the rectangle is
			super(x, y, i, j, type);
		   
		this.destinationScene=destinationScene;
		this.sourceId=sourceId;
		this.destinationId=destinationId;
		this.direction=direction;
		this.desiredPlacement = new Position(desiredPlacementX,desiredPlacementY,direction);
		this.sourceScene = sourceScene; 
		
	}
	//##########################################################
	public TransitionPoint(String sql,int id) { 
		super(sql,id);
		String[] split = sql.split(",");
	   
		int pointX = Integer.parseInt(split[5]);
		int pointY = Integer.parseInt(split[6]); 
		
		int sourceSceneId = Integer.parseInt(split[7]);
		int sourcePointId = Integer.parseInt(split[8]);
		
		int destinationSceneId = Integer.parseInt(split[9]);
		int destinationPointId = Integer.parseInt(split[10]);
		
		int dir = Integer.parseInt(split[11]); 
 
		
		this.destinationScene=destinationSceneId;
		this.sourceId=sourcePointId;
		this.destinationId=destinationPointId;
		this.direction = dir; 
		this.desiredPlacement = new Position(pointX,pointY,dir);
		this.sourceScene = sourceSceneId; 
		  
	}

	//##########################################################
 
	public int getDestinationScene() { return destinationScene; } 
	public int getSourceId() { return sourceId; } 
	public int getDestinationId() { return destinationId; } 
	public void setDestinationScene(int id) { destinationScene = id; } 
	public void setDestinationId(int id) { destinationId = id; }
	public int getSourceScene() { return sourceScene; }
	public Position getDesiredPos() {return desiredPlacement;}
	public void setDesiredPos(Position p) { desiredPlacement = p;direction = p.pos.getValeur(); } 
	 
	//##########################################################
	
	@Override
	public String toSqlValues() {
		return 	 p1.x+","
				+p1.y+"," 
				+p2.x+","
				+p2.y+","
				+type.getValeur()+","
				+desiredPlacement.x+","
				+desiredPlacement.y+","
				+sourceScene+","
				+sourceId+","
				+destinationScene+","
				+destinationId+","
				+direction;
		  
		 
	}
  

	//##########################################################

	@Override public String toString() {
		return 	"tpoint sourceScene="+sourceScene+" sourceid="+  
				+sourceId+" destScene="
				+destinationScene+" destid="
				+destinationId+" ("+p1.toString()+","+p2.toString()+")";
	}
 



	@Override public TransitionPoint instanciate(String sql, int id) { return new TransitionPoint(sql,id); }
	@Override public TransitionPoint getObject() { return this; } 
	
	
	
 

	@Override
	boolean isValidPossessor(SQLLine l, GameObject owner) { 
		return l!=null&&owner!=null&&l.getInt("possessor") == owner.id   ;
	}

	@Override
	boolean isValidElement(SQLLine l) { 
		 if (l.getInt("type") == ZoneSort.TPOINT.getValeur())
     		return true;
		return false;
	}

static TransitionPoint instance = new TransitionPoint(0, 0, 0, 0, ZoneType.LINE, 0, 0, 0, 0, 0, 0, 0);
	@Override
	GameObject default_instance() { 
		return instance;
	}
 



	public static List<Zone> getByOwner(int ownerId){
		return getbyPossessor(instance,GameObject.getById(ownerId));  
		 
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + sourceId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TransitionPoint other = (TransitionPoint) obj;
		if (sourceId != other.sourceId)
			return false;
		return true;
	} 
	public static List<Zone> getAll( ) {
		return getAll(instance);
	}
	public static Zone get( int id) {
	    return get(instance,id);
	}
	@Override public String title() {
		try {
			return super.possessorObj().title()+"_TP_"+super.title();
		} catch (Exception e) {
			return super.title();
		}
	}


}
 