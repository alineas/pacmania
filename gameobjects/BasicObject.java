package me.nina.gameobjects;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.Border;

import me.nina.gameobjects.GameObject;
import me.nina.gameobjects.Animation.AnimationState;
import me.nina.gameobjects.BasicObject.ObjectType;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Main;
import me.nina.maker.Utils;
import me.nina.objects2d.Img;
import me.nina.sqlmanipulation.SQLLine;
import me.nina.sqlmanipulation.SQLTable;

public class BasicObject extends GameObject  { private static final long serialVersionUID = 1L;
	public Animation icone;//the icon for represent object in the scene in node editor
     public ObjectState selectedState;  
	private boolean started;//in game context
	private String title; 
     
     protected BasicObject(SQLLine line) { super(line);   }

	
 	public BasicObject() {
		// TODO Auto-generated constructor stub
	}


	public BasicObject(String title) {
		this.title=title;
	}
	public BasicObject save() {
		Map<String,String> val = new HashMap<>(); 
		val.put("title",title);
		val.put("type",ObjectType.GROUND.getValeur()+"");
		return get(SQLTable.getTable(getTable()).insert(val)); 
	}

	public static  enum ObjectType {
	     GROUND(0),INVENTORY(1);  
	    private final int valeur; 
	    ObjectType(int valeur) { this.valeur = valeur;  } 
	    public int getValeur() {  return valeur; }  
	    public static ObjectType from(String s) { 
	    	return from(Integer.parseInt(s));
	    }
	    public static ObjectType from(int valeur) {
	        for (ObjectType type : values()) {
	            if (type.getValeur() == valeur) {
	                return type;
	            }
	        }
	        throw new IllegalArgumentException("Aucun ObjectType correspondant � la valeur : " + valeur);
	    }
	    
	     
	    
	}

	//###############################################
		public void populate() {   //in game context 
			
			icone = Animation.get(getSelectedState().animID());   
			icone.setLocation(new java.awt.Point(getSelectedState().pos().x,getSelectedState().pos().y));
			 icone.rewindAll();
			if(!started) {
				icone.forwardAll();//at the start, dont play the reversed loop anim
				started=true;
			}
		
		
		}

	//###############################################
	public void populateFrames(double zoom) {    if(getSelectedState()==null)setSelectedState(this.initialState());
		
			 
			icone = Animation.get(getSelectedState().animID());
			icone.loadGif();
			 icone.setScale(zoom);
		 
		  
		icone.setLocation(new java.awt.Point((int) (selectedState.pos().x*zoom),(int) (selectedState.pos().y*zoom)));  

		
	}

	//###############################################
	ImageIcon getThumbmail(BufferedImage img,String name) {  
		//return new ImageIcon(img);
		return new ImageIcon(img.getScaledInstance(50, 50, Image.SCALE_SMOOTH));

	}

	//###############################################
	@Override public String getFolder() {
		String path = "";   
		if(type() ==ObjectType.GROUND)
			path = possessorObj().getFolder()+"basicobjects/"; 
		 
		new File(path).mkdirs();
		return  path ; 
	}
	

		
	 
	//###############################################
	//############################################### //database handling
	public ObjectState initialState() { return ObjectState.get(line.getInt("initialstate")); }
	public void updateInitialState(int id) { line.update("initialstate", id+"");  }
	public ObjectType type() { return ObjectType.from(line.getInt("type")); }  
	public void updateType(ObjectType typ) { line.update("type", typ.getValeur()+"");}
	 
	//###############################################
 
	//############################################### //statics

	public ObjectState getSelectedState() {
		if(selectedState == null)
			selectedState=initialState();
		return selectedState;
	}



	public void setSelectedState(ObjectState selectedState) {
		this.selectedState = selectedState;
		
		
		if(childs().size() != 0) {
			if(Animation.get(selectedState.animID()).state() == AnimationState.INVISIBLE)
				for(BasicObject a : childs()) { //is overrided by checkboxes in obj node populate
					hiddenObjects.add(a);
					 
				}
			else
				for(BasicObject a : childs()) {//is overrided by checkboxes in obj node populate
					hiddenObjects.remove(a);
					 
				}
		}
	}

 
	public void addChild(int id) { line.addInArray("childs", id+""); }
	public int zindex() { return line.getInt("zindex"); }
	public void updateZindex(int i) { line.update("zindex", i+"");}
	public List<BasicObject> childs() { 
		List<BasicObject> temp = new ArrayList<>();
		for(String a:line.getArray("childs") )if(!a.equals(""))
			temp.add(BasicObject.get(Integer.parseInt(a)));
		return temp;
	}

	private void removeChild(BasicObject rem) {
		line.remFromArray("childs", rem.id+"");
	}

 
  

		@Override public List<GameObject> getChilds() {
			List<GameObject> liste = new ArrayList<>() ; 
				liste.addAll(ObjectState.getByObject(id));
					 
			return liste;
		}
 
		@Override String getTable() { return "basicobject"; }
				static GameObject instance = new BasicObject();
		@Override GameObject default_instance() { 
			return instance;
		}

		@Override GameObject instanciate(SQLLine l) {
			if( l.getInt("type") == ObjectType.GROUND.getValeur())  
				return new BasicObject(l);  
			 
			return new InventoryObject (l);  
			
		}

		@Override boolean isValidElement(SQLLine l) {
			return l.getInt("type") != ObjectType.INVENTORY.getValeur(); 
		} 
		 
		@Override boolean isValidPossessor(SQLLine l, GameObject possessor) {
			return possessor.id == l.getInt("possessor") &&
					l.getInt("type") != ObjectType.INVENTORY.getValeur(); //inv have no possessor
		}
		 public static List<BasicObject> getObjects(Scene scene) {
			return getbyPossessor(instance , scene);
		}
		public static List<BasicObject> getAll() {return getAll(instance); } 
 
		public static BasicObject get(int id) {
			BasicObject temp = get(instance,id);
			if(temp!=null)return temp;
			return InventoryObject.get(id);
			}

		
		
	    public final static List<BasicObject> hiddenObjects = new ArrayList<>();  //in game context

		@Override
		public void delete() {if(!Main.getUserConfirmation("are you sure ?"))return;
		for(BasicObject b : BasicObject.getAll()) 
			if(b.childs().contains(this))
				b.removeChild(this);
			  
			for(BasicObject a : childs())
				a.delete();
			for(ObjectState a : ObjectState.getByObject(id))
				a.delete();
			super.delete();
		}


	

 

		


		@Override  public 	Set<AshType> possibleGetters() {
			Set<AshType> temp = new HashSet<>(); 
			temp.add(AshType.HAS_IN_HAND);
			return temp;
		}
		@Override public Set<AshType> possibleSetters() {
			Set<AshType> temp = new HashSet<>();
			temp.add(AshType.WILL_HAVE_IN_HAND);
			return temp;
		}
		@Override public Set<AshType> possibleRemovers() {
			Set<AshType> temp = new HashSet<>();
			temp.add(AshType.HAS_IN_HAND);
			return temp;
		}


		public boolean canBeFullscreen() {
			for(ObjectState a : ObjectState.getByObject(id)) { 
				if(a.isFullscreen())
					return true;
			}
			return false;
		}
		
} 

