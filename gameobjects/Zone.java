package me.nina.gameobjects;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.QuadCurve2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import me.nina.gameobjects.GameObject;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Utils; 
import me.nina.objects2d.Point;
import me.nina.sqlmanipulation.SQLLine;
import me.nina.sqlmanipulation.SQLTable;
import me.nina.sqlmanipulation.SQLWrapper; 



public class Zone extends GameObject implements SQLWrapper<Zone>   {
	 
	public static Zone DEFAULT_INSTANCE = new Zone(0,0,0,0,ZoneType.RECTANGLE); 
	protected ZoneType type;
	protected Point p1,p2,p3 ; 
	private boolean modifiable = true;
	private boolean modifiableX = true; 
	//##############################################
	public static  enum ZoneSort {
		NORMAL(0),TPOINT(1),COLLISION(2),SPECIAL(3),STAIR(4),STAIRLIST(5),SPAWN_POINT(6) ; 
		private final int valeur; 
		ZoneSort(int valeur) { this.valeur = valeur;  } 
		public int getValeur() {  return valeur; } 
		public static ZoneSort from(String s) { 
			return from(Integer.parseInt(s)); }
		public static ZoneSort from(int valeur) {
			for (ZoneSort type : values()) 
				if (type.getValeur() == valeur) 
					return type;  
			throw new IllegalArgumentException("Aucun ZoneSort correspondant � la valeur : " + valeur);
		}
	}
	//##############################################
	public static enum ZoneType {
		RECTANGLE(0),ARC(-3),LINE(-1),  OVALE(1),UNFILLED_RECT(2),UNFILLED_OVALE(3);
	      
	    private final int valeur; 
	    ZoneType(int valeur) { this.valeur = valeur;  } 
	    public int getValeur() {  return valeur; } 
	    //@Override public String toString() { return String.valueOf(getValeur()); }
	    public static ZoneType from(String s) { 
	    	return from(Integer.parseInt(s));
	    }
	    public static ZoneType from(int valeur) {
	        for (ZoneType type : values()) {
	            if (type.getValeur() == valeur) {
	                return type;
	            }
	        }
	        throw new IllegalArgumentException("Aucun ZoneType correspondant � la valeur : " + valeur);
	    }
	}
	//##############################################
	public int save(int owner,String title) {
		Map<String,String> val = new HashMap<>();  
		val.put("value", this.toSqlValues());  
		if(this instanceof TransitionPoint)
			val.put("type", ZoneSort.TPOINT.getValeur()+"");  
		 
		else if(this instanceof SpecialZoneInv)
			val.put("type", ZoneSort.SPECIAL.getValeur()+""); 
		else if(this instanceof Stair)
			val.put("type", ZoneSort.STAIR.getValeur()+""); 
		else if(this instanceof StairList)
			val.put("type", ZoneSort.STAIRLIST.getValeur()+""); 
		else if(this instanceof SpawnPoint)
			val.put("type", ZoneSort.SPAWN_POINT.getValeur()+""); 
		else 
			val.put("type", ZoneSort.NORMAL.getValeur()+"");  
		val.put("possessor",owner+"");  
		val.put("title",title);  
		int id = SQLTable.getTable("zonelist").insert(val);  
		 
		 return id;
		 
	}
	//############################################## 
	public void update(String sql) {
		p1 = new Point(Integer.parseInt(sql.split(",")[0]),Integer.parseInt(sql.split(",")[1]));
		p2=new Point(Integer.parseInt(sql.split(",")[2]),Integer.parseInt(sql.split(",")[3]));
		type=ZoneType.from(sql.split(",")[4]); 
		 
		try {p3=new Point( Integer.parseInt(sql.split(",")[5]),Integer.parseInt(sql.split(",")[6])); 
		} catch (Exception e) { } //if curved line
		grid.clear(); 
		line.update("value", sql);initGride();
	} 
	
	//##############################################
	public Zone(int x, int y, int i, int j, ZoneType type  ) {
		super(); 
		this.type=type; 
		this.p1 = new Point(x,y);
		this.p2 = new Point(i,j);
		p3 = mid();
		 initGride( );
	} 
	
	//##############################################
	public Zone(String line,int id) {  
		this(Integer.parseInt(line.split(",")[0]),Integer.parseInt(line.split(",")[1]),Integer.parseInt(line.split(",")[2]),Integer.parseInt(line.split(",")[3]),
				ZoneType.from(line.split(",")[4]),id ); 
		 
		try {p3=new Point( Integer.parseInt(line.split(",")[5]),Integer.parseInt(line.split(",")[6])); grid.clear(); initGride( );   } catch (Exception e) { } //if curved line
		
	}
 
	//##############################################
	
	public Zone(Point p, Point p2, ZoneType t) {
		this(p.x,p.y,p2.x,p2.y,t); 
	}
	public Zone(int x, int y, int i, int j, ZoneType type, int id) {
		super(SQLTable.getTable("zonelist").getById(id));
		this.type=type; 
		this.p1 = new Point(x,y);
		this.p2 = new Point(i,j);
		p3 = new Point(x()+w()/2, y()+h()/2);
		  initGride( );
	}
	//##############################################
	public Zone(int x, int y, int i, int j, ZoneType type , int offsetX,int offsetY, Point p) {
		this(x+offsetX,y+offsetY,i+offsetX,j+offsetY,type);
		setP3(p.x,p.y);grid.clear(); 
		initGride( );
	}
	public Zone p(int i) { return new Zone(x(),y(), i(),j(),type,i,i,p3);  }
	public Zone xp(int i) { return new Zone(x(),y(), i(),j(),type,i,0,p3);  }
	public Zone yp(int i) { return new Zone(x(),y(), i(),j(),type,0,i,p3);  }
	public Point mid() { 
		int midX = (x() + i()) / 2;
	    int midY = (y() + j()) / 2;
	    return new Point(midX, midY); }
	//##############################################
	//##############################################
	//############################# interface 
	@Override
	public String toSqlValues() { if(type == ZoneType.ARC)
		return p1.x+","+p1.y+","+p2.x+","+p2.y+","+type.getValeur()+","+p3.x+","+p3.y;
	return p1.x+","+p1.y+","+p2.x+","+p2.y+","+type.getValeur();
	}
	//##############################################
	//##############################################
	//###############################methods 
	//############################################## 
	public void initGride( ) {   
		Set<Point> points = new HashSet<>(); 
		if(type == ZoneType.OVALE) 			for(Point p:Utils.getUnfilledOvale(x(),y(),i(),j())) points.add(p); 
		if(type == ZoneType.RECTANGLE) 		for(Point p:Utils.getUnfilledRectangle(p1, p2)) points.add(p);  
		if(type == ZoneType.LINE) 			for(Point p:Utils.getLinePoints(x(),y(),i(),j())) points.add(p);
		if(type == ZoneType.ARC) 			for(Point p:Utils.getArcPoints(x(),y(),i(),j(),p3.x,p3.y)) points.add(p); 
		if(type == ZoneType.UNFILLED_OVALE) for(Point p:Utils.getUnfilledOvale(x(),y(),i(),j())) points.add(p);
		if(type == ZoneType.UNFILLED_RECT) 	for(Point p:Utils.getUnfilledRectangle(p1,p2)) points.add(p);
		if(points.size() == 0)Utils.log("no points when instanciate type "+type+" for "+toSqlValues()); 
	
		if(type == ZoneType.RECTANGLE||type == ZoneType.UNFILLED_RECT||type == ZoneType.UNFILLED_OVALE||type == ZoneType.OVALE) { 
	        int x = Math.min(p1.x, p2.x);
	        int y = Math.min(p1.y, p2.y); 
	        int width = Math.abs(p1.x - p2.x);
	        int height = Math.abs(p1.y - p2.y);
	        int x2 = x + width;
	        int y2 = y + height;
	        p1 = new Point(x,y);
	        p2 = new Point(x2,y2);
		}
		  for (Point p : points) {
	            Point gridPoint = getGridPointByNormalPoint(p);
	             
	            if (!grid.containsKey(gridPoint)) {
	                grid.put(gridPoint, new HashSet<Point>());
	            }
	            grid.get(gridPoint).add(p);
	              
	        }  
	        
		  points.clear();
	 
	}
	
	
	 public static final int GRID_SIZE = 20;
	    public Map<Point, Set<Point>> grid = new HashMap<>(); 
	    public static Point getGridPointByNormalPoint(Point p) {
	        int gridX = p.x / GRID_SIZE;
	        int gridY = p.y / GRID_SIZE;
	        return new Point(gridX, gridY);
	    }
	
	
	
	    public boolean isInZone(Point normal) { 
	    	if(this.type==ZoneType.RECTANGLE||(type==ZoneType.UNFILLED_RECT&&this instanceof SpawnPoint))return Utils.isInRectangle(normal, x(), y(), i(), j());
	    	if(this.type==ZoneType.OVALE||(type==ZoneType.UNFILLED_OVALE&&this instanceof SpawnPoint))return Utils.isInOval(normal, x(), y(), i(), j());
	        Point gridPoint = getGridPointByNormalPoint(normal);
	        Set<Point> boundingBoxPoints = grid.get(gridPoint); 
	        if(boundingBoxPoints==null)return false;
	        return boundingBoxPoints.contains(normal);
	    }
	
	
	    public boolean isNear(Point p,  int tolerance) { 
	    	if(this.type==ZoneType.RECTANGLE)return Utils.isInRectangle(p, x(), y(), i(), j(),tolerance);
	    	if(this.type==ZoneType.OVALE)return Utils.isInOval(p, x(), y(), i(), j(),tolerance);
	        Point gridPoint = Zone.getGridPointByNormalPoint(p);
	        
	        Set<Point> linePoints = grid.get(gridPoint); 
	        if (linePoints != null) {
	            for (Point linePoint : linePoints) {
	                if (Utils.getDistanceBetweenPoints(p, linePoint) <= tolerance) {
	                    return true;
	                }
	            }
	        }
	        return false;
	    } 
	
	
	
	
	
	@Override public String title() {
		try {
			return this.possessorObj().title()+"_zone_"+super.title();
		} catch (Exception e) {
			return super.title();
		}
	}
 
	//############################################## 
 
	public Point getIntersectPoint(Zone l) { 
	    for (Map.Entry<Point, Set<Point>> entry : grid.entrySet()) {
	        Point gridPoint = entry.getKey();
	        Set<Point> currentZonePoints = entry.getValue();
	        Set<Point> otherZonePoints = l.grid.get(gridPoint);
	        if (otherZonePoints != null) { 
	            for (Point p1 : currentZonePoints) 
	                for (Point p2 : otherZonePoints) 
	                    if (Utils.getDistanceBetweenPoints(p1, p2) < 6)
	                        return p2;
	        }
	    }
	    return null;
	} 
	public boolean isCollided(Zone l) {  
		return Utils.isCollided(l.x(), l.y(), l.i(), l.j(),x(),y(),i(),j());
		/*for(Point p:points)
			for(Point p2 : l.getPoints())
				if(Utils2d.getDistanceBetweenPoints(p, p2) <6) 
				return true;
		return false;*/
	}
	//############################################## 
	public Rectangle toRect(   ) { 
		return new Rectangle(x(),y(), w(),h());
	}	
 
	//##############################################
	public void draw(Graphics g) {
		int x = Math.min(p1.x, p2.x);
	    int y = Math.min(p1.y, p2.y); 
	    
	    Graphics2D g2d = (Graphics2D) g;

	 // D�finir l'�paisseur de la ligne
	 float strokeWidth = 6.0f; // Changer cette valeur pour l'�paisseur d�sir�e

	 // D�finir l'�paisseur de la ligne
	 g2d.setStroke(new BasicStroke(strokeWidth));

	 // Dessiner les diff�rentes formes avec l'�paisseur de ligne d�finie
	 if (type == ZoneType.LINE) {
	     g2d.drawLine(p1.x, p1.y, p2.x, p2.y);
	 }
	 if (type == ZoneType.ARC) {
	     QuadCurve2D curve = new QuadCurve2D.Float(p1.x, p1.y, p3.x, p3.y, p2.x, p2.y);
	     g2d.draw(curve);
	 }
	 if (type == ZoneType.UNFILLED_RECT) {
	     g2d.drawRect(x, y, w(), h());
	 }
	 if (type == ZoneType.UNFILLED_OVALE) {
	     g2d.drawOval(x, y, w(), h());
	 }
		 if(type == ZoneType.RECTANGLE)
				g.fillRect(x, y, w(), h());
		 if(type == ZoneType.OVALE)
			 g.fillOval(x, y, w(), h()); 
		
	}
	
	
	
	public Point getCorner1() {
		if(type == ZoneType.OVALE || type == ZoneType.UNFILLED_OVALE)
			return getC1();
		return p1;
	}
	
	public Point getCorner2() {
		if(type == ZoneType.OVALE || type == ZoneType.UNFILLED_OVALE)
			return getC2();
		return p2;
	}
	
	
	 
	
    private Point getC2() { 
	    int centerX = (int) ((x() + i()) / 2.0);
	    int centerY = (int) ((y() + j()) / 2.0);
	    int semiMajorAxis = (int) Math.abs((i() - x()) / 2.0);
	    int semiMinorAxis = (int) Math.abs((j() - y()) / 2.0);
	    int angle2 = (int) Math.toRadians(110);
	    int corner2X = (int) (centerX + semiMajorAxis * Math.cos(angle2));
	    int corner2Y = (int) (centerY + semiMinorAxis * Math.sin(angle2));
	    return new Point(corner2X,corner2Y);

	}
	private Point getC1() { 
	    int centerX = (int) ((x() + i()) / 2.0);
	    int centerY = (int) ((y() + j()) / 2.0);
	    int semiMajorAxis = (int) Math.abs((i() - x()) / 2.0);
	    int semiMinorAxis = (int) Math.abs((j() - y()) / 2.0);
	    int angle1 = (int) Math.toRadians(250);
	    int corner1X = (int) (centerX + semiMajorAxis * Math.cos(angle1));
	    int corner1Y = (int) (centerY + semiMinorAxis * Math.sin(angle1)); 
	    return new Point(corner1X,corner1Y);

	}
	 
	//##############################################
	public void draw(boolean selected, Graphics g, Color c) { 
		if(type == ZoneType.ARC ||type == ZoneType.LINE 
				|| type == ZoneType.UNFILLED_RECT || type == ZoneType.UNFILLED_OVALE)
			if(selected) {
				g.setColor(c ); 	 draw(g) ; 
				g.setColor(Color.WHITE); 	p(1).draw(g) ; p(-2).draw(g) ; 
				g.setColor(c ); 	p(-1).draw(g) ;   
			}else {
				g.setColor(c );		draw(g) ; 
				g.setColor(Color.BLACK); 	p(1).draw(g) ;
				g.setColor(c );  	p(-1).draw(g) ;  
			}
		 
		 draw(g); 
	}
 
	//##############################################
	public void fill(Graphics g,Color c) {
		g.setColor(c);
		int x = Math.min(p1.x, p2.x);
	    int y = Math.min(p1.y, p2.y);
		if (type == ZoneType.OVALE)
			g.fillOval(x,y, w(),h());
		else
			g.fillRect(x,y, w(),h());
	}
	public void fill(Graphics g, double zoom,Color c) {
		g.setColor(c);
		int x = Math.min(p1.x, p2.x);
	    int y = Math.min(p1.y, p2.y);
		if (type == ZoneType.OVALE)
			g.fillOval((int) (x*zoom),(int) (y*zoom), (int) (w()*zoom),(int) (h()*zoom));
		else
			g.fillRect((int) (x*zoom),(int) (y*zoom), (int) (w()*zoom),(int) (h()*zoom));
	}
	//##############################################
	//##############################################
	//##############################################
	public void setP1(int i, int j) { p1 = new Point(i,j);  }
	public void setP2(int i, int j) { p2 = new Point(i,j);  }
	public void setP3(int i, int j) {  p3 = new Point(i,j);  } 
	public void setP1(Point p ) {setP1(p.x,p.y);  }
	public void setP2(Point p ) { setP2(p.x,p.y);  }
	public void setP3(Point p ) {  setP3(p.x,p.y); } 
	public ZoneType getType() { return type; } 
	public Point getP1() { return p1; } 
	public Point getP2() { return p2; }
	public Point getP3() { return p3; }
	public int x() {return p1.x;}
	public int y() {return p1.y;}
	public int i() {return p2.x;}
	public int j() {return p2.y;}
	public int w() {return Math.abs(p1.x - p2.x);}
	public int h() {return Math.abs(p1.y - p2.y);}
	//public List<Point> getPoints() { if(points.size()==0)initPointsArray(); return points; }
	public void setType(ZoneType t) { type = t; }
	public void setUnmodifiable() { modifiable=false; } 
	public boolean isModifiable() { return modifiable; }
	public void setUnmodifiableX() { modifiableX =false; } 
	public boolean isModifiableX() { return modifiableX; }

	//##############################################
	//##############################################
	//##############################################
 
	
 
	
	
	@Override public String toString() { return p1+" "+p2+" "+p3+" "+type; }
	public Zone copie() {
		Zone ret = new Zone(p1.x,p1.y,p2.x,p2.y, type);
		ret.setP3(p3); 
		return ret;
	} 
	public void zoome(double zoom) {
		p1 = new Point((int) (p1.x/zoom),(int) (p1.y/zoom));
		p2 = new Point((int) (p2.x/zoom),(int) (p2.y/zoom));
		p3 = new Point((int) (p3.x/zoom),(int) (p3.y/zoom));
	}
	public void unzoome(double zoom) {
		p1 = new Point((int) (p1.x*zoom),(int) (p1.y*zoom));
		p2 = new Point((int) (p2.x*zoom),(int) (p2.y*zoom));
		p3 = new Point((int) (p3.x*zoom),(int) (p3.y*zoom));
	}
	@Override
	public List<GameObject> getChilds() {
		List<GameObject> liste = new ArrayList<>() ;
		// for(AshType a:AshType.values())
		//	 if(ash(a)!=-1)
		//		 liste.add(Ash.get(ash(a)));
		 
		
		return liste;
	}
	 
	@Override public Zone instanciate(String sql,int id) { 
		Zone z =  new Zone(sql, id);
		
		return z; 
		
		}
	@Override public Zone getObject() { return this; }
	 
	@Override
	public String getFolder() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	GameObject instanciate(SQLLine l) {
		if (l.getInt("type") == ZoneSort.NORMAL.getValeur())
    		return new Zone(l.getString("value"),l.id); 
    	else if (l.getInt("type") == ZoneSort.TPOINT.getValeur())
    		return new TransitionPoint(l.getString("value"),l.id);
    	else if (l.getInt("type") == ZoneSort.SPECIAL.getValeur())
    		return new SpecialZoneInv(l.getString("value"),l.id);
    	else if (l.getInt("type") == ZoneSort.STAIR.getValeur())
    		return new Stair(l.getString("value"),l.id);
    	else if (l.getInt("type") == ZoneSort.STAIRLIST.getValeur())
    		return new StairList(l.getString("value"),l.id);
    	else if (l.getInt("type") == ZoneSort.SPAWN_POINT.getValeur())
    		return new SpawnPoint(l.getString("value"),l.id);
		return null;
	}

	@Override
	boolean isValidPossessor(SQLLine l, GameObject possessor) { 
		 return l.getInt("type") == ZoneSort.NORMAL.getValeur() && l.getInt("possessor") == possessor.id;
	}

	@Override
	boolean isValidElement(SQLLine l) { 
		 if (l.getInt("type") == ZoneSort.NORMAL.getValeur())
     		return true;
		return false;
	}

	@Override
	String getTable() { 
		return "zonelist";
	}

	@Override
	GameObject default_instance() { 
		return Zone.DEFAULT_INSTANCE;
	}
 
	public static List<Zone> getByOwner(int ownerId){
		 return getbyPossessor(Zone.DEFAULT_INSTANCE,GameObject.getById(ownerId));
	}

	
	public static List<Zone> getAll( ) { 
	    return getAll(Zone.DEFAULT_INSTANCE);
	}
	public static Zone get( int id) {
	    return get(Zone.DEFAULT_INSTANCE,id);
	}
 
 


	@Override public boolean equals(Object obj) {
		if(!(obj instanceof Zone)) return false;
		if (this == obj)
			return true; 
	
		Zone other = (Zone) obj;
		if (p1 == null) {
			if (other.p1 != null)
				return false;
		} else if (!p1.equals(other.p1))
			return false;
		if (p2 == null) {
			if (other.p2 != null)
				return false;
		} else if (!p2.equals(other.p2))
			return false;
		return true;
	}
	
	@Override  public 	Set<AshType> possibleGetters() {
		Set<AshType> temp = new HashSet<>();
		temp.add(AshType.HAS_BEEN_DISABLED ); 
		temp.add(AshType.HAS_BEEN_COLLIDED ); 
		temp.add(AshType.IS_IN_ZONE ); 
		temp.add(AshType.ZONE_HAS_BEEN_CLICKED ); 
		return temp;
	}
	@Override  public 	Set<AshType> possibleRemovers() {
		Set<AshType> temp = new HashSet<>();
		temp.add(AshType.HAS_BEEN_DISABLED );  
		return temp;
	}
	@Override public Set<AshType> possibleSetters() {
		Set<AshType> temp = new HashSet<>();
		temp.add(AshType.WILL_BE_DISABLED ); 
		temp.add(AshType.WILL_BE_IN_POSITION ); 
		temp.add(AshType.WILL_BE_CLICKED ); 
		return temp;
	}

	
	public void delete() { super.delete();} 
	public String getTitle( ) {  return line.getString("title");  } 
	
}
