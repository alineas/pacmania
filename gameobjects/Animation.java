package me.nina.gameobjects;

import me.nina.objects2d.Position;
import me.nina.sqlmanipulation.SQLLine;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.swing.SwingUtilities;
 

import agame.MainGame; 
import me.nina.editionnodes.ObjectsNode;
import me.nina.gameobjects.Animation.AnimationState;
import me.nina.gameobjects.Zone.ZoneType;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Utils;
import me.nina.objects2d.Img;
import me.nina.objects2d.ImgUtils;
import me.nina.objects2d.Pixel;
import me.nina.sqlmanipulation.SQLTable;

public class Animation extends GameObject   {  private static final long serialVersionUID = 1L;
int priority=-1;int priorityToRem = -1;
	public static  enum AnimationState {
		INVISIBLE(-1),INFINITE_LOOP(-2),LOOP(-3), MONO(-4),ALEATORY_LOOP(-5);
      
		private final int valeur; 
		AnimationState(int valeur) { this.valeur = valeur;  } 
		public int getValeur() {  return valeur; } 
		public static AnimationState from(String s) { 
			return from(Integer.parseInt(s)); }
		public static AnimationState from(int valeur) {
			for (AnimationState type : values()) 
				if (type.getValeur() == valeur) 
					return type;  
			throw new IllegalArgumentException("Aucun AnimationState correspondant � la valeur : " + valeur);
		}
	}
  
	public Img gifFile; 
    private int index = 0;  
	private double scale = 1.0;  
	public boolean selected;  
	public boolean idle;
	public Zone zone;
	private Position posFrom; 
	private int prioFrom=-1; 
	protected int pressedX;
	protected int pressedY ;
	private boolean reversed;
	private Position posTo;
	private int prioTo=-1;
	public Zone offsetFrom;
	public Zone offsetTo;
	private Animation idleCorresponding;
	private String folder; 
	
	//###############################################
	 
	public Animation() { }
	
	//###############################################

	public Animation(SQLLine line) {
		super(line); 
		setOpaque(false);  
		if(!sounde().equals("")||!music().equals(""))
			try {folder = possessorObj().possessorObj().getFolder();
				 } catch (Exception e) {Utils.log("erreur sounde "+sounde()+" "+possessorObj().title() );} 
	}
	
	//###############################################
	public void loadGif() { 
		
		if(gifFile != null)
			return;
		
		if(!image().contains("/"))
			gifFile = ImgUtils.getImg(GameObject.getById(possessor()).getFolder()+image(),shadow()); 
		else 
			gifFile = ImgUtils.getImg( image(),shadow()); 
		
		
	    selected=false; 
	    Utils.setSize(this, (int) (getFrames().get(0).getWidth()*scale), (int) (getFrames().get(0).getHeight()*scale));
	    if(state()==AnimationState.MONO)
	    	setIdleFrame( 0); 
	}
  
 
	//######################################################################## 
	 
		public void paintIt(Graphics g ) { // dans le contexte du jeu
		    loadGif();  
		    if (state() != AnimationState.INVISIBLE) {
		        try {
		        	if (state() != AnimationState.MONO){
		                setIndex(getIndex() + 1); 
		                if( sonorizedFrames().contains(getIndex())){
		                	MainGame .soundPlayer.playAudio(folder+sounde(),true);
		                }
	 
		            }
		            if (getIndex() >= this.frameOrder().size()) {
		                if (state() == AnimationState.INFINITE_LOOP || 
		                		state() == AnimationState.ALEATORY_LOOP && new Random().nextBoolean()&& new Random().nextBoolean()&& new Random().nextBoolean()&& new Random().nextBoolean()) {
		                    setIndex(0); 
		                    if( sonorizedFrames().contains(getIndex())){
		                    	MainGame .soundPlayer.playAudio(folder+sounde(),true);
		                    }
	 	                }else
		                    setIndex(getIndex() - 1);  
		                

		            }
		             

		            int xx = getX();
		            int yy = getY();
		            BufferedImage currentFrame = gifFile.frames.get(this.frameOrder().get(getIndex()));  
		            if (scale != 1.0)
		                currentFrame = ImgUtils.scal(currentFrame,scale);
		             
		            
		            if(priorityGroundPoint()!=0||priorityToAddForBypass()!=0) {
		    			try {
		    				BufferedImage movingImage = ImgUtils.getCopy(currentFrame);  
		    				final int addx = (int) (xx -  posFrom().x );//used only if specified for ground obj
							final int addy = (int) (yy -  posFrom().y ); 
							
			    			movingImage = Utils.unprioritize(movingImage,addx,addy,yy+movingImage.getHeight()+priorityToAddForBypass(),false,null); 
		    				g.drawImage(movingImage, addx, addy, null);
		    			} catch (Exception e) { e.printStackTrace(); }
		            }else 
		            
		            
		            g.drawImage(currentFrame, xx, yy, null);

		        } catch (Exception e) { e.printStackTrace(); }
		    }
		}
	//############################################### 
	@Override
	protected void paintComponent(Graphics g) {
	    super.paintComponent(g);
	    try {
	        loadGif();
	        BufferedImage currentFrame = gifFile.frames.get(frameOrder().get(getIndex())); // Utilisation du getter pour obtenir l'index

	        if (!idle) {
	            setIndex(getIndex() + 1); // Utilisation du setter pour incr�menter l'index
	            if (getIndex() >= frameOrder().size()) {
	                if (state() == AnimationState.INFINITE_LOOP)
	                    rewindAll();
	                else
	                    setIndex(getIndex() - 1); // Utilisation du setter pour d�cr�menter l'index
	            }
	        }

	        if (scale != 1.0)
	            currentFrame = ImgUtils.scal(currentFrame,scale); 
	        if (idleCorresponding != null)
	            g.drawImage(idleCorresponding.getActualFrame(), posFrom().x, posFrom().y, null);

	        if (state() != AnimationState.INVISIBLE) {
	            g.drawImage(currentFrame, 0, 0, null);
	            int w = getScalledWidth() / 2;
	            int h = getScalledHeight();
	            offsetFrom = new Zone(w + posFrom().x, h + posFrom().y - 5, w + posFrom().x + 5, h + posFrom().y, ZoneType.RECTANGLE);
	            offsetFrom.fill(g, Color.GRAY);
	            offsetTo = new Zone(w + posTo().x, h + posTo().y - 5, w + posTo().x + 5, h + posTo().y, ZoneType.RECTANGLE);
	            offsetTo.fill(g, Color.blue);
	        } else {
	            BufferedImage semiTransparentImage = new BufferedImage(currentFrame.getWidth(), currentFrame.getHeight(), BufferedImage.TYPE_INT_ARGB);
	            Graphics2D g2d = semiTransparentImage.createGraphics();
	            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f)); // Niveau d'opacit� 50%
	            g2d.drawImage(currentFrame, 0, 0, null);
	            g2d.dispose();
	            g.drawImage(semiTransparentImage, 0, 0, null);
	        }

	        if (selected) 
	                g.drawRect(3, 3, getWidth() - 5, getHeight() - 5 );
 
	            g.setColor(new Color(122, 122, 122, 190));
	            for (Zone z : ObjectsNode.zonesToDraw) { 
	                Rectangle localRect = SwingUtilities.convertRectangle(getParent(), z.toRect(), this);
	                if (z.getType() == ZoneType.RECTANGLE) g.fillRect(localRect.x, localRect.y, localRect.width, localRect.height);
	                else if (z.getType() == ZoneType.OVALE)
	                    g.fillOval(localRect.x, localRect.y, localRect.width, localRect.height);
	                else { 
	                    java.awt.Point p1 = new java.awt.Point(z.getP1().x,z.getP1().y) ;
	                    java.awt.Point p2 = new java.awt.Point(z.getP2().x,z.getP2().y) ;

	                    Point p1converted = SwingUtilities.convertPoint(getParent(), p1, this);
	                    Point p2converted = SwingUtilities.convertPoint(getParent(), p2, this);
	                    new Zone(p1converted.x, p1converted.y, p2converted.x, p2converted.y, ZoneType.LINE).draw(true, g, new Color(255, 255, 255, 128));
	                }
	            }
	        

	    } catch (Exception e) {
	        e.printStackTrace();
	        setIndex(0); // Utilisation du setter pour r�initialiser l'index en cas d'exception
	    }
	}
	
	//############################################### 
	public void run() {
		 if (!idle) { loadGif();
	    if (state() != AnimationState.INVISIBLE) { // dans le contexte du jeu
	        if (state() != AnimationState.MONO) {
	            if (!reversed)
	                setIndex(getIndex() + 1); // Utilisation du setter pour incr�menter l'index
	            else if (getIndex() > 0)
	                setIndex(getIndex() - 1); // Utilisation du setter pour d�cr�menter l'index
	        }

	        if (getIndex() >= frameOrder().size()) {
	            if (state() == AnimationState.INFINITE_LOOP)
	                setIndex(0); // R�initialiser l'index � 0 en boucle infinie
	            else if (getIndex() > 0)
	                setIndex(getIndex() - 1); // D�cr�menter l'index pour la boucle finie
	        } else{
	        	 if( sonorizedFrames().contains(getIndex())){
		                MainGame .soundPlayer.playAudio(folder+sounde(),true);
	        	 }
	        }
	    }
	}}
	  
	//############################################### 
	public int getScalledWidth() { return (int) (getActualFrame().getWidth()*scale); }
	public int getScalledHeight() { return (int) (getActualFrame().getHeight()*scale); }
	public List<BufferedImage> getFrames() { loadGif(); return gifFile.frames; } //editor
	public BufferedImage getActualFrame() { return getFrames().get(this.frameOrder().get(getIndex())); }
	public BufferedImage getFirstFrame() { return getFrames().get(0); }
	public BufferedImage getLastFrame() { int nbr=this.frameOrder().get(this.frameOrder().size()-1); return getFrames().get(nbr); }
	public void reverseOrder() { List<Integer> temp = frameOrder(); Collections.reverse(temp); this.updateOrder(temp); } //editor
	public void resetOrder() { List<Integer> temp = new ArrayList<>(); for(int i = 0 ; i < getFrames().size() ; i++) temp.add(i); this.updateOrder(temp); }
	public void setIdleFrame(int idleFrame) { //new Throwable().printStackTrace();
	setIndex(idleFrame); idle=true; }
	public int currentIndex() { return getIndex(); }
	public void rewindOneFrame() { setIndex(getIndex() - 1); if (getIndex() <0) setIndex(frameOrder().size()-1); }//inv explorer
	public void forwardOneFrame() { setIndex(getIndex() + 1); if (getIndex() >= frameOrder().size()) setIndex(0); }//inv explorer
	public void forwardAll() { setIndex(frameOrder().size()-1); }//game start 
	public void rewindAll() { setIndex(0); 
	if(MainGame .instance!=null&&!sounde().equals("")&& sonorizedFrames().size()==0)
		MainGame .soundPlayer.playAudio(folder+sounde(),true);
	
	if(MainGame.instance!=null&&!music().equals("") && !MainGame.soundPlayer.actualMidiPath.equals(music()))
		MainGame .soundPlayer.playMidi(folder+music() ,true,false);
	
	}	public int lastFrameNumber() { return frameOrder().get(frameOrder().size()-1); }
	public boolean loopFinished() { 
		/*Utils.log("################### finished loop");
		Utils.log("state()==AnimationState.INVISIBLE "+(state()==AnimationState.INVISIBLE));
		Utils.log("state()==AnimationState.MONO "+(state()==AnimationState.MONO));
		Utils.log("!reversed&&getIndex() >= frameOrder().size()-1 "+(!reversed&&getIndex() >= frameOrder().size()-1));
		Utils.log("reversed&&getIndex() == 0 "+(reversed&&getIndex() == 0));
		Utils.log("###################  ");*/
		 
	    return state()==AnimationState.INVISIBLE
	    		||state()==AnimationState.MONO
	    		||!reversed&&getIndex() >= frameOrder().size()-1
	    		|| reversed&&getIndex() == 0; }
	//############################################### 
	//############################################### 
	  
	public void setSize(int max) {//inv objects / obj state
		loadGif();
	    double currentWidth = getWidth();
	    double currentHeight = getHeight(); 
  
	    int newWidth = (int) (currentWidth * scale);
	    int newHeight = (int) (currentHeight * scale);

	    if (newWidth <= max && newHeight <= max) {
	        return; // No need to resize if both dimensions are within the limit.
	    }

	    scale = max / Math.max(currentWidth, currentHeight);
	    Utils.setSize(this, max, max);
	}
	//############################################### 
	public void setScale(double d) { //zoomable object node
		Utils.setSize(this, (int) (getActualFrame().getWidth()*d), (int) (getActualFrame().getHeight()*d));this.scale= d; } 
	 
	//############################################### 
		public static int generateAnimation(
				GameObject possessor, 
				String title,  
				int stateOrIdleFrameNumber, 
				String fileName) { 
			Map<String,String> val = new HashMap<>(); 
			Img icone=null; 
			if(!fileName.contains("/"))
				icone = ImgUtils.getImg(possessor.getFolder()+fileName,-1);
			else 
				icone = ImgUtils.getImg(fileName,-1);
				val.put("img", fileName); 
			 
			SQLTable tabl = SQLTable.getTable("animation");
			val.put("title",  title);
			val.put("posfrom", new Position(0,0,0).toSqlValues());
			val.put("posto", new Position(0,0,0).toSqlValues()); 

			val.put("possessor", possessor.id+"");  
			if(stateOrIdleFrameNumber>=0 ) {
				val.put("state", AnimationState.MONO .getValeur()+""); 
				val.put("frameorder", stateOrIdleFrameNumber+""); 
			}
			else {
				val.put("state", stateOrIdleFrameNumber+""); 

				String v = "";
				for(int i = 0 ; i < icone.frames.size() ; i++)
					v=v+","+i; 
				if(v.equals(""))v="0";//empty frame list in non gif files or mono frame gif
				val.put("frameorder", v.replaceAll("^,", "")); 
			}
			return tabl.insert(val) ;
		}
	 
	
	//############################################### 
	//############################################### 
	//##############################sql
	public static List<Animation> getAnimations(GameObject el) {
		 
		return getbyPossessor(instance,el);
	}

	public void updatePosFrom(int offsetX, int offsetY) {
		Position old = posFrom(); 
		Position newe = new Position(old.x+offsetX,old.y+offsetY,old.pos.getValeur());
		if(offsetX==0&&offsetY==0)
			line.update("posfrom", "0,0,0");
		else
			line.update("posfrom", newe.toSqlValues());
		posFrom=null; 
	}
	
	public void updatePosTo(int offsetX, int offsetY) {
		Position old = posTo(); 
		Position newe = new Position(old.x+offsetX,old.y+offsetY,old.pos.getValeur());
		if(offsetX==0&&offsetY==0)
			line.update("posto", "0,0,0");
		else
			line.update("posto", newe.toSqlValues());
		posTo=null;
		
	}
	
	
	public LinkedList<Integer> sonorizedFrames(){
		return line.getList("sonorizedframes");
	}
	public void addSonorized(int i) {
		line.addInArray("sonorizedframes",i+"");
	}
	public void remSonorized(int i) {
		line.remFromArray("sonorizedframes",i+"");
	}
	public void resetSono() {
		line.update("sonorizedframes", "");
	}
	public void updateOrder(List<Integer> order) {
		String val = ""; for(int i : order) val=val+","+i;
		line.update("frameorder", val.replaceAll("^,", ""));
	}
	public List<Integer> frameOrder() {  return line.getList("frameorder"); } 
 
	@Override public List<GameObject> getChilds() {
		List<GameObject> liste = new ArrayList<>() ; 
		liste.add(GameObject.getById(this.possessor()));
		return liste;
	}
	public static Animation get(int id) { return get(instance,id); }  
	public static List<GameObject> getAll() { return getAll(instance); }  
	public Position posFrom() { if(posFrom==null)posFrom= new Position(line.getString("posfrom"));return posFrom; } 
	public Position posTo() { if(posTo==null)posTo= new Position(line.getString("posto"));return posTo; } 
	
	public int prioFrom() {  if(prioFrom==-1)prioFrom=  line.getInt("priofrom") ;if(prioFrom==-998)prioFrom=0;return prioFrom; } 
	public int prioTo() {  if(prioTo==-1)prioTo=  line.getInt("prioto") ;if(prioTo==-998)prioTo=0;return prioTo; } 
	public void updatePrioTo( int y) { prioTo=y; line.update("prioto", y+""); }
	public void updatePrioFrom( int y) { prioFrom=y; line.update("priofrom", y+""); }
	
	public String image() { return line.getString("img"); } 
	//public int talke() { return line.getInt("talk"); }  
	public void updateImage(String s) { line.update("img", s +""); } 
	//public void updateTalk(int s) { line.update("talk", s +""); } 
	public String music() { return line.getString("music"); }  
	public void updateMusic(String fileName) { line.update("music", fileName); } 
	public String sounde() { return line.getString("sound"); }  
	public void updateSound(String fileName) { line.update("sound", fileName); } 
	public void unsonorize() {
		line.update("sound", "");resetSono();line.update("music", "");
	}
	int shad=-1;
	public int shadow() { if(shad==-1)shad=line.getInt("shadow");return shad; }  
	public void updateShadow(int i) { line.update("shadow", i+""); } 
	
	
	public void updateState(AnimationState s) { line.update("state", s.getValeur()+""); } 
	public AnimationState state( ) { return AnimationState.from(line.getInt("state")); }  
	@Override public String getFolder() { return GameObject.getById(possessor()).getFolder(); }
	@Override GameObject instanciate(SQLLine l) { return new Animation(l); } 
	@Override boolean isValidElement(SQLLine l) {  return true; } 
	@Override String getTable() {  return "animation"; }
	static Animation instance = new Animation();
	@Override GameObject default_instance() { return instance; } 
	@Override boolean isValidPossessor(SQLLine l, GameObject possessor) { 
		return possessor.id == l.getInt("possessor") ;  } 
	@Override public Set<AshType> possibleGetters() {
		Set<AshType> temp = new HashSet<>();  return temp; }
	@Override public Set<AshType> possibleRemovers() {
		Set<AshType> temp = new HashSet<>();  return temp; }
	@Override public Set<AshType> possibleSetters() {
		Set<AshType> temp = new HashSet<>();
		return temp;
	}

	public void setReversed(boolean b) { reversed = b; } 
	public boolean isReversed() {  return reversed; } 
	public void setCorrespondingAnim(Animation idleCorresponding) { this.idleCorresponding=idleCorresponding; } 
	public void unsetIdle() { idle=false; }

    public int getIndex() {
        return index;
    }
public static boolean test=false; 
    public void setIndex(int newIndex) {//if(newIndex==0)new Throwable().printStackTrace();
        this.index = newIndex;
    } 
 
	public int priorityToAddForBypass() {
		if(priority==-1) {
			if(line.getInt("takepriority") == -998)
				priority=0;
			else 
				priority = line.getInt("takepriority");
		}  
		return priority;
	}
	public void updatePriorityGroundPoint(int addToFeetY) { line.update("priority", addToFeetY+""); } 
	
	public int priorityGroundPoint() {
		if(priorityToRem==-1) {
			if(line.getInt("priority") == -998)
				priorityToRem=0;
			else 
				priorityToRem = line.getInt("priority");
		}  
		return priorityToRem;
	}
	public void updatePriorityToAdd(int feety) { priority = feety;line.update("takepriority", feety+""); } 
	//




}
