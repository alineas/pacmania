package me.nina.gameobjects;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import me.nina.gameobjects.GameObject;
import me.nina.gameobjects.Character.PosType;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Main;
import me.nina.maker.Utils;
import me.nina.sqlmanipulation.SQLLine;
import me.nina.sqlmanipulation.SQLTable;

public class CharacterState extends GameObject { private static final long serialVersionUID = 1L;
private double scal = -1.0; 

	private CharacterState(SQLLine l) {
		super(l); 
	}
	
	public CharacterState(String string) {
		// TODO Auto-generated constructor stub
	}

	public static CharacterState get(int id) { 
		 return get(instance,id);
	}
	public static List<CharacterState> getAll() {
		return getAll(instance);
	}  
	public static List<CharacterState> getByPossessor(int possessor) { 
		return getbyPossessor(instance,GameObject.getById(possessor));
	} 
	public Animation movingAnimation(PosType type){   try { 
		return Animation.get( Integer.parseInt(
						 line .duoManager.getString("moveanimation", type.getValeur()+"")
						 ));  } catch ( Exception e) {return null; }  
	} 
	public Animation idleAnimation(PosType type){   try {
		return Animation.get( Integer.parseInt(
						 line .duoManager.getString("idleanimation", type.getValeur()+"")
						 )); } catch ( Exception e) {return null; }  
	}
	public Animation talkAnimation(PosType type){  try { 
		 return Animation.get( Integer.parseInt(
						 line .duoManager.getString("talkanimation", type.getValeur()+"")
						 )); } catch ( Exception e) {return null; }  
	}
	
	public double scaleCorrection() {if (scal == -1.0)
		try { scal =  Double.parseDouble(line.getString("scalecorrection"));
		} catch (NumberFormatException e) {  scal = 0;  } 
		return scal;
	}
	public void updateScaleCorrection(double d) {
		scal = -1.0;
		line.update("scalecorrection", ""+d);
	}
	public void updateTalkAnimation(Animation anim, PosType type) {
		Animation talk = talkAnimation(type);
		if(talk!=null) { 
		talk.line.delete();}
		line .duoManager.setValue("talkanimation", type.getValeur()+"",anim.id+""); 
	}
	public void updateIdleAnimation(Animation anim, PosType type) {
		Animation idle = idleAnimation(type);
		if(idle!=null) { 
		idle.line.delete();}
		line .duoManager.setValue("idleanimation", type.getValeur()+"",anim.id+""); 
	}
	public void updateMovingAnimation(Animation anim, PosType type) {
		Animation moving = movingAnimation(type);
		if(moving!=null) { 
		moving.line.delete();}
		line .duoManager.setValue("moveanimation", type.getValeur()+"",anim.id+""); 
	}
 
	@Override public String getFolder() {
		String path = possessorObj().getFolder()+title()+"/";
		new File(path).mkdirs(); 
		return  path ; 
	}
	
 
 
	@Override
	public List<GameObject> getChilds() {
	List<GameObject> liste = new ArrayList<>() ;
	/*for(AshType a : AshType.values()) { 
		if(ash(a) != -1) 
			liste.add(Ash.get(ash(a)));
	}*/
		//liste.add(possessorObj());
		  
		return liste;
	}

	@Override
	GameObject instanciate(SQLLine l) { 
		return new CharacterState(l);
	}
 

	@Override
	boolean isValidElement(SQLLine l) { 
		return true;
	}

	@Override
	String getTable() { 
		return "characterstate";
	}
static CharacterState instance= new CharacterState("");
	@Override
	GameObject default_instance() { 
		return instance;
	}

	@Override
	boolean isValidPossessor(SQLLine l, GameObject possessor) {
		// TODO Auto-generated method stub
		return possessor.id == l.getInt("possessor") ;

	}

	@Override public void delete() {if(!Main.getUserConfirmation("are you sure ?"))return;
		 for(PosType p : PosType.values()){
			 if(this.idleAnimation(p)!=null)this.idleAnimation(p).delete();
			 if(this.movingAnimation(p)!=null)this.movingAnimation(p).delete();
			 if(this.talkAnimation(p)!=null)this.talkAnimation(p).delete();
		 }
		super.delete();
	}

	@Override
	public
	void updateTitle(String text) {
		for(GameObject a : Animation.getAll()) { 
			if(((Animation)a).image().contains("/"+title()))
				((Animation)a).updateImage(((Animation)a).image().replaceAll("/"+title(), "/"+text));
					if(a.title().contains(title())) 
						a.updateTitle(a.title().replaceAll(title(), text));
		}
		super.updateTitle(text,true); 
		
		
	}

 
	@Override  public 	Set<AshType> possibleGetters() {
		Set<AshType> temp = new HashSet<>();
		temp.add(AshType.IS_IN_STATE );  
		return temp;
	}
	@Override  public 	Set<AshType> possibleRemovers() {
		Set<AshType> temp = new HashSet<>();
		return temp;
	}
	@Override  public 	Set<AshType> possibleSetters() {
		Set<AshType> temp = new HashSet<>();
		temp.add(AshType.WILL_BE_IN_STATE );
		temp.add(AshType.WILL_INSIDE_OUTSIDE_THIS_STATE);
		temp.add(AshType.WILL_INSIDE_THIS_STATE);
		temp.add(AshType.WILL_OUTSIDE_THIS_STATE);
		temp.add(AshType.WILL_INSIDE_WAIT_AND_OUTSIDE_STATE);
		
		return temp;
	}
}
