package me.nina.gameobjects;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToolTip;
import javax.swing.JWindow;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.TransferHandler;
import javax.swing.TransferHandler.TransferSupport;

import me.nina.editors.Editor;
import me.nina.gameobjects.Character.PosType;
import me.nina.maker.ActionExplorer;
import me.nina.maker.Ashe;
import me.nina.maker.Ashe.AshType;
import me.nina.maker.Main;
import me.nina.maker.Utils;
import me.nina.sqlmanipulation.SQLLine;
import me.nina.sqlmanipulation.SQLTable;

public class Action extends GameObject {  private static final long serialVersionUID = 1L;
	protected static JPanel pane;
	private static JScrollPane scroll;     
	public JButton buttonNodes;
	public ClickablePanelWithTitle panelToSet,panelrefuseds,panelToRem,panelNeededs;
	private double zoom;
	private int possessor = -1;
	private String html;
	private String title;
	protected int currentIndex=-1; 
	protected int releasedIndex=-1;
	private int megaowner=-1;
	private static LinkedList itemsToMove; 
	 
	//########################################################################
	private Action(SQLLine line) { super(line);  }
	public Action() { }
	public Action(int possessor) { this.possessor=possessor; }
	public Action(String title) { this.title=title; }
	public Action save() {
		Map<String,String> val = new HashMap<>(); 
		if(possessor!=-1)val.put("possessor",possessor+"");
		if(title!=null)val.put("title",title);
		return get(SQLTable.getTable(getTable()).insert(val)); 
	}
	public Action(Action a,boolean condensed) { //for right menu
		super(a.line);  
		 
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS)); 
		 
		populate(condensed); 
	}
	//########################################################################
	public Action(Action a,int middleX, int middleY, double zoom,Editor nodeEditor) {//for object node utilities
		super(a.line);this.zoom=zoom;
				
		buttonNodes = new JButton(a.title()); 
		 
		int fontSize = (int) (30 * zoom); 
		Font font = new Font("Arial", Font.PLAIN, fontSize); 
		buttonNodes.setFont(font);
		
		setOpaque(false);

		setBounds((int) (middleX*zoom), (int) (middleY*zoom) ,(int) (1000*zoom),(int) (200*zoom)); 
		 
		buttonNodes.setBounds((int) (middleX*zoom), (int) (middleY*zoom),(int) (1000*zoom),(int) (40*zoom)); 
		nodeEditor.add(buttonNodes);
		 
		panelToSet = new ClickablePanelWithTitle("to remember",font);  
		panelToSet.setBounds((int) (middleX*zoom), (int) (middleY*zoom)+(int) (40*zoom),(int) (400*zoom),(int) (40*zoom)); 
		panelToSet.setOpaque(false); 
		nodeEditor.add(panelToSet);
		 
		panelToRem = new ClickablePanelWithTitle("to forget",font);  
		panelToRem.setBounds((int) (middleX*zoom), (int) (middleY*zoom)+(int) (40*zoom)*2,(int) (400*zoom),(int) (40*zoom)); 
		panelToRem.setOpaque(false); 
		nodeEditor.add(panelToRem);
		 
		panelrefuseds = new ClickablePanelWithTitle("refuse",font);  
		panelrefuseds.setBounds((int) (middleX*zoom), (int) (middleY*zoom)+(int) (40*zoom)*3,(int) (400*zoom),(int) (40*zoom)); 
		panelrefuseds.setOpaque(false); 
		nodeEditor.add(panelrefuseds);
		 
		panelNeededs = new ClickablePanelWithTitle("need",font);  
		panelNeededs.setBounds((int) (middleX*zoom), (int) (middleY*zoom)+(int) (40*zoom)*4,(int) (400*zoom),(int) (40*zoom)); 
		panelNeededs.setOpaque(false); 
		nodeEditor.add(panelNeededs);
		
		setBorder(BorderFactory.createLineBorder(Color.BLACK));
		
	}
	//########################################################################
	@Override public void setLocation(int x,int y) {
		super.setLocation(x, y); 
		if(buttonNodes!=null)
		 try {  
			 buttonNodes.setBounds(x,y,(int) (1000*zoom),(int) (40*zoom)); 
			 panelToSet.setLocation(x,y+(int) (40*zoom)); 
			 panelToRem.setLocation(x,y+(int) (40*zoom)*2); 
			 panelrefuseds.setLocation(x,y+(int) (40*zoom)*3); 
			 panelNeededs.setLocation(x,y+(int) (40*zoom)*4); 
			 
			 
			 } catch (Exception e) { e.printStackTrace();
		}
	        
	}
	//########################################################################
	public class ClickablePanelWithTitle extends JPanel { 
	    public ClickablePanelWithTitle(String string, Font font) {
	        setLayout(new BorderLayout()); 
	        JLabel titleLabel = new JLabel(string);
	        titleLabel.setFont(font);
	        titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
	        add(titleLabel, BorderLayout.NORTH);
  
	    } 
	}
	//######################################################################## 
	public void populate(boolean condensed) {
	    removeAll();
	    JPanel mainPanel = new JPanel();
	    
	    if (!condensed) {
	        mainPanel.setLayout(new GridLayout(3, 2)); 
	        if (this.megaOwner() == -998)
	            mainPanel.add(createPanelWithTextField("Title:", title(), 1));
	        else
	            mainPanel.add(createPanelWithTextField("Title:", title().replaceAll("\\("+MegaAction.get(this.megaOwner()).title()+"\\)", ""), 1));

	        mainPanel.add(createPanelWithTextField("Timer in seconds:", timer() != -998 ? String.valueOf(timer()) : "", 2));

	        mainPanel.add(createPanelWithTextField("Chance / 10:", random() != -998 ? random() + "chance / 10" : "", 3));

	        add(mainPanel);

	        add(getPanel("detector(s) of the action:", AddWat.DETECTOR, detectors(), this::remDetector, null));
	        add(getPanel("ash to set/remember after action", AddWat.ASHTOSET, ashToSet(), this::remAshToSet, this::updateAshToSetList));
	        add(getPanel("ash refused", AddWat.ASHREFUSED, ashRefused(), this::remAshRefused, this::updateAshRefusedList));
	        add(getPanel("ash needed", AddWat.ASHNEEDED, ashNeeded(), this::remAshNeeded, this::updateAshNeededList));
	    } else {
	        mainPanel.setLayout(new GridLayout(1, 1)); 
	        String title=title();
	        if (this.megaOwner() != -998) 
	        	title=title().replaceAll("\\("+MegaAction.get(this.megaOwner()).title()+"\\)", "") ;

	        mainPanel.add(new JLabel(title 
	                                        + ",Tim=" + (timer() != -998 ? String.valueOf(timer()) : "")
	                                        + ",rand=" + (random() != -998 ? random() + "/10" : "")));
	        
	        add(mainPanel);
LinkedList<Integer> detectors = detectors();
if(detectors.size()>0)
	        add(getCondensedPanel("detector(s) of the action:", detectors,false));
else {List<Action> list = new ArrayList<>();
	for(Action action : Action.getAll())
		for(Ashe toSet : action.ashToSet())
			if(toSet.possessor == id)
				list.add(action);
	add(getCondensedPanel("caller(s) of the action:", list,false));
}
	        add(getCondensedPanel("ash to set/remember after action", ashToSet(),true));
	        add(getCondensedPanel("ash refused", ashRefused(),false));
	        add(getCondensedPanel("ash needed", ashNeeded(),false));
	    }

	    updateTextField();

	    repaint();
	    revalidate();
	    if (Main.actualEditor != null) Main.actualEditor.populate();
	}
	//########################################################################
	public JPanel createPanelWithTextField(String labelText, String defaultValue, int type) {
	    JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    JLabel label = new JLabel(labelText);
	    panel.add(label );
	    JTextField textField = new JTextField(defaultValue);
	    panel.add(textField );
	    JButton b = new JButton("save");b.setMargin(new Insets(0, 0, 0, 0));
	    panel.add(b);
	    if(type==3){Utils.setSize(textField, 100, 20);
	    JButton b2 = new JButton("x");b2.setMargin(new Insets(0, 0, 0, 0));
	    panel.add(b2);
	    	b.addActionListener(e -> {
		     int value = Integer.parseInt(textField.getText());
		     addAshToSet(new Ashe(id, AshType.WILL_DO_WITH_RANDOM, 1));
		     updateRandom(value);
		 });
	    	
	    	b2.addActionListener(e -> { 
			     remAshToSet(new Ashe(id, AshType.WILL_DO_WITH_RANDOM, 1),-2);
			     updateRandom(-998);
			 });
	    }
	    if(type==2){Utils.setSize(textField, 100, 20);
	    JButton b2 = new JButton("x");b2.setMargin(new Insets(0, 0, 0, 0));
	    panel.add(b2);
	    	b.addActionListener(e -> {
			     int value = Integer.parseInt(textField.getText());
			     addAshToSet(new Ashe(id, AshType.WILL_DO_AFTER_TIMER, 1));
			     updateTimer(value);
			 });
	    	b2.addActionListener(e -> { 
			     remAshToSet(new Ashe(id, AshType.WILL_DO_AFTER_TIMER, 1),-2);
			     updateTimer(-998);
			 });
	    }
	    if(type==1){Utils.setSize(textField, 230, 20);
	    	b.addActionListener(e -> updateTitle(textField.getText()));
	    }
	    return panel;
	}
	//########################################################################
	@FunctionalInterface public interface ListUpdater<T> { void updateList(List<?> list); }
	@FunctionalInterface interface ButtonAction { void performAction(Object  id, int index); }
	private <T> void insertItemAtPosition(List<?> list, int index, T item) { 
	    List<T> typedList = (List<T>) list;  typedList.add(index, item); }
	//########################################################################
	
	
	Set<Integer> selectedIndices = new TreeSet<>();
	
	
	private JPanel getCondensedPanel(String title, List<?> list,boolean toSet) {
	    JPanel panel = new JPanel(new BorderLayout());
	    panel.setBorder(BorderFactory.createTitledBorder(title));
String actor=null;
String actpos=null;
PosType pos=null;
	    JPanel itemListPanel = new JPanel(new GridLayout(0, 1));
	    for (Object item : list) {
	    	
	        JLabel itemLabel = new JLabel(formatItemLabel(100,item).replaceAll("<.?html>", ""));
	          
	        
	        if (toSet&&item instanceof Ashe && (((Ashe) item).type() == AshType.WILL_LOOK_BOT_LEFT)) {
	        	actpos=((Ashe) item).possessor().title();pos=PosType.BOT_LEFT;
		    }
	        else  if (toSet&&item instanceof Ashe && (((Ashe) item).type() == AshType.WILL_LOOK_BOT_RIGHT)) {
	        	actpos=((Ashe) item).possessor().title();pos=PosType.BOT_RIGHT;
		    }
	        else if (toSet&&item instanceof Ashe && (((Ashe) item).type() == AshType.WILL_LOOK_BOTTOM)) {
	        	actpos=((Ashe) item).possessor().title();pos=PosType.BOTTOM;
		    }
	        else if (toSet&&item instanceof Ashe && (((Ashe) item).type() == AshType.WILL_LOOK_LEFT)) {
	        	actpos=((Ashe) item).possessor().title();pos=PosType.LEFT;
		    }
	        else  if (toSet&&item instanceof Ashe && (((Ashe) item).type() == AshType.WILL_LOOK_RIGHT)) {
	        	actpos=((Ashe) item).possessor().title();pos=PosType. RIGHT;
		    }
	        else  if (toSet&&item instanceof Ashe && (((Ashe) item).type() == AshType.WILL_LOOK_TOP)) {
	        	actpos=((Ashe) item).possessor().title();pos=PosType.TOP ;
		    }
	        else if (toSet&&item instanceof Ashe && (((Ashe) item).type() == AshType.WILL_LOOK_TOP_LEFT)) {
	        	actpos=((Ashe) item).possessor().title();pos=PosType.TOP_LEFT;
		    }
	        else if (toSet&&item instanceof Ashe && (((Ashe) item).type() == AshType.WILL_LOOK_TOP_RIGHT)) {
	        	actpos=((Ashe) item).possessor().title();pos=PosType.TOP_RIGHT;
		    } 
	        else if (toSet&&item instanceof Ashe && (((Ashe) item).type() == AshType.IS_THE_ACTOR)) {
	        	actor=((Ashe) item).possessor().title();
		    }
	        else {if(actor!=null) {itemLabel.setText(actor+":"+itemLabel.getText());actor=null;}
	        if(pos!=null) {
	        	if(item instanceof Ashe && (((Ashe) item).possessor()  instanceof CharacterState )) {
	        		ToolTipManager.sharedInstance().registerComponent(itemLabel);
	        		CharacterState state = (CharacterState)((Ashe) item).possessor();
	        		final PosType pos2 = pos; 
	        		
	        		 
	        		itemLabel.addMouseListener(new MouseAdapter() {
                        private JWindow tooltipWindow; 
                        @Override public void mouseEntered(MouseEvent e) { state.movingAnimation(pos2).loadGif();
                            tooltipWindow = createGifToolTip(state.movingAnimation(pos2).gifFile.path, itemLabel);
                            Point location = itemLabel.getLocationOnScreen();
                            tooltipWindow.setLocation(location.x, location.y + itemLabel.getHeight());
                            tooltipWindow.setVisible(true);  } 
                        private JWindow createGifToolTip(String path, JLabel itemLabel) { 
                        	JWindow window = new JWindow();
                            ImageIcon gifIcon = new ImageIcon(state.movingAnimation(pos2).gifFile.path);
                            JLabel gifLabel = new JLabel(gifIcon);
                            window.getContentPane().add(gifLabel);
                            window.pack();
                            return window;  } 
						@Override public void mouseExited(MouseEvent e) { 
							if (tooltipWindow != null)  tooltipWindow.dispose();  }
                    });
                }
	        	
	        	
	        	 
	                 
	        	itemLabel.setText(actpos+":"+new String(pos+":").replaceAll("PosType.","").toLowerCase()+itemLabel.getText().replaceAll(actpos, "") );pos=null;actpos=null;}
	        	itemListPanel.add(itemLabel);
	        }
	        itemLabel.setText(itemLabel.getText().replaceAll("WILL_INSIDE_THIS_STATE", "[->]"));
	        itemLabel.setText(itemLabel.getText().replaceAll("WILL_OUTSIDE_THIS_STATE", "[<-]"));
	        itemLabel.setText(itemLabel.getText().replaceAll("WILL_BE_IN_STATE", "[=]"));
	        itemLabel.setText(itemLabel.getText().replaceAll("WILL_BE_IN_POSITION", "[POS]"));
	        itemLabel.setText(itemLabel.getText().replaceAll("WILL_BE_CLICKED", "[CLIC]"));
	        itemLabel.setText(itemLabel.getText().replaceAll("ACTION_TO_DO_WITH_BEFORE_CONDITION_CHECK", "[ACT BEF CHK]"));
	        itemLabel.setText(itemLabel.getText().replaceAll("ACTION_TO_DO_WITH_AFTER_CONDITION_CHECK", "[ACT AFT CHK]"));

	    }
	    
	    JScrollPane scrollPane = new JScrollPane(itemListPanel);
	    panel.add(scrollPane, BorderLayout.CENTER);

	    return panel;
	}
	
	
	
	 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	private JPanel getPanel(String title, AddWat wat, List<?> list, ButtonAction action, ListUpdater<?> listUpdater) {
	    JPanel panel = new JPanel();
	    panel.setOpaque(true);
	    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

	    // Title panel
	    JPanel titlePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	    titlePanel.setOpaque(false);

	    JLabel titleLabel = new JLabel(title);
	    titleLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
	    JButton addButton = new JButton("+");
	    addButton.addActionListener(e -> new ActionExplorer(this, wat).setVisible(true));

	    titlePanel.add(new JLabel("       "));
	    titlePanel.add(titleLabel);
	    titlePanel.add(addButton);
	    titlePanel.add(new JLabel("       "));
	    panel.add(titlePanel);

	    // Popup menu
	    JPopupMenu popupMenu = createPopupMenu(list, listUpdater);

	    // Item panels
	    int place = 0;
	     selectedIndices = new TreeSet<>();
	    for (Object item : list) {
	        final int index = place;
	        JPanel itemPanel = createItemPanel(item, index, list, listUpdater, action, popupMenu, panel);
	        panel.add(itemPanel);
	        place++;
	    }

	    if (wat == AddWat.DETECTOR && list.isEmpty()) {
	        panel.setBackground(Color.BLUE);
	    }

	    panel.setBorder(BorderFactory.createLineBorder(Color.black));
	    return panel;
	}

	private JPopupMenu createPopupMenu(List<?> list, ListUpdater<?> listUpdater) {
	    JPopupMenu popupMenu = new JPopupMenu();

	    JMenuItem copyItem = new JMenuItem("Copy");
	    copyItem.addActionListener(e -> copyItems(list ));
	    popupMenu.add(copyItem);

	    JMenuItem cutItem = new JMenuItem("Cut");
	    cutItem.addActionListener(e -> cutItems(list,  listUpdater));
	    popupMenu.add(cutItem);

	    JMenuItem pasteItem = new JMenuItem("Paste");
	    pasteItem.addActionListener(e -> pasteItems(list, listUpdater));
	    popupMenu.add(pasteItem);

	    return popupMenu;
	}

	private JPanel createItemPanel(Object item, int index, List<?> list, ListUpdater<?> listUpdater, ButtonAction action, JPopupMenu popupMenu, JPanel panel) {
	    JPanel itemPanel = new JPanel(new GridBagLayout());
	    itemPanel.setOpaque(false);

	    if (item instanceof Ashe && (((Ashe) item).type() == AshType.ACTION_TO_DO_WITH_BEFORE_CONDITION_CHECK || ((Ashe) item).type() == AshType.ACTION_TO_DO_WITH_AFTER_CONDITION_CHECK)) {
	        itemPanel.setBackground(Color.blue);
	    }

	    JLabel itemLabel = createItemLabel(listUpdater, item, list, popupMenu, panel);
	    JPanel buttonPanel = createButtonPanel(item, index, list, listUpdater, action);

	    GridBagConstraints gbc = new GridBagConstraints();
	    gbc.fill = GridBagConstraints.HORIZONTAL;
	    gbc.weightx = 0.66;
	    Utils.setSize(itemLabel, 210, 45);
	    itemPanel.add(itemLabel, gbc);

	    gbc.weightx = 0.33;
	    itemPanel.add(buttonPanel, gbc);

	    return itemPanel;
	}

	private JLabel createItemLabel(ListUpdater<?> listUpdater, Object item, List<?> list,  JPopupMenu popupMenu, JPanel panel) {
	    JLabel itemLabel = new JLabel();
	    itemLabel.setText(formatItemLabel(60,item));
	    itemLabel.setToolTipText(getItemToolTip(item));

	    itemLabel.addMouseListener(new MouseAdapter() {
	        @Override
	        public void mousePressed(MouseEvent e) {
	            handleMousePressed(item, e, itemLabel, list );
	        }

	        @Override
	        public void mouseReleased(MouseEvent e) {
	            handleMouseReleased(listUpdater, e, itemLabel, list,  popupMenu, panel);
	        }

	        @Override
	        public void mouseEntered(MouseEvent e) {
	            releasedIndex = list.indexOf(item);
	        }
	    });

	    return itemLabel;
	}

	private JPanel createButtonPanel(Object item, int index, List<?> list, ListUpdater<?> listUpdater, ButtonAction action) {
	    JPanel buttonPanel = new JPanel();
	    buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));

	    JButton upButton = createMoveButton("\u2191", index > 0, () -> {
	        Collections.swap(list, index, index - 1);
	        listUpdater.updateList(list);
	        populate(false);
	    });

	    JButton downButton = createMoveButton("\u2193", index < list.size() - 1, () -> {
	        Collections.swap(list, index, index + 1);
	        listUpdater.updateList(list);
	        populate(false);
	    });

	    JButton deleteButton = new JButton("x");
	    deleteButton.addActionListener(e -> {
	        action.performAction(item, index);
	        populate(false);
	    });

	    buttonPanel.add(upButton);
	    buttonPanel.add(downButton);
	    buttonPanel.add(deleteButton);

	    return buttonPanel;
	}

	private JButton createMoveButton(String label, boolean enabled, Runnable action) {
	    JButton button = new JButton(label);
	    button.setEnabled(enabled);
	    button.addActionListener(e -> action.run());
	    button.setMargin(new Insets(0, 0, 0, 0));
	    Utils.setSize(button, 20, 30);
	    return button;
	}

	private void handleMousePressed(Object item, MouseEvent e, JLabel itemLabel, List<?> list ) {
		if (e.isControlDown()) {
	        int index = list.indexOf(item);
	        if (selectedIndices.contains(index)) {
	            itemLabel.setBorder(null);
	            selectedIndices.remove(index);
	        } else {
	            itemLabel.setBorder(BorderFactory.createLineBorder(Color.black));
	            selectedIndices.add(index);
	        }
	    } else  if (e.getButton() != MouseEvent.BUTTON3){
	        // Clear previous selection if control is not held down
	        selectedIndices.clear();
	        itemLabel.setBorder(BorderFactory.createLineBorder(Color.black));
	        selectedIndices.add(list.indexOf(item));
	    }
	    currentIndex = list.indexOf(item);
	    System.out.println("Selected Indices: " + selectedIndices);
	}

	private void handleMouseReleased(ListUpdater<?> listUpdater, MouseEvent e, JLabel itemLabel, List<?> list,  JPopupMenu popupMenu, JPanel panel) {
		 if (e.getButton() == MouseEvent.BUTTON3) {
		        resetBorders(panel);
		        popupMenu.show(e.getComponent(), e.getX(), e.getY());
		    } else if (!e.isControlDown() && currentIndex != -1 && releasedIndex != -1 && currentIndex != releasedIndex) {
		        moveItems(list, selectedIndices);
		        listUpdater.updateList(list);
		        populate(false);
		    }
	}

	private void resetBorders(JPanel panel) {
	    for (Component comp : panel.getComponents()) {
	        if (comp instanceof JPanel) {
	            for (Component innerComp : ((JPanel) comp).getComponents()) {
	                if (innerComp instanceof JLabel) {
	                    ((JLabel) innerComp).setBorder(null);
	                }
	            }
	        }
	    }
	}

	private void copyItems(List<?> list ) {
	    itemsToMove = new LinkedList<>();System.out.println("List Contents: " + list);
	    for (int index : selectedIndices) {
	        Object item = list.get(index);
	        itemsToMove.add(item);
	        System.out.println("Copy Item at Index " + index + ": " + item);
	    }
	    System.out.println("Copied Items: " + itemsToMove);
	}

	private void cutItems(List<?> list,  ListUpdater<?> listUpdater) {
		  itemsToMove = new LinkedList<>();
		    List<Object> itemsToRemove = new ArrayList<>();
		    List<Integer> indices = new ArrayList<>(selectedIndices);
		    Collections.sort(indices);
		    
		    for (int index : indices) {
		        itemsToMove.add(list.get(index));
		        itemsToRemove.add(list.get(index));
		    }
		    list.removeAll(itemsToRemove);
		    selectedIndices.clear();
		    System.out.println("Cut Items: " + itemsToMove);
		    listUpdater.updateList(list);
		    populate(false);
	}

	private void pasteItems(List<?> list, ListUpdater<?> listUpdater) {
		if (itemsToMove != null && !itemsToMove.isEmpty() && releasedIndex != -1) {
	        int insertIndex = releasedIndex;
	        for (Object item : itemsToMove) {
	            insertItemAtPosition(list, insertIndex++, item);
	        }
	        System.out.println("Paste Items: " + itemsToMove);
	        itemsToMove.clear();
	        selectedIndices.clear();
	        listUpdater.updateList(list);
	        populate(false);
	    } else {
	        System.out.println("Paste Failed: itemsToMove is empty or releasedIndex is invalid.");
	    }
	}

 

	private void moveItems(List<?> list, Set<Integer> selectedIndices) {
		List<Object> itemsToMove = new LinkedList<>();
	    List<Integer> indices = new ArrayList<>(selectedIndices);
	    Collections.sort(indices);

	    for (int index : indices) {
	        itemsToMove.add(list.get(index));
	    }
	    
	    for (int i = indices.size() - 1; i >= 0; i--) {
	        list.remove((int) indices.get(i));
	    }
	    
	    int insertIndex = releasedIndex > currentIndex ? releasedIndex - itemsToMove.size() : releasedIndex;
	    
	    for (Object item : itemsToMove) {
	        insertItemAtPosition(list, insertIndex++, item);
	    }
	    selectedIndices.clear();
	}

	private String formatItemLabel(int size, Object item) {
		if(item instanceof Action)
			return ((Action)item).title() ;
	    String labelText = (item instanceof Integer) ?
	            GameObject.getById((int) item).title() :
	            ((Ashe) item).getNbr() != 0 ? ((Ashe) item).getNbr() + " x " + ((Ashe) item).title() : ((Ashe) item).title();
	    if (labelText.length() > size) {
	        labelText = labelText.substring(0, size) + "...";
	    }
	    return "<html>" + labelText + "</html>";
	}

	private String getItemToolTip(Object item) {
	    return (item instanceof Integer) ? GameObject.getById((int) item).title() : ((Ashe) item).title();
	}
  
	//########################################################################
	public void populateRight( ) { 

		if(pane == null) {
			pane = new JPanel();
			pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
			scroll = new JScrollPane(pane);
			Main.rightController.add(scroll); 
			//Utils.setSize(pane, 300, 1000);
			scroll.getVerticalScrollBar().setUnitIncrement(10);
		}
		
		pane.removeAll();
		Main.rightController.repaint();
		Main.rightController.revalidate();
		Main.actualEditor.populate();
		 
		Action ac = new Action(this,false);
		Main.rightController.actualAction = ac;
		JButton d = new JButton("delete this action"); 
		JPanel x = new JPanel();  pane.add(x);  x.add(d);
		d.addActionListener(e->{ delete(); pane.remove(ac); } ); 
		 
		pane.add(ac);
		pane.repaint();
		pane.revalidate();scroll.getVerticalScrollBar().setValue(0);
	} 
	
	//########################################################################
	public void  updateAshToSetList(List<?>l) { line.update("setash", "");
		for (int i = 0; i < l.size(); i++)  addAshToSet( (Ashe) l.get(i));  }
	//public void   updateAshToRemList(List<?>l) { line.update("remash", ""); 
	//	for (int i = 0; i < l.size(); i++)  addAshToRem( (Ashe) l.get(i));  }
	public void  updateAshNeededList(List<?>l) { line.update("neededash", "");   
		for (int i = 0; i < l.size(); i++)  addAshNeeded( (Ashe) l.get(i));  } 
	public void  updateAshRefusedList(List<?>l) { line.update("refusedash", "");   
		for (int i = 0; i < l.size(); i++)  addAshRefused( (Ashe) l.get(i));  } 
	
	public LinkedList <Ashe> ashToSet() { return Ashe.getlist(line.getString("setash")); }
	//public LinkedList <Ashe> ashToRem() { return Ashe.getlist(line.getString("remash")); }
	public LinkedList <Ashe> ashNeeded() { return Ashe.getlist(line.getString("neededash")); } 
	public LinkedList <Ashe> ashRefused() { return Ashe.getlist(line.getString("refusedash")); } 
	public LinkedList <Integer> detectors() {return line.getList("possessor");}
	//public LinkedList <Integer> destinations() {return line.getList("objectdest");}
	  
	public void addAshToSet(Ashe a) {line.addInArray("setash", a.toSqlValue());  } 
	public void addAshNeeded(Ashe a) {line.addInArray("neededash", a.toSqlValue());  } 
	public void addAshRefused(Ashe a) {line.addInArray("refusedash", a.toSqlValue());  } 
	 
	   
	public void remAshToSet(Object a,int index) {line.remFromArray("setash", ((Ashe)a).toSqlValue(), index);  } 
	public void remAshNeeded(Object a,int index) {line.remFromArray("neededash", ((Ashe)a).toSqlValue(), index);  } 
	public void remAshRefused(Object a,int index) {line.remFromArray("refusedash", ((Ashe)a).toSqlValue(), index);  } 
	  
	
	public void addDetector(Object id) {line.addInArray("possessor", (int)id+"");   }
	public void remDetector(Object id,int index) {line.remFromArray("possessor", (int)id+"", index);   }
	  
	public int megaOwner() {   if(megaowner==-1)megaowner= line.getInt("megaowner"); return megaowner;}
	public void updateMegaOwner(int i) { megaowner=i;line.update("megaowner",i+""); }
	
	public int timer() { return line.getInt("timer"); }
	public void updateTimer(int s) { line.update("timer", s+""); }
	
	public int random() { return line.getInt("random"); }
	public void updateRandom(int s) { line.update("random", s+""); }
	
  
  //########################################################################
    protected void updateTextField() {
    	try {
            StringBuilder text = new StringBuilder("<html><body>");
     		
    		text.append("<p><strong>").append(title()).append(" :</strong></p>");
    		for(Integer a:detectors())
    		text.append("<p><strong>").append("when we detect : "
    				+GameObject.getById(a).title()).append(" </strong></p>");
    		if(timer() != -998)text.append("<p><strong>").append("after "+timer()+"s").append(" :</strong></p>");
    		if(random() != -998)text.append("<p><strong>").append("action run with "+random()+"chance/10").append(" :</strong></p>");
           
            text.append("<strong>will remember/set:</strong><ul> ");
            for (Ashe ash :ashToSet())  text.append("<li>"+ash.getNbr()+" "+ash. title()+" </li>");
             
           
               
            text.append("</ul>"+"<strong>we need:</strong><ul> ");
            for (Ashe ash :ashNeeded()) text.append("<li>"+ash. title()+" </li>");
             
            text.append("</ul>"+"<strong>we refuse:</strong><ul> ");
            for (Ashe ash :ashRefused()) text.append("<li>"+ash. title()+" </li>");
            
            text.append("</ul>");
             
            text.append("</body></html>");
 
            html=text.toString();
        } catch (Exception e) {
            e.printStackTrace();
            
        }
    }

 
	//######################################################################## 
	@Override public List<GameObject> getChilds() {
		List<GameObject> liste = new ArrayList<>() ; 
		liste.add(possessorObj()); 
			return liste;
	}
	@Override public String toString() { return html; }
	@Override public String getFolder() { return null; } 
	public static List<Action> getAll( ) { return getAll(instance); }
	public static List<Action> getByDetector( GameObject possessor) {  return getbyPossessor(instance, possessor); }
	@Override GameObject instanciate(SQLLine l) { return new Action(l); }
	@Override boolean isValidElement(SQLLine l) {  return true; } 
	@Override String getTable() {  return "action"; }
	static Action instance= new Action();
	public static Action get(int i) { return get(instance,i); }
	@Override GameObject default_instance() {  return instance; } 
	@Override boolean isValidPossessor(SQLLine l, GameObject possessor) { 
			return l.getList("possessor").contains( possessor.id); }
	 
	public String getHtml() { updateTextField(); return html; }

	@Override  public 	Set<AshType> possibleSetters() {
		Set<AshType> temp = new HashSet<>();  
		temp.add(AshType.WILL_DO_AFTER_TIMER); 
		temp.add(AshType.WILL_DO_WITH_RANDOM);
		temp.add(AshType.ACTION_TO_DO_WITH_BEFORE_CONDITION_CHECK);
		temp.add(AshType.ACTION_TO_DO_WITH_AFTER_CONDITION_CHECK);
		return temp;
	}
	@Override  public 	Set<AshType> possibleRemovers() {
		Set<AshType> temp = new HashSet<>();   
		temp.add(AshType.WILL_DO_AFTER_TIMER); 
		return temp;
	}

	@Override  public 	Set<AshType> possibleGetters() {
		Set<AshType> temp = new HashSet<>(); 
		temp.add(AshType.ACTION_CAN_BE_EXECUTED); 
		return temp;
	}

	public static Action copy(Action a) {
		Action newe = null;
		 
		newe=new Action("copied "+a.title() ).save();
		if(a.detectors().size()!=0)for(int r : a.detectors()) newe.addDetector(r);
		if(a.ashToSet()!=null)for(Ashe r : a.ashToSet()) newe.addAshToSet(r);
		if(a.ashNeeded()!=null)for(Ashe r : a.ashNeeded()) newe.addAshNeeded(r);
		if(a.ashRefused()!=null)for(Ashe r : a.ashRefused()) newe.addAshRefused(r);
		return newe;
	}

	


	
	 
	public static  enum AddWat {
	     DETECTOR(0),ASHTOSET(1),ASHREFUSED(3),ASHNEEDED(4),TEST_IN_GAME(5);  
	    private final int valeur; 
	    AddWat(int valeur) { this.valeur = valeur;  } 
	    public int getValeur() {  return valeur; }  
	    public static AddWat from(String s) { 
	    	return from(Integer.parseInt(s));
	    }
	    public static AddWat from(int valeur) {
	        for (AddWat type : values()) {
	            if (type.getValeur() == valeur) {
	                return type;
	            }
	        }
	        throw new IllegalArgumentException("Aucun AddWat correspondant � la valeur : " + valeur);
	    }
	    
	     
	    
	}
	
	@Override public String title(){
		if(this.megaOwner()!=-998)
			return super.title()+" ("+MegaAction.get(this.megaOwner()).title()+")";
		else return super.title();
	}
	 
} 
  


 