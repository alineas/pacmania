package me.nina.gameobjects;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import agame.MainGame;
import me.nina.gameobjects.GameObject;
import me.nina.editors.Editor;
import me.nina.gameobjects.Character.PosType;
import me.nina.maker.Ashe.AshType;
import me.nina.objects2d.Point;
import me.nina.maker.Ashe;
import me.nina.maker.Main;
import me.nina.maker.Utils;
import me.nina.sqlmanipulation.SQLLine;
import me.nina.sqlmanipulation.SQLTable;

public class Character extends GameObject  { private static final long serialVersionUID = 1L;
private PosType posType = PosType.BOTTOM;
private CharacterState selectedStat ;
private Animation currentAnime;  
public Point feetPosition = null;  
protected double scale = 1.0; 
public CharacterState getSelectedStat() {
	return selectedStat;
}

public int feetY() { return feetPosition.y; } 
public int feetX() { return feetPosition.x; }
public void setPos(Point pos) { feetPosition=pos;}
public void setSelectedStat(CharacterState selectedStat ) {
	try {
		this.getCurrentAnime().unsetIdle();
		setSelectedStat(selectedStat,true);
	} catch (Exception e) {Utils.log("error in setselectedstat!!!");
		this.selectedStat=this.getInitialState(); 
		setCurrentAnime(Animation.get(selectedStat.movingAnimation( PosType.BOTTOM).id),false) ;
	} 
}
public void setSelectedStat(CharacterState selectedStat,boolean doTheOffsetJob) {
	try {
		if(selectedStat == null)
			new Throwable().printStackTrace(); 
		 MainGame.instance.removeAsh(new Ashe(this.selectedStat.id,AshType.IS_IN_STATE,1)); 
		this.selectedStat = selectedStat;  
		  
		 setCurrentAnime( selectedStat.movingAnimation( getPosTyp()) ,doTheOffsetJob ); 
		  
		 this.getCurrentAnime().unsetIdle();
		 MainGame.instance.addAsh(new Ashe(selectedStat.id,AshType.IS_IN_STATE,1)); 
		 
		 currentAnime.rewindAll();
		 currentAnime.setReversed(false);
			
		 currentAnime.run();
	} catch (Exception e) {e.printStackTrace();
		Utils.log("error in setselectedstat");
	}
}

private String title;
	protected Character(SQLLine l) {
		super(l);  
		selectedStat = getInitialState();
	}
	public Character(String title) {
		super();
		this.title=title;
		

		
		 
		
		
	}
	
	public int save() {
		Map<String,String> val = new HashMap<>();  
		val.put("title", title);  
		SQLTable tabl = SQLTable.getTable("character");
		return tabl.insert(val);
	}
	public void generateInitialState(int id) {
		Map<String,String> val = new HashMap<>();
		val.put("possessor", id+"");
		val.put("title", title+"_initial state");
		SQLTable tabl = SQLTable.getTable("characterstate");  
		int stateId = tabl.insert(val);
		get(id).updateInitialState(stateId); 
	}
	
	public static Character get(int id) { 
		return get (instance,id);
	}
	public static List<Character> getAll() { 
		return getAll(instance);
	} 
	public CharacterState getInitialState() {
		return CharacterState.get(line.getInt("initialstate"));
	}
 
	
	public void updateInitialState(int i) {
		line.update("initialstate",i+"");
	}
	
	@Override public String getFolder() {
		String path = "games/"+Main.gameName+"/character/"+title()+"/";
		new File(path).mkdirs();
		return  path ; 
	}


	
	public enum PosType {
	    RIGHT(0), LEFT(1),BOTTOM(2),TOP(3),BOT_RIGHT(4),BOT_LEFT(5),TOP_LEFT(6),TOP_RIGHT(7),UNUSED(8);
	      
	    private final int valeur; 
	    PosType(int valeur) { this.valeur = valeur;  } 
	    public int getValeur() {  return valeur; } 
	    //@Override public String toString() { return String.valueOf(getValeur()); }
	    public static PosType from(String s) { 
	    	return from(Integer.parseInt(s));
	    }
	    public static PosType from(int valeur) {
	        for (PosType type : values()) {
	            if (type.getValeur() == valeur) {
	                return type;
	            }
	        }
	        throw new IllegalArgumentException("Aucun postype correspondant � la valeur : " + valeur);
	    }
	    
	    public static PosType calculatePosType(int xin, int yin, int xto, int yto) {
	         

	        double angle = Math.atan2(yto - yin, xto - xin);
	        double angleDegrees = Math.toDegrees(angle);
	        if (angleDegrees < 0) angleDegrees += 360;

	        if (angleDegrees >= 337.5 || angleDegrees < 22.5) {
	            return PosType.RIGHT;
	        } else if (angleDegrees >= 22.5 && angleDegrees < 67.5) {
	        	return PosType.BOT_RIGHT;
	        } else if (angleDegrees >= 67.5 && angleDegrees < 112.5) {
	        	return PosType.BOTTOM;
	        } else if (angleDegrees >= 112.5 && angleDegrees < 157.5) {
	        	return PosType.BOT_LEFT;
	        } else if (angleDegrees >= 157.5 && angleDegrees < 202.5) {
	        	return PosType.LEFT;
	        } else if (angleDegrees >= 202.5 && angleDegrees < 247.5) {
	        	return PosType.TOP_LEFT;
	        } else if (angleDegrees >= 247.5 && angleDegrees < 292.5) {
	        	return PosType.TOP;
	        } else {
	        	return PosType.TOP_RIGHT;
	        }
	    }
	    
	}
 
 
	@Override
	public List<GameObject> getChilds() {
	List<GameObject> liste = new ArrayList<>() ;
		
	List<CharacterState> lst = CharacterState.getByPossessor(id);
		/*for(AshType a : AshType.values()) {
			//if(z.ash(a) != -1) 
			//	liste.add(Ash.get(z.ash(a)));
			if(ash(a) != -1) 
				liste.add(Ash.get(ash(a)));
		}*/
		for(CharacterState z : lst) {
			
			liste.add(z);
				
		}

		 
				
		  
		return liste;
	}
	@Override
	GameObject instanciate(SQLLine l) { 
		return new Character(l);
	}
 
	@Override
	boolean isValidElement(SQLLine l) { 
		return true;
	}
	@Override
	String getTable() { 
		return "character";
	}
	static Character instance= new Character("");
	@Override
	GameObject default_instance() {
		return instance;
	}
	@Override
	boolean isValidPossessor(SQLLine l, GameObject possessor) {
		// TODO Auto-generated method stub
		return false;
	}
	 
	@Override public void delete() {if(!Main.getUserConfirmation("are you sure ?"))return;
		for(CharacterState a:CharacterState.getByPossessor(id))
			a.delete();
		super.delete();
		 
	}
	
	public void updateTalker(int id) {
		line.update("messagetalker", id+"");
	}
	public int talker( ) {
		return line.getInt("messagetalker");
	}
	@Override public void updateTitle(String text) {
		for(GameObject a : CharacterState.getAll())
					if(a.title().contains(title())) 
						a.updateTitle(a.title().replaceAll(title(), text));
		super.updateTitle(text);
	}
	public Animation getCurrentAnime() {
		if(currentAnime == null){Utils.log("null anim sent, returning to initial!!!");
			 selectedStat=this.getInitialState() ;  
			 setCurrentAnime(Animation.get(selectedStat.movingAnimation( PosType.BOTTOM).id),false) ; 
					 
				 
		}
		return currentAnime;
	} 
	public void setCurrentAnime(Animation c, boolean doTheOffsetJob) { if(doTheOffsetJob&&!c.equals(currentAnime)&&feetPosition!=null) {
		int offsetX = (int) ((currentAnime.posTo().x - currentAnime.posFrom().x)*(scale+getSelectedStat().scaleCorrection()));
		int offsetY = (int) ((currentAnime.posTo().y - currentAnime.posFrom().y)*(scale+getSelectedStat().scaleCorrection())); 
		Point pos = new Point(feetX()+offsetX,feetY()+offsetY);
		if(currentAnime.isReversed())pos = new Point(feetX()-offsetX,feetY()-offsetY);
		setPos(pos); } 
		this.currentAnime = c;
	}
   

	@Override  public 	Set<AshType> possibleGetters() {
		Set<AshType> temp = new HashSet<>();
		temp.add(AshType.HAS_BEEN_CLICKED);
		temp.add(AshType.IS_ARRIVED);
		temp.add(AshType.IS_THE_ACTOR);
		temp.add(AshType.HAS_BEEN_SPAWNED);
		temp.add(AshType.HAS_NOTHING_IN_HAND);

		return temp;
	}
	@Override  public 	Set<AshType> possibleRemovers() {
		Set<AshType> temp = new HashSet<>();

		temp.add(AshType.HAS_BEEN_SPAWNED);

		return temp;
	}
	@Override  public 	Set<AshType> possibleSetters() {
		Set<AshType> temp = new HashSet<>();
		temp.add(AshType.WILL_BE_SPAWNED);
		temp.add(AshType.WILL_BE_DESPAWN);
		temp.add(AshType.WILL_BE_IN_POSITION);
		temp.add(AshType.WILL_BE_CLICKED);
		temp.add(AshType.WILL_BE_LOOKED);
		temp.add(AshType.WILL_LOOK_BOTTOM);
		temp.add(AshType.WILL_LOOK_LEFT);
		temp.add(AshType.WILL_LOOK_RIGHT);
		temp.add(AshType.WILL_LOOK_TOP);
		
		temp.add(AshType.WILL_LOOK_TOP_RIGHT);
		temp.add(AshType.WILL_LOOK_TOP_LEFT);
		temp.add(AshType.WILL_LOOK_BOT_RIGHT);
		temp.add(AshType.WILL_LOOK_BOT_LEFT);

		
		temp.add(AshType.IS_THE_ACTOR);
		temp.add(AshType.SAVE_BEFORE_DEAD);
		temp.add(AshType.RESTORE_BEFORE_DEAD);
		temp.add(AshType.RECORD_ACTUAL_POSITION);
		temp.add(AshType.WILL_BE_IN_POSITION_RECORDED);
		return temp;
	}
	public PosType getPosTyp() {
		return posType;
	}
	public void setPosType(PosType posTyp) { if(posTyp == this.posType)return;
		if(posTyp==null) {Utils.log("postype null sent");
			new Throwable().printStackTrace();
			 
			}  Animation old = Animation.get(selectedStat.movingAnimation( getPosTyp()).id  );
		try {
			
			this.posType = posTyp;  
			setCurrentAnime(Animation.get(selectedStat.movingAnimation( getPosTyp()).id),true);
			currentAnime.rewindAll();
			Utils.log("____________postype anim switched to "+currentAnime.title()); 
		} catch (Exception e) {e.printStackTrace();Utils.log("setpostype called with wrong anim value... anim was "+old.title());
		 this.selectedStat=this.getInitialState(); 
		 setCurrentAnime(Animation.get(selectedStat.movingAnimation( PosType.BOTTOM).id),false );
		}  
		}
}
